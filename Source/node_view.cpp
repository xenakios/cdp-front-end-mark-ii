#include "node_view.h"
#include "node_component.h"
#include "node_factory.h"
#include <map>
#include "jcdp_audio_playback.h"
#include "plugin_gui.h"
#include "node_chooser_components.h"

extern std::unique_ptr<PropertiesFile> g_propsfile;

NodeView::NodeView(std::shared_ptr<IJCDPreviewPlayback> previewplayback, node_inspector * inspector) :
m_audio_player(previewplayback), m_node_inspector(inspector)
{
    startTimer(0, 100);
#ifdef TEST_DRAGGINGCOMPONENTXXX
#ifdef WIN32
	std::vector<String> testimages{"C:\\Net_Downloads\\10122016\\Iannis_Xenakis_grande.jpg",
    "C:\\Net_Downloads\\10122016\\wishart.jpg",
    "C:\\Net_Downloads\\10122016\\stockhausenia.jpg"};

#else
	std::vector<String> testimages{ "/Users/teemu/Downloads/28112016/Iannis_Xenakis_grande.jpg",
		"/Users/teemu/Downloads/28112016/trevorcropped.jpg",
		"/Users/teemu/Downloads/28112016/stockhausen.jpg" };
#endif

    int i=0;
    for (auto& e: testimages)
    {
        auto comp = std::make_shared<DragTestComponent>(this);
        comp->setBounds(1+10*i, 1+10*i, 200, 200);
        comp->m_dragger = &m_dragger;
        comp->m_imgfn = e;
        addAndMakeVisible(comp.get());
        m_drag_tests.push_back(comp);
        ++i;
    }
#endif    
	UndoEntry entry;
	entry.m_desc = "Empty document";
	m_undo_history.push_back(entry);
}

void NodeView::paint(Graphics& g)
{
    g.fillAll(Colours::lightsteelblue);
    
    for (auto& e : m_node_components)
    {
        
        int y0 = e->getY();
        int i = 0;
        auto node = e->getNode();
        //bool numoutsdrawn = false;
		auto& busses = node->getBussesLayout();
		int num_in_busses = busses.getNumInputBuses();
		for (int busnum = 0; busnum < num_in_busses; ++busnum)
		{
			auto inbus = busses.getInputBusMutable(busnum);
			int inbuswidth = e->getWidth() / num_in_busses;
			int inbusxcor = e->getX() + inbuswidth * busnum;
			int inbusycor = e->getY() - 20;
			int inbusheight = 20;
			g.drawRect(inbusxcor, inbusycor, inbuswidth - 3, inbusheight);
			String name = inbus->getName();
			g.drawText(name, inbusxcor, inbusycor, inbuswidth - 3, inbusheight, Justification::centred);
		}
		int num_out_busses = busses.getNumOutputBuses();
		for (int busnum = 0; busnum < num_out_busses; ++busnum)
		{
			auto outbus = busses.getOutputBusMutable(busnum);
			int outbuswidth = e->getWidth() / num_out_busses;
			int outbusxcor = e->getX() + outbuswidth * busnum;
			int outbusycor = e->getBottom() + 1;
			int outbusheight = 20;
			g.drawRect(outbusxcor, outbusycor, outbuswidth - 3, outbusheight);
			String name = outbus->getName();
			g.drawText(name, outbusxcor, outbusycor, outbuswidth - 3, outbusheight, Justification::centred);
		}
        for (auto& source_node : node->m_sources)
        {
            int num_sources = (int)node->m_sources.size();
            //int w = e->getWidth();
            int half_w = e->getWidth() / 2;
            double foo = (double)half_w / (num_sources-1);
            int x0 = (double)(e->getX()+half_w)  + foo*i - half_w/2 ;
            if (num_sources == 1)
                x0 = e->getX() + half_w;
            Component* comp = source_node->m_component;
            if (comp == nullptr)
                continue;
            g.setColour(Colours::black);
            int x1 = comp->getX() + (comp->getRight() - comp->getX()) / 2;
            int y1 = comp->getBottom();
            Line<float> line((float)x1, (float)y1+20, (float)x0, (float)y0-20);
            g.drawArrow(line, 1.0, 10.0, 10.0);
            //if (numoutsdrawn == false)
            {
                g.setColour(Colours::white);
                //g.fillRect(x1-8,y1,16,16);
                g.setColour(Colours::black);
                int numoutchans = node->getCurrentNumOutputChannels();
				//g.drawText(String(numoutchans), x1-8, y1, 16, 16, Justification::centred);
                //numoutsdrawn = true;
            }
            ++i;
        }
    }
    g.setColour(Colours::green);
    g.strokePath(m_selection_path, PathStrokeType(2.0f));
    g.setColour(Colours::black);
    /*
	for (int i = 0; i < m_render_jobs.size(); ++i)
    {
        String txt = m_render_jobs[i].m_node->get_name();
        g.drawText(txt, 1, 1 + i * 20, 300, 19, Justification::left);
    }
	*/
	/*
	for (int i = 0; i < m_topological_render_order.size(); ++i)
	{
		String txt = m_topological_render_order[i]->get_name();
		g.drawText(txt, 1, 1 + i * 20, 300, 19, Justification::left);
	}
	*/
	for (int i = 0; i < m_undo_history.size(); ++i)
	{
		if (i != m_undo_level)
			g.setColour(Colours::black);
		else g.setColour(Colours::white);
		String txt = m_undo_history[i].m_desc;
		g.drawText(txt, 1, 1 + i * 20, 300, 19, Justification::left);
	}
}

void NodeView::resized()
{
    
}

void NodeView::removeNodeComponent(process_node* node)
{
    for (int i=0;i<m_node_components.size();++i)
    {
        if (m_node_components[i]->getNode().get()==node)
        {
            m_node_components.erase(m_node_components.begin()+i);
            repaint();
            return;
        }
    }
}

void NodeView::showNodeList(int x, int y, bool replace)
{
    node_ref_vec selnodes = getSelectedNodes();
    if (replace == true && selnodes.size() != 1)
        return;
    auto screenpos = getBoundsInParent();
    juce::Rectangle<int> area(x+screenpos.getX(), y+screenpos.getY(), 10, 10);
    auto progcomponent = new CDPProgramsComponent(m_audio_player);
    Component* topcomp = getParentComponent();
    if (topcomp != nullptr)
        progcomponent->setSize(progcomponent->getWidth(), topcomp->getHeight() - 90);
    CallOutBox& callobox = CallOutBox::launchAsynchronously(progcomponent, area, topcomp);
    progcomponent->OnProgramSelected = [&callobox, x,y, replace, selnodes, this](String name)
    {
        if (replace == false)
            add_node_with_component(name, x, y, 160, 40, false);
        else
        {
            int old_x = selnodes[0]->m_component->getX();
            int old_y = selnodes[0]->m_component->getY();
            int old_w = selnodes[0]->m_component->getWidth();
            int old_h = selnodes[0]->m_component->getHeight();
            auto oldsources = selnodes[0]->m_sources;
            auto olddests = selnodes[0]->m_destinations;
            node_ref_vec& allnodes = m_graph.getNodes();
            node_ref_vec newdests;
            for (auto& ita : olddests)
            {
                for (auto& itb : allnodes)
                    if (itb.get() == ita)
                        newdests.push_back(itb);
            }
            removeSelectedNodes();
            auto new_node = add_node_with_component(name, old_x, old_y, old_w, old_h, false);
            for (auto& it : oldsources)
                connect_nodes(it, new_node,true);
            for (auto& it : newdests)
                connect_nodes(new_node, it,true);
            repaint();
        }
        
        callobox.dismiss();
    };
}

void NodeView::replaceNodes()
{
    
}

void NodeView::setNodeInspector(node_inspector * inspec)
{
    m_node_inspector = inspec;
}

void NodeView::addDefaultNodes()
{
    /*std::shared_ptr<process_node> node_source =*/ add_node_with_component("Input Sound File", getWidth() / 2, 10, 220, 75, false);
	addUndoState("Initial state");
}

void NodeView::showNodeInfoText()
{
    if (m_selected_node == nullptr)
        return;
    for (auto& e : node_factory::instance()->m_entries)
    {
        if (e.m_name == m_selected_node->get_type_name())
        {
            m_selected_node->m_component->showBubbleMessage(e.m_tiptext, false, 0);
            return;
        }
    }
}

void NodeView::doNodeCreateMenu(int xcor, int ycor)
{
    PopupMenu topmenu;
	topmenu.addItem(10000, "Automatic render", true, m_auto_render_enabled);
	PopupMenu nodetypesmenu;
    std::map<String,PopupMenu> exemenus;
    std::map<int,String> nodetypes;
    int id = 1;
    for (auto& e : node_factory::instance()->m_entries)
    {
        if (exemenus.count(e.m_exe)==0)
        {
            PopupMenu submenu;
            exemenus[e.m_exe]=submenu;
        }
        exemenus[e.m_exe].addItem(id,e.m_name,true,false);
        nodetypes[id]=e.m_name;
        ++id;
    }
    for (auto& e : exemenus)
        nodetypesmenu.addSubMenu(e.first, e.second);
	topmenu.addSubMenu("Add node", nodetypesmenu);
	int r = topmenu.show();
    if (r > 0)
    {
		if (r < 10000)
		{
			String nodetype = nodetypes[r];
			add_node_with_component(nodetype, xcor, ycor, 160, 60, false);
			addUndoState("Add node");
		}
		if (r == 10000)
		{
			m_auto_render_enabled = !m_auto_render_enabled;
		}
    }
}

void NodeView::mouseDown(const MouseEvent & ev)
{
    if (ev.mods.isRightButtonDown() == false)
    {
        for (auto& e : m_node_components)
            e->setGUIStatus(NodeComponentStatus::NotSelected);
        m_selected_node = nullptr;
        if (m_node_inspector!=nullptr)
			m_node_inspector->setNodes({});
        m_selection_path.clear();
        m_selection_path.startNewSubPath((float)ev.x, (float)ev.y);
        repaint();
        return;
    }
    if (ev.mods.isRightButtonDown()==true)
    {
        //doNodeCreateMenu(ev.x,ev.y);
        showNodeList(ev.x, ev.y, false);
    }
    
}

void NodeView::mouseUp(const MouseEvent &)
{
    m_selection_path.closeSubPath();
    selectNodesWithinPath();
    if (m_node_inspector!=nullptr)
		m_node_inspector->setNodes(getSelectedNodes());
    m_selection_path.clear();
    repaint();
}

void NodeView::mouseDrag(const MouseEvent & ev)
{
    if (ev.mods.isRightButtonDown() == true)
        return;
    m_selection_path.lineTo((float)ev.x, (float)ev.y);
    repaint();
}

void NodeView::mouseMove(const MouseEvent & ev)
{
    return;
    for (int i = 0; i < m_undo_history.size(); ++i)
	{
		Rectangle<int> undoentryrect(1, 1 + i * 20, 300, 19);
		if (undoentryrect.contains(ev.x,ev.y))
		{
			if (i != m_last_mouse_move_recalled)
			{
				recallUndoState(i);
				m_last_mouse_move_recalled = i;
				m_undo_level = i;
				break;
			}
		}
	}
}

void NodeView::timerCallback(int id)
{
    if (id == 0)
    {
        NodeView* target_view = this;
        //if (target_view->getGraphRenderState() == GraphRenderState::Idle)
        //	return;
        double curtime = Time::getMillisecondCounterHiRes();
        auto iter = m_render_jobs.begin();
        while (iter!=m_render_jobs.end())
            //for (int i = 0; i < m_render_jobs.size(); ++i)
        {
            auto& job = *iter;
            if (curtime>=job.m_start_time)
            {
                if (job.m_is_running == false)
                {
                    job.m_is_running = true;
                    visit_sources(job.m_node, [](process_node* n) { n->set_idle(); });
                    repaint();
                }
                job.m_node->tick();
                if (job.m_node->get_processing_state() == process_node::FinishedOK
                    || job.m_node->get_processing_state() == process_node::FinishedWithError)
                {
                    
                    iter = m_render_jobs.erase(iter);
                    repaint();
                    continue;
                }
                
            }
            ++iter;
        }
        if (m_render_jobs.size() == 0 && m_destinations_to_render.size()>0)
        {
            for (int i=m_destinations_to_render.size()-1;i>=0;--i)
            {
                Logger::writeToLog("render destination " + m_destinations_to_render[i]->get_name());
                renderNode(m_destinations_to_render[i], false, false);
            }
            m_destinations_to_render.clear();
            
        }
    }
    
}

void NodeView::setSelectedNodesForInspector()
{
    if (m_node_inspector!=nullptr)
		m_node_inspector->setNodes(getSelectedNodes());
}

void NodeView::testTopologicalSort(process_node* node)
{
	if (node == nullptr)
		node = m_selected_node.get();
	m_topological_render_order.clear();
	if (m_selected_node == nullptr)
	{
		Logger::writeToLog("No selected node");
		return;
	}
	std::vector<process_node*> sinknodes;
	visit_targets(node, [&sinknodes](process_node* destnode)
	{
		//Logger::writeToLog("Visiting destination " + destnode->get_name());
		if (destnode->m_destinations.size() == 0)
			sinknodes.push_back(destnode);
	});
	if (sinknodes.size() == 0)
	{
		Logger::writeToLog("No sink nodes found!");
		return;
	}
	Logger::writeToLog(String(sinknodes.size()) + " sink nodes");
	std::set<process_node*> duplcheckset;
	for (auto& e : sinknodes)
	{
		auto temp_order = topoSort(e);
		for (auto& f : temp_order)
			if (duplcheckset.count(f) == 0)
			{
				m_topological_render_order.push_back(f);
				duplcheckset.insert(f);
			}
	}
	if (m_topological_render_order.size() == 0)
	{
		Logger::writeToLog("Order could not be determined");
		return;
	}
	repaint();
	std::thread th([this]()
	{
		for (auto& e : m_topological_render_order)
		{
			if (e->prepareToRender() == false)
			{
				//e->m_ready_sources = 0;
				//e->set_idle();
			}
		}
		int sanity = 0;
		int readynodes = 0;
        while (true)
		{
            
            for (auto& e : m_topological_render_order)
			{
                if (e->m_topo_render_state == TopoRenderState::Rendering ||
                    e->m_topo_render_state == TopoRenderState::NeedsRendering)
                {
                    if (e->performRender()==TopoRenderState::FinishedOK)
                    {
                        ++readynodes;
                        if (readynodes==m_topological_render_order.size())
                            break;
                    }
                }
            }
            //Logger::writeToLog(String(readynodes)+" ready nodes");
            if (readynodes >= (int)m_topological_render_order.size())
				break;
			Thread::sleep(100);
			++sanity;
			if (sanity == 100)
			{
				Logger::writeToLog("Sanity failed");
				break;
			}
		}
		Logger::writeToLog("Rendering finished");
	});
	th.detach();
}

void NodeView::setAutoRenderEnabled(bool b)
{
	if (m_auto_render_enabled == false && b == true)
	{
		// render now
	}
	m_auto_render_enabled = b;
}

void dumpUndoEntry(UndoEntry& entry)
{
	String debugtxt;
	debugtxt << entry.m_desc << " ";
	for (auto& vt : entry.m_document)
	{
		debugtxt << vt.getProperty("uid").toString() << " " << (uint64)vt.getObjectAddress() << " ";
	}
	Logger::writeToLog(debugtxt);
	
}

void NodeView::addUndoState(String desc)
{
	std::map<int, ValueTree> old_doc_states;
	//std::map<int, ValueTree> new_doc_states;
	document_t old_doc = m_undo_history.back().m_document;
	for (auto& e : old_doc)
		old_doc_states[(int)e.getProperty("uid")] = e;
	document_t new_doc;
	for (auto& e : m_graph.getNodes())
	{
		int uid = e->getUniqueID();
		if (e->isDirty() == true)
		{
			new_doc.push_back(getNodeValueTree(e.get()));
			e->setDirty(false);
		}
		else
		{
			if (old_doc_states.count(uid) > 0)
				new_doc.push_back(old_doc_states[uid]);
		}
	}
	UndoEntry entry;
	entry.m_desc = desc;
	entry.m_document = new_doc;
	entry.m_connections = getConnectionsValueTree();
	m_undo_history.push_back(entry);
	m_undo_level = m_undo_history.size()-1;
	dumpUndoEntry(entry);
}

void NodeView::recallUndoState(int index)
{
	if (index >= 0 && index < m_undo_history.size())
	{
		restoreGraphFromDocument(m_undo_history[index].m_document);
		restoreConnectionsFromTree(m_undo_history[index].m_connections);
	}
}

void NodeView::doUndo()
{
	--m_undo_level;
	if (m_undo_level < 0)
		m_undo_level = 0;
	recallUndoState(m_undo_level);
}

void NodeView::doRedo()
{
	++m_undo_level;
	if (m_undo_level >= m_undo_history.size())
		m_undo_level = m_undo_history.size() - 1;
	recallUndoState(m_undo_level);
}

template<typename T,typename U=T>
using pairvector = std::vector<std::pair<T, U>>;

void NodeView::nodeManipulated(std::shared_ptr<process_node> node,
                               MouseAction act, int x_par, int y_par)
{
    std::shared_ptr<process_node> node_a;
    std::shared_ptr<process_node> node_b;
    std::shared_ptr<process_node> node_c;
    std::shared_ptr<process_node> node_d;
    if (act == MouseAction::MouseDown)
    {
        if (ModifierKeys::getCurrentModifiers().isShiftDown() == false)
        {
            if (node->m_component->isSelected() == true)
                return;
            int numselnodes = numSelectedNodes();
            if (numselnodes > 0)
            {
                for (auto& e : m_node_components)
                    e->setGUIStatus(NodeComponentStatus::NotSelected);
            }
            
            node->m_component->setGUIStatus(NodeComponentStatus::Targetted);
            setSelectedNodesForInspector();
            m_selected_node = node;
            if (m_audio_player!=nullptr && m_audio_player->is_playing() == false)
                m_audio_player->set_node(m_selected_node);
        }
        else
        {
            if (node->m_component->isSelected() == true)
                node->m_component->setGUIStatus(NodeComponentStatus::NotSelected);
            else
                node->m_component->setGUIStatus(NodeComponentStatus::Selected);
            setSelectedNodesForInspector();
        }
        repaint();
        return;
    }
    if (act == MouseAction::MouseUp)
    {
        m_drag_duplicate_was_done = false;
    }
    if (act == MouseAction::DragWithControlModifier && m_drag_duplicate_was_done == false
        && (abs(x_par)>50 || abs(y_par)>50))
    {
        double bench_t0 = Time::getMillisecondCounterHiRes();
        node_ref_vec nodes_to_dupl;
        std::map<node_ref, int> int_mapping;
        int index = 0;
        for (auto& e : m_node_components)
        {
            if (e->isSelected() == true)
            {
                nodes_to_dupl.push_back(e->getNode());
                int_mapping[e->getNode()] = index;
                ++index;
            }
        }
        pairvector<int> connections;
        for (auto& e : nodes_to_dupl)
        {
            for (auto& src_node : e->m_sources)
            {
                if (int_mapping.count(src_node) > 0)
                {
                    int index_a = int_mapping[src_node];
                    int index_b = int_mapping[e];
                    connections.emplace_back(index_a,index_b);
                }
            }
        }
        //Logger::writeToLog("there are " + String(connections.size()) + " connections within duplicated nodes");
        node_ref_vec duplicated_nodes;
        for (auto& e : nodes_to_dupl)
        {
            auto dupl = m_graph.duplicateNode(e, false);
            NodeComponent* comp = e->m_component;
            add_node_to_view(dupl, comp->getX(), comp->getY(), comp->getWidth(), comp->getHeight());
            auto& duplcomp = dupl->m_component;
            duplcomp->setGUIStatus(NodeComponentStatus::Selected);
            comp->setGUIStatus(NodeComponentStatus::NotSelected);
            m_selected_node = dupl;
            duplicated_nodes.push_back(dupl);
            
        }
        for (auto& e : connections)
        {
            auto node_a = duplicated_nodes[e.first];
            auto node_b = duplicated_nodes[e.second];
            connect_nodes(node_a, node_b, true);
        }
        //if (m_node_inspector != nullptr)
        //	m_node_inspector->setNodes(duplicated_nodes);
        m_drag_duplicate_was_done = true;
        double bench_t1 = Time::getMillisecondCounterHiRes();
        Logger::writeToLog("Drag duplicating nodes took " + String(bench_t1 - bench_t0) + " ms");
        repaint();
    }
    for (auto& e : m_node_components)
    {
        const int snap_size = 1;
        int new_xcor = (e->m_original_pos.x + x_par)/snap_size*snap_size;
        if (new_xcor < 0)
            new_xcor = 0;
        int new_ycor = (e->m_original_pos.y + y_par)/snap_size*snap_size;
        if (new_ycor < 0)
            new_ycor = 0;
        
        if (act == MouseAction::Drag ||
            (act==MouseAction::DragWithControlModifier && m_drag_duplicate_was_done == true))
        {
            if (e->isSelected() == true)
            {
                
                e->setTopLeftPosition(new_xcor, new_ycor);
                if (new_xcor + e->getWidth() > getWidth())
                    setSize(new_xcor + e->getWidth(), getHeight());
                if (new_ycor + e->getHeight() > getHeight())
                    setSize(getWidth(), new_ycor + e->getHeight());
            }
            if (e.get() == node->m_component)
                continue;
            int pos1x = e->getX() + (e->getWidth() / 2);
            int pos1y = e->getBottom();
            Point<int> pos1(pos1x, pos1y);
            
            int pos2x = node->m_component->getX() + (node->m_component->getWidth() / 2);
            int pos2y = node->m_component->getY();
            Point<int> pos2(pos2x, pos2y);
            int dist = pos1.getDistanceFrom(pos2);
            //int dist = pos1.getDistanceFrom(node->m_component->getPosition());
            if (dist < 100)
                e->m_is_close_to_node = true;
            else e->m_is_close_to_node = false;
            if (is_node_connection_compatible(e->getNode().get(),node.get())==false)
                e->m_is_close_to_node = false;
            if (m_drag_duplicate_was_done == true)
                e->m_is_close_to_node = false;
            
        }
        if (act == MouseAction::MouseUp)
        {
            e->m_original_pos = e->getPosition();
            if (e.get() != node->m_component)
            {
                if (e->m_is_close_to_node == true)
                {
                    node_a = e->getNode();
                    node_b = node;
                }
                
            }
            if (node->m_component->m_is_close_to_node == false)
            {
                node_c = e->getNode();
                node_d = node;
            }
            e->m_is_close_to_node = false;
            
        }
    }
    if (node_a != nullptr && node_b != nullptr && (node_a != node_b))
    {
        if (m_auto_connect_nodes == true)
        {
            connect_nodes_if_no_feedback(node_a, node_b);
            if (node_b->m_component!=nullptr)
                node_b->m_component->updateConnectionsList();
            if (m_node_inspector != nullptr)
                m_node_inspector->updateConnectionsList();
        }
        
    }
    //if (node_c != nullptr && node_d != nullptr)
    //	disconnect_nodes(node_c, node_d);
    repaint();
    
}

void NodeView::add_node_to_view(std::shared_ptr<process_node> new_node, int xcor, int ycor, int w, int h)
{
    std::unique_ptr<NodeComponent> comp(new NodeComponent(new_node,this));
    new_node->ProcessFinishedFunc = [this](process_node* np)
    {
        if (np->get_output_filenames().size() > 0)
        {
            np->m_component->set_audio_file(np->get_output_filenames()[0]);
        }
		np->setDirty(false);
    };
    new_node->ProcessErrorFunc = [this](process_node* no)
    {
        m_graph_render_state = GraphRenderState::FinishedError;
        m_graph.setRenderState(GraphRenderState::FinishedError);
        no->m_component->showBubbleMessage(no->get_error_text(),true,8000);
		no->setDirty(false);
    };
    comp->NodeManipulatedFunc = [this](std::shared_ptr<process_node> node, MouseAction act, int x_par, int y_par)
    {
        this->nodeManipulated(node, act, x_par, y_par);
    };
    comp->NodeMenuRequested = [this](std::shared_ptr<process_node> node)
    {
        node_menu_requested(node);
    };
    comp->setBounds(xcor, ycor, w, h);
    comp->m_original_pos = { xcor,ycor };
    if (xcor+w>=getWidth())
    {
        setSize(xcor+w+5, getHeight());
        if (m_viewport!=nullptr)
            m_viewport->setViewPosition(xcor, m_viewport->getViewPositionY());
    }
    if (ycor + h >= getHeight())
    {
        setSize(getWidth(), ycor + h + 5);
        if (m_viewport != nullptr)
            m_viewport->setViewPosition(m_viewport->getViewPositionX(), ycor);
    }
    addAndMakeVisible(comp.get());
    m_node_components.push_back(std::move(comp));
}

void NodeView::connect_nodes_if_no_feedback(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b)
{
    connect_nodes(a, b);
    bool has_cycle = has_circular_dependency(a);
    if (has_cycle == true)
    {
        disconnect_nodes(a, b);
        AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
                                         "CDP Frontend error",
                                         "Feedback connections not allowed", "OK",
                                         this);
        
    }
}

std::shared_ptr<process_node> NodeView::add_node_with_component(String procname, int xcor, int ycor, int w, int h,
                                                                bool skiperrordlg)
{
    //Logger::writeToLog("Creating node from name " + procname);
    auto new_node = m_graph.createNode(procname);
    if (new_node == nullptr)
    {
        if (skiperrordlg == false)
            AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
                                             "CDP Frontend",
                                             "Could not create node of type:\n"+procname, "OK",
                                             this);
        return nullptr;
    }
    
    add_node_to_view(new_node, xcor, ycor, w, h);
    if (new_node->getCustomEditorType2() == process_node::WindowCustomEditor)
    {
        AudioProcessorEditor* ed = static_cast<AudioProcessorEditor*>(new_node->getCustomEditor());
        /*PluginWindow* pwin =*/ new PluginWindow(ed, new_node.get(), PluginWindow::Normal);
    }
    setAllNodesSelectedState(false);
    if (new_node->m_component != nullptr)
    {
        new_node->m_component->setGUIStatus(NodeComponentStatus::Targetted);
        if (m_node_inspector != nullptr)
            m_node_inspector->setNodes({ new_node });
        m_selected_node = new_node;
    }
	return new_node;
}

void NodeView::node_menu_requested(std::shared_ptr<process_node> node)
{
    std::vector<std::shared_ptr<process_node>> temp_nodes = m_graph.getNodes();
    PopupMenu menu;
    
    int foo_id = 1000000;
    std::map<String,PopupMenu> input_bus_menus_map;
    int num_in_busses = node->getBussesLayout().getNumInputBuses();
    for (int i=0;i<num_in_busses;++i)
    {
        auto in_bus = node->getBussesLayout().getInputBusMutable(i);
        if (input_bus_menus_map.count(in_bus->getSignalFormat())==0)
        {
            input_bus_menus_map[in_bus->getSignalFormat()] = PopupMenu();
        }
        for (auto& e : temp_nodes)
        {
            if (e!=node)
            {
                int num_out_busses = e->getBussesLayout().getNumOutputBuses();
                for (int j=0;j<num_out_busses;++j)
                {
                    auto out_bus = e->getBussesLayout().getOutputBusMutable(j);
                    if (in_bus->isFormatCompatibleWith(*out_bus))
                    {
                        input_bus_menus_map[in_bus->getSignalFormat()].addItem(foo_id, e->get_name());
                        ++foo_id;
                    }
                }
            }
                
        }
        
    }
    for (auto& e : input_bus_menus_map)
    {
        String submenu_name = String("Connect ") + e.first+ " input to";
        menu.addSubMenu(submenu_name, e.second);
    }
    int id = 50000;
    PopupMenu inputs_menu2;
    for (auto& e : temp_nodes)
    {
        if (e != node && is_node_connection_compatible(e.get(),node.get()))
            inputs_menu2.addItem(id, e->get_name());
        ++id;
    }
    if (inputs_menu2.getNumItems()>0)
        menu.addSubMenu("Connect input to", inputs_menu2);
    
    PopupMenu inputs_menu;
    id = 10000;
    for (auto& in_node : node->m_sources)
    {
        inputs_menu.addItem(id, in_node->get_name());
        ++id;
    }
    if (inputs_menu.getNumItems()>0)
        menu.addSubMenu("Disconnect input", inputs_menu);
    
    id = 20000;
    PopupMenu outputs_menu;
    for (auto& out_node : node->m_destinations)
    {
        outputs_menu.addItem(id, out_node->get_name());
        ++id;
    }
    if (outputs_menu.getNumItems()>0)
        menu.addSubMenu("Disconnect output", outputs_menu);
    
    id = 30000;
    PopupMenu outputs_menu2;
    for (auto& out_node : temp_nodes)
    {
        if (is_node_connection_compatible(node.get(),out_node.get()) && out_node != node)
            outputs_menu2.addItem(id, out_node->get_name());
        ++id;
        
    }
    if (outputs_menu2.getNumItems()>0)
        menu.addSubMenu("Connect output to", outputs_menu2);
    
    /*
     PopupMenu swaps_menu;
     id = 40000;
     for (auto& out_node : temp_nodes)
     {
     if (is_node_connection_compatible(out_node.get(), node.get()))
     swaps_menu.addItem(id, out_node->get_name());
     ++id;
     }
     menu.addSubMenu("Swap connections with", swaps_menu);
     */
    menu.addItem(10, "Replace node...");
    menu.addItem(5, "Duplicate node");
    menu.addItem(6, "Duplicate node with connections");
    if (node->get_bypass_mode() != process_node::BypassNotSupported)
    {
        bool is_by_passed = node->get_bypass_mode() == process_node::Bypassed;
        menu.addItem(7, "Bypass node", true, is_by_passed);
    }
    menu.addItem(8, "Show custom GUI", true, false);
    //menu.addItem(9, "Show large waveform", true, false);
    int result = menu.show();
    if (result == 10)
    {
        auto mousepos = getMouseXYRelative();
        showNodeList(mousepos.x, mousepos.y, true);
    }
    if (result == 9)
    {
        /*
         String fn;
         if (node->get_output_filenames().size() > 0)
         fn = node->get_output_filenames()[0];
         WaveformWithEditor* wb = new WaveformWithEditor(node);
         wb->setSize(800, 400);
         wb->setAudioFile(fn);
         juce::Rectangle<int> area(5, 5, 10, 10);
         CallOutBox::launchAsynchronously(wb, area, getTopLevelComponent());
         return;
         */
    }
    if (result == 8)
    {
        if (PluginWindow::getWindowFor(node.get()) == nullptr)
        {
            if (node->getCustomEditorType2()==process_node::WindowCustomEditor)
            {
                AudioProcessorEditor* ed = static_cast<AudioProcessorEditor*>(node->getCustomEditor());
                /*PluginWindow* pwin =*/ new PluginWindow(ed, node.get(), PluginWindow::Normal);
            }
        }
    }
    
    if (result == 7)
    {
        if (node->get_bypass_mode() == process_node::Bypassed)
            node->set_bypass_mode(process_node::NotBypassed);
        else node->set_bypass_mode(process_node::Bypassed);
    }
    if (result == 5 || result == 6)
    {
        if (result == 5)
            duplicateNodes(false,false);
        else duplicateNodes(true,false);
        
    }
    if (result >= 10000 && result < 20000)
    {
        disconnect_nodes(node->m_sources[result - 10000], node);
    }
    if (result >= 20000 && result < 30000)
    {
        disconnect_nodes(node, node->m_destinations[result - 20000]);
    }
    if (result >= 30000 && result < 40000)
    {
        for (auto& e : m_node_components)
        {
            if (e->isSelected() == true && e->getNode() != temp_nodes[result - 30000])
            {
                connect_nodes_if_no_feedback(e->getNode(), temp_nodes[result - 30000]);
				addUndoState("Connect node output");
				//connect_nodes(e->m_node, temp_nodes[result - 300]);
            }
        }
        
    }
    if (result >= 50000 && result < 60000)
    {
        for (auto& e : m_node_components)
        {
            if (e->isSelected() == true && e->getNode() != temp_nodes[result - 50000])
            {
                connect_nodes_if_no_feedback(temp_nodes[result - 50000], e->getNode());
				addUndoState("Connect node input");
				//connect_nodes(temp_nodes[result - 500], e->m_node);
            }
        }
        
    }
    if (result >= 40000 && result < 50000)
    {
        auto other = temp_nodes[result - 40000];
        auto sources_1 = node->m_sources;
        auto sources_2 = other->m_sources;
        auto dests_1 = node->m_destinations;
        auto dests_2 = other->m_destinations;
        disconnect_node_from_graph(other);
        disconnect_node_from_graph(node);
        node->m_sources = sources_2;
        node->m_destinations = dests_2;
        other->m_sources = sources_1;
        other->m_destinations = dests_1;
        for (auto& e : node->m_destinations)
        {
            e->m_sources.push_back(node);
        }
        for (auto& e : other->m_destinations)
        {
            e->m_sources.push_back(other);
        }
    }
    if (m_node_inspector!=nullptr)
        m_node_inspector->updateConnectionsList();
    
    
    repaint();
}

node_ref_vec NodeView::getSelectedNodes()
{
    node_ref_vec v;
    for (auto& e : m_node_components)
        if (e->isSelected() == true)
            v.push_back(e->getNode());
    return v;
}

int NodeView::numSelectedNodes() const
{
    int result = 0;
    for (auto& e : m_node_components)
        if (e->isSelected() == true)
            ++result;
    return result;
}

void NodeView::duplicateNodes(bool with_connections, bool withRelativeConnections)
{
    auto nodes = getSelectedNodes();
    if (nodes.size()==0)
        return;
    int max_x = 0;
    for (auto& e : nodes)
        if (e->m_component != nullptr && e->m_component->getRight() > max_x)
            max_x = e->m_component->getRight();
    for (auto& e : m_node_components)
        e->setGUIStatus(NodeComponentStatus::NotSelected);
    node_ref_vec new_nodes;
    int cnt = 0;
    std::map<node_ref, int> integerindexmap;
    for (auto& e : nodes)
    {
        auto new_node = m_graph.duplicateNode(e, with_connections);
        int w = e->m_component->getWidth();
        int h = e->m_component->getHeight();
        int x = max_x + (e->m_component->getRight()-max_x) + 10;
        int y = e->m_component->getY();
        add_node_to_view(new_node, x, y, w, h);
        new_node->m_component->setGUIStatus(NodeComponentStatus::Selected);
        new_nodes.push_back(new_node);
        m_selected_node = new_node;
        integerindexmap[e] = cnt;
        ++cnt;
    }
    if (withRelativeConnections == true)
    {
        pairvector<int> connections;
        for (auto& e : nodes)
        {
            for (auto& src_node : e->m_sources)
            {
                if (integerindexmap.count(src_node) > 0)
                {
                    int index_a = integerindexmap[src_node];
                    int index_b = integerindexmap[e];
                    connections.emplace_back(index_a, index_b);
                }
            }
        }
        for (auto& e : connections)
        {
            auto node_a = new_nodes[e.first];
            auto node_b = new_nodes[e.second];
            connect_nodes(node_a, node_b, true);
        }
    }
    
    
    setSelectedNodesForInspector();
    repaint();
}

void NodeView::removeSelectedNodes()
{
    for (int i = 0; i < m_node_components.size(); ++i)
    {
        if (m_node_components[i]->isSelected() == true)
        {
            disconnect_node_from_graph(m_node_components[i]->getNode());
            for (int j = 0; j < m_graph.getNodes().size(); ++j)
            {
                if (m_graph.getNodes()[j]->m_component == m_node_components[i].get())
                {
                    PluginWindow::closeCurrentlyOpenWindowsFor(m_graph.getNodes()[j].get());
                    if (NodeDeletedFunc)
                        NodeDeletedFunc(m_graph.getNodes()[j].get());
                    m_graph.getNodes().erase(m_graph.getNodes().begin() + j);
                    m_selected_node = nullptr;
                    getLastEditedEnvelopeParameter(true);
                    break;
                }
            }
            
            
        }
    }
    remove_from_container(m_node_components, [](const std::unique_ptr<NodeComponent>& c)
                          {
                              return c->isSelected();
                          });
    
    if (m_node_inspector!=nullptr)
		m_node_inspector->setNodes({});
	addUndoState("Remove node");
	repaint();
}

void NodeView::removeNodes(const node_ref_vec & nodes_to_remove)
{
	if (nodes_to_remove.size() == 0)
		return;
	m_selected_node = nullptr;
	getLastEditedEnvelopeParameter(true);
	for (int j = 0; j < nodes_to_remove.size(); ++j)
	{
        disconnect_node_from_graph(nodes_to_remove[j]);
		PluginWindow::closeCurrentlyOpenWindowsFor(nodes_to_remove[j].get());
		if (NodeDeletedFunc)
			NodeDeletedFunc(nodes_to_remove[j].get());
		m_graph.removeNode(nodes_to_remove[j]);
	}
	remove_from_container(m_node_components, [nodes_to_remove](const std::unique_ptr<NodeComponent>& c)
	{
        for (auto& e : nodes_to_remove)
            if (c->getNode()==e)
                return true;
        return false;
	});
	if (m_node_inspector!=nullptr)
		m_node_inspector->setNodes({});
}

void NodeView::doForSelectedNodes(std::function<void(node_ref)> f)
{
    for (auto& e : m_node_components)
    {
        if (e->isSelected()==true)
        {
            f(e->getNode());
        }
    }
}

void NodeView::updateParametersOfSelectedNodes()
{
    for (auto& e : m_node_components)
    {
        if (e->isSelected() == true)
        {
            e->getNode()->update_params();
        }
    }
}

void NodeView::setAllNodesSelectedState(bool b)
{
    node_ref_vec vec;
    for (auto& e : m_node_components)
    {
        if (b==true)
            e->setGUIStatus(NodeComponentStatus::Selected);
        else e->setGUIStatus(NodeComponentStatus::NotSelected);
        if (b == true)
            vec.push_back(e->getNode());
    }
    if (m_node_inspector != nullptr)
    {
        if (b == false)
            m_node_inspector->setNodes({});
        else
            m_node_inspector->setNodes(vec);
    }
    if (b == false)
        m_selected_node = nullptr;
    repaint();
}

String NodeView::load_graph_query_filename()
{
    File initdir = File(g_propsfile->getValue("general/last_graph_save_dir"));
    FileChooser myChooser("Load graph",
                          initdir,
                          "*.cdpfeproject");
    if (myChooser.browseForFileToOpen() == false)
        return String();
    File file(myChooser.getResult());
    if (file.existsAsFile() == false)
        return String();
    loadGraphFromXmlFile(file);
    g_propsfile->setValue("general/last_graph_save_dir", file.getParentDirectory().getFullPathName());
    return myChooser.getResult().getFullPathName();
}

String NodeView::save_graph_query_filename()
{
    return String();
}



String NodeView::loadGraphFromXmlFile(File file)
{
    XmlDocument xmldoc(file);
    XmlElement* xmlelem = xmldoc.getDocumentElement();
    if (xmlelem != nullptr)
    {
        ValueTree doctree = ValueTree::fromXml(*xmlelem);
        delete xmlelem;
		if (doctree.isValid() == true)
		{
			restoreGraphFromValueTree(doctree);
			return String();
		}
		else return "Value tree was not valid";
        
    }
    return "File did not contain valid XML";
}

inline void setPropertiesHelper(ValueTree& tree, const char* key, const juce::var& v)
{
    tree.setProperty(key, v, nullptr);
}

template<typename... Args>
inline void setPropertiesToValueTree(ValueTree& tree, const char* key, const juce::var& v, Args&&... args)
{
    tree.setProperty(key, v, nullptr);
    setPropertiesHelper(tree, args...);
}

String NodeView::saveGraphToXmlFile(File file)
{
    ValueTree doctree = getGraphValueTree();
	XmlElement* xml = doctree.createXml();
    xml->writeToFile(file, "");
    delete xml;
    return String();
}

ValueTree NodeView::getGraphValueTree()
{
	ValueTree doctree("graph_document");
	doctree.setProperty("nodeview_w", getWidth(), nullptr);
	doctree.setProperty("nodeview_h", getHeight(), nullptr);
	if (m_viewport != nullptr)
	{
		//setPropertiesToValueTree(doctree, "nodeview_x", m_viewport->getViewPosition().x, "nodeview_y", m_viewport->getViewPosition().y);
		doctree.setProperty("nodeview_x", m_viewport->getViewPosition().x, nullptr);
		doctree.setProperty("nodeview_y", m_viewport->getViewPosition().y, nullptr);
	}
	ValueTree nodestree("nodes");
	for (auto& e : m_graph.getNodes())
	{
		ValueTree procstate = e->getStateValueTree();
		procstate.setProperty("type", e->get_type_name(), nullptr);
		procstate.setProperty("name", e->get_name(), nullptr);
		procstate.setProperty("uid", e->getUniqueID(), nullptr);
		procstate.setProperty("x", e->m_component->getX(), nullptr);
		procstate.setProperty("y", e->m_component->getY(), nullptr);
		procstate.setProperty("w", e->m_component->getWidth(), nullptr);
		procstate.setProperty("h", e->m_component->getHeight(), nullptr);
		ValueTree splitterstate = e->m_component->m_layout.saveState();
		procstate.addChild(splitterstate, -1, nullptr);
		nodestree.addChild(procstate, -1, nullptr);

	}
	doctree.addChild(nodestree, -1, nullptr);
	ValueTree connections("node_connections");
	int count = 0;
	for (auto& e : m_graph.getNodes())
	{
		for (auto& srcnode : e->m_sources)
		{
			ValueTree connection("conn");
			connection.setProperty("a", e->getUniqueID(), nullptr);
			connection.setProperty("b", srcnode->getUniqueID(), nullptr);
			connections.addChild(connection, -1, nullptr);
		}
	}
	doctree.addChild(connections, -1, nullptr);
	return doctree;
}

void NodeView::restoreGraphFromValueTree(ValueTree doctree)
{
	if (m_viewport != nullptr)
	{
		int view_w = doctree.getProperty("nodeview_w", getWidth());
		int view_h = doctree.getProperty("nodeview_h", getHeight());
		int view_x = doctree.getProperty("node_view_x", m_viewport->getViewPosition().x);
		int view_y = doctree.getProperty("node_view_y", m_viewport->getViewPosition().y);
		setSize(view_w, view_h);
		m_viewport->setViewPosition(view_x, view_y);
	}
	ValueTree nodes = doctree.getChildWithName("nodes");
	if (nodes.isValid() == true)
	{
		m_graph.getNodes().clear();
		m_node_components.clear();
		std::map < int, std::shared_ptr<process_node>> nodesmap;
		int numnodes = nodes.getNumChildren();
		for (int i = 0; i < numnodes; ++i)
		{
			ValueTree procnode = nodes.getChild(i);
			String type = procnode.getProperty("type");
			String name = procnode.getProperty("name");
			int x = procnode.getProperty("x");
			int y = procnode.getProperty("y");
			int w = procnode.getProperty("w");
			int h = procnode.getProperty("h");
			int uid = procnode.getProperty("uid");
			auto new_node = add_node_with_component(type, x, y, w, h, true);
			if (new_node != nullptr)
			{
				if (new_node->m_component)
				{
					ValueTree splitterstate = procnode.getChildWithName("sls");
					new_node->m_component->m_layout.restoreState(splitterstate);
					new_node->m_component->resized();
				}
				new_node->set_name(name);
				new_node->restoreStateFromValueTree(procnode);
				nodesmap[uid] = new_node;
			}
		}
		ValueTree connstree = doctree.getChildWithName("node_connections");
		if (connstree.isValid() == true)
		{
			int num_cons = connstree.getNumChildren();
			for (int i = 0; i < num_cons; ++i)
			{
				ValueTree child = connstree.getChild(i);
				int node_id_a = child.getProperty("a");
				int node_id_b = child.getProperty("b");
				auto node_a = nodesmap[node_id_a];
				auto node_b = nodesmap[node_id_b];
				if (node_a != nullptr && node_b != nullptr)
				{
					connect_nodes(node_b, node_a);
				}
			}
		}
		repaint();
	}
}

ValueTree NodeView::getNodeValueTree(process_node* node)
{
	if (node == nullptr)
		return ValueTree();
	ValueTree procstate = node->getStateValueTree();
	procstate.setProperty("type", node->get_type_name(), nullptr);
	procstate.setProperty("name", node->get_name(), nullptr);
	procstate.setProperty("uid", node->getUniqueID(), nullptr);
	procstate.setProperty("x", node->m_component->getX(), nullptr);
	procstate.setProperty("y", node->m_component->getY(), nullptr);
	procstate.setProperty("w", node->m_component->getWidth(), nullptr);
	procstate.setProperty("h", node->m_component->getHeight(), nullptr);
	ValueTree splitterstate = node->m_component->m_layout.saveState();
	procstate.addChild(splitterstate, -1, nullptr);
	return procstate;
}

void NodeView::restoreNodeFromTree(process_node* node,ValueTree tree)
{
	node->restoreStateFromValueTree(tree);
	String name = tree.getProperty("name");
	if (name.isEmpty() == false)
		node->set_name(name);
	node->setUniqueID(tree.getProperty("uid"));
	if (node->m_component != nullptr)
	{
		int x = tree.getProperty("x");
		int y = tree.getProperty("y");
		int w = tree.getProperty("w");
		int h = tree.getProperty("h");
		node->m_component->setBounds(x, y, w, h);
	}
}

ValueTree NodeView::getConnectionsValueTree()
{
	ValueTree connections("node_connections");
	int count = 0;
	for (auto& e : m_graph.getNodes())
	{
		for (auto& srcnode : e->m_sources)
		{
			ValueTree connection("conn");
			connection.setProperty("a", e->getUniqueID(), nullptr);
			connection.setProperty("b", srcnode->getUniqueID(), nullptr);
			connections.addChild(connection, -1, nullptr);
		}
	}
	return connections;
}

void NodeView::restoreConnectionsFromTree(ValueTree tree)
{
	std::map<int, node_ref> nodesmap;
	for (auto& e : m_graph.getNodes())
	{
		nodesmap[e->getUniqueID()] = e;
		e->m_sources.clear();
		e->m_destinations.clear();
	}
	int num_cons = tree.getNumChildren();
	for (int i = 0; i < num_cons; ++i)
	{
		ValueTree child = tree.getChild(i);
		int node_id_a = child.getProperty("a");
		int node_id_b = child.getProperty("b");
		auto node_a = nodesmap[node_id_a];
		auto node_b = nodesmap[node_id_b];
		if (node_a != nullptr && node_b != nullptr)
		{
			Logger::writeToLog("Undo : reconnecting " + String(node_id_b) + "->" + String(node_id_a));
			connect_nodes(node_b, node_a, true);
		}
	}
}

document_t NodeView::getDocument()
{
	document_t doc;
	for (auto& node : m_graph.getNodes())
	{
		ValueTree nodestate = getNodeValueTree(node.get());
		doc.push_back(nodestate);
	}
	doc.push_back(getConnectionsValueTree());
	return doc;
}

void NodeView::restoreGraphFromDocument(const document_t & doc)
{
	std::map<int,node_ref> current_nodes;
	for (auto& e : m_graph.getNodes())
		current_nodes[e->getUniqueID()]=e;
	std::map<int,ValueTree> document_nodes;
	for (auto& e : doc)
		document_nodes[e.getProperty("uid")]=e;
	for (auto& e : document_nodes)
	{
		if (current_nodes.count(e.first) > 0)
		{
			Logger::writeToLog("Undo : Restoring state " + e.second.getProperty("name").toString());
			restoreNodeFromTree(current_nodes[e.first].get(), e.second);
		}
	}
	node_ref_vec nodes_to_remove;
	for (auto& e : current_nodes)
	{
		if (document_nodes.count(e.first) == 0)
		{
			nodes_to_remove.push_back(e.second);
		}
	}
	if (nodes_to_remove.size() > 0)
	{
		Logger::writeToLog("Undo : Removing " + String(nodes_to_remove.size()) + " nodes");
		removeNodes(nodes_to_remove);
	}
	for (auto& e : document_nodes)
	{
		if (current_nodes.count(e.first) == 0)
		{
			Logger::writeToLog("Undo : Recreating " + e.second.getProperty("name").toString());
			String nodetype = e.second.getProperty("type");
			auto new_node = add_node_with_component(nodetype, 0, 0, 10, 10, true);
			if (new_node != nullptr)
			{
				restoreNodeFromTree(new_node.get(), e.second);
			}
		}
	}
	
}

void NodeView::restoreGraphFromUndoEntry(UndoEntry & entry)
{
}

String NodeView::renderNode(process_node* node_to_render, bool blockcurrentthread, bool renderdestinations)
{
	//testTopologicalSort(node_to_render);
	auto target_node = node_to_render;
    if (target_node == nullptr)
        return "No node";
    NodeView* target_view = this;
    //if (target_node->get_output_filenames().size() > 0)
    {
        m_render_elapsed_clock_time = 0.0;
        m_render_start_clock_time = Time::getMillisecondCounterHiRes();
        visit_sources(target_node, [](process_node* n) { n->set_idle(); });
        if (blockcurrentthread == false)
        {
            m_render_jobs.emplace_back(target_node, Time::getMillisecondCounterHiRes());
            if (renderdestinations == true)
            {
                visit_targets(target_node, [this,target_node](process_node* destnode)
                              {
                                  if (destnode!=target_node)
                                      m_destinations_to_render.push_back(destnode);
                              });
            }
            target_view->setGraphRenderState(GraphRenderState::Rendering);
            return String();
        }
        else
        {
            while (true)
            {
                target_node->tick();
                Thread::sleep(50);
                if (target_view->getGraphRenderState() == GraphRenderState::FinishedError)
                    return "Blocking render failed with error";
                if (target_node->get_processing_state() == process_node::FinishedOK)
                {
                    double temp = Time::getMillisecondCounterHiRes();
                    m_render_elapsed_clock_time = temp - m_render_start_clock_time;
                    repaint();
                    return String();
                }
            }
        }
    }
    return "Should not get this";
}

String NodeView::renderSelected(bool blockcurrentthread)
{
    auto target_node = m_selected_node.get();
    if (target_node == nullptr)
        return "No node";
    return renderNode(target_node, blockcurrentthread, true);
    
}

void NodeView::renderNodeDeferred(process_node * node, int delay)
{
	if (node == nullptr)
        return;
    stopTimer(100);
    m_render_jobs.emplace_back(node, Time::getMillisecondCounterHiRes() + delay);
    visit_targets(node, [this, node](process_node* destnode)
                  {
                      if (destnode != node)
                          m_destinations_to_render.push_back(destnode);
                  });
    startTimer(100, delay);
}

void NodeView::forceRenderSelected()
{
    if (m_selected_node == nullptr)
        return;
    m_selected_node->m_component->set_audio_file(String());
    //m_audio_player->set_audio_file(String());
    m_selected_node->removeLastOutputFile();
}

void NodeView::rotateSources()
{
    if (m_selected_node == nullptr)
        return;
    std::rotate(m_selected_node->m_sources.begin(),
                m_selected_node->m_sources.begin()+1,
                m_selected_node->m_sources.end());
    if (m_node_inspector != nullptr)
        m_node_inspector->updateConnectionsList();
}

void NodeView::permutateSources()
{
    if (m_selected_node == nullptr)
        return;
    std::next_permutation(m_selected_node->m_sources.begin(), m_selected_node->m_sources.end());
    if (m_node_inspector != nullptr)
        m_node_inspector->updateConnectionsList();
}

void NodeView::showLastEnvelope()
{
    auto par = getLastEditedEnvelopeParameter(false);
    if (par != nullptr)
        showEnvelopeCalloutBox(par, getTopLevelComponent());
}

void NodeView::renameSelectedNode()
{
    if (m_selected_node == nullptr)
        return;
    std::map<String,process_node*> usednames;
    for (auto& e : m_graph.getNodes())
    {
        usednames[e->get_name()] = e.get();
    }
    AlertWindow dlg(CharPointer_UTF8("λ"), "Enter new node name", AlertWindow::InfoIcon, this);
    dlg.addTextEditor("name", m_selected_node->get_name(), "Name");
    dlg.addButton("OK", 1, KeyPress(KeyPress::returnKey));
    dlg.addButton("Cancel", 2, KeyPress(KeyPress::escapeKey));
    int r = dlg.runModalLoop();
    if (r == 1)
    {
        String new_name = dlg.getTextEditorContents("name");
        if (new_name.isEmpty() == false && new_name!=m_selected_node->get_name())
        {
            if (usednames.count(new_name) > 0)
            {
                process_node* node = usednames[new_name];
                node->m_component->showBubbleMessage("Name already used by this node",true,8000);
                return;
            }
            m_selected_node->set_name(new_name);
            m_selected_node->m_component->repaint();
			addUndoState("Rename node");
        }
    }
}

void NodeView::disconnectSelectedNodeIO(bool inputs, bool outputs)
{
    if (m_selected_node == nullptr)
        return;
    if (inputs == true && outputs == true)
        doForSelectedNodes([this](node_ref node) { disconnect_node_from_graph(node);} );
    if (inputs == true && outputs == false)
        doForSelectedNodes([this](node_ref node) { disconnect_node_inputs(node);} );
    if (inputs == false && outputs == true)
        doForSelectedNodes([this](node_ref node) { disconnect_node_outputs(node);} );
    repaint();
}

void NodeView::connectOutputToClosestInput(bool below)
{
    if (m_selected_node == nullptr)
        return;
    int sel_x_cor = m_selected_node->m_component->getX() + m_selected_node->m_component->getWidth() / 2;
    int sel_y_cor = m_selected_node->m_component->getBottom();
    Point<int> pt1{ sel_x_cor,sel_y_cor };
    int index = -1;
    double mindist = 10000000.0;
    for (int i=0;i<m_node_components.size();++i)
    {
        NodeComponent* comp = m_node_components[i].get();
        if (comp != m_selected_node->m_component)
        {
            int targ_x_cor = comp->getX() + comp->getWidth() / 2;
            int targ_y_cor = comp->getY();
            Point<int> pt2{ targ_x_cor,targ_y_cor };
            double dista = pt2.getDistanceFrom(pt1);
            if (dista < mindist && below == true && targ_y_cor>sel_y_cor)
            {
                mindist = dista;
                index = i;
            }
        }
    }
    if (index == -1)
        Logger::writeToLog("No closest node found!");
    else
    {
        String name = m_node_components[index]->getNode()->get_name();
        //Logger::writeToLog("Closest node should be " + name);
        connect_nodes(m_selected_node, m_node_components[index]->getNode());
    }
    repaint();
}

void NodeView::selectNodesWithinPath()
{
    for (auto& e : m_node_components)
    {
        auto r = e->getBoundsInParent();
        if (m_selection_path.contains((float)r.getX()+r.getWidth()/2.0f, (float)r.getY()+r.getHeight()/2.0f))
        {
            e->setGUIStatus(NodeComponentStatus::Selected);
        }
        else e->setGUIStatus(NodeComponentStatus::NotSelected);
        
    }
}


