#ifndef node_component_h
#define node_component_h


#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"
#include "node_properties_panel.h"
#include "sol.hpp"
#include "waveform_component.h"
#include "misc_components.h"

class NodeView;

enum class NodeComponentStatus
{
	NotSelected,
	Selected,
	Targetted
};

class NodeComponent : public Component, public ChangeListener
{
public:
    	
	NodeComponent(std::shared_ptr<process_node> node, NodeView* view);
	~NodeComponent();
    int m_waveform_h = 60;
    void resized();
    void changeListenerCallback(ChangeBroadcaster* cbc);
    void set_audio_file(String fn, bool forcethumbupdate=false);
    void paint(Graphics& g) override;
    void paintOverChildren(Graphics& g) override;
    void mouseDown(const MouseEvent& e);
    void mouseUp(const MouseEvent &e);
    std::function<void(std::shared_ptr<process_node>)> NodeMenuRequested;
    std::function<void(std::shared_ptr<process_node>,MouseAction,int,int)> NodeManipulatedFunc;
	void set_playcurpos(double x);
    void mouseDrag(const MouseEvent& e);
    
    bool m_is_close_to_node = false;
	
    NodeComponentStatus getGUIStatus() const { return m_gui_status; }
	void setGUIStatus(NodeComponentStatus b);
	bool isSelected() const;
	void update_params_inspector();
	void updateConnectionsList();
	void showBubbleMessage(String msg, bool is_error, int timeout);
    Point<int> m_original_pos;
	node_ref getNode() { return m_node; }
	NodeView* m_node_view = nullptr;
    SplitterLayout m_layout;
private:
	std::shared_ptr<process_node> m_node;
	Label m_name_label;
	Label m_status_label;
	std::unique_ptr<node_inspector> m_params_inspector;
    std::unique_ptr<ResizableCornerComponent2> m_corner_resizer;
	std::unique_ptr<WaveformComponentBase> m_wavecomponent;
	
	String m_wave_fn;
	
	NodeComponentStatus m_gui_status = NodeComponentStatus::NotSelected;
    double m_playcurpos = 0.0;
	bool m_was_seeked = false;
    double m_anim_phase = 0.0;
};

class node_inspector;
class IJCDPreviewPlayback;


class ParameterManager : public Component, public ListBoxModel
{
public:
    struct entry_t
    {
        String m_par_name;
        String m_node_name;
    };
    ParameterManager(std::vector<std::shared_ptr<process_node>>* nodes);
    int getNumRows() override
    {
        return (int)m_params.size();
    }
    void paintListBoxItem(int rowNumber, Graphics &g, int width, int height, bool rowIsSelected) override
    {
        
        if (rowNumber >= 0 && rowNumber < m_params.size())
        {
            if (rowIsSelected == true)
            {
                g.setColour(Colours::lightblue);
                g.fillRect(0, 0, width, height);
            }
            g.setColour(Colours::black);
            String text = m_params[rowNumber].m_node_name+"/"+m_params[rowNumber].m_par_name;
            g.drawText(text, 1, 1, width, height, Justification::centredLeft);
        }
        
    }
    void updateModel()
    {
        m_params.clear();
        for (auto& e : *m_nodes)
        {
            String nodename = e->get_name();
            for (auto& par : e->get_parameters())
            {
                entry_t entry;
                entry.m_node_name = nodename;
                entry.m_par_name = par->m_name;
                m_params.push_back(entry);
            }
        }
        auto compar = [](entry_t& a, entry_t& b)
        {
            return a.m_node_name+a.m_par_name < b.m_node_name+b.m_par_name;
        };
        std::sort(m_params.begin(),m_params.end(),compar);
    }
private:
    std::vector<std::shared_ptr<process_node>>* m_nodes = nullptr;
    ListBox m_listbox;
    std::vector<entry_t> m_params;
};

class CustomParameterComponent : public Component
{
public:
    virtual int getDefaultHeight() = 0;
};

class TextFileParameterComponent : public CustomParameterComponent,
	public TextEditor::Listener, public Timer
{
public:
	TextFileParameterComponent(node_parameter* par);
	void resized() override;
	void timerCallback() override;
	void textEditorTextChanged(TextEditor& ed) override;
	String generateTextFile();
private:
	TextEditor m_text_edit;
	BubbleMessageComponent m_bubble;
	node_parameter* m_par = nullptr;
};

class ControlFactory
{
public:
	ControlFactory() {}
	static ControlFactory* getInstance(bool destroy=false);
	std::unique_ptr<CustomParameterComponent> createFromParameter(node_parameter* par);
private:
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ControlFactory)
};

class DragTestComponent : public Component
{
public:
	DragTestComponent(NodeView* view);
	ComponentDragger* m_dragger = nullptr;
	String m_imgfn;
	NodeView* m_view = nullptr;
	bool m_has_done_duplicate = false;
	void mouseDown(const MouseEvent& ev) override;
	void mouseDrag(const MouseEvent& ev) override;

	void paint(Graphics& g) override;
	void resized() override;
	ComponentBoundsConstrainer m_constrain;
	ResizableCornerComponent m_corner_comp;

};


#endif /* node_component_h */
