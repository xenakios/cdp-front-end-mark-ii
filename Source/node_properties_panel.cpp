#include "preset_component.h"
#include "node_properties_panel.h"
#include "node_component.h"
#include "node_view.h"
#include "envelope_component.h"
#include "mixersequencer_node.h"
#include "chain_node.h"
#include <memory>
#include <map>
#include "parameter_linker.h"


extern std::unique_ptr<PropertiesFile> g_propsfile;
extern std::unique_ptr<TooltipWindow> g_tooltipwindow;
extern std::unique_ptr<Slider> g_par_helper_slider;

void node_inspector::build_props_panel()
{
	if (m_nodes.size() == 0)
	{
		m_param_panel = nullptr;
		m_custom_editor = nullptr;
		m_file_listbox->setVisible(false);
		m_args_editor->setVisible(false);
		return;
	}
	if (m_nodes.front()->get_supported_input_type().isEmpty()==false)
		m_file_listbox->setVisible(true);
	else m_file_listbox->setVisible(false);
	m_args_editor->setVisible(true);
	if (m_nodes.size() == 1)
	{
		if (m_nodes.front()->getCustomEditorType2() == process_node::InspectorCustomEditor)
		{
			m_param_panel = nullptr;
			Component* ed = m_nodes.front()->getCustomEditor();
            ed->getProperties().set("is_in_node_component", m_is_node_component_inspector);
            m_custom_editor = std::unique_ptr<Component>(ed);
			m_view_port->setSize(getWidth(), getHeight());
			m_view_port->setViewedComponent(m_custom_editor.get(), false);
			resized();
			return;
		}
	}
	m_custom_editor = nullptr;
	if (m_nodes.size() == 1)
	{
		
		m_preset_component = std::make_unique<PresetComponent>(m_nodes.front()->get_type_name());
		addAndMakeVisible(m_preset_component.get());
		m_preset_component->GetPresetValueTree = [this]()
		{
			return m_nodes.front()->getStateValueTree();
		};
		m_preset_component->OnPresetChanged = [this](ValueTree state)
		{
			m_nodes.front()->restoreStateFromValueTree(state);
		};
		
	}
	m_param_panel = std::make_unique<parameter_panel>(m_nodes,m_param_linker);
	
	//setSize(300, 300);
	
	m_view_port->setSize(getWidth(), getHeight());
	m_view_port->setViewedComponent(m_param_panel.get(), false);
	
	//addAndMakeVisible(m_param_panel.get());
	resized();

}

void node_inspector::set_show_procname(bool b)
{
	m_show_procname = b;
	if (m_param_panel != nullptr)
		m_param_panel->set_show_procname(b);
}

void node_inspector::updateConnectionsList() 
{ 
	m_file_listbox->updateContent(); 
	m_file_listbox->repaint();
}

node_inspector::node_inspector(bool is_nodecomp_inspector)
{
    m_is_node_component_inspector = is_nodecomp_inspector;
    StateChangedFunc = [](node_inspector*) {};
	m_view_port = std::make_unique<Viewport>();
	m_view_port->setBounds(0, 0, 10, 10);
	addAndMakeVisible(m_view_port.get());
	m_file_listbox = std::make_unique <ConnectionsListBox>();
	m_file_listbox->setBounds(0, 0, 10, 10);
	addAndMakeVisible(m_file_listbox.get());
	
	m_args_editor = std::make_unique<TextEditor>();
	m_args_editor->setReadOnly(true);
	m_args_editor->setBounds(0, 0, 10, 10);
	m_args_editor->setMultiLine(true);
	addAndMakeVisible(m_args_editor.get());
	m_args_editor->setVisible(false);
	m_file_listbox->setVisible(false);
	startTimer(1,1000);
}

void node_inspector::setNodes(node_ref_vec nodes)
{
	if (m_nodes == nodes)
		return;
	m_nodes = nodes;
	build_props_panel();
	if (m_nodes.size()>0)
		m_file_listbox->set_node(m_nodes[0]);
	repaint();
}

void node_inspector::removeNode(process_node * n)
{
	remove_from_container(m_nodes, [n](node_ref& node)
	{
		return node.get() == n;
	});
	build_props_panel();
	repaint();
}

void node_inspector::resized()
{
	int bot = 2;
	if (m_preset_component != nullptr)
	{
		m_preset_component->setBounds(2, 2, getWidth() - 3, 21);
		bot = m_preset_component->getBottom();
	}
	
	if (m_view_port != nullptr)
	{
		m_view_port->setBounds(2, bot, getWidth() - 4, getHeight() - 4);
	}
	
	if (m_param_panel != nullptr)
	{
		m_param_panel->setBounds(0, 0, getWidth() - 22, m_param_panel->get_content_height());
		bot = m_param_panel->getBottom()+22;
	}
	if (m_custom_editor != nullptr)
	{
		int custmaxheight = m_custom_editor->getProperties()["maxh"];
		if (custmaxheight == 0)
			custmaxheight = 100;
		if (custmaxheight == -1 && m_view_port != nullptr)
			custmaxheight = m_view_port->getHeight() - 20;
		m_custom_editor->setBounds(0, 0, getWidth() - 22, custmaxheight);
		bot = m_custom_editor->getBottom();
	}
	if (m_file_listbox != nullptr && m_param_panel != nullptr)
	{
		m_file_listbox->setBounds(0, bot, getWidth(), 100);
	}
	if (m_file_listbox != nullptr && m_custom_editor != nullptr)
	{
		m_file_listbox->setBounds(0, bot, getWidth(), 100);
	}
	if (m_args_editor != nullptr && m_file_listbox != nullptr)
	{
		m_args_editor->setBounds(0, m_file_listbox->getBottom(), getWidth(), 120);
	}
}

void node_inspector::valueChanged(Value& v)
{
	//Logger::writeToLog("foo " + v.toString());
	m_last_changed_value = v.getValue();
	stopTimer(0);
	startTimer(0,500);
}

void node_inspector::timerCallback(int id)
{
	if (id == 0)
	{
		stopTimer(0);
		StateChangedFunc(this);
		//Logger::writeToLog("value has changed to " + m_last_changed_value.toString());
	}
	if (id == 1)
	{
		if (m_nodes.size() > 0)
		{
			if (m_nodes.front()->usesCommandLine() == true)
			{
				String temp = m_nodes.front()->get_arguments().joinIntoString(" ");
				m_args_editor->setText(temp);
			}
			else m_args_editor->setVisible(false);
		}
		if (m_param_panel!=nullptr)
			m_param_panel->updateParameterComponentsEnabled();
	}
}

void node_inspector::paint(Graphics& g)
{
	g.fillAll(Colours::darkgrey);
	if (m_show_procname == false)
		return;
	g.setColour(Colours::white);
	if (m_nodes.size() == 0)
		g.drawText("No node selected", getBounds(), Justification::centredTop);
	if (m_nodes.size() == 1)
	{
		String text = m_nodes.front()->get_name();
		g.drawText(text, 1, 1, getWidth() - 2, 30, Justification::centredTop);
	}
	if (m_nodes.size() > 1)
		g.drawText("Multiple selected", 1, 1, getWidth() - 2, 30, Justification::centredTop);

}

ConnectionsListBox::ConnectionsListBox()
{
	setModel(this);
	setColour(backgroundColourId, Colours::grey);
	setRowHeight(20);

}

void ConnectionsListBox::set_node(std::shared_ptr<process_node> node)
{
	m_node = node;
	updateContent();
}

bool ConnectionsListBox::isInterestedInDragSource(const SourceDetails & dragSourceDetails)
{
	if (int(dragSourceDetails.description) >= 0)
		return true;
	return false;
}

void ConnectionsListBox::itemDragMove(const SourceDetails & )
{

}

void ConnectionsListBox::itemDropped(const SourceDetails & dragSourceDetails)
{
	int new_index = getInsertionIndexForPosition(dragSourceDetails.localPosition.getX(), dragSourceDetails.localPosition.getY());
	if (new_index >= 0)
	{
		int old_index = dragSourceDetails.description;
		auto f = m_node->m_sources.begin() + old_index;
		auto l = m_node->m_sources.begin() + old_index + 1;
		auto p = m_node->m_sources.begin() + new_index;
		slide(f, l, p);
		m_node->connections_changed();
		m_node->setDirty(true);
		updateContent();
		repaint();
	}
}

int ConnectionsListBox::getNumRows()
{
	if (m_node == nullptr)
		return 0;
	return (int)m_node->m_sources.size();
}

var ConnectionsListBox::getDragSourceDescription(const SparseSet<int>& currentlySelectedRows)
{
	if (currentlySelectedRows.size() > 0)
		return currentlySelectedRows[0];
	return -1;
}

void ConnectionsListBox::paintListBoxItem(int rowNumber, Graphics & g, int width, int height, bool rowIsSelected)
{
	if (m_node == nullptr)
		return;
	if (rowNumber < 0 || rowNumber >= m_node->m_sources.size())
		return;
	if (rowIsSelected == true)
		g.fillAll(Colours::blue);
	else g.fillAll(Colours::grey);
	g.setColour(Colours::white);
	g.setFont(height*0.8f);
	g.drawText(m_node->m_sources[rowNumber]->get_name(), 5, 0, width, height, Justification::centredLeft);
}

parameter_panel::parameter_panel(node_ref_vec nodes, ParameterLinker* lp) :
	m_nodes(nodes), m_param_linker(lp)
{
	int numparstoshow = 0;
	for (auto& e : m_nodes)
		numparstoshow += e->get_parameters().size();
	m_param_comps.resize(numparstoshow);
	int cnt = 0;
	for (auto& node : m_nodes)
	{
		auto& params = node->get_parameters();
		

		for (auto& e : params)
		{
			int index_to_use = cnt; // e.m_gui_order;
			//if (index_to_use<0)
			//    index_to_use = cnt;
			auto parcomp = std::make_unique<parameter_component>(e);
			parcomp->setParameterLinker(m_param_linker);
			parcomp->setBounds(0, 0, 10, 10);
			parcomp->addChangeListener(this);
			addAndMakeVisible(parcomp.get());
			m_param_comps[index_to_use] = std::move(parcomp);
			++cnt;
		}
	}
    
	setSize(300, get_content_height());
}

void parameter_panel::resized()
{
	int yoffs = get_top_margin();
	
    for (int i = 0; i<m_param_comps.size(); ++i)
	{
		int h = m_param_comps[i]->getSuggestedHeigth();
		m_param_comps[i]->setBounds(0, yoffs, getWidth(), h);
		yoffs += h + 2;
	}
}

int parameter_panel::get_content_height() const
{
	if (m_nodes.size() == 0)
		return get_top_margin();
	
    int yoffs = get_top_margin();
	for (int i = 0; i<m_param_comps.size(); ++i)
	{
		int h = m_param_comps[i]->getSuggestedHeigth();
		yoffs += h + 2;
	}
	return yoffs;
}

void parameter_panel::changeListenerCallback(ChangeBroadcaster *)
{
	if (m_nodes.size() == 0)
		return;
	for (auto& node : m_nodes)
		if (node->m_component != nullptr)
			node->m_component->repaint();
}

int parameter_panel::get_top_margin() const
{
	if (m_show_proc_name == false)
		return 0;
	return 50;
}

parameter_component::parameter_component(parameter_ref param) :
	m_param(param)
{
	m_name_label = std::make_unique < Label >();
	String idtxt;
	if (param->m_parent_node != nullptr)
		idtxt = String(param->m_parent_node->getUniqueID());
	m_name_label->setText(idtxt+"/"+m_param->m_name, dontSendNotification);
	m_name_label->setBounds(0, 0, 150, 20);
	m_name_label->setColour(Label::textColourId, Colours::white);
	addAndMakeVisible(m_name_label.get());
	if (m_param->m_choice_strings.size() == 0)
	{
		if (m_param->m_control_type.isEmpty() == false)
		{
			m_custom_component = ControlFactory::getInstance()->createFromParameter(m_param.get());
			addAndMakeVisible(m_custom_component.get());
			m_custom_component->setBounds(0,0,10,10);
			m_heigth = m_custom_component->getDefaultHeight();
		}
		else
		{
			if (m_param->m_param_type == parameterType::Bool == true)
			{
				m_toggle_but = std::make_unique<ToggleButton>();
				m_toggle_but->setBounds(0, 0, 10, 10);
				m_toggle_but->setToggleState((bool)m_param->getValue(), dontSendNotification);
				m_toggle_but->setButtonText(m_param->m_name);
				addAndMakeVisible(m_toggle_but.get());
				m_toggle_but->addListener(this);
				m_name_label->setVisible(false);
				m_toggle_but->setColour(ToggleButton::textColourId, Colours::white);
			}
			if (m_param->m_param_type == parameterType::FloatingPoint)
			{
				m_slider = std::make_unique<Slider>(Slider::LinearBar, Slider::TextBoxLeft);
				m_slider->setRange(m_param->m_min_value.getValue(), m_param->m_max_value.getValue(), m_param->m_step_size);
				m_slider->setSkewFactor(m_param->m_skew, m_param->m_sym_skew);
				if (m_param->m_midpoint_skew == true)
					m_slider->setSkewFactorFromMidPoint(m_param->m_midpoint);
				m_slider->setValue(m_param->getValue(),dontSendNotification);
				m_slider->setBounds(0, 0, 10, 10);
				m_slider->setColour(Slider::textBoxTextColourId, Colours::white);
				
				
				addAndMakeVisible(m_slider.get());
				m_slider->addListener(this);
			}
			if (m_param->m_param_type == parameterType::String)
				
			{
				m_text_edit = std::make_unique<TextEditor>();
				m_text_edit->setBounds(0, 0, 10, 10);
				m_text_edit->setText(m_param->getValue().toString(), false);
				m_text_edit->addListener(this);
				addAndMakeVisible(m_text_edit.get());
				
			}
			if (m_param->m_param_type == parameterType::StringFile)
			{
				File initdir = File(g_propsfile->getValue("general/last_sound_import_dir"));
				m_filenamecomponent = std::make_unique<FilenameComponent>(
					"", initdir, true, false, false, "*.wav;*.aif;*.aiff;*.flac;*.ogg", "", "Select a file...");
				m_filenamecomponent->setRecentlyUsedFilenames(StringArray());
				m_filenamecomponent->setBounds(0, 0, 10, 10);
				m_filenamecomponent->addListener(this);
				addAndMakeVisible(m_filenamecomponent.get());
			}
		}
	}
	else
	{
		m_combo = std::make_unique<ComboBox>();
		int index = -1;
		for (int i = 0; i < m_param->m_choice_strings.size(); ++i)
		{
			m_combo->addItem(m_param->m_choice_strings[i], i + 1);
			if (m_param->getValue() == m_param->m_choice_values[i])
				index = i;
		}
		m_combo->setSelectedItemIndex(index,dontSendNotification);
		m_combo->setBounds(0, 0, 10, 10);
		m_combo->addListener(this);
		addAndMakeVisible(m_combo.get());
	}
	m_param_props_but = std::make_unique<TextButton>();
	m_param_props_but->setButtonText("...");
	m_param_props_but->setBounds(0, 0, 10, 10);
	addAndMakeVisible(m_param_props_but.get());
	if (m_filenamecomponent!=nullptr)
		m_param_props_but->setVisible(false);
	m_param_props_but->addListener(this);

	m_env_enable_but = std::make_unique<TextButton>();
	m_env_enable_but->setButtonText("Enab");
	m_env_enable_but->setBounds(0, 0, 10, 10);
	addAndMakeVisible(m_env_enable_but.get());
	m_param->addValueListener(this);
	m_param->m_max_value.addListener(this);
	if (m_param->m_can_be_automated == false)
		m_env_enable_but->setVisible(false);
	if (m_name_label->isVisible() == true)
		m_name_label->setTooltip(m_param->m_tip_text);
	else if (m_toggle_but != nullptr)
		m_toggle_but->setTooltip(m_param->m_tip_text);
}

parameter_component::~parameter_component()
{
    m_param->removeValueListener(this);
	m_param->m_max_value.removeListener(this);
	
}

void parameter_component::resized()
{
	m_param_props_but->setBounds(getWidth() - 75, 2, 30, 18);
	m_env_enable_but->setBounds(m_param_props_but->getRight() + 5, 2, 35, 18);
	if (m_custom_component != nullptr)
		m_custom_component->setBounds(m_name_label->getRight() + 5, 0, 
			m_param_props_but->getX() - m_name_label->getWidth() - 10, getHeight());
	if (m_toggle_but != nullptr)
	{
		m_toggle_but->setBounds(m_name_label->getRight() + 5, 0, 100 , getHeight());
	}
	
	if (m_slider != nullptr)
		m_slider->setBounds(m_name_label->getRight() + 5, 0, m_param_props_but->getX() - m_name_label->getWidth() - 10, getHeight());
	if (m_text_edit != nullptr)
		m_text_edit->setBounds(m_name_label->getRight() + 5, 0, m_param_props_but->getX() - m_name_label->getWidth() - 10, getHeight());
	if (m_filenamecomponent != nullptr)
		m_filenamecomponent->setBounds(m_name_label->getRight() + 5, 0, getWidth() - m_name_label->getWidth() - 10, getHeight());
	if (m_combo != nullptr)
		m_combo->setBounds(m_name_label->getRight() + 5, 0, m_param_props_but->getX() - m_name_label->getWidth() - 10, getHeight());
}

Image getSliderResponseImage(Slider* slid, int size)
{
    Image result(Image::RGB,size,size,false);
    Graphics g(result);
    g.fillAll(Colours::black);
    g.setColour(Colours::white);
    for (int i=0;i<size/2;++i)
    {
        double norm = 1.0/size*(i*2);
        double val = slid->proportionOfLengthToValue(norm);
        val = jmap(val,slid->getMinimum(),slid->getMaximum(),0.0,(double)size);
        norm = 1.0/size*(i*2+1);
        double val2 = slid->proportionOfLengthToValue(norm);
        val2 = jmap(val2,slid->getMinimum(),slid->getMaximum(),0.0,(double)size);
        g.drawLine((float)i*2, (float)(size-val), (float)i*2+1, (float)(size-val2), 1.25f);
    }
    
    return result;
}

int queryParameterRangeDlg(node_parameter* par, Slider* slid)
{
	node_parameter* m_param = par;
	AlertWindow dlg(CharPointer_UTF8("λ"), "Parameter range", AlertWindow::NoIcon, nullptr);
    dlg.addTextEditor("lowlim", m_param->m_min_value.getValue(), "Minimum");
	dlg.addTextEditor("hilim", m_param->m_max_value.getValue(), "Maximum");
	dlg.addButton("OK", 1);
	dlg.addButton("Cancel", 2);
	int result = dlg.runModalLoop();
	if (result == 1)
	{
		double minv = dlg.getTextEditorContents("lowlim").getDoubleValue();
		double maxv = dlg.getTextEditorContents("hilim").getDoubleValue();
		m_param->m_min_value = minv;
		m_param->m_max_value = maxv;
		m_param->m_custom_range_set = true;
	}
    return result;
}

node_parameter* g_last_edited_envelope_par = nullptr;

node_parameter* getLastEditedEnvelopeParameter(bool settonull)
{
	if (settonull == false)
		return g_last_edited_envelope_par;
	g_last_edited_envelope_par = nullptr;
	return nullptr;
}

void showEnvelopeCalloutBox(node_parameter* par, Component* parentComponent)
{
	if (par == nullptr)
		return;
	juce::Rectangle<int> area(5, 5, 10, 10);
	EnvelopeContainerComponent* comp = new EnvelopeContainerComponent;
	//comp->setSize(800, 400);
	String nametodisplay;
	if (par->m_parent_node == nullptr)
		nametodisplay = par->m_name;
	else nametodisplay = par->m_parent_node->get_name() + "/" + par->m_name;
	comp->m_env_comp->ValueFromNormalized = [par](double x)
	{ 
		if (g_par_helper_slider == nullptr)
			g_par_helper_slider = std::make_unique<Slider>();
		g_par_helper_slider->setRange(par->m_min_value.getValue(),
			par->m_max_value.getValue(), par->m_step_size);
		g_par_helper_slider->setSkewFactor(par->m_skew, par->m_sym_skew);
		if (par->m_midpoint_skew == true)
			g_par_helper_slider->setSkewFactorFromMidPoint(par->m_midpoint);
		return g_par_helper_slider->proportionOfLengthToValue(x);
	};
	comp->m_env_comp->TimeFromNormalized = [par](double x) { return par->m_env_time_scale*x; };
	comp->m_env_comp->set_envelope(par->get_envelope(), par->m_parent_node, nametodisplay);
	comp->m_env_comp->OnEnvelopeEdited = [par](breakpoint_envelope*)
    {
        ++par->m_envelope_change_count;
        if (par->m_parent_node && par->m_parent_node->m_component &&
            par->m_parent_node && par->m_parent_node->m_component->m_node_view)
        {
			if (par->m_parent_node->m_component->m_node_view->isAutoRenderEnabled()==true)
				par->m_parent_node->m_component->m_node_view->renderNodeDeferred(par->m_parent_node);
        }
    };
	comp->updateScriptText();
	par->m_automation_enabled = true;
	g_last_edited_envelope_par = par;
	CallOutBox::launchAsynchronously(comp, area, parentComponent);
}

void parameter_component::buttonClicked(Button * but)
{
	if (m_toggle_but != nullptr && m_toggle_but.get() == but)
	{
		m_param->setValue(but->getToggleState());
		renderParentNode();
	}
#ifdef DONT_USE_FILENAMECOMPONENT
	if (m_text_edit != nullptr && but != nullptr && but == m_param_props_but.get())
	{

		File file = queryAudioFile();
		if (file.existsAsFile())
		{
			m_param->m_value = file.getFullPathName();
			if (m_param->m_parent_node != nullptr)
			{
				Logger::writeToLog("visiting destinations");
				for (auto& dest : m_param->m_parent_node->m_destinations)
				{
					Logger::writeToLog("Visiting dest " + dest->get_name());
					//dest->InputFiles = m_param->m_value.toString();
					dest->sourcesHaveChanged();
					dest->sendActionMessage("infilechanged");
				}

			}
			sendChangeMessage();
		}
	}
#endif
	if (m_slider != nullptr && but != nullptr && but == m_param_props_but.get())
	{
		PopupMenu menu;
		menu.addItem(1, "Set parameter range...", true, false);
		if (m_param->m_can_be_automated == true)
		{
			menu.addItem(2, "Show and enable envelope", true, false);
			menu.addItem(3, "Automation enabled", true, m_param->m_automation_enabled);
		}
		PopupMenu masters_menu;
		PopupMenu slave_mode_menu;
		std::map<int, node_parameter*> master_params_map;
		if (m_par_linker != nullptr)
		{
			menu.addItem(4, "Set as master parameter", true, false);
			
			auto graph_nodes = m_param->m_parent_node->m_owner_graph->getNodes();
			int par_id = 1000;
			for (auto& nodeptr : graph_nodes)
			{
				if (nodeptr.get() == m_param->m_parent_node)
					continue;
				auto& params = nodeptr->get_parameters();
				for (auto& par : params)
				{
					masters_menu.addItem(par_id, nodeptr->getShortName()+"/"+par->m_name, true, false);
					master_params_map[par_id] = par.get();
					++par_id;
				}
			}
			menu.addSubMenu("Set as slave to parameter", masters_menu, true);
			slave_mode_menu.addItem(100000,"Linear",true,false);
			slave_mode_menu.addItem(100001, "Inverse", true, false);
			
            //slave_mode_menu.addCustomItem(100002, m_parlinkscript_edit.get(), 250, 25, false);
            slave_mode_menu.addItem(100002, "Custom formula...", true, false);
			menu.addSubMenu("Slave mapping mode", slave_mode_menu, true);
		}
		int result = menu.show();
		if (result >= 1000)
		{
			node_parameter* master_par = master_params_map[result];
			if (result<100000)
				m_par_linker->addSlaveParameter(master_par, m_param.get());
			if (result == 100000)
				m_par_linker->setSlaveMappingScript(master_par, m_param.get(), "return x");
			if (result == 100001)
				m_par_linker->setSlaveMappingScript(master_par, m_param.get(), "return 1.0-x");
			if (result == 100002)
            {
                m_parlinkscript_edit = new TextEditor;
                m_parlinkscript_edit->setSize(300, 25);
                CallOutBox& cb = CallOutBox::launchAsynchronously(m_parlinkscript_edit,
                                                                  m_param_props_but->getBoundsInParent(), getParentComponent());
                //m_par_linker->setSlaveMappingScript(master_par, m_param.get(), "return 0.5+0.5*math.sin(2*3.141593*x)");
            }
			return;
		}
		
		if (result == 1)
		{
			queryParameterRangeDlg(m_param.get(),m_slider.get());
		}
		if (result == 2)
		{
			showEnvelopeCalloutBox(m_param.get(), getTopLevelComponent());
		}
		if (result == 3)
		{
			m_param->m_automation_enabled = !m_param->m_automation_enabled;
			m_param->m_envelope_change_count++;
		}
	}
}

void parameter_component::sliderValueChanged(Slider * slid)
{
	if (slid != nullptr && slid == m_slider.get())
	{
		m_param->setValue(slid->getValue());
		renderParentNode();
	}
}

void parameter_component::sliderDragEnded(Slider * slid)
{
	if (slid == m_slider.get() && getNodeView()!=nullptr)
	{
		getNodeView()->addUndoState("Parameter changed");
	}
}

NodeView* parameter_component::getNodeView()
{
	if (m_param->m_parent_node==nullptr)
		return nullptr;
	if (m_param->m_parent_node->m_component == nullptr)
		return nullptr;
	return m_param->m_parent_node->m_component->m_node_view;
}

void parameter_component::textEditorTextChanged(TextEditor & ed)
{
	if (&ed == m_text_edit.get())
	{
		m_param->setValue(ed.getText());
		renderParentNode();
	}
    if (&ed == m_parlinkscript_edit && m_par_linker!=nullptr)
    {
        
    }
}

void parameter_component::comboBoxChanged(ComboBox * cb)
{
	if (cb != nullptr && cb == m_combo.get())
	{
		if (cb->getSelectedItemIndex() >= 0 && cb->getSelectedItemIndex() < m_param->m_choice_values.size())
		{
			m_param->setValue(m_param->m_choice_values[cb->getSelectedItemIndex()]);
			renderParentNode();
		}
	}
}

void parameter_component::valueChanged(Value & v)
{
	if (m_toggle_but != nullptr)
	{
		if (m_param->refersToSameSourceAs(v))
			m_toggle_but->setToggleState(v.getValue(), dontSendNotification);
	}
	if (m_slider != nullptr)
	{
		if (m_param->refersToSameSourceAs(v))
			m_slider->setValue(m_param->getValue(), dontSendNotification);
		if (v.refersToSameSourceAs(m_param->m_min_value) == true)
			m_slider->setRange(v.getValue(), m_param->m_max_value.getValue(), m_param->m_step_size);
		if (v.refersToSameSourceAs(m_param->m_max_value) == true)
			m_slider->setRange(m_param->m_min_value.getValue(), v.getValue(), m_param->m_step_size);
	}
	if (m_text_edit != nullptr)
	{
		if (m_param->refersToSameSourceAs(v))
			m_text_edit->setText(v.toString(), false);
	}
	if (m_combo != nullptr)
	{
		if (m_param->refersToSameSourceAs(v))
		{
			int index = -1;
			for (int i = 0; i < m_param->m_choice_values.size(); ++i)
				if (m_param->m_choice_values[i] == v.getValue())
				{
					index = i;
					break;
				}
			if (index >= 0)
				m_combo->setSelectedItemIndex(index, dontSendNotification);
		}
	}
}

void parameter_component::filenameComponentChanged(FilenameComponent * fileComponentThatHasChanged)
{
	File file = fileComponentThatHasChanged->getCurrentFile();
	if (file.existsAsFile() == false)
		return;
	String fn = file.getFullPathName();
	g_propsfile->setValue("general/last_sound_import_dir", file.getParentDirectory().getFullPathName());
	m_param->setValue(fn);
	if (m_param->m_parent_node != nullptr)
	{
		for (process_node* dest: m_param->m_parent_node->m_destinations)
		{
			//Logger::writeToLog("Visiting dest " + dest->get_name());
			//dest->InputFiles = m_param->m_value.toString();
			dest->sourcesHaveChanged();
			dest->sendActionMessageTrampoline("infilechanged");
		}
		m_param->m_parent_node->m_component->set_audio_file(fn);
		m_param->m_parent_node->m_component->repaint();
        m_param->m_parent_node->m_component->grabKeyboardFocus();
		renderParentNode();
		getNodeView()->addUndoState("File changed");
	}
	sendChangeMessage();
}

void parameter_component::renderParentNode()
{
	if (m_param->m_parent_node != nullptr && m_param->m_parent_node->m_component != nullptr)
	{
		NodeView* view = m_param->m_parent_node->m_component->m_node_view;
		if (view != nullptr && view->isAutoRenderEnabled()==true)
		{
			view->renderNodeDeferred(m_param->m_parent_node);
		}
	}
}
