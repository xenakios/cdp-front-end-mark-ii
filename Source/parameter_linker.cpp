#include "parameter_linker.h"

ParameterLinker::ParameterLinker()
{
	m_lua.open_libraries(sol::lib::base, sol::lib::math);
}

void ParameterLinker::addMasterParameter(node_parameter * par)
{
	auto entry = std::make_shared<parameter_master_t>();
	entry->m_par = par;
	m_links.push_back(entry);
}

void ParameterLinker::addSlaveParameter(node_parameter * master, node_parameter * slave, String mappingcode)
{
	int master_index = findMaster(master);
	if (master_index == -1)
	{
		addMasterParameter(master);
		master_index = m_links.size() - 1;
	}
	if (master_index >= 0)
	{
		auto entry = std::make_shared<parameter_slave_t>(mappingcode);
		entry->m_par = slave;
		entry->m_master_par = master;
		entry->m_lua = &m_lua;
        master->addValueListener(entry.get());
		m_links[master_index]->m_slave_pars.push_back(entry);
	}
}

void ParameterLinker::setSlaveMappingScript(node_parameter * master, node_parameter * slave, String txt)
{
	for (auto& itmaster : m_links)
	{
		for (auto& itslave : itmaster->m_slave_pars)
		{
			if (itslave->m_par == slave)
			{
				itslave->setMappingScript(txt);
				return;
			}
		}
	}
}

int ParameterLinker::findMaster(node_parameter* par)
{
	for (int i = 0; i < m_links.size(); ++i)
		if (m_links[i]->m_par == par)
			return i;
	return -1;
}

void ParameterLinker::parameter_slave_t::valueChanged(Value & v)
{
	if (m_lua == nullptr)
		return;
	double master_v = v.getValue();
	auto master_helper_range = make_range_from_parameter(m_master_par);
	auto slave_helper_range = make_range_from_parameter(m_par);
	try
	{
		m_lua->set("x", master_helper_range.convertTo0to1(master_v));
		sol::function_result r = m_lua->script(m_script.toStdString());
		if (r.valid() == true)
		{
			double slave_v = r.get<double>();
			m_par->setValue(slave_helper_range.convertFrom0to1(slave_v));
		}
	}
	catch (std::exception& excep)
	{
		Logger::writeToLog(excep.what());
	}
}
