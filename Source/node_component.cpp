
#include "node_component.h"
#include "node_view.h"
#include "node_factory.h"
#include "jcdp_audio_playback.h"
#include "plugin_gui.h"
#include "plugin_process_node.h"
#include "envelope_component.h"
#include "waveform_component.h"
#include "mixersequencer_node.h"
#include "preset_component.h"
#include <map>
#include "jcdp_utilities.h"

extern std::unique_ptr<AudioThumbnailCache> g_thumb_cache;
extern std::unique_ptr<AudioFormatManager> g_format_manager;
extern std::unique_ptr<PropertiesFile> g_propsfile;

NodeComponent::NodeComponent(std::shared_ptr<process_node> node, NodeView* view) :
	m_node(node), m_node_view(view), m_layout(this,true)
{
	setWantsKeyboardFocus(true);
	m_node->m_component = this;
	
	if (node->get_output_type() == "wav" || node->get_output_type()=="ana")
	{
		m_wavecomponent = std::make_unique<WaveformComponentBase>(false);
		addAndMakeVisible(m_wavecomponent.get());
		m_wavecomponent->OnClicked = [this](double tpos)
		{
			m_node->seek(tpos);
			m_was_seeked = true;
		};
	}
		
	m_corner_resizer = std::make_unique<ResizableCornerComponent2>(this, nullptr);
	m_corner_resizer->OnResizedCallback = [this](int, int)
	{
		if (m_node_view != nullptr)
		{
			m_node->setDirty(true);
			m_node_view->addUndoState("Resize node");
		}
	};

	m_params_inspector = std::make_unique<node_inspector>(true);
	m_params_inspector->setNodes({ m_node });
	m_params_inspector->set_show_procname(false);
	m_params_inspector->addChangeListener(this);
	addAndMakeVisible(m_params_inspector.get());
	
	addAndMakeVisible(&m_name_label);
	m_name_label.setInterceptsMouseClicks(false,false);
	m_name_label.setColour(Label::textColourId, Colours::white);
	addAndMakeVisible(&m_status_label);
	m_status_label.setColour(Label::textColourId, Colours::white);
	m_layout.addComponent(&m_name_label, 20.0f, 20.0f, 20.0f, false);
	if (m_wavecomponent!=nullptr)
		m_layout.addComponent(m_wavecomponent.get(), -0.01f, -0.99f, -0.2f, true);
	m_layout.addComponent(m_params_inspector.get(), -0.01f, -0.99f, -0.2f, false);
	m_layout.addComponent(&m_status_label, 20.0f, 20.0f, 20.0f, false);
	addAndMakeVisible(m_corner_resizer.get());
}

NodeComponent::~NodeComponent()
{
	
}

void NodeComponent::resized()
{
    m_corner_resizer->setBounds(getWidth() - 15, getHeight() - 20, 20, 20);
	m_layout.performLayout(3, 0, getWidth() - 6, getHeight());
	if (getParentComponent() != nullptr)
        getParentComponent()->repaint();
}

void NodeComponent::changeListenerCallback(ChangeBroadcaster *)
{
    repaint();
}

void NodeComponent::set_audio_file(String fn, bool /*forcethumbupdate*/)
{
	/*
	if (fn.isEmpty()==true)
	{
        Logger::writeToLog("Removing thumb of "+m_wave_fn);
        m_wave_fn = "";
		if (m_thumb!=nullptr)
            g_thumb_cache->removeThumb(m_thumb->getHashCode());
		return;
	}
	*/
	//Logger::writeToLog("NodeComponent::set_audio_file " + fn);
	if (fn == m_wave_fn)
		return;
	if (fn.endsWith(".wav") == true || fn.endsWith(".ana") == true)
	{
		m_wave_fn = fn;
		if (m_wavecomponent != nullptr)
			m_wavecomponent->setAudioFile(fn);
	}
}

void NodeComponent::paintOverChildren(Graphics& g)
{
    if (m_node->m_topo_render_state == TopoRenderState::Rendering ||
		m_node->get_processing_state() == process_node::Busy)
    {
        double progress = m_node->renderProgress();
        if (progress<0.0)
        {
            float offset = cos(2*3.141593*m_anim_phase);
            float progress_xcor = getWidth()/2.0+offset*getWidth()/2.0;
            g.setColour(Colours::black);
            g.fillRect(0.0f,0.0f,(float)getWidth(),25.0f);
			g.setColour(Colours::lightseagreen);
            g.fillRect(progress_xcor,0.0f,10.0f,25.0f);
            m_anim_phase+=0.01;
        }
        if (progress>=0.0)
        {
            float progress_xcor = getWidth()*progress;
            g.setColour(Colours::lightseagreen);
            g.fillRect(0.0f,0.0f,progress_xcor,25.0f);
            g.setColour(Colours::black);
            g.fillRect(progress_xcor,0.0f,(float)getWidth()-progress_xcor,25.0f);
        } 
    }
}

void NodeComponent::paint(Graphics & g)
{
    if (m_node->m_topo_render_state != TopoRenderState::Rendering)
    //if (m_node->get_processing_state() != process_node::Busy)
    {
        
		if (m_is_close_to_node == false)
		{
			if (m_gui_status == NodeComponentStatus::NotSelected)
				g.fillAll(Colours::darkgrey);
			if (m_gui_status == NodeComponentStatus::Selected) 
				g.fillAll(Colours::darkslategrey);
			if (m_gui_status == NodeComponentStatus::Targetted)
				g.fillAll(Colours::darkgreen);
		}
            else g.fillAll(Colours::white);
    }
    else
    {
        
        if (m_node->get_processing_state() == process_node::FinishedWithError)
            g.fillAll(Colours::red);
        /*
        if (m_node->m_topo_render_state == TopoRenderState::Rendering)
        {
            double progress = m_node->renderProgress();
            if (progress<0.0)
            {
                float offset = cos(2*3.141593*m_anim_phase);
                float progress_xcor = getWidth()/2.0+offset*getWidth()/2.0;
                g.fillAll(Colours::black);
                Image trevor = ImageCache::getFromFile(File("/Users/teemu/Downloads/28112016/trevorcropped.jpg"));
                g.drawImage(trevor, progress_xcor, 0, 50, 50, 0, 0, 170, 192);
                //g.setColour(Colours::lightseagreen);
                //g.fillRect(progress_xcor,0.0f,10.0f,(float)getHeight());
                m_anim_phase+=0.03;
            }
            if (progress>=0.0)
            {
                float progress_xcor = getWidth()*progress;
                g.setColour(Colours::lightseagreen);
                g.fillRect(0.0f,0.0f,progress_xcor,(float)getHeight());
                g.setColour(Colours::black);
                g.fillRect(progress_xcor,0.0f,(float)getWidth()-progress_xcor,(float)getHeight());
            } //else
              //  g.fillAll(Colours::yellow);
        }
         */
        
        
    }
	String name_string = m_node->get_name();
	if (m_node->get_bypass_mode() == process_node::Bypassed)
		name_string.append(" (bypassed)", 20);
	if (m_node->isDirty() == true)
		name_string = "*" + name_string;
	m_name_label.setText(name_string, dontSendNotification);
	m_status_label.setText(String(m_node->get_render_elapsed_milliseconds() / 1000.0, 2) + " secs elapsed",dontSendNotification);
}

void NodeComponent::mouseDown(const MouseEvent & e)
{
	int wave_h = (int)((getHeight() - 60)*0.25);
	if (m_node->get_parameters().size() == 0 && m_node->getCustomEditorType2() == process_node::NoCustomEditor)
		wave_h = getHeight() - 60;
	/*
	if (m_thumb!=nullptr && e.mods.isLeftButtonDown() && e.y >= 40 && e.y < 40+wave_h)
	{
		double seekpos = m_thumb->getTotalLength() / getWidth()*e.x;
		m_node->seek(seekpos);
		m_was_seeked = true;
		return;
	}
	*/
	m_original_pos = getPosition();
    if (NodeManipulatedFunc && e.mods.isRightButtonDown() == false)
        NodeManipulatedFunc(m_node, MouseAction::MouseDown, 0, 0);
    if (e.mods.isRightButtonDown() == true)
    {
        NodeMenuRequested(m_node);
    }
}

void NodeComponent::mouseUp(const MouseEvent &ev)
{
	if (m_was_seeked == false)
	{
		if (NodeManipulatedFunc)
            NodeManipulatedFunc(m_node, MouseAction::MouseUp, 0, 0);
		if (m_node_view != nullptr && ev.getDistanceFromDragStart() > 0)
		{
			m_node->setDirty(true);
			m_node_view->addUndoState("Move node");
		}
	}
	else
	{
		m_was_seeked = false;
	}
}

void NodeComponent::set_playcurpos(double x)
{
	if (m_wavecomponent!=nullptr)
		m_wavecomponent->setPlayCursorPosition(x);
}

void NodeComponent::mouseDrag(const MouseEvent & e)
{
	if (m_was_seeked == true)
		return;
	Point<int> originalPos = m_original_pos;// getLocalPoint(getParentComponent(), getPosition());
    //Point<int> pos(originalPos + Point<int>(e.getDistanceFromDragStartX(), e.getDistanceFromDragStartY()));
    
    if (getParentComponent() != nullptr)
    {
        int x_delta = e.getDistanceFromDragStartX();
        int y_delta = e.getDistanceFromDragStartY();
		if (NodeManipulatedFunc)
		{
			if (e.mods.isCommandDown() == false)
                NodeManipulatedFunc(m_node, MouseAction::Drag, x_delta, y_delta);
			if (e.mods.isCommandDown() == true)
                NodeManipulatedFunc(m_node, MouseAction::DragWithControlModifier, x_delta, y_delta);
		}
    }
}

void NodeComponent::setGUIStatus(NodeComponentStatus status) 
{ 
	m_gui_status = status; 
	repaint(); 
}

bool NodeComponent::isSelected() const
{
	if (m_gui_status == NodeComponentStatus::Selected ||
		m_gui_status == NodeComponentStatus::Targetted)
		return true;
	return false;
}

void NodeComponent::update_params_inspector() { m_params_inspector->build_props_panel(); }

void NodeComponent::updateConnectionsList() 
{ 
	m_params_inspector->updateConnectionsList(); 

}

void NodeComponent::showBubbleMessage(String msg, bool is_error, int timeout)
{
	BubbleMessageComponent* bub = new BubbleMessageComponent;
	AttributedString attstring(msg);
	if (is_error == true)
	{
		attstring.setFont(18.0f);
		attstring.setColour(Colours::white);
		bub->setColour(BubbleComponent::backgroundColourId, Colours::crimson);
	}
	getTopLevelComponent()->addChildComponent(bub);
	bub->showAt(this, attstring, timeout, true, true);
}

ParameterManager::ParameterManager(std::vector<std::shared_ptr<process_node>>* nodes) :
    m_nodes(nodes)
{
    updateModel();
    m_listbox.setModel(this);
    addAndMakeVisible(&m_listbox);
    m_listbox.setBounds(0,0,500,700);
    setSize(500, 700);
}

class WaveFormComponent : public CustomParameterComponent, 
	public ChangeListener, public Timer
{
public:
	WaveFormComponent(node_parameter* par) : m_par(par)
	{
		startTimer(1000);
        addAndMakeVisible(&m_zoomscr);
        m_zoomscr.RangeChanged=[this](double t0, double t1)
        {
            m_view_start = t0;
            m_view_end = t1;
            repaint();
        };
		setOpaque(true);
	}
	~WaveFormComponent()
	{
		delete m_thumb;
	}
    int getDefaultHeight() override { return 45; }
	void timerCallback() override
	{
		MD5 h = m_par->m_parent_node->get_state_hash();
		if (h != m_hash)
		{
			m_hash = h;
			initThumb();
		}
	}
	void paint(Graphics& g) override
	{
		g.fillAll(Colours::black);
		g.setColour(Colours::white);
		if (m_thumb != nullptr)
		{
			juce::Rectangle<int> rect(0, 0, getWidth(), getHeight()-12);
			if (m_thumb != nullptr)
			{
                double starttimesecs = m_thumb->getTotalLength()*m_view_start;
                double endtimesecs = m_thumb->getTotalLength()*m_view_end;

				m_thumb->drawChannels(g, rect, starttimesecs, endtimesecs, 1.0f);
				g.setColour(Colours::red);
                float xc = (float)jmap(m_pos,m_view_start,m_view_end,0.0,(double)getWidth());
                g.drawLine(xc, 0.0f, xc, (float)getHeight(), 2.0f);
			}
		}
	}
	void changeListenerCallback(ChangeBroadcaster*) override
	{
		repaint();
	}
	void mouseDrag(const MouseEvent& ev) override
	{
        if (m_thumb == nullptr)
            return;
        m_pos = jmap((double)ev.x,0.0,(double)getWidth(),m_view_start,m_view_end);
		m_pos = jlimit(0.0, 1.0, m_pos);
		m_par->setValue(m_thumb->getTotalLength()*m_pos);
		repaint();
	}
	
	void mouseDown(const MouseEvent&) override
	{
    }

    void resized() override
    {
        m_zoomscr.setBounds(0,getHeight()-11,getWidth(),11);
    }
private:
	node_parameter* m_par = nullptr;
	AudioThumbnail* m_thumb = nullptr;
	FileInputSource* m_thumb_source = nullptr;
	String m_fn;
	MD5 m_hash;
	double m_pos = 0.0;
    double m_view_start = 0.0;
    double m_view_end = 1.0;
	
	void initThumb()
	{
		String fn = m_par->m_parent_node->getSourceFileName(0, 0);
		if (fn.isEmpty() == false)
		//if (m_par->m_parent_node->m_sources.size() > 0 &&
		//	m_par->m_parent_node->m_sources[0]->get_output_filenames().size() > 0)
		{
			//String fn = m_par->m_parent_node->m_sources[0]->get_output_filenames()[0];
			m_fn = fn;
			File thumbfile(fn);
			m_thumb_source = new FileInputSource(thumbfile);
			delete m_thumb;
			m_thumb = new AudioThumbnail(64, *g_format_manager, *g_thumb_cache);
			m_thumb->setSource(m_thumb_source);
			m_thumb->addChangeListener(this);
			repaint();
		}
		
	}
    zoom_scrollbar m_zoomscr;
};

class RoutingMatrixComponent : public CustomParameterComponent, 
	public ActionListener,
	public Timer
{
public:
    RoutingMatrixComponent(node_parameter* par) : m_par(par)
    {
		int64 temp = m_par->m_parent_node->get_dynamic_properties()["routingmatrixptr"];
        m_rm = (routing_matrix*)temp;
		m_par->m_parent_node->addActionListener(this);
		//m_par->m_parent_node->get_parameters()[2].m_value.addListener(this);
		startTimer(1000);
    }
	~RoutingMatrixComponent()
	{
		m_par->m_parent_node->removeActionListener(this);
	}
	void timerCallback() override
	{
		repaint();
	}
	
	void actionListenerCallback(const String& msg) override
	{
		repaint();
	}
	void paint(Graphics& g) override
    {
        g.fillAll(Colours::black);
        g.setColour(Colours::white);
        if (m_rm!=nullptr)
        {
            int numins = m_rm->getMaxNumInputs();
            int numouts = m_rm->getMaxNumOutputs();
            int gridsize = m_gridsize;
			for (int i = 0; i < numins + 1; ++i)
			{
				int xcor = i*gridsize;
				g.drawLine(xcor, 0, xcor, gridsize*numouts);
			}
			for (int i = 0; i < numouts+1; ++i)
			{
				int ycor = i*gridsize;
				g.drawLine(0, ycor, gridsize*numins, ycor);
			}
			
			for (int i=0;i<numins;++i)
            {
				for (int j=0;j<numouts;++j)
                {
					int xcor = i*gridsize;
                    int ycor = j*gridsize;
					if (m_rm->is_connected(i, j)==connection_state::Connected)
                        g.fillRect(xcor+1,ycor+1,gridsize-2,gridsize-2);
                }
            }
        }
    }
    void mouseDown(const MouseEvent& ev) override
    {
		if (ev.mods.isRightButtonDown() == true)
		{
			PopupMenu popmenu;
			popmenu.addItem(1, "Reset to default", true, false);
			popmenu.addItem(2, "Reset fully", true, false);
			int r = popmenu.show();
			if (r == 1)
				m_rm->initDefault(m_rm->getMaxNumInputs(), m_rm->getMaxNumOutputs());
			if (r == 2)
				m_rm->clear_all();
			repaint();
			return;
		}
		int row = ev.y/m_gridsize;
        int col = ev.x/m_gridsize;
        if (col>=0 && col<m_rm->getMaxNumInputs() &&
            row>=0 && row<m_rm->getMaxNumOutputs())
        {
            if (m_rm->is_connected(col, row)==connection_state::Connected)
                m_rm->set_connection(col, row, false);
            else m_rm->set_connection(col, row, true);
            m_rm->m_was_manually_edited = true;
        }
        repaint();
    }
    int getDefaultHeight() override { return 200; }
private:
    node_parameter* m_par = nullptr;
    routing_matrix* m_rm = nullptr;
    int m_gridsize = 16;
};

ControlFactory* g_control_factory = nullptr;

ControlFactory * ControlFactory::getInstance(bool destroy)
{
	if (destroy == true)
	{
		delete g_control_factory;
		return nullptr;
	}
	if (g_control_factory == nullptr)
		g_control_factory = new ControlFactory;
	return g_control_factory;
}

std::unique_ptr<CustomParameterComponent> ControlFactory::createFromParameter(node_parameter* par)
{
	if (par->m_control_type == "msattoffs")
	{
		WaveFormComponent* ed = new WaveFormComponent(par);
		return std::unique_ptr<CustomParameterComponent>(ed);
	}
    if (par->m_control_type == "routingmatrix")
    {
        return std::make_unique<RoutingMatrixComponent>(par);
    }
    return nullptr; //std::unique_ptr<CustomParameterComponent>();
}

TextFileParameterComponent::TextFileParameterComponent(node_parameter * par)
	: m_par(par)
{
	addAndMakeVisible(&m_text_edit);
	m_text_edit.setMultiLine(true);
	m_text_edit.setReturnKeyStartsNewLine(true);
	m_text_edit.addListener(this);
	//addChildComponent(&m_bubble);
}

void TextFileParameterComponent::resized()
{
	m_text_edit.setBounds(0, 0, getWidth(), getHeight());
}

void TextFileParameterComponent::timerCallback()
{
	stopTimer();
	BubbleMessageComponent* bub = new BubbleMessageComponent;
	getTopLevelComponent()->addChildComponent(bub);
	bub->showAt(this, AttributedString(m_text_edit.getText()), 5000, true, true);
}

void TextFileParameterComponent::textEditorTextChanged(TextEditor & ed)
{
	stopTimer();
	startTimer(500);
}

String TextFileParameterComponent::generateTextFile()
{
	return String();
}

DragTestComponent::DragTestComponent(NodeView * view) :
	m_corner_comp(this, &m_constrain), m_view(view)
{
	addAndMakeVisible(&m_corner_comp);
	m_constrain.setSizeLimits(30, 30, 4096, 4096);
}

void DragTestComponent::mouseDown(const MouseEvent & ev)
{
	this->toFront(true);
	m_has_done_duplicate = false;
	if (m_dragger)
		m_dragger->startDraggingComponent(this, ev);
}

void DragTestComponent::mouseDrag(const MouseEvent& ev)
{
	if (m_dragger)
	{
		if (ev.mods.isCommandDown() == false)
			m_dragger->dragComponent(this, ev, nullptr);
		if (ev.getDistanceFromDragStart()>10 && ev.mods.isCommandDown() == true)
		{
			if (m_has_done_duplicate == false)
			{
				auto dupl = std::make_shared<DragTestComponent>(m_view);
				dupl->m_dragger = m_dragger;
				dupl->m_imgfn = m_imgfn;
				dupl->setBounds(ev.x, ev.y, getWidth(), getHeight());
				dupl->m_view = m_view;
				m_view->addAndMakeVisible(dupl.get());
				m_view->m_drag_tests.push_back(dupl);
				m_dragger->dragComponent(dupl.get(), ev, nullptr);
				m_has_done_duplicate = true;
				dupl->m_has_done_duplicate = true;
			}
			else
				m_dragger->dragComponent(this, ev, nullptr);
		}

	}
}

void DragTestComponent::paint(Graphics & g)
{
	Image trevor = ImageCache::getFromFile(File(m_imgfn));
	if (trevor.isValid() == true)
	{
		int img_w = trevor.getWidth();
		int img_h = trevor.getHeight();
		g.drawImage(trevor, 0, 0, getWidth(), getHeight(), 0, 0, img_w, img_h);
	}
	else
	{
		g.fillAll(Colours::lightgrey);
		g.drawText("No image", 0, 0, getWidth(), getHeight(), Justification::centred);
	}
}

void DragTestComponent::resized()
{
	m_corner_comp.setBounds(getWidth() - 20, getHeight() - 20, 20, 20);
}
