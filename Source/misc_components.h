#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

template<typename Comp>
class LabelledComponent : public Component
{
public:
	template<typename... Args>
	LabelledComponent(String labelText, int labelWidth, Args&&... args) : m_labw(labelWidth)
	{
		addAndMakeVisible(&m_label);
		m_label.setText(labelText, dontSendNotification);
		m_comp = std::make_unique<Comp>(std::forward<Args>(args)...);
		addAndMakeVisible(m_comp.get());
	}
	~LabelledComponent()
	{

	}
	Comp* get() { return m_comp.get(); }
	void resized() override
	{
		m_label.setBounds(0, 0, m_labw, 25);
		m_comp->setBounds(m_labw + 5, 0, getWidth() - m_labw - 5, getHeight());
	}
private:
	std::unique_ptr<Comp> m_comp;
	Label m_label;
	int m_labw = 0;
};

class SplitterLayout
{
public:
	SplitterLayout(Component* parent, bool isvertical) : m_parent_component(parent), m_is_vertical(isvertical)
	{

	}
	void addComponent(Component* comp, double minsize, double maxsize, double preferredsize, bool addresizer);
	void performLayout(int x, int y, int w, int h);
	ValueTree saveState();
	void restoreState(ValueTree state);
private:
	Component* m_parent_component = nullptr;
	bool m_is_vertical = false;
	StretchableLayoutManager m_layout;
	std::vector<std::shared_ptr<StretchableLayoutResizerBar>> m_resizers;
	std::vector<Component*> m_components;
};

class ResizableCornerComponent2 : public ResizableCornerComponent
{
public:
	ResizableCornerComponent2(Component* componentToResize,
		ComponentBoundsConstrainer* constrainer) : ResizableCornerComponent(componentToResize, constrainer)
	{
		OnResizedCallback = [](int, int) {};
	}
	std::function<void(int, int)> OnResizedCallback;
	void mouseUp(const MouseEvent& ev) override
	{
		ResizableCornerComponent::mouseUp(ev);
		OnResizedCallback(0, 0);
	}
private:

};

