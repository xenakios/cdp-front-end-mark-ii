#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "jcdp_utilities.h"
#include "misc_components.h"


class process_node;

class PluginWindowContent : public Component, public Timer
{
public:
	PluginWindowContent(Component* comp);
	~PluginWindowContent()
	{
		delete m_plugin_editor;
	}
	void timerCallback() override;
	Component* m_plugin_editor = nullptr;
	TextButton m_menu_but;
	LabelledComponent<Slider> m_tail_slider;
	LabelledComponent<ComboBox> m_num_inputs_combo;
	LabelledComponent<ComboBox> m_num_outputs_combo;
	void resized() override;
private:
	int m_plug_w = 0;
	int m_plug_h = 0;
    FlexBox m_flex1;
};

/** A desktop window containing a plugin's UI. */
class PluginWindow : public DocumentWindow, public Slider::Listener, public Timer,
	public ComboBox::Listener
{
public:
	enum WindowFormatType
	{
		Normal = 0,
		Generic,
		Programs,
		Parameters,
		NumTypes
	};

	PluginWindow(Component* pluginEditor, process_node*, WindowFormatType);
	~PluginWindow();

	static PluginWindow* getWindowFor(process_node* node);

	static void closeCurrentlyOpenWindowsFor(process_node* node);
	static void closeAllCurrentlyOpenWindows();

	void moved() override;
	void closeButtonPressed() override;
	void sliderValueChanged(Slider* slid) override;
	void timerCallback() override;
	void comboBoxChanged(ComboBox* c) override;
	void resized() override;
	/*
	int getDesktopWindowStyleFlags() const override
	{
		return ComponentPeer::windowHasCloseButton | ComponentPeer::windowHasTitleBar | ComponentPeer::windowIsResizable | ComponentPeer::windowHasMinimiseButton;
	}
	*/
private:
	process_node* m_owner = nullptr;
	WindowFormatType type;
	PluginWindowContent* m_content_component = nullptr;
	float getDesktopScaleFactor() const override { return 1.0f; }
	bool m_inited = false;
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(PluginWindow)
};

inline String toString(PluginWindow::WindowFormatType type)
{
	switch (type)
	{
	case PluginWindow::Normal:     return "Normal";
	case PluginWindow::Generic:    return "Generic";
	case PluginWindow::Programs:   return "Programs";
	case PluginWindow::Parameters: return "Parameters";
	default:                       return String();
	}
}

inline String getLastXProp(PluginWindow::WindowFormatType type) { return "uiLastX_" + toString(type); }
inline String getLastYProp(PluginWindow::WindowFormatType type) { return "uiLastY_" + toString(type); }
inline String getOpenProp(PluginWindow::WindowFormatType type) { return "uiopen_" + toString(type); }

