/*
This file is part of CDP Front-end.

CDP front-end is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

CDP front-end is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CDP front-end.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JCDP_AUDIO_PLAYBACK_H
#define JCDP_AUDIO_PLAYBACK_H

//#define BUILD_REAPER_PLUGIN

#include <atomic>
#include <memory>
#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"
#include "jcdp_utilities.h"

#ifdef BUILD_REAPER_PLUGIN
#include "Source/reaper_plugin.h"
#endif

#ifndef WIN32
#include <pthread.h>
#endif



#ifdef BUILD_REAPER_PLUGIN
#ifdef WIN32 

class jcdp_mutex
{
public:
    jcdp_mutex(preview_register_t* preg) : m_preg(preg)
    {
        InitializeCriticalSection(&m_preg->cs);
    }
    ~jcdp_mutex()
    {
        DeleteCriticalSection(&m_preg->cs);
    }
    void lock()
    {
        EnterCriticalSection(&m_preg->cs);
    }
    void unlock()
    {
        LeaveCriticalSection(&m_preg->cs);
    }
    preview_register_t* m_preg=nullptr;
};
#else
class jcdp_mutex
{
public:
    jcdp_mutex(preview_register_t* preg) : m_preg(preg)
    {
        pthread_mutexattr_t mta;
        pthread_mutexattr_init(&mta);
        pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&m_preg->mutex, &mta);
    }
    ~jcdp_mutex()
    {
        pthread_mutex_destroy(&m_preg->mutex);
    }
    void lock()
    {
        pthread_mutex_lock(&m_preg->mutex);
    }
    void unlock()
    {
        pthread_mutex_unlock(&m_preg->mutex);
    }
    preview_register_t* m_preg=nullptr;
};
#endif
#endif

class process_node;

class IJCDPreviewPlayback
{
public:
    virtual ~IJCDPreviewPlayback() {}
    virtual bool is_playing()=0;
    virtual void start()=0;
    virtual void stop()=0;
    virtual void seek(double)=0;
    virtual double get_position()=0;
	
	virtual void set_node(node_ref) = 0;
	virtual node_ref getNode() = 0;
	virtual void set_volume(double gain)=0;
	virtual bool is_looped() = 0;
	virtual void set_looped(bool b) = 0;
	std::function<void(void)> OnFileEnd;
	virtual AudioDeviceManager* get_audiodevicemanager() = 0;
};

#ifdef BUILD_REAPER_PLUGIN
class reaper_audio_preview : public IJCDPreviewPlayback
{
public:
    reaper_audio_preview();
    ~reaper_audio_preview();
    bool is_playing();
    void start();
    void stop();
    void seek(double);
    double get_position();
    void set_audio_file(String);
    void set_volume(double gain);
	bool is_looped() { return m_looped; }
	void set_looped(bool b) 
	{ 
		m_mutex.lock();
		m_looped = b;
		m_prev_reg.loop = b;
		m_mutex.unlock();
	}
	
private:
	bool m_looped = true;
	PCM_source* m_src=nullptr;
    preview_register_t m_prev_reg;
    jcdp_mutex m_mutex;
    std::atomic<bool> m_is_playing={false};
    String m_filename;
};
#endif

class juce_audio_preview : public IJCDPreviewPlayback, public AudioIODeviceCallback
{
public:
    juce_audio_preview();
    ~juce_audio_preview();
    void audioDeviceIOCallback(const float** inputChannelData,
                               int numInputChannels,
                               float** outputChannelData,
                               int numOutputChannels,
                               int numSamples);
	void audioDeviceAboutToStart(AudioIODevice*) override;
    void audioDeviceStopped() override { }
    void seek(double seconds) override;
	double get_position() override;
    bool is_playing() override { return m_is_playing; }
    void start() override;
    void stop() override;
    void set_volume(double gain) override;
	bool is_looped() override { return m_looped; }
	void set_looped(bool b) override;
	AudioDeviceManager* get_audiodevicemanager() override { return m_manager.get(); }
	void set_node(node_ref) override;
	node_ref getNode() override { return m_src_node; }
private:
	bool m_looped = true;
	CriticalSection m_mutex;
    std::atomic<bool> m_is_playing={false};
    std::unique_ptr<AudioDeviceManager> m_manager;
    NamedValueSet m_properties;
	std::shared_ptr<process_node> m_src_node;
	double m_gain = 1.0;
};

#endif // JCDP_AUDIO_PLAYBACK_H
