#pragma once

#include "cdp_graph.h"
#include "sol.hpp"
#include "lua_helpers.h"

enum class ArgumentType
{
    Unknown,
    InputFile,
    InputFileArray,
    OutputFile,
	OutputFileArray,
    FloatParameter,
    IntParameter,
    BoolParameter,
    ChoiceParameter,
    StringParameter,
    TextFileParameter,
    Last
};

inline String argumentTypeToString(ArgumentType arg)
{
	if (arg == ArgumentType::Unknown)
		return "Unknown";
	if (arg == ArgumentType::InputFile)
		return "InputFile";
	if (arg == ArgumentType::InputFileArray)
		return "InputFileArray";
	if (arg == ArgumentType::OutputFile)
		return "OutputFile";
	if (arg == ArgumentType::OutputFileArray)
		return "OutputFileArray";
	if (arg == ArgumentType::FloatParameter)
		return "FloatParameter";
	if (arg == ArgumentType::IntParameter)
		return "IntParameter";
	if (arg == ArgumentType::BoolParameter)
		return "BoolParameter";
	if (arg == ArgumentType::ChoiceParameter)
		return "ChoiceParameter";
	if (arg == ArgumentType::StringParameter)
		return "StringParameter";
	if (arg == ArgumentType::TextFileParameter)
		return "TextFileParameter";
	return String();
}

class luacdpnode : public process_node, public Value::Listener
{
public:
    
	luacdpnode(String procname);
	void tick() override;
    String get_error_text() override;
	void removeLastOutputFile() override;
	StringArray get_arguments() override;
	void set_arguments(StringArray) override {}
	int getNumOutChansForNumInChans(int inchans) override;
	void valueChanged(Value& v) override;
	process_node::CustomGUIType getCustomEditorType2() override;
	Component* getCustomEditor() override;
private:
	// Inherited via process_node
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	void connections_changed() override;
	void update_params() override;
	bool init_lua_processor(String name);
	//String m_tip_text;
	String m_exe;
	String m_mode;
	String m_input_type;
	String m_output_type;
	String m_out_fn;
	String m_channel_conf;
	int m_num_input_files_required = 1;
	int m_num_input_files_supported = 1;
    
	bool m_params_inited = false;
	bool m_io_types_inited = false;
	String m_custom_editor_type;
	std::vector<ArgumentType> m_argument_pattern;
	sound_file_info_t m_sound_file_info;
	ArgumentType detectArgumentType(sol::table& argtable);
    void addFloatParameter(sol::table& argtable);
    void addBoolParameter(sol::table& argtable);
    void addStringParameter(sol::table& argtable);
    void addInputFileArgument(sol::table& argtable, bool is_array);
    void addOutputFileArgument(sol::table& argtable);
    void addChoiceParameter(sol::table& argtable);
	void updateFloatParameters(sol::table& argtable);
	// Why guess? Well, because it's messy how this should be determined...
	int guessNumOutChans(int numins);
};

void destroy_lua_node_state();
