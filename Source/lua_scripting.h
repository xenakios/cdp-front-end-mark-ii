#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "sol.hpp"
#include "cdp_graph.h"
#include <thread>
#include <mutex>
#include <future>

class NodeView;

class ScriptingEngine
{
public:
	ScriptingEngine(process_graph* graph, NodeView* view);
	// Execute script from a file synchronously. Script should not depend on coroutine yields and preferably should not 
	// do any operations that consume a lot of time. Returns a non-empty string if an error occurred.
	String executeScriptFromFile(String filename);
	// Execute script from a file asynchronously using a separate thread. Script should not depend on coroutine yields.
	// Completion callback is called with a non-empty string containing an error message if an error occurred.
	// False is returned if running the script failed immediately. True is returned if the script started executing
	// in the thread.
	bool executeScriptFromFileAsync(String filename, std::function<void(String)> completion_callback);
private:
	sol::state m_lua;
	std::mutex m_mutex;
	process_graph* m_graph = nullptr;
	NodeView* m_node_view = nullptr;
	void initAPI();
};