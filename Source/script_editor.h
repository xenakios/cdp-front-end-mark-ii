#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <functional>

// Helper class for dummy implementations of the virtual methods that might not be needed
class ValueTreeListenerHelper : public ValueTree::Listener
{
public:
	void valueTreeChildAdded(ValueTree &parentTree, ValueTree &childWhichHasBeenAdded) override
	{

	}
	void valueTreeChildRemoved(ValueTree &parentTree, ValueTree &childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved) override
	{

	}
	void valueTreeChildOrderChanged(ValueTree &parentTreeWhoseChildrenHaveMoved, int oldIndex, int newIndex) override
	{

	}
	void valueTreeParentChanged(ValueTree &treeWhoseParentHasChanged) override
	{

	}
};


class ScriptTextEditor : public TextEditor, 
	public ValueTreeListenerHelper,
	public TextEditor::Listener
{
public:
	ScriptTextEditor(ValueTree data);
	bool keyPressed(const KeyPress& ev) override;
	std::function<void(String)> OnExecuteScript;
	void valueTreePropertyChanged(ValueTree &treeWhosePropertyHasChanged, const Identifier &property) override;
	void textEditorTextChanged(TextEditor& ed) override;
private:
	ValueTree m_data;
};

