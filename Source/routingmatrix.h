#ifndef ROUTINGMATRIX
#define ROUTINGMATRIX

#include <vector>
#include <algorithm>
#include "../JuceLibraryCode/JuceHeader.h"

enum class connection_state
{
    Invalid=0,
    Connected,
    Disconnected
};

class routing_matrix
{
public:

    routing_matrix() {}
    routing_matrix(int numins, int numouts)
        : m_max_num_ins(numins), m_max_num_outs(numouts)
    {
        m_matrix.resize(m_max_num_ins*m_max_num_outs);
    }
    //routing_matrix(const routing_matrix&) = delete;
    //routing_matrix& operator=(const routing_matrix&) = delete;
	void initDefault(int numins, int numouts)
	{
		resize(numins, numouts);
		clear_all();
		if (numins == numouts)
			for (int i = 0; i < numouts; ++i)
				set_connection(i, i, true);
		else if (numouts == 1 && numins > 0)
			for (int i = 0; i < numins; ++i)
				set_connection(i, 0, true);
		else if (numouts > numins)
		{
			for (int i = 0; i < numouts; ++i)
				set_connection(i % numins, i, true);
		}
		else if (numouts < numins)
		{
			for (int i = 0; i < numins; ++i)
				set_connection(i, i % numouts, true);
		}
	}
    int highestUsedOutput() const
    {
        int result=-1;
        for (int i=0;i<m_max_num_ins;++i)
        {
            for (int j=0;j<m_max_num_outs;++j)
            {
                if (m_matrix[i*m_max_num_outs+j]>0.0)
                {
                    if (j>result)
                        result=j;
                }
            }
        }
        return result;
    }
    void process(const AudioSampleBuffer& inbuf, AudioSampleBuffer& outbuf,bool prezero_output)
    {
		if (prezero_output == true)
			outbuf.clear();
		int numins = inbuf.getNumChannels();
		int numouts = outbuf.getNumChannels();
		int inputs_to_process=std::min(numins,m_max_num_ins);
        int outputs_to_process=std::min(numouts,m_max_num_outs);
        /*
		if (m_is_passthrough == true && numins == numouts)
        {
            for (int i=0;i<nframes*inputs_to_process;++i)
            {
                outbuf[i]=inbuf[i];
            }
            return;
        }
		*/
		int nframes = inbuf.getNumSamples();
		for (int i = 0; i < inputs_to_process; ++i)
		{
			const float* inbufptr = inbuf.getReadPointer(i);
			for (int j = 0; j < outputs_to_process; ++j)
			{
				float* outbufptr = outbuf.getWritePointer(j);
				float gain = m_matrix[i*m_max_num_outs + j];
				if (std::abs(gain)>0.0)
                {
                    /*
                    for (int k = 0; k < nframes; ++k)
                    {
                        outbufptr[k] += gain*inbufptr[k];
                    }
                    */
                    FloatVectorOperations::addWithMultiply(outbufptr, inbufptr, gain, nframes);
                }
			}
		}
		
    }

    void set_connection(int input_chan, int output_chan, bool connected)
    {
        if (input_chan>=m_max_num_ins || output_chan>=m_max_num_outs)
            return;
        double gain=0.0;
        if (connected==true)
            gain=1.0;
        m_matrix[input_chan*m_max_num_outs+output_chan]=gain;
        updatePassThroughState();
    }
    connection_state is_connected(int inputchan, int outputchan) const
    {
        if (inputchan>=m_max_num_ins || outputchan>=m_max_num_outs)
            return connection_state::Invalid;
        if (m_matrix[inputchan*m_max_num_outs+outputchan]>0.0)
                return connection_state::Connected;
        return connection_state::Disconnected;
    }
    void clear_all()
    {
        std::fill(m_matrix.begin(),m_matrix.end(),0.0);
        m_is_passthrough = false;
    }
    void resize(int ins, int outs)
    {
        if (ins==m_max_num_ins && outs==m_max_num_outs)
            return;
        int oldnumins=m_max_num_ins;
        int oldnumouts=m_max_num_outs;
        std::vector<double> oldmatrix=m_matrix;
        m_matrix.resize(ins*outs);
        clear_all();
        for (int i=0;i<oldnumins;++i)
        {
            for (int j=0;j<oldnumouts;++j)
            {
                int oldindex=i*oldnumouts+j;
                int newindex=i*outs+j;
                if (newindex<m_matrix.size() && oldindex<oldmatrix.size())
                    m_matrix[newindex]=oldmatrix[oldindex];
            }
        }
        m_max_num_ins=ins;
        m_max_num_outs=outs;
        updatePassThroughState();
    }
    

    int getMaxNumInputs() const { return m_max_num_ins; }
    int getMaxNumOutputs() const { return m_max_num_outs; }
    bool isPassThrough() const { return m_is_passthrough; }
    bool m_was_manually_edited=false;
private:
    std::vector<double> m_matrix;
    int m_max_num_ins=0;
    int m_max_num_outs=0;
    bool m_uses_default = false;
    bool m_is_passthrough = false;
    
    void updatePassThroughState()
    {
        m_is_passthrough = false;
        int count = 0;
        for (int i=0;i<m_max_num_ins;++i)
            for (int j=0;j<m_max_num_outs;++j)
                if (i==j && is_connected(i,j)==connection_state::Connected)
                    ++count;
        if (count == m_max_num_ins)
            m_is_passthrough = true;
    }
};

#endif // ROUTINGMATRIX

