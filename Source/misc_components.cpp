#include "misc_components.h"
void SplitterLayout::addComponent(Component * comp, double minsize, double maxsize, double preferredsize, bool addresizer)
{
	//m_parent_component->addAndMakeVisible(comp);
	// Component
	m_layout.setItemLayout(m_components.size() + 0, minsize, maxsize, preferredsize);
	// Resizer bar
	if (addresizer == true)
	{
		m_layout.setItemLayout((int)m_components.size() + 1, 5.0f, 5.0f, 5.0f);
	}
	else
		m_layout.setItemLayout((int)m_components.size() + 1, 0.0f, 0.0f, 0.0f);
	auto resizer = std::make_shared<StretchableLayoutResizerBar>(&m_layout, m_components.size() + 1, !m_is_vertical);
	if (addresizer == true)
		m_parent_component->addAndMakeVisible(resizer.get());
	m_resizers.push_back(resizer);
	m_components.push_back(comp);
	m_components.push_back(resizer.get());
}

void SplitterLayout::performLayout(int x, int y, int w, int h)
{
	if (m_components.size() == 0)
		return;
	m_layout.layOutComponents(m_components.data(), (int)m_components.size(), x, y, w, h, m_is_vertical, true);
}

ValueTree SplitterLayout::saveState()
{
	ValueTree vt("sls");
	vt.setProperty("numcomps", (int)m_components.size(), nullptr);
	for (int i = 0; i<m_components.size(); ++i)
	{
		vt.setProperty("pos" + String(i), m_layout.getItemCurrentPosition(i), nullptr);
	}
	return vt;
}

void SplitterLayout::restoreState(ValueTree state)
{
	int numstored = state.getProperty("numcomps");
	//Logger::writeToLog("num comps in layout state "+String(numstored));
	for (int i = 0; i<m_components.size(); ++i)
	{
		if (i<numstored)
		{
			int pos = state.getProperty("pos" + String(i));
			m_layout.setItemPosition(i, pos);
		}
	}

}
