#include "plugin_process_node.h"

#include <memory>
#include <algorithm>
#include <random>
#include "node_component.h"
#include "node_view.h"

plugin_process_node::plugin_process_node(String procname, AudioPluginInstance * pinst) : process_node("")
{
	m_plugin_instance = pinst;
	m_buses_layout.addInputBus("wav");
	m_buses_layout.addOutputBus("wav");
	
	m_proc_name = procname;
	m_type_name = procname;
	m_dyn_props.set("taillen", 0.0);
	m_dyn_props.set("num_ins_to_use", 2);
	m_dyn_props.set("num_outs_to_use", 2);
	int initial_inputs = m_plugin_instance->getNumInputChannels();
	int initial_outputs = m_plugin_instance->getNumOutputChannels();
	m_num_supported_input_channels = initial_inputs;
	m_num_current_output_channels = initial_outputs;
	Logger::writeToLog("plugin io at ctor " + String(initial_inputs) + "/" + String(initial_outputs));
	m_proc_buffer.setSize(64, 256);
	m_proc_buffer.clear();
	m_plugin_instance->setPlayConfigDetails(initial_inputs, initial_outputs, 44100.0, 128);
	MidiBuffer dummybuf;
	m_plugin_instance->processBlock(m_proc_buffer, dummybuf);
	m_bypassmode = NotBypassed;
    for (int i=0;i<m_plugin_instance->getNumParameters();++i)
    {
        String parname = m_plugin_instance->getParameterName(i);
        double parval = m_plugin_instance->getParameter(i);
        parameter_ref param(new node_parameter(parname, "", 0.0, 1.0, parval, true, 0.0, 1.0));
		param->m_parent_node = this;
		m_parameters.push_back(param);
	}
    m_plugin_instance->addListener(this);
	for (int i = 0; i < m_parameters.size(); ++i)
		m_parameters[i]->addValueListener(this);
    startTimer(1,100);
	
}

plugin_process_node::~plugin_process_node()
{
	if (m_plugin_instance != nullptr && m_plugin_instance->getActiveEditor() != nullptr)
	{
		//delete m_plugin_instance->getActiveEditor();
	}
	delete m_plugin_instance;
}

void plugin_process_node::audioProcessorParameterChanged (AudioProcessor *processor, int parameterIndex, float newValue)
{
    if (processor == m_plugin_instance && parameterIndex>=0 && parameterIndex<m_parameters.size())
    {
       m_parameters[parameterIndex]->setValue(newValue);
	   stopTimer(2);
	   if (m_block_undo_adding == false)
	   {
		   startTimer(2, 1000);
		   m_last_changed_par = parameterIndex;
	   }
    }
	// Maybe the plugin has increased its parameter count...
	if (processor == m_plugin_instance && parameterIndex>=m_parameters.size())
	{
		Logger::writeToLog("Looks like " + m_plugin_instance->getName() + " now has " + 
			String(m_plugin_instance->getNumParameters()) + " parameters");
		// Now should rebuild the internal parameter list or something...
	}
}

void plugin_process_node::audioProcessorChanged (AudioProcessor *processor)
{
	if (processor == m_plugin_instance && m_component && m_component->m_node_view)
	{
		//m_component->m_node_view->addUndoStateForNode("Change plugin state", this);
	}
}

void plugin_process_node::audioProcessorParameterChangeGestureEnd(AudioProcessor * processor, int parameterIndex)
{
	if (processor == m_plugin_instance && m_component && m_component->m_node_view)
	{
		//m_component->m_node_view->addUndoStateForNode("Change plugin parameter", this);
	}
}

StringArray plugin_process_node::get_output_filenames()
{
	return StringArray{ m_out_fn };
}

String plugin_process_node::get_supported_input_type()
{
	return "wav";
}

String plugin_process_node::get_output_type()
{
	return "wav";
}

MD5 plugin_process_node::extra_state_hash()
{
	MemoryBlock block;
	m_plugin_instance->getStateInformation(block);
    int numchans = m_dyn_props["num_outs_to_use"];
    block.append(&numchans, sizeof(int));
    numchans = m_dyn_props["num_ins_to_use"];
    block.append(&numchans, sizeof(int));
    /*
	for (int i = 0; i < m_plugin_instance->getNumParameters(); ++i)
	{
		float pval = m_plugin_instance->getParameter(i);
		block.append((void*)&pval, sizeof(float));
	}
	*/
	MD5 result(block);
	return result;
}

ValueTree plugin_process_node::getAdditionalStateValueTree()
{
	if (m_plugin_instance != nullptr)
	{
		ValueTree tree("customstate");
		MemoryBlock block;
		m_plugin_instance->getStateInformation(block);
		MemoryOutputStream stream;
		GZIPCompressorOutputStream zip(&stream);
		zip.write(block.getData(), block.getSize());
		zip.flush();
		tree.setProperty("pluginchunkzip", stream.getMemoryBlock(), nullptr);
		tree.setProperty("num_ins", m_dyn_props["num_ins_to_use"], nullptr);
		tree.setProperty("num_outs", m_dyn_props["num_outs_to_use"], nullptr);
		return tree;
	}
	return ValueTree();
}

void plugin_process_node::restoreAdditionalStateFromValueTree(ValueTree tree)
{
	if (m_plugin_instance != nullptr)
	{
		m_block_undo_adding = true;
		if (tree.hasProperty("num_ins"))
			m_dyn_props.set("num_ins_to_use", tree.getProperty("num_ins"));
		if (tree.hasProperty("num_outs"))
			m_dyn_props.set("num_outs_to_use", tree.getProperty("num_outs"));
		MemoryBlock* block = tree.getProperty("pluginchunkzip").getBinaryData();
		if (block!=nullptr)
		{
			juce::MemoryInputStream stream(*block, false);
			juce::GZIPDecompressorInputStream zip(&stream, false);
			MemoryBlock decompressedblock;
			zip.readIntoMemoryBlock(decompressedblock);
			m_plugin_instance->setStateInformation(decompressedblock.getData(), (int)decompressedblock.getSize());
		}
		m_block_undo_adding = false;
	}
}

void plugin_process_node::removeLastOutputFile()
{
	if (m_pstate == Busy)
		return;
	File file(m_out_fn);
	if (file.existsAsFile() == true)
		file.deleteFile();
}

void plugin_process_node::valueChanged(Value& var)
{
	//Logger::writeToLog(var.toString());
	if (m_plugin_instance == nullptr)
		return;
	if (MessageManager::getInstance()->isThisTheMessageThread() == false)
		return;
	for (int i = 0; i < m_parameters.size(); ++i)
	{
		if (m_parameters[i]->refersToSameSourceAs(var))
		{
			float val = var.getValue();
			//if (m_plugin_instance->getParameter(i) != val)
			{
				m_plugin_instance->setParameter(i, val);
			}
		}
	}
}

String plugin_process_node::get_error_text()
{
	return m_error_text;
}

int plugin_process_node::getNumOutChansForNumInChans(int inchans)
{
    return m_num_supported_input_channels;
    if (inchans == m_num_supported_input_channels && inchans == m_num_current_output_channels)
		return inchans;
	return inchans;
}

process_node::CustomGUIType plugin_process_node::getCustomEditorType2()
{
	if (m_plugin_instance != nullptr)
	{
		return process_node::WindowCustomEditor;
	}
	return process_node::NoCustomEditor;
}

Component * plugin_process_node::getCustomEditor()
{
	if (m_plugin_instance != nullptr)
	{
		if (m_plugin_instance->hasEditor() == true)
			return m_plugin_instance->createEditorIfNeeded();
		else
			return new GenericAudioProcessorEditor(m_plugin_instance);
	}
	return nullptr;
}

void plugin_process_node::prepareToPlay(int expectedbufsize, int numchans, double sr)
{
	if (m_plugin_instance == nullptr)
		return;
	double infile_len = 0.0;
	if (m_sources.size() > 0 && m_sources[0]->get_output_filenames().size()>0)
	{
		WavAudioFormat wavformat;
		String infn = m_sources[0]->get_output_filenames()[0];
		MemoryMappedAudioFormatReader* reader = wavformat.createMemoryMappedReader(File(infn));
		if (reader != nullptr)
		{
			m_audio_in_reader = std::unique_ptr<MemoryMappedAudioFormatReader>(reader);
			m_audio_in_reader->mapEntireFile();
			auto info = get_audio_source_info_cached(infn);
			infile_len = (double)info.get_length_seconds();
		}
	}
	double taillen = m_dyn_props["taillen"];
	if (m_audio_in_reader == nullptr && taillen < 1.0)
		taillen = 1.0;
	if (infile_len < 0.001)
		infile_len = taillen;
	if (m_audio_in_reader!=nullptr)
	{
		m_outlen = (int64_t)infile_len*sr+taillen*sr;
		m_fade_env.ClearAllNodes();
		m_fade_env.AddNode({ 0.0,1.0 });
		m_fade_env.AddNode({ infile_len - 0.01,1.0 });
		m_fade_env.AddNode({ infile_len,0.0 });
		int requested_out_chans = m_dyn_props["num_outs_to_use"];
		if (requested_out_chans == 0)
			requested_out_chans = 2;
		int numoutchans = requested_out_chans; 
		int numinchans = std::min((int)m_dyn_props["num_ins_to_use"], m_plugin_instance->getTotalNumInputChannels());
		m_proc_buffer = AudioSampleBuffer(64, expectedbufsize);
		m_plugin_instance->reset();
		if (numchans > m_plugin_instance->getNumOutputChannels())
			numchans = m_plugin_instance->getNumOutputChannels();
		m_plugin_instance->setPlayConfigDetails(numinchans, numchans, sr, expectedbufsize);
		m_plugin_instance->prepareToPlay(sr, m_proc_buf_size);
	}
}

void plugin_process_node::seek(double s)
{
	if (m_audio_in_reader!=nullptr)
		m_outcounter = s*m_audio_in_reader->sampleRate;
}

void plugin_process_node::processAudio(float ** buf, int numchans, int numframes, double sr)
{
	// Currently broken when no input file, but should support that too
	if (m_audio_in_reader == nullptr)
		return;
	double in_sr = m_audio_in_reader->sampleRate;
	int requested_out_chans = m_dyn_props["num_outs_to_use"];
	if (requested_out_chans == 0)
		requested_out_chans = 2;
	int numoutchans = numchans;
	int numinchans = std::min((int)m_dyn_props["num_ins_to_use"], m_plugin_instance->getTotalNumInputChannels());
	int framestoread = std::min((int64_t)numframes, m_outlen - m_outcounter);
	if (m_audio_in_reader != nullptr)
	{
		if (m_outcounter + framestoread<m_audio_in_reader->lengthInSamples)
			m_audio_in_reader->read(&m_proc_buffer, 0, framestoread, m_outcounter, true, true);
	}
	else
	{
		m_proc_buffer.clear();
	}
	float** outbufptrs = m_proc_buffer.getArrayOfWritePointers();
	for (int i = 0; i < numframes; ++i)
	{
		float gain = m_fade_env.GetInterpolatedNodeValue((double)(m_outcounter + i) / in_sr);
		for (int j = 0; j < numinchans; ++j)
		{
			outbufptrs[j][i] *= gain;
		}
	}
	double normtime = 1.0 / m_outlen*m_outcounter;
	for (int i = 0; i<m_parameters.size(); ++i)
	{
		node_parameter& par = *m_parameters[i];
		if (par.m_automation_enabled == true && par.hasEnvelope() == true)
		{
			double parval = par.get_envelope()->GetInterpolatedNodeValue(normtime);
			m_plugin_instance->setParameter(i, (float)parval);
		}
	}
	MidiBuffer dummybuf;
	m_plugin_instance->processBlock(m_proc_buffer, dummybuf);
	if (numinchans < numchans)
	{
		for (int i = numinchans; i < numchans; ++i)
		{
			//m_proc_buffer.clear(i, 0, m_proc_buf_size);
		}
	}
	auto arrptrs = m_proc_buffer.getArrayOfReadPointers();
	for (int i = 0; i < numchans; ++i)
	{
		for (int j = 0; j < numframes; ++j)
		{
			buf[i][j] = arrptrs[i][j];
		}
	}
	m_outcounter += numframes;
	if (m_outcounter >= m_outlen)
		m_outcounter = 0;
}

double plugin_process_node::getPlaybackPosition()
{
	if (m_audio_in_reader != nullptr)
		return (double)m_outcounter / m_audio_in_reader->sampleRate;
	return 0.0;
}

void plugin_process_node::timerCallback(int id)
{
	if (id == 1)
	{
		if (m_pstate == Busy)
			return;
		if (m_plugin_instance != nullptr)
		{
			//m_dummy_buffer.clear();
			//m_plugin_instance->processBlock(m_dummy_buffer, m_dummy_mb);
		}
	}
	if (id == 2)
	{
		if (m_last_changed_par != -2)
		{
			if (m_plugin_instance && m_component && m_component->m_node_view)
			{
				m_block_undo_adding = true;
				if (m_last_changed_par>=0)
					m_component->m_node_view->addUndoState("Change plugin parameter");
				if (m_last_changed_par == -1)
					m_component->m_node_view->addUndoState("Change plugin general state");
				m_block_undo_adding = false;
			}
			m_last_changed_par = -2;
			stopTimer(2);
		}
	}
}

void plugin_process_node::tick()
{
	tick_sources();
	if (m_pstate == Busy && m_error_text.isEmpty() == false)
	{
		m_pstate = FinishedWithError;
		if (ProcessErrorFunc)
            ProcessErrorFunc(this);
		return;
	}
	if (m_pstate == Busy && m_thread_state == 1)
	{
		m_thread->join();
		m_thread.reset();
		end_benchmark();
		m_audio_out_writer->flush();
		m_audio_out_writer.reset();
        TempFileContainer::instance().add(m_out_fn);
		m_pstate = FinishedOK;
		setAudioFileToPlay(m_out_fn);
		if (ProcessFinishedFunc)
            ProcessFinishedFunc(this);
		return;
	}
	auto finish_state = are_sources_finished();
	if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
	{
		m_error_text = "Error in source(s)";
		m_pstate = FinishedWithError;
		ProcessErrorFunc(this);
	}
	if (finish_state == SourcesState::FinishedOK)
	{
		if (m_pstate == Idle)
		{
			if (m_bypassmode == Bypassed)
			{
				// This needs fixing for the case when there are no source nodes...
				if (m_sources.size() > 0)
				{
					m_out_fn = m_sources[0]->get_output_filenames()[0];
					m_pstate = FinishedOK;
					setAudioFileToPlay(m_out_fn);
					if (ProcessFinishedFunc)
						ProcessFinishedFunc(this);
					return;
				}
			}
			m_error_text = "";
            auto r = initOutputFile("plugin_node", "wav");
            if (r.second=="*")
                return;
            if (r.first.isEmpty())
                return;
            m_out_fn = r.first;
			String infn;
			if (m_sources.size() > 0)
			{
				StringArray outfilenames = m_sources[0]->get_output_filenames();
				if (outfilenames.size() > 0)
					infn = outfilenames[0];
			}
			if (infn.isEmpty()==false && infn.containsIgnoreCase("wav") == false)
			{
				m_pstate = FinishedWithError;
				m_error_text = "Source has incompatible file format";
				if (ProcessErrorFunc)
					ProcessErrorFunc(this);
				return;
			}
			m_render_progress = 0.0;
			File audio_in_file(infn);
			auto info = get_audio_source_info_cached(infn);
			double in_sr = info.samplerate;
			if (in_sr < 1.0)
				in_sr = 44100.0;
			int requested_out_chans = m_dyn_props["num_outs_to_use"];
			if (requested_out_chans==0)
                requested_out_chans = 2;
            int numoutchans = requested_out_chans; // std::min((int)m_dyn_props["num_outs_to_use"], m_plugin_instance->getTotalNumOutputChannels());
			int numinchans = std::min((int)m_dyn_props["num_ins_to_use"], m_plugin_instance->getTotalNumInputChannels());
			File audio_out_file(m_out_fn);
			WavAudioFormat wavformat;
			OutputStream* outstream = audio_out_file.createOutputStream();
            StringPairArray metadata;
            metadata.set(WavAudioFormat::bwavOriginator, CharPointer_UTF8("λ VST/AU plugin node"));
            m_audio_out_writer = std::unique_ptr<AudioFormatWriter>(wavformat.createWriterFor(
				outstream, in_sr, numoutchans, 32, metadata, 0));
			if (m_audio_out_writer==nullptr)
            {
                m_error_text = "Could not create audio file writer";
                m_pstate = FinishedWithError;
                if (ProcessErrorFunc)
                    ProcessErrorFunc(this);
                return;
            }
            m_outcounter = 0;
			MemoryMappedAudioFormatReader* mmreader = wavformat.createMemoryMappedReader(audio_in_file);
			m_audio_in_reader = std::unique_ptr<MemoryMappedAudioFormatReader>(mmreader);
			if (m_audio_in_reader != nullptr)
			{
				m_audio_in_reader->mapEntireFile();
			}
			double taillen = m_dyn_props["taillen"];
			if (m_audio_in_reader == nullptr && taillen < 1.0)
				taillen = 1.0;
			m_outlen = (int64_t)(info.m_length_frames + in_sr*taillen);
			double infile_len = (double)info.get_length_seconds();
			if (infile_len<0.001)
				infile_len = taillen;
			m_proc_buffer = AudioSampleBuffer(64, m_proc_buf_size);
			m_plugin_instance->reset();
			m_plugin_instance->setPlayConfigDetails(numinchans, numoutchans, in_sr, m_proc_buf_size);
			m_plugin_instance->prepareToPlay(in_sr, m_proc_buf_size);
			Logger::writeToLog("io at process " + String(m_plugin_instance->getNumInputChannels()) + "/" + String(m_plugin_instance->getNumOutputChannels()));
			start_benchmark();
			m_thread_state = 0;
			m_pstate = Busy;
			auto tfunc = [this, numinchans,numoutchans, infile_len,in_sr]()
			{
				float mmreaderbuf[64];
				m_fade_env.ClearAllNodes();
				m_fade_env.AddNode({ 0.0,1.0 });
				m_fade_env.AddNode({ infile_len-0.01,1.0 });
				m_fade_env.AddNode({ infile_len,0.0 });
				MidiBuffer mb;
				int64_t outcounter = 0;
				while (outcounter < m_outlen)
				{
					int framestoread = std::min((int64_t)m_proc_buf_size, m_outlen - outcounter);
					if (m_audio_in_reader != nullptr)
					{
						if (outcounter+framestoread<m_audio_in_reader->lengthInSamples)
							m_audio_in_reader->read(&m_proc_buffer, 0, framestoread, outcounter, true, true);
					}
					else
					{
						m_proc_buffer.clear();
					}
					float** outbufptrs = m_proc_buffer.getArrayOfWritePointers();
					for (int i = 0; i < m_proc_buf_size; ++i)
					{
						float gain = m_fade_env.GetInterpolatedNodeValue((double)(outcounter + i) / in_sr);
						for (int j = 0; j < numinchans; ++j)
						{
							outbufptrs[j][i] *= gain;
						}
					}
					double normtime = 1.0 / m_outlen*outcounter;
					for (int i=0;i<m_parameters.size();++i)
					{
						node_parameter& par = *m_parameters[i];
						if (par.m_automation_enabled == true && par.hasEnvelope()==true)
						{
							double parval = par.get_envelope()->GetInterpolatedNodeValue(normtime);
							m_plugin_instance->setParameter(i, (float)parval);
						}
					}
					m_plugin_instance->processBlock(m_proc_buffer, mb);
					if (numinchans < numoutchans)
					{
						for (int i = numinchans; i < numoutchans; ++i)
						{
							//m_proc_buffer.clear(i, 0, m_proc_buf_size);
						}
					}
					m_audio_out_writer->writeFromAudioSampleBuffer(m_proc_buffer, 0, m_proc_buf_size);
					outcounter += m_proc_buf_size;
					m_render_progress = normtime;
				}
				m_thread_state = 1;
			};
			m_thread = std::make_unique<std::thread>(tfunc);
		}
	}
}

