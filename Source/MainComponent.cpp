#include <map>
#include "MainComponent.h"
#include "jcdp_audio_playback.h"

#ifdef WIN32
#include "Windows.h"
#endif
#include "node_factory.h"
#include "lua_helpers.h"
#include "node_component.h"
#include "node_view.h"
#include "plugin_gui.h"
#include <random>
#include "lua_scripting.h"

#ifdef IS_REAPER_EXTENSION_PLUGIN
#include "reaper_plugin_functions.h"
#endif

#undef min
#undef max

extern std::unique_ptr<AudioThumbnailCache> g_thumb_cache;
extern std::unique_ptr<AudioFormatManager> g_format_manager;
extern std::unique_ptr<PropertiesFile> g_propsfile;
extern String g_cdp_render_location;
extern String g_cdp_bin_location;

void test_gui_independent_graph()
{
	std::mt19937 randgen;
	std::uniform_real_distribution<double> dist(1.0, 16.0);
	process_graph graph;
	auto node_in = graph.createNode("Input Sound File");
	node_in->set_parameter(0, "C:/MusicAudio/sourcesamples/suicide.wav");
	auto node_vibrato = graph.createNode("6 Add vibrato");
	node_vibrato->set_parameter(0, dist(randgen));
	node_vibrato->set_parameter(1, 12.0);
	connect_nodes(node_in, node_vibrato);
	String result = graph.renderNode(node_vibrato.get(), true);
	if (result.isEmpty() && node_vibrato->get_output_filenames().size()>0)
	{
		Logger::writeToLog("succesfully rendered node " + node_vibrato->get_output_filenames()[0]);
		graph.removeNode(node_vibrato);
		node_vibrato = graph.createNode("1 Reverse");
		connect_nodes(node_in, node_vibrato);
		result = graph.renderNode(node_vibrato.get(), true);
		if (result.isEmpty() && node_vibrato->get_output_filenames().size() > 0)
		{
			Logger::writeToLog("succesfully rendered node " + node_vibrato->get_output_filenames()[0]);
		}
	}
	else
		Logger::writeToLog(result);
}

enum class SettingType
{
	Unknown,
	String,
	FileName,
	Integer,
	Choice
};

class SettingComponent : public Component, public Button::Listener
{
public:
	SettingComponent(String guiname, String settings_id) :
		m_gui_desc(guiname), m_settings_id(settings_id)
	{
		addAndMakeVisible(&m_label);
		m_label.setText(m_gui_desc, dontSendNotification);
	}
	void initChoice(StringArray choicestrings, int defaultvalue)
	{
		m_combobox = std::make_unique<ComboBox>();
		addAndMakeVisible(m_combobox.get());
		for (int i = 0; i<choicestrings.size(); ++i)
		{
			m_combobox->addItem(choicestrings[i], i + 1);
		}
		int id = g_propsfile->getIntValue(m_settings_id, defaultvalue);
		m_combobox->setSelectedId(id, dontSendNotification);
	}
	void initFileName(bool is_directory)
	{
		m_is_directory = is_directory;
		m_text_edit = std::make_unique<TextEditor>();
		addAndMakeVisible(m_text_edit.get());
		String txt = g_propsfile->getValue(m_settings_id);
		m_text_edit->setText(txt, false);
		m_button = std::make_unique<TextButton>();
		m_button->setButtonText("...");
		addAndMakeVisible(m_button.get());
		m_button->addListener(this);
	}
	void buttonClicked(Button*) override
	{
		if (m_settings_id == "general/cdp_bin_loc")
		{
			String loc = query_cdp_binaries_location();
			if (loc.isEmpty() == false)
			{
				m_text_edit->setText(loc,dontSendNotification);
			}
		}
		if (m_settings_id == "general/lua_defs_fn")
		{
			FileChooser myChooser("Please select CDP Lua definitions file...",
				File(),
				"*.lua");
			if (myChooser.browseForFileToOpen() == true)
			{
				m_text_edit->setText(myChooser.getResult().getFullPathName());
			}
		}
		if (m_settings_id == "general/cdp_render_location")
		{
			String loc = query_cdp_render_location();
			if (loc.isEmpty() == false)
			{
				m_text_edit->setText(loc);
			}
		}
	}
	void resized() override
	{
		int labw = 300;
		int comph = 25;
		int butw = 0;
		if (m_button != nullptr)
			butw = 30;
		m_label.setBounds(0, 0, labw, comph);
		if (m_combobox != nullptr)
			m_combobox->setBounds(labw + 5, 0, getWidth() - labw - butw - 15, comph);
		if (m_text_edit != nullptr)
			m_text_edit->setBounds(labw + 5, 0, getWidth() - labw - butw - 15, comph);
		if (m_button != nullptr)
			m_button->setBounds(getWidth() - butw - 5, 0, butw, comph);
	}
	SettingType m_type = SettingType::Unknown;
	String m_gui_desc;
	String m_settings_id;
	void updateSettings()
	{
		if (m_combobox != nullptr)
			g_propsfile->setValue(m_settings_id, m_combobox->getSelectedId());
		if (m_text_edit != nullptr)
			g_propsfile->setValue(m_settings_id, m_text_edit->getText());
		if (UpdateSettingCallback)
			UpdateSettingCallback();
	}
	std::function<void(void)> UpdateSettingCallback;
private:
	Component* m_parent_component = nullptr;
	std::unique_ptr<ComboBox> m_combobox;
	std::unique_ptr<TextButton> m_button;
	std::unique_ptr<TextEditor> m_text_edit;
	Label m_label;
	bool m_is_directory = false;
};

class GeneralSettingsComponent : public Component
{
public:
	GeneralSettingsComponent()
	{
		addFileNameSetting("CDP binaries location", "general/cdp_bin_loc",true);
		addFileNameSetting("CDP temp files location", "general/cdp_render_location",true);
		addFileNameSetting("Lua definitions file", "general/lua_defs_fn", false);
		addChoiceSetting("Delete temporary files on shutdown", "general/deltempfilesshutdown", 
			{"Ask","Don't delete","Delete" },3);
		addChoiceSetting("When document changed outside application", "general/docmodifoutsidebehav",
		{ "Ask","Load modified document automatically","Do nothing" }, 1);
		addChoiceSetting("Max number of CPU cores for CDP processing", "general/num_cdp_cpucores",
		{ "1","2","3","4","5","6","7","8" }, 2);
		setSize(100, 100);
	}
	void addChoiceSetting(String guiname, String setting_id, StringArray choices,int defval)
	{
		auto comp = std::make_shared<SettingComponent>(guiname, setting_id);
		comp->initChoice(choices,defval);
		addAndMakeVisible(comp.get());
		m_components.push_back(comp);
	}
	void addFileNameSetting(String guiname,String setting_id, bool is_dir)
	{
		auto comp = std::make_shared<SettingComponent>(guiname, setting_id);
		comp->initFileName(is_dir);
		addAndMakeVisible(comp.get());
		m_components.push_back(comp);
	}
	void resized() override
	{
		for (int i = 0; i < m_components.size(); ++i)
		{
			m_components[i]->setBounds(1, 10 + i * 30, getWidth() - 2, 25);
		}
	}
	void update_settings()
	{
		for (auto& e : m_components)
			e->updateSettings();
	}
private:
	std::vector<std::shared_ptr<SettingComponent>> m_components;
};



class PreferencesComponent : public Component, public Button::Listener
{
public:
	
	PreferencesComponent(AudioDeviceManager* adm, ApplicationCommandManager* cmdman) 
		: m_tab_component(TabbedButtonBar::TabsAtTop), 
		m_audio_dev_manager(adm), m_cmd_manager(cmdman)
	{
		addAndMakeVisible(&m_tab_component);
		//addAndMakeVisible(&m_ok_button);
		m_ok_button.addListener(this);

		m_gen_settings = new GeneralSettingsComponent;
		m_tab_component.addTab("General", Colours::lightgrey, m_gen_settings, false);

		m_audio_dev_component = new AudioDeviceSelectorComponent(*m_audio_dev_manager, 0, 0, 2, 8, false, false, true, false);
		m_tab_component.addTab("Audio device", Colours::lightgrey, m_audio_dev_component, false);
		
		m_plugin_manager.addDefaultFormats();
		m_plugin_list_component = new PluginListComponent(m_plugin_manager,
			node_factory::instance()->getPluginList(), m_dead_mans_file,
			g_propsfile.get());
		m_tab_component.addTab("Plugins", Colours::lightgrey, m_plugin_list_component, false);
		
		m_keymapeditor = new KeyMappingEditorComponent(*m_cmd_manager->getKeyMappings(), true);
		m_tab_component.addTab("Keyboard shortcuts", Colours::lightgrey, m_keymapeditor, false);
		int tabindex = g_propsfile->getIntValue("general/prefspage", 0);
		m_tab_component.setCurrentTabIndex(tabindex);
		setSize(800, 700);
	}
	~PreferencesComponent()
	{
		g_propsfile->setValue("general/prefspage", m_tab_component.getCurrentTabIndex());
		delete m_audio_dev_component;
		delete m_plugin_list_component;
		delete m_keymapeditor;
		delete m_gen_settings;
	}
	void resized() override
	{
		m_tab_component.setBounds(0, 0, getWidth(), getHeight() - 25);
		m_ok_button.setBounds(getWidth() - 100, getHeight() - 25, 95, 24);
	}
	void buttonClicked(Button* but) override
	{
		if (but == &m_ok_button)
		{

		}
	}
	void updateSettings()
	{
		auto xml = m_cmd_manager->getKeyMappings()->createXml(true);
		g_propsfile->setValue("general/keyboard_shortcuts", xml);
		delete xml;
		m_gen_settings->update_settings();
		String temp = g_propsfile->getValue("general/cdp_render_location");
		if (temp.isEmpty() == false)
			g_cdp_render_location = temp;
		temp = g_propsfile->getValue("general/cdp_bin_loc");
		if (temp.isEmpty() == false)
			g_cdp_bin_location = temp;
		node_factory::instance()->init_lua_defined_programs();
		int numcores = g_propsfile->getIntValue("general/num_cdp_cpucores", 2);
		childprocess_pool::get_instance()->setNumAvailable(size_t(numcores));
	}
private:
	TabbedComponent m_tab_component;
	TextButton m_ok_button;
	AudioDeviceSelectorComponent* m_audio_dev_component = nullptr;
	PluginListComponent* m_plugin_list_component = nullptr;
	AudioDeviceManager* m_audio_dev_manager = nullptr;
	AudioPluginFormatManager m_plugin_manager;
	ApplicationCommandManager* m_cmd_manager = nullptr;
	File m_dead_mans_file;
	KeyMappingEditorComponent* m_keymapeditor = nullptr;
	GeneralSettingsComponent* m_gen_settings = nullptr;
};

//==============================================================================
MainContentComponent::MainContentComponent(std::shared_ptr<IJCDPreviewPlayback> player, ParameterLinker* lp) :
	m_audio_player(player), m_param_linker(lp),
	//m_volume_dial_labelled("Volume", 90, Slider::RotaryVerticalDrag, Slider::TextBoxRight),
	m_main_layout(FlexBox::Direction::column, FlexBox::Wrap::noWrap, FlexBox::AlignContent::stretch,
		FlexBox::AlignItems::flexStart, FlexBox::JustifyContent::flexStart),
	m_toprow_layout(FlexBox::Direction::row, FlexBox::Wrap::noWrap, FlexBox::AlignContent::stretch,
		FlexBox::AlignItems::flexStart, FlexBox::JustifyContent::flexStart),
	m_inspec_nodeview_layout(this,false)

{

#ifdef WIN32
	if (SetEnvironmentVariableA("CDP_SOUND_EXT", "wav") == 0)
		Logger::writeToLog("Could not set CDP_SOUND_EXT environment variable");
	if (SetEnvironmentVariableA("CDP_NOCLIP_FLOATS", "1") == 0)
		Logger::writeToLog("Could not set CDP_NOCLIP_FLOATS environment variable");
	if (SetEnvironmentVariableA("CDP_MEMORY_BBSIZE", "4096") == 0)
		Logger::writeToLog("Could not set CDP_MEMORY_BBSIZE environment variable");
#else
	if (putenv(strdup("CDP_SOUND_EXT=wav")) != 0)
		Logger::writeToLog("Could not set CDP_SOUND_EXT environment variable");
	if (putenv(strdup("CDP_NOCLIP_FLOATS=1")) != 0)
		Logger::writeToLog("Could not set CDP_NOCLIP_FLOATS environment variable");
	if (putenv(strdup("CDP_MEMORY_BBSIZE=4096")) != 0)
		Logger::writeToLog("Could not set CDP_MEMORY_BBSIZE environment variable");
#endif
	

	//m_favorite_nodes = { "Pvoc/Analyse", "Pvoc/Resynth", "Modify Radical - 1 Reverse",
	//	"Stretch Time - 1 stretch/compress duration of infile without changing the pitch" };

	for (int i = 0; i < 8; ++i)
		m_favorite_nodes.add("(none)");

	addAndMakeVisible(&m_playbackstatuslabel);
	m_playbackstatuslabel.setFont(18.0f);
	addAndMakeVisible(&m_renderstatuslabel);

	m_render_button = std::unique_ptr<TextButton>(new TextButton("Render"));
	m_render_button->setBounds(5, 5, 60, 26);
	addAndMakeVisible(m_render_button.get());
	m_render_button->addListener(this);

	m_show_param_manager = std::make_unique<TextButton>("Menu...");
	m_show_param_manager->setBounds(170, 5, 90, 26);
	addAndMakeVisible(m_show_param_manager.get());
	m_show_param_manager->addListener(this);

	m_volume_dial = std::make_unique<Slider>(Slider::RotaryVerticalDrag, Slider::TextBoxRight);
	m_volume_dial->setRange(-24, 6.0, 0.5);
	m_volume_dial->setBounds(70, 5, 90, 30);
	m_volume_dial->setValue(-3.0);
	m_volume_dial->addListener(this);
	m_volume_dial->setTooltip("Audio volume");
	addAndMakeVisible(m_volume_dial.get());

	addAndMakeVisible(&m_inspector_tabs);
	m_inspector_tabs.TabChangedFunc = [this](int index)
	{
		m_active_node_view->setNodeInspector(m_inspector_tabs.getInspector(index));
	};
	
	
	m_active_node_view = std::make_shared<NodeView>(m_audio_player, nullptr);
	addAndMakeVisible(m_active_node_view.get());
	m_active_node_view->setBounds(0, 0, 50, 50);
	m_node_view_port.setViewedComponent(m_active_node_view.get(), false);
	m_active_node_view->m_viewport = &m_node_view_port;
	m_active_node_view->NodeDeletedFunc = [this](process_node* node)
	{
		m_inspector_tabs.cleanUpRemovedNode(node);
	};

	addAndMakeVisible(&m_node_view_port);
	m_node_view_port.setBounds(0, 60, 800, 600);

	startTimer(0,100);
	addTabbedNodeInspector();
	setWantsKeyboardFocus(true);
	
	initActions();
	m_cmd_manager = std::make_unique<ApplicationCommandManager>();
	m_cmd_manager->registerAllCommandsForTarget(this);
	addKeyListener(m_cmd_manager->getKeyMappings());
	auto xml = g_propsfile->getXmlValue("general/keyboard_shortcuts");
	if (xml!=nullptr)
    {
        m_cmd_manager->getKeyMappings()->restoreFromXml(*xml);
        delete xml;
    }
	
	
	double vol = g_propsfile->getDoubleValue("general/preview_volume", -3.0);
	m_volume_dial->setValue(vol);
	loadFavorites();
	int defnumcores = childprocess_pool::get_instance()->getNumAvailable();
	int numcores = g_propsfile->getIntValue("general/num_cdp_cpucores", defnumcores);
	childprocess_pool::get_instance()->setNumAvailable(numcores);
	//m_txed = new TextFileParameterComponent(nullptr);
	//m_txed->setBounds(500, 200, 500, 400);
	//addAndMakeVisible(m_txed);
	buildFlexLayouts();
	m_inspec_nodeview_layout.addComponent(&m_inspector_tabs, -0.01, -0.5, -0.20, true);
	m_inspec_nodeview_layout.addComponent(&m_node_view_port, -0.5, -0.99, -0.8, false);
	startTimer(100, 4000);
	setSize(1200, 800);
	m_script_engine = std::make_unique<ScriptingEngine>(m_active_node_view->getGraph(), m_active_node_view.get());
}

MainContentComponent::~MainContentComponent()
{
	delete m_txed;
	g_propsfile->setValue("general/preview_volume", m_volume_dial->getValue());
	saveFavorites();
	PluginWindow::closeAllCurrentlyOpenWindows();
	
}

void MainContentComponent::test_heavy_graph()
{
	/*
	m_node_inspector->setVisible(false);
	auto mixnode = add_node_with_component("Mix", 150 + 10 * 50, 125 + 17 * 20, 100, 50);
	for (int i = 0; i < 20; ++i)
	{
		auto node = add_node_with_component("Modify/Radical Reverse", 150 + i * 50, 100, 45, 20);
		connect_nodes(m_source_node, node);
		for (int j = 0; j < 16; ++j)
		{
			auto node2 = add_node_with_component("Modify/Radical Reverse", 150 + i * 50, 125+j*20, 45, 20);
			connect_nodes(node, node2);
			node = node2;
		}
		connect_nodes(node, mixnode);
	}
	repaint();
	*/
}

void MainContentComponent::updateWindowTitle()
{
	if (getParentComponent() != nullptr)
	{
		String docname = m_current_doc_fn;
		if (docname.isEmpty() == true)
			docname = "Untitled";
		if (m_document_dirty == false)
			getParentComponent()->setName(String(CharPointer_UTF8("λ - "))+ docname);
		else
			getParentComponent()->setName(String(CharPointer_UTF8("λ - "))+ docname + "*");
	}
}

void MainContentComponent::addFavoriteNode(int index)
{
	if (index >= 0 && index < m_favorite_nodes.size())
	{
		auto pos = m_active_node_view->getMouseXYRelative();
		m_active_node_view->add_node_with_component(m_favorite_nodes[index], pos.x, pos.y, 150, 55, false);
	}
}

void MainContentComponent::saveFavorites()
{
	XmlElement* favs_element = new XmlElement("favorite_nodes");
	for (int i = 0; i < m_favorite_nodes.size(); ++i)
	{
		XmlElement* fav_element = new XmlElement(String("fav" + String(i)));
		favs_element->addChildElement(fav_element);
		fav_element->setAttribute("name", m_favorite_nodes[i]);
	}
	g_propsfile->setValue("general/favorites", favs_element);
	delete favs_element;
}

void MainContentComponent::loadFavorites()
{
	XmlElement* elem = g_propsfile->getXmlValue("general/favorites");
	if (elem != nullptr)
	{
		int count = elem->getNumChildElements();
		m_favorite_nodes.clear();
		for (int i = 0; i < count; ++i)
		{
			XmlElement* fav_element = elem->getChildElement(i);
			if (fav_element != nullptr)
			{
				String name = fav_element->getStringAttribute ("name");
				if (name.isEmpty() == false)
					m_favorite_nodes.add(name);
			}
		}
		if (count < 8)
			for (int i = count; i < 8; ++i)
				m_favorite_nodes.add("(none)");
	}
	delete elem;
}

void MainContentComponent::getAllCommands(Array<CommandID>& commands)
{
	for (int i = 0; i < m_actions.size(); ++i)
		commands.add(100 + i);
}

void MainContentComponent::getCommandInfo(CommandID commandID, ApplicationCommandInfo & result)
{
	if (commandID >= 100 && commandID<100+m_actions.size())
	{
		action_t& act = m_actions[commandID - 100];
		result.defaultKeypresses = { act.m_shortcut };
		result.setInfo(act.m_shortname, act.m_description, "Main window", 0);
	}
}

void MainContentComponent::togglePlayback()
{
	if (m_audio_player->is_playing() == false)
	{
		auto node = m_active_node_view->getSelectedNode();
		if (node != nullptr)
		{
			if (node->isOfflineOnly() == true)
			{
				startRender();
				m_audio_player->set_node(node);
				m_audio_player->start();
			}
			else
			{
				m_audio_player->set_node(node);
				m_audio_player->start();
			}
		}
	}
	else
	{
		m_audio_player->stop();
	}
}

void MainContentComponent::showPreferences()
{
	m_audio_player->stop();
	AlertWindow dlg(CharPointer_UTF8("λ"), "Preferences", AlertWindow::NoIcon, nullptr);
	PreferencesComponent* comp = new PreferencesComponent(m_audio_player->get_audiodevicemanager(),
		m_cmd_manager.get());
	dlg.addCustomComponent(comp);
	dlg.addButton("Cancel", 2);
	dlg.addButton("OK", 1);
	int result = dlg.runModalLoop();
	if (result == 1)
	{
		node_factory::instance()->init_plugins(true);
		comp->updateSettings();
	}
	delete comp;
	if (result == 1)
	{
		XmlElement* ahwxml = m_audio_player->get_audiodevicemanager()->createStateXml();
		if (ahwxml != nullptr)
			g_propsfile->setValue("audiodevicesettings", ahwxml);
		delete ahwxml;
	}
}

void MainContentComponent::removeCurrentGraph()
{
	m_audio_player->stop();
	m_selected_node = nullptr;
	m_active_node_view.reset();
}

bool MainContentComponent::perform(const InvocationInfo & info)
{
	if (info.commandID >= 100 && info.commandID<100+m_actions.size())
	{
		action_t& act = m_actions[info.commandID - 100];
		if (act.m_performfunc)
		{
			act.m_performfunc();
            m_active_node_view->repaint();
			if (act.m_changes_document == true)
			{
				m_document_dirty = true;
				updateWindowTitle();
			}
			return true;
		}
	}
	return false;
}

void MainContentComponent::initActions()
{
	m_actions.emplace_back("Toggle playback", "Toggle audio playback", "Main window", KeyPress(' ', ModifierKeys::noModifiers, 0), [this]()
	{
		togglePlayback();
	}, false);
	m_actions.emplace_back("Open graph...", "Open graph file...", "Main window", KeyPress('o', ModifierKeys::commandModifier, 0), [this]()
	{
		test_load();
	}, false);
	m_actions.emplace_back("Save graph as...", "Save graph file as...", "Main window", 
		KeyPress('s', ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this]()
	{
		save_document(String());
	}, false);
	m_actions.emplace_back("Preferences...", "Show preferences", "Main window", KeyPress('p', ModifierKeys::noModifiers, 0), [this]()
	{
		showPreferences();
	}, false);
	m_actions.emplace_back("Toggle node inspector", "Show/hide node inspector", "Main window", KeyPress('i', ModifierKeys::noModifiers, 0), [this]()
	{
		m_inspector_tabs.setVisible(!m_inspector_tabs.isVisible());
		buildFlexLayouts();
		resized();
	});
	m_actions.emplace_back("Render node", "Render selected node and dependencies", "Main window", KeyPress('r', ModifierKeys::noModifiers, 0), [this]()
	{
		startRender();
	});
	m_actions.emplace_back("Delete node", "Delete selected node", "Main window", KeyPress(KeyPress::backspaceKey), [this]()
	{
		m_active_node_view->removeSelectedNodes();
		repaint();
	});
	m_actions.emplace_back("Update parameters", "Update parameters of selected node", "", KeyPress('u', ModifierKeys::commandModifier, 0), [this]()
	{
		m_active_node_view->updateParametersOfSelectedNodes();
	});
	m_actions.emplace_back("Export autonamed", "Export selected node with automatic file name", "", KeyPress('1', ModifierKeys::commandModifier, 0), [this]()
	{
		exportNodeToAutoNamedFile(m_selected_node);
	});
	m_actions.emplace_back("Set autoname export folder...", "Set autoname export folder...", "", KeyPress('2', ModifierKeys::commandModifier, 0), [this]()
	{
		queryAutoNameExportFolder();
	});
	m_actions.emplace_back("Export asking name...", "Export selected node asking file name...", "", KeyPress('3', ModifierKeys::commandModifier, 0), [this]()
	{
		exportNodeToFilePromptingName(m_selected_node);
	});
	m_actions.emplace_back("Force render", "Force rendering of node even if the audio should already exist", "",
		KeyPress('r', ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->forceRenderSelected();
		startRender();
	});
	m_actions.emplace_back("Rotate inputs", "Rotate order of inputs of selected node", "", KeyPress('0', ModifierKeys::noModifiers, 0), [this]()
	{
		m_active_node_view->rotateSources();
	});
	m_actions.emplace_back("Save graph", "Save graph file", "Main window",
		KeyPress('s', ModifierKeys::commandModifier, 0), [this]()
	{
		save_document(m_current_doc_fn);
	}, false);
	m_actions.emplace_back("Show last envelope", "Show last edited envelope", "", KeyPress('9', ModifierKeys::noModifiers, 0), [this]()
	{
		m_active_node_view->showLastEnvelope();
	});
	m_actions.emplace_back("Disconnect all IO", "Disconnect inputs and outputs of selected node", "", KeyPress('d', ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->disconnectSelectedNodeIO(true, true);
	}, true);
	m_actions.emplace_back("Disconnect inputs", "Disconnect inputs of selected node", "", KeyPress('f', ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->disconnectSelectedNodeIO(true, false);
	}, true);
	m_actions.emplace_back("Disconnect outputs", "Disconnect outputs of selected node", "", KeyPress('g', ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->disconnectSelectedNodeIO(false, true);
	}, true);
	m_actions.emplace_back("Connect to closest below", "Connect output to input of closest node below", "", KeyPress('h', ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->connectOutputToClosestInput(true);
	}, true);
	
	for (int i = 0; i < 8; ++i)
	{
		String desc = "Add node from favorites " + String(i + 1);
		m_actions.emplace_back(desc, desc, "", KeyPress('1'+i, ModifierKeys::shiftModifier, 0), [this,i]()
		{
			addFavoriteNode(i);
		}, true);
	}
	for (int i = 0; i < 8; ++i)
	{
		String desc = "Set as favorite node " + String(i + 1);
		m_actions.emplace_back(desc, desc, "", KeyPress('1' + i, ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this, i]()
		{
			auto n = m_active_node_view->getSelectedNode();
			if (n != nullptr)
			{
				m_favorite_nodes.set(i, n->get_type_name());
				AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
					"CDP Frontend",
					n->get_type_name() + " was set as favorite " + String(i+1), "OK",
					this);
			}
		}, true);
	}
	
    m_actions.emplace_back("Test Lua script", "Test Lua script", "", KeyPress('l', ModifierKeys::noModifiers, 0), [this]()
    {
		String scriptfn = g_propsfile->getValue("general/testscriptlocation");
		if (does_file_exist(scriptfn))
		{
			String r = m_script_engine->executeScriptFromFile(scriptfn);
			if (r.isEmpty() == false)
			{
				AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
					"Lua execution error",
					r, "OK",
					this);
	}
		}

        
    }, true);
	
	m_actions.emplace_back("Add node...", "Add node from list...", "", KeyPress('+',ModifierKeys::shiftModifier , 0), [this]()
	{
		auto mousepos = m_active_node_view->getMouseXYRelative();
		m_active_node_view->showNodeList(mousepos.x,mousepos.y,false);
	}, true);
	m_actions.emplace_back("About...", "Show about screen...", "", KeyPress(KeyPress::F1Key, ModifierKeys::noModifiers, 0), [this]()
	{
		showAboutScreen();
	}, false);
	m_actions.emplace_back("Duplicate nodes", "Duplicate selected nodes", "", KeyPress('d', ModifierKeys::commandModifier, 0), [this]()
                           {
                               m_active_node_view->duplicateNodes(false,false);
                           }, true);
    m_actions.emplace_back("Duplicate nodes with connections", "Duplicate selected nodes with its connections", "", KeyPress('d', ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this]()
                           {
                               m_active_node_view->duplicateNodes(false,true);
                           }, true);
	m_actions.emplace_back("New inspector", "New node inspector tab", "", KeyPress('i', ModifierKeys::commandModifier, 0), [this]()
	{
		addTabbedNodeInspector();
	}, false);
	m_actions.emplace_back("Remove inspector", "Remove current node inspector tab", "", KeyPress('i', ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this]()
	{
		removeCurrentTabbedNodeInspector();
	}, false);
	m_actions.emplace_back("Select all nodes", "Select all nodes", "", KeyPress('a', ModifierKeys::commandModifier, 0), [this]()
	{
		m_active_node_view->setAllNodesSelectedState(true);
	}, true);
	m_actions.emplace_back("Deselect all nodes", "Deselect all nodes", "", KeyPress('a', ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->setAllNodesSelectedState(false);
	}, true);
	m_actions.emplace_back("Permutate node inputs", "Permutate order of node inputs", "", KeyPress('p', ModifierKeys::commandModifier | ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->permutateSources();
	}, true);
	m_actions.emplace_back("Test GUI independent graph", "Test GUI independent graph", "", KeyPress(KeyPress::F10Key, ModifierKeys::noModifiers , 0), [this]()
	{
		test_gui_independent_graph();
	}, true);
	m_actions.emplace_back("Replace node...", "Replace selected node...", "", KeyPress(KeyPress::F9Key, ModifierKeys::noModifiers, 0), [this]()
	{
		auto mousepos = m_active_node_view->getMouseXYRelative();
		m_active_node_view->showNodeList(mousepos.x, mousepos.y, true);
	}, true);
	m_actions.emplace_back("Rename node...", "Rename selected node...", "", KeyPress(KeyPress::F5Key, ModifierKeys::noModifiers, 0), [this]()
	{
		m_active_node_view->renameSelectedNode();
	}, true);
	m_actions.emplace_back("Show node info text", "Show info text of selected node", "", KeyPress('h', ModifierKeys::noModifiers, 0), [this]()
	{
		m_active_node_view->showNodeInfoText();
	}, true);
	m_actions.emplace_back("Render nodes toposort test", "Toposort test", "Main window", KeyPress('r', ModifierKeys::commandModifier, 0), [this]()
	{
		m_active_node_view->testTopologicalSort(nullptr);
	});
	m_actions.emplace_back("Undo", "Undo", "Main window", KeyPress('z', ModifierKeys::commandModifier, 0), [this]()
	{
		m_active_node_view->doUndo();
	});
	m_actions.emplace_back("Redo", "Redo", "Main window", KeyPress('z', ModifierKeys::commandModifier|ModifierKeys::shiftModifier, 0), [this]()
	{
		m_active_node_view->doRedo();
	});
    m_actions.emplace_back("Run heavy test", "Run heavy test", "Main window", KeyPress(KeyPress::F6Key, ModifierKeys::commandModifier, 0), [this]()
                           {
		if (m_test_state == TestState::Idle || m_test_state == TestState::Finished)
		{
			run_all_nodes_test(m_test_state);
		}
		else if (m_test_state == TestState::Running)
		{
			m_test_state = TestState::CancelRequested;
		}
    });
#ifdef IS_REAPER_EXTENSION_PLUGIN
	m_actions.emplace_back("Export as new item", "Export as new item in Reaper", "", KeyPress('4', ModifierKeys::commandModifier, 0), [this]()
	{
		exportNodeToReaperTimeLine(m_selected_node);
	});
	
#endif
}

void MainContentComponent::showAboutScreen()
{
	String juceversiontxt = String("JUCE ")+String(JUCE_MAJOR_VERSION) + "." + String(JUCE_MINOR_VERSION);
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		CharPointer_UTF8("λ"),
		"Environment for CDP and other sound processings (Beta 1)\nBuilt on "+String(__DATE__)+"\n"
        "(c) 2015-2016 Xenakios.\n\nUses Lua file originally by Djeroek from Renoise CDP frontend by afta8/Djeroek.\n\n"
		"VST PlugIn Interface Technology by Steinberg Media Technologies GmbH\n\n"
		"Lua : Copyright 1994–2016 Lua.org, PUC-Rio.\n\n"
		"sol2 : Copyright (c) 2013-2016 Rapptz, ThePhD and contributors\n\n"
		"SQLite : Public domain\n\n"
		+juceversiontxt+ " (c) Roli. Used under the GPL license.\n\n"
		"GPL licensed source code for this application at : https://bitbucket.org/xenakios/cdp-front-end-mark-ii/src\n"
        ,"OK",
		this);
}

void MainContentComponent::updateInspectorTabNames()
{
	for (int i = 0; i < m_inspector_tabs.getNumTabs(); ++i)
		m_inspector_tabs.setTabName(i, "Inspector " + String(i + 1));
}

void MainContentComponent::buildFlexLayouts()
{
	m_main_layout.items.clear();
	m_toprow_layout.items.clear();
	
	m_toprow_layout.items.add(FlexItem(70.0f, 35.0f, *m_volume_dial));
	m_toprow_layout.items.add(FlexItem(70.0f, 25.0f, *m_show_param_manager).withMargin(10));
	m_toprow_layout.items.add(FlexItem(70.0f, 25.0f, *m_render_button).withMargin(10));
	m_toprow_layout.items.add(FlexItem(70.0f, 25.0f, m_playbackstatuslabel).withFlex(2.0f));
	m_main_layout.items.add(FlexItem(400.0f,40.0f,m_toprow_layout));
}

void MainContentComponent::addTabbedNodeInspector()
{
	int count = m_inspector_tabs.getNumTabs();
	node_inspector* inspec = new node_inspector(false);
	inspec->setParameterLinker(m_param_linker);
	m_inspector_tabs.setTabBarDepth(22);
	m_inspector_tabs.addTab("Inspector", Colours::white, inspec, true);
	m_inspector_tabs.setCurrentTabIndex(count, false);
	updateInspectorTabNames();
}

void MainContentComponent::removeCurrentTabbedNodeInspector()
{
	if (m_inspector_tabs.getNumTabs() < 2)
		return;
	m_inspector_tabs.removeTab(m_inspector_tabs.getCurrentTabIndex());
	m_inspector_tabs.setCurrentTabIndex(0, false);
	m_active_node_view->setNodeInspector(m_inspector_tabs.getInspector(m_inspector_tabs.getCurrentTabIndex()));
	updateInspectorTabNames();
}

void MainContentComponent::sliderValueChanged(Slider* slid)
{
	if (slid == m_volume_dial.get())
	{
		double gain = exp(slid->getValue()*0.11512925464970228420089957273422);
		m_audio_player->set_volume(gain);
	}
}

bool MainContentComponent::keyPressed(const KeyPress&)
{
	return false;
}

void MainContentComponent::mouseDown(const MouseEvent&)
{
#ifdef JCDP_OLD_NODE_CTX_MENU
	if (ev.mods.isRightButtonDown() == false)
	{
		for (auto& e : m_node_components)
			e->set_selected(false);
		return;
	}
	juce::Rectangle<int> area(ev.x, ev.y, 10, 10);
	auto progcomponent = new CDPProgramsComponent;
	CallOutBox& callobox = CallOutBox::launchAsynchronously(progcomponent, area, this);
	progcomponent->OnProgramSelected = [ev,this](String name) 
	{ 
		add_node_with_component(name, ev.x, ev.y, 200, 95);
		
	};
	return;

	std::map<int, String> node_factory_names;
	std::map<String, std::shared_ptr<PopupMenu>> submenusmap;
	std::shared_ptr<PopupMenu> luamenu = std::make_shared<PopupMenu>();
	
	PopupMenu menu;
	PopupMenu noslashmenu;
	menu.addItem(1, "Create multiple...");
	int id = 10;
	for (auto& e : g_node_factory->m_entries)
	{
		if (e.m_lua_defined == false)
		{
			if (e.m_name.contains("/") == true)
			{
				String submenuname = e.m_name.upToFirstOccurrenceOf("/", false, false);
				if (submenusmap.count(submenuname) == 0)
				{
					submenusmap[submenuname] = std::make_shared<PopupMenu>();
				}
				String procname = e.m_name.fromFirstOccurrenceOf("/", false, false);
				submenusmap[submenuname]->addItem(id, procname);
			}
			else
				noslashmenu.addItem(id, e.m_name);
		}
		else
		{
			luamenu->addItem(id, e.m_name);
		}

		node_factory_names[id] = e.m_name;
		++id;
	}
	menu.addSubMenu("Misc", noslashmenu, true);
	menu.addSubMenu("Lua defined", *luamenu, true);
	for (auto &e : submenusmap)
	{
		menu.addSubMenu(e.first, *e.second.get(), true);
	}
	int result = menu.show();
	if (result == 1)
	{
		AlertWindow dlg("CDP","Create multiple processor nodes",AlertWindow::QuestionIcon,this);
		StringArray available_procs;
		for (auto& e : g_node_factory->m_entries)
			available_procs.add(e.m_name);
		dlg.addTextEditor("count", "2");
		dlg.addComboBox("proc", available_procs);
		dlg.addButton("OK", 1, KeyPress(KeyPress::returnKey));
		dlg.addButton("Cancel", 2);
		int result = dlg.runModalLoop();
		if (result == 1)
		{
			int numprocs = dlg.getTextEditor("count")->getText().getIntValue();
			if (numprocs > 1 && numprocs < 17)
			{
				String procname = dlg.getComboBoxComponent("proc")->getText();
				for (int i = 0; i < numprocs; ++i)
				{
					auto node = add_node_with_component(procname, ev.x + 205 * i,ev.y,200,55);
					node->m_component->set_selected(true);
				}
			}
		}
	}
	if (node_factory_names.count(result)>0)
	{
		add_node_with_component(node_factory_names[result], ev.x, ev.y,200,95);
	}
#endif
}

void MainContentComponent::connect_nodes_if_no_feedback(std::shared_ptr<process_node> a, std::shared_ptr<process_node>  b)
{
	connect_nodes(a, b);
	bool has_cycle = has_circular_dependency(a);
	if (has_cycle == true)
	{
		disconnect_nodes(a, b);
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"CDP Frontend error",
			"Feedback connections not allowed", "OK",
			this);

	}
	
}

inline String timeToString(juce::Time t)
{
	char buf[16];
	sprintf(buf, "%02d:%02d.%03d", t.getMinutes(), t.getSeconds(), t.getMilliseconds());
	return buf;
}

void MainContentComponent::timerCallback(int id)
{
	if (id == 0)
	{
		Time time(m_audio_player->get_position()*1000.0);
		String text = timeToString(time) + " CPU " + String(m_audio_player->get_audiodevicemanager()->getCpuUsage()*100.0,1) + " %";
		if (m_audio_player->is_playing() == false)
			text = "Stopped";
		m_playbackstatuslabel.setText(text,dontSendNotification);
		auto playing_node = m_audio_player->getNode();
		if (m_audio_player->is_playing() == true && playing_node != nullptr)
		{
			if (playing_node->m_component!=nullptr)
				playing_node->m_component->set_playcurpos(m_audio_player->get_position());
		}
		m_selected_node = m_active_node_view->getSelectedNode();
		repaint();
	}
	if (id == 100)
	{
		deleteUnusedTemporaryFiles();
	}
}

void MainContentComponent::deleteUnusedTemporaryFiles()
{
	return;
#ifdef GC_CDP_TEMPFILES
    // Let's not potentially mess things up while things are rendering. Obviously
	// running this temp files deletion function should be possible while rendering too,
	// but for now this check and bail out shall suffice...
	if (m_active_node_view->getGraphRenderState() == GraphRenderState::Rendering)
		return;
	auto tempfiles = get_temp_files();
	Logger::writeToLog("Deleting temporary files...");
	auto nodes = m_active_node_view->getNodes();
	std::set<String> used_files;
	for (auto& e : nodes)
	{
		for (auto& fn : e->get_output_filenames())
		{
			used_files.insert(fn);
		}
	}
	for (auto& fn : tempfiles)
	{
		if (used_files.count(fn) == 0)
			Logger::writeToLog(fn + " is no longer used, could be deleted");
	}
#endif
}

String MainContentComponent::startRender(bool blockcurrenthread)
{
	return m_active_node_view->renderSelected(blockcurrenthread);
}

String MainContentComponent::exportNodeToFile(std::shared_ptr<process_node> n, String fn)
{
	if (n == nullptr)
		return "No node";
	if (n->get_output_type() != "wav")
		return "Node does not produce wav output";
	if (startRender(true).isEmpty() == false)
		return "Error when rendering node";
	if (n->get_output_filenames().size() == 0)
		return "Node produced no files";
	return String();
}

void MainContentComponent::exportNodeToAutoNamedFile(std::shared_ptr<process_node> n)
{
	File destdir(g_propsfile->getValue("general/autoexportfolder"));
	if (destdir.exists() == false)
	{
		if (queryAutoNameExportFolder() == false)
			return;
	}
	String destdirpath = g_propsfile->getValue("general/autoexportfolder");
	String fn;
	for (int i = 0; i < 10000; ++i)
	{
		String temp = destdirpath+"/node_export_" + String(i) + ".wav";
		File file(temp);
		if (file.existsAsFile() == false)
		{
			fn = temp;
			break;
		}
	}
	if (fn.isEmpty() == false)
	{
		String err = exportNodeToFile(n, fn);
		if (err.isEmpty() == true)
		{
			File sourcefile(n->get_output_filenames()[0]);
			File exportfile(fn);
			if (sourcefile.copyFileTo(fn) == false)
			{
				AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
					"Error",
					"Could not copy file to export destination", "OK",
					this);
			} else
            {
                m_renderstatuslabel.setText("Exported "+fn,dontSendNotification);
            }
		}
		else
		{
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
				"Error",
				err, "OK",
				this);
		}
	}
	else
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error",
			"Could not generate output file name", "OK",
			this);
	}
	
	
	
}

void MainContentComponent::exportNodeToFilePromptingName(std::shared_ptr<process_node> n)
{
	if (n == nullptr)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error",
			"No source node", "OK",
			this);
		return;
	}
	if (n->get_output_type() != "wav")
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error",
			"Node does not produce wav output", "OK",
			this);
		return;
	}
	File initdir = File(g_propsfile->getValue("general/last_node_export_dir"));
	FileChooser myChooser("Export node",
		initdir,
		"*.wav");
	if (myChooser.browseForFileToSave(true) == false)
		return;
	String fn = myChooser.getResult().getFullPathName();
	String err = exportNodeToFile(n, fn);
	if (err.isEmpty() == true)
	{
		File sourcefile(n->get_output_filenames()[0]);
		File exportfile(fn);
		g_propsfile->setValue("general/last_node_export_dir", exportfile.getParentDirectory().getFullPathName());
		if (sourcefile.copyFileTo(fn) == false)
		{
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
				"Error",
				"Could not copy file to export destination", "OK",
				this);
		}
	}
	else
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error",
			err, "OK",
			this);
	}
}

#ifdef IS_REAPER_EXTENSION_PLUGIN
void MainContentComponent::exportNodeToReaperTimeLine(node_ref n)
{
	auto h = n->get_state_hash();
	char buf[2048] = { 0 };
	GetProjectPath(buf, 2048);
	String outfn(String(CharPointer_UTF8(buf))+"/"+h.toHexString()+".wav");
	Logger::writeToLog(outfn);
	String r = exportNodeToFile(n, outfn);
	if (r.isEmpty())
	{
		if (n->get_output_filenames().size() == 0)
			return;
		File sourcefile(n->get_output_filenames()[0]);
		File exportfile(outfn);
		if (sourcefile.copyFileTo(outfn) == false)
		{
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
				"Error",
				"Could not copy file to export destination", "OK",
				this);
			return;
		}
		InsertMedia(outfn.toRawUTF8(), 0);
	}
	else
		Logger::writeToLog(r);
}
#endif

bool MainContentComponent::queryAutoNameExportFolder()
{
	FileChooser chooser("Choose folder where to export nodes");
	bool result = chooser.browseForDirectory();
	if (result == true)
	{
		g_propsfile->setValue("general/autoexportfolder", chooser.getResult().getFullPathName());
		return true;
	}
	return false;
}

void MainContentComponent::buttonClicked(Button* b)
{
	if (b == m_render_button.get())
	{
		startRender();
	}
    if (b == m_show_param_manager.get())
    {
        PopupMenu menu;
        menu.addItem(1,"Preferences...",true, false);
        menu.addItem(2,"About...",true, false);
        int r = menu.show();
        if (r == 1)
        {
            showPreferences();
        }
        if (r == 2)
        {
            showAboutScreen();
        }
        //juce::Rectangle<int> area(5, 5, 10, 10);
        //ParameterManager* parcomponent = new ParameterManager(&m_test_node_view->getNodes());
        // /*CallOutBox& callobox = */ CallOutBox::launchAsynchronously(parcomponent, area, this);
        
    }
}

void MainContentComponent::paint (Graphics& g)
{
	g.fillAll(Colours::lightgrey);
	
}

void MainContentComponent::save_document(String fn)
{
	if (fn.isEmpty() == true)
	{
		File initdir = File(g_propsfile->getValue("general/last_graph_save_dir"));
		FileChooser myChooser("Save graph",
			initdir,
			"*.cdpfeproject");
		if (myChooser.browseForFileToSave(true) == false)
			return;
		fn = myChooser.getResult().getFullPathName();
	}
	File file(fn);
	m_active_node_view->saveGraphToXmlFile(file);
	g_propsfile->setValue("general/last_graph_save_dir", file.getParentDirectory().getFullPathName());
	m_current_doc_fn = fn;
	m_document_dirty = false;
	updateWindowTitle();
    m_cur_doc_last_saved = file.getLastModificationTime();
}

void MainContentComponent::checkIfDocumentModifiedOutside()
{
    File file(m_current_doc_fn);
    if (file.existsAsFile()==true && file.getLastModificationTime()>m_cur_doc_last_saved)
    {
        //Logger::writeToLog("Document has been edited outside application");
        m_cur_doc_last_saved = file.getLastModificationTime();
        int result = AlertWindow::showOkCancelBox(AlertWindow::WarningIcon, "CDP", "Document changed outside application.\nDo you want to load the changed version?","Yes","No");
        if (result == 1)
        {
            Logger::writeToLog("Gonna load the document...");
            m_active_node_view->loadGraphFromXmlFile(file);
            repaint();
            m_document_dirty = false;
            updateWindowTitle();
        }
        
    }
}

void MainContentComponent::deferredInit()
{
	if (m_active_node_view != nullptr)
		m_active_node_view->addDefaultNodes();
}

void MainContentComponent::setParameterLinker(ParameterLinker * linker)
{
	m_param_linker = linker;
}

void MainContentComponent::test_load()
{
	String result = m_active_node_view->load_graph_query_filename();
	File temp(result);
	if (temp.existsAsFile() == true)
	{
		m_current_doc_fn = result;
        m_cur_doc_last_saved = temp.getLastModificationTime();
		m_document_dirty = false;
		updateWindowTitle();
    }
}

void MainContentComponent::resized()
{
	if (m_main_layout.items.size() > 0)
	{
		m_main_layout.performLayout(juce::Rectangle<int>{0,0,getWidth()-10, getHeight()});
		m_inspec_nodeview_layout.performLayout(0, 50, getWidth(), getHeight()-70);
		// Ensure the nodeview fills the scrollable viewport
		if (m_active_node_view->getWidth() < m_node_view_port.getWidth() ||
			m_active_node_view->getHeight() < m_node_view_port.getHeight())
		{
			m_active_node_view->setBounds(0, 0, m_node_view_port.getWidth(), m_node_view_port.getHeight());
		}
	}
}


