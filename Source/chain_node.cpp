#include "chain_node.h"
#include "node_factory.h"
#include "node_component.h"
#include "node_view.h"

chain_process_node::chain_process_node(String procname)
{
    m_proc_name = procname;
    m_type_name = procname;
    
    // Stretch Time - 1
    //m_user_nodes.push_back(g_node_factory->create_from_name("Modify Radical - 1 Reverse"));
    //m_user_nodes.push_back(g_node_factory->create_from_name("Valhalla"));
	m_user_nodes.push_back(node_factory::instance()->create_from_name("Distort Repeat - Timestretch soundfile"));
	m_user_nodes.push_back(node_factory::instance()->create_from_name("Blur Blur"));
	//m_user_nodes.push_back(g_node_factory->create_from_name("Stretch Time - 1"));
	//m_user_nodes.push_back(g_node_factory->create_from_name("Distort Average - average"));
	m_user_nodes.push_back(node_factory::instance()->create_from_name("Modify Radical - 1 Reverse"));
	m_user_nodes.push_back(node_factory::instance()->create_from_name("Valhalla"));
    
	//m_user_nodes.push_back(g_node_factory->create_from_name("Modify Radical - 1 Reverse"));
	//m_user_nodes.push_back(g_node_factory->create_from_name("Modify Speed - 6 Add vibrato"));
	// 
}

void chain_process_node::buildInternalGraph()
{
	NodeView* view = nullptr;
	for (auto& e : m_active_editors)
	{
		if ((bool)e->getProperties()["is_in_node_component"] == true)
		{
			view = e->m_node_view.get();
			break;
		}
	}
	if (view == nullptr)
		Logger::writeToLog("nodeview null");
	//if (m_sources.size()>0 && m_sources[0]->m_component!=nullptr)
	//	view = m_sources[0]->m_component->m_node_view;
    for (auto& e : m_internal_nodes)
        for (auto& f : e)
        {
            disconnect_node_from_graph(f);
            if (view!=nullptr)
				view->removeNodeComponent(f.get());
        }
    m_internal_nodes.clear();
	if (m_sources.size() == 0)
		return;
	auto theProcessErrorFunc = [](process_node* node)
	{
		Logger::writeToLog("Chain node error callback " + node->get_name() + " " + node->get_error_text());
	};
	auto theProcessFinishedFunc = [](process_node* node)
	{
		Logger::writeToLog("Chain node finished callback " + node->get_name());
	};
	double bench_t0 = Time::getMillisecondCounterHiRes();
	// fake in chans here because for testing the input file node is used, 
	// would really need to get actual number of in chans
	int orignumchans = m_sources[0]->getNumOutChansForNumInChans(0);
	int numsplits = 1;
	int numchanspersplit = orignumchans;
	bool is_split = false;
	String curformat = m_sources[0]->get_output_type();
	Logger::writeToLog("Chain node has " + String(orignumchans) + " channels at start");
	//m_internal_nodes.emplace_back(node_ref_vec({m_sources[0]}));
	std::vector<int> chanspernodevec;
	//chanspernodevec.push_back(numchanspersplit);
	for (int i = 0; i < m_user_nodes.size(); ++i)
	{
		int outchans = m_user_nodes[i]->getNumOutChansForNumInChans(orignumchans);
		if (outchans == 0)
		{
			Logger::writeToLog("zero output chans for " + m_user_nodes[i]->get_name() + ", aborting");
			return;
		}
		//Logger::writeToLog(m_user_nodes[i]->get_name() + " produces " + String(outchans) + " output chans for " + String(numchans) + " input chans");
		if (curformat == "ana" && m_user_nodes[i]->get_output_type() == "wav")
		{
			Logger::writeToLog("Format was ana, now switching to wav by creating pvoc resynths");
			node_ref_vec pvoc_synths;
			for (int j = 0; j < numsplits; ++j)
			{
				auto pvresynth = node_factory::instance()->create_from_name("PVOC RESYNTH");
				if (pvresynth == nullptr)
					Logger::writeToLog("Could not create pvoc resynth");
				pvoc_synths.emplace_back(pvresynth);
			}
			m_internal_nodes.emplace_back(pvoc_synths);
			chanspernodevec.push_back(numchanspersplit);
			curformat = "wav";
		}
		if (numsplits > 1 && m_user_nodes[i]->getNumOutChansForNumInChans(orignumchans) == orignumchans)
		{
			Logger::writeToLog("Could now merge " + String(numsplits) + " chans for processing with " + m_user_nodes[i]->get_name());
			auto merge_node = node_factory::instance()->create_from_name("Interleave mono files");
			m_internal_nodes.emplace_back(node_ref_vec{ merge_node });
			numchanspersplit = orignumchans;
			outchans = orignumchans;
			numsplits = 1;
			chanspernodevec.push_back(numchanspersplit);
			is_split = false;
		}
		if (orignumchans>outchans)
		{
			numsplits = (orignumchans / outchans) + (orignumchans % outchans);
			//numchans/outchans;
			numchanspersplit = outchans;
			if (is_split == false)
			{
				
				Logger::writeToLog("split into " + String(numsplits) + " chains of " + String(numchanspersplit) + " channels here");
				node_ref_vec splitters;
				for (int j = 0; j < numsplits; ++j)
				{
					auto chorder = node_factory::instance()->create_from_name("Chorder - Reorder soundfile channels");
                    chorder->set_parameter(0, String::charToString(j+97));
					splitters.emplace_back(chorder);
				}
				m_internal_nodes.emplace_back(splitters);
				chanspernodevec.push_back(numchanspersplit);
				is_split = true;
			}
			if (curformat == "wav" && m_user_nodes[i]->get_output_type() == "ana")
			{
				Logger::writeToLog("Adding pvoc anal nodes here");
				node_ref_vec pvocnodes;
				for (int j = 0; j < numsplits; ++j)
				{
					// PVOC ANAL � Create PVOC analysis file.
					auto pvnode = node_factory::instance()->create_from_name("Create PVOC analysis file");
					if (pvnode == nullptr)
						Logger::writeToLog("Could not create pvoc anal");
					pvocnodes.emplace_back(pvnode);
				}
				m_internal_nodes.emplace_back(pvocnodes);
				chanspernodevec.push_back(numchanspersplit);
				curformat = "ana";
			}
			node_ref_vec duplnodes;
			for (int j = 0; j < numsplits; ++j)
			{
				auto dupl = node_factory::instance()->duplicate_node(m_user_nodes[i], false);
				duplnodes.emplace_back(dupl);
			}
			m_internal_nodes.emplace_back(duplnodes);
			chanspernodevec.push_back(numchanspersplit);
		}
		else
		{
			if (curformat == "wav" && m_user_nodes[i]->get_output_type() == "ana")
			{
				Logger::writeToLog("Adding pvoc ana node for mono");
				m_internal_nodes.emplace_back(node_ref_vec{ node_factory::instance()->create_from_name("Create PVOC analysis file") });
				chanspernodevec.push_back(numchanspersplit);
				curformat = "ana";
			}
			m_internal_nodes.emplace_back(node_ref_vec{ m_user_nodes[i] });
			chanspernodevec.push_back(numchanspersplit);
		}
		if (i == m_user_nodes.size() - 1)
		{
			if (curformat == "ana")
			{
				Logger::writeToLog("At chain end with ana format, have to insert resynths");
				node_ref_vec pvoc_synths;
				for (int j = 0; j < numsplits; ++j)
				{
					auto pvresynth = node_factory::instance()->create_from_name("PVOC RESYNTH");
					if (pvresynth == nullptr)
						Logger::writeToLog("Could not create pvoc resynth");
					pvoc_synths.emplace_back(pvresynth);
				}
				m_internal_nodes.emplace_back(pvoc_synths);
				chanspernodevec.push_back(numchanspersplit);
				curformat = "wav";
			}
		}
		if (i == m_user_nodes.size() - 1 && numsplits > 1)
		{
			Logger::writeToLog("At chain end, have to interleave " + String(numsplits) + " chans");
			auto merge_node = node_factory::instance()->create_from_name("Interleave mono files");
			m_internal_nodes.emplace_back(node_ref_vec{ merge_node });
			chanspernodevec.push_back(orignumchans);
		}
		
	}
	for (int i = 0; i < m_internal_nodes[0].size(); ++i)
		connect_nodes(m_sources[0], m_internal_nodes[0][i], true);
	int i = 0;
	for (auto& e : m_internal_nodes)
	{
		String txt(i);
		txt += "  ";
		int j = 0;
		if (i < m_internal_nodes.size()-1)
		{
			int curnumnodes = m_internal_nodes[i].size();
			int nextnumnodes = m_internal_nodes[i + 1].size();
			int curnumchanspernode = chanspernodevec[i];
			int nextnumchanspernode = chanspernodevec[i + 1];
			if (curnumnodes == nextnumnodes)
			{
				for (int k = 0; k < curnumnodes; ++k)
					connect_nodes(m_internal_nodes[i][k], m_internal_nodes[i + 1][k], true);
			}
			if (curnumnodes < nextnumnodes)
			{
				for (int k0 = 0; k0 < nextnumnodes; ++k0)
				{
					int cur_index = k0 / curnumchanspernode;
					connect_nodes(m_internal_nodes[i][cur_index], m_internal_nodes[i + 1][k0], true);
				}
			}
			if (curnumnodes > nextnumnodes)
			{
				for (int k0 = 0; k0 < curnumnodes; ++k0)
				{
					int next_index = k0 / nextnumchanspernode;
					if (next_index<m_internal_nodes[i+1].size())
						connect_nodes(m_internal_nodes[i][k0], 
							m_internal_nodes[i + 1][next_index], true);
				}
			}
		}
		for (auto& f : e)
		{
			txt += f->getShortName() + " ";
			f->ProcessFinishedFunc = theProcessFinishedFunc;
			f->ProcessErrorFunc = theProcessErrorFunc;
			f->set_idle();
			if (view!=nullptr)
				view->add_node_to_view(f, j * 100, 50+i * 50, 95, 30);
			++j;
		}
		//Logger::writeToLog(txt);
		++i;
	}
	double bench_t1 = Time::getMillisecondCounterHiRes();
	Logger::writeToLog("building internal graph took " + String(bench_t1 - bench_t0) + " ms");

}

process_node::CustomGUIType chain_process_node::getCustomEditorType2()
{
	return process_node::InspectorCustomEditor;
}

Component * chain_process_node::getCustomEditor()
{
	chain_process_node_component* comp = new chain_process_node_component(this);
	comp->getProperties().set("maxh", -1);
	return comp;
}

void chain_process_node::connections_changed()
{
    buildInternalGraph();
}

StringArray chain_process_node::get_output_filenames()
{
    return StringArray{m_out_fn};
}

String chain_process_node::get_supported_input_type()
{
    return "wav";
}

String chain_process_node::get_output_type()
{
    return "wav";
}

void chain_process_node::tick()
{
    tick_sources();
	if (m_internal_nodes.size() == 0)
	{
		m_pstate = FinishedWithError;
		m_error_text = "No internal nodes";
		if (ProcessErrorFunc)
			ProcessErrorFunc(this);
		return;
	}
	if (m_pstate == Busy && m_internal_nodes.back()[0]->get_processing_state()==FinishedOK)
    {
        m_pstate = FinishedOK;
        m_out_fn = m_internal_nodes.back()[0]->get_output_filenames()[0];
        TempFileContainer::instance().add(m_out_fn);
        setAudioFileToPlay(m_out_fn);
        if (ProcessFinishedFunc)
            ProcessFinishedFunc(this);
        return;
    }
    if (are_sources_finished()==SourcesState::NotFinished)
        return;
    if (m_pstate == Idle)
    {
        m_pstate = Busy;
    }
    if (m_pstate == Busy && m_internal_nodes.back()[0]->get_processing_state()!=FinishedOK)
    {
        m_internal_nodes.back()[0]->tick();
        int errcnt = 0;
		for (auto& e : m_internal_nodes)
		{
			for (auto& f : e)
				if (f->get_processing_state() == FinishedWithError)
					++errcnt;
		}
        if (errcnt>0)
        {
            m_pstate = FinishedWithError;
            m_error_text = "One or more chain nodes failed processing";
            if (ProcessErrorFunc)
                ProcessErrorFunc(this);
        }
    }

}

MD5 chain_process_node::extra_state_hash()
{
    return MD5();
}

chain_process_node_component::chain_process_node_component(chain_process_node* node)
    : m_node(node)
{
    addAndMakeVisible(&m_listbox);
    m_listbox.setModel(this);
	m_node_view = std::make_unique<NodeView>(nullptr, nullptr);
	addAndMakeVisible(m_node_view.get());
    //setOpaque(true);
	m_node->m_active_editors.push_back(this);
}

chain_process_node_component::~chain_process_node_component()
{
	for (int i = 0; i < m_node->m_active_editors.size(); ++i)
	{
		if (m_node->m_active_editors[i] == this)
		{
			m_node->m_active_editors.erase(m_node->m_active_editors.begin() + i);
			break;
		}
	}
}

int chain_process_node_component::getNumRows()
{
    return (int)m_node->m_user_nodes.size();
}

void chain_process_node_component::paintListBoxItem (int rowNumber, Graphics &g, int width, int height, bool rowIsSelected)
{
    if (rowNumber>=0 && rowNumber<m_node->m_user_nodes.size())
    {
        //g.fillAll(Colours::white);
        if (rowIsSelected==false)
            g.fillAll(Colours::grey);
        else g.fillAll(Colours::blue);
        g.setColour(Colours::white);
        String txt = m_node->m_user_nodes[rowNumber]->get_name();
        g.drawText(txt, 0, 0, width, height, Justification::left);
    }
}

void chain_process_node_component::paint(Graphics& g)
{
	return;
	if (m_cur_drag_row == -1)
        return;
    g.setColour(Colours::red);
    float ycor = m_cur_drag_row * m_listbox.getRowHeight();
    g.drawLine(0.0f, ycor, getWidth(),ycor,2.0f);
}

void chain_process_node_component::resized()
{
	int w = getWidth();
	m_listbox.setBounds(0,0, 150, getHeight());
	m_node_view->setBounds(155, 0, w - 160, getHeight());
}


bool chain_process_node_component::isInterestedInDragSource(const SourceDetails &dragSourceDetails)
{
    if (int(dragSourceDetails.description) >= 0)
        return true;
    return false;
}

void chain_process_node_component::itemDragMove(const SourceDetails &dragSourceDetails)
{
    return;
    int new_index = m_listbox.getInsertionIndexForPosition(dragSourceDetails.localPosition.getX(), dragSourceDetails.localPosition.getY());
    Logger::writeToLog("Drag row "+String(new_index));
    m_cur_drag_row = new_index;
    repaint();
}

void chain_process_node_component::itemDropped(const SourceDetails &dragSourceDetails)
{
    int new_index = m_listbox.getInsertionIndexForPosition(dragSourceDetails.localPosition.getX(), dragSourceDetails.localPosition.getY());
    if (new_index >= 0)
    {
        int old_index = dragSourceDetails.description;
        auto f = m_node->m_user_nodes.begin() + old_index;
        auto l = m_node->m_user_nodes.begin() + old_index + 1;
        auto p = m_node->m_user_nodes.begin() + new_index;
        slide(f, l, p);
        m_node->connections_changed();
        m_listbox.updateContent();
        repaint();
        m_cur_drag_row = -1;
    }
}

var chain_process_node_component::getDragSourceDescription(const SparseSet< int > &currentlySelectedRows)
{
    if (currentlySelectedRows.size() > 0)
        return currentlySelectedRows[0];
    return -1;
}
