#pragma once

#include <memory>
#include "../JuceLibraryCode/JuceHeader.h"
#include "routingmatrix.h"
#include <map>
#include <mutex>

enum class MouseAction
{
    None,
    Drag,
    DragWithControlModifier,
    MouseDown,
    MouseUp
};


inline double average(double a, double b)
{
    return (a+b)/2;
}

template<typename T>
inline T* varToPointer(var v)
{
	return (T*)(int64)v;
}

template<typename I>
inline std::pair<I, I> slide(I f, I l, I p)
{
	if (p < f) return { p, std::rotate(p,f,l) };
	if (l < p) return { std::rotate(f,l,p), p };
	return { f, l };
}

template<typename Cont, typename T, typename F>
inline void execute_for_first_found(const Cont& cont, const T& tofind, F&& action)
{
	for (const auto& e : cont)
		if (e == tofind)
		{
			action(e);
			break;
		}
}

inline void remove_file_if_exists(String fn)
{
	File file(fn);
	if (file.exists() == true)
	{
		file.deleteFile();
	}
}

struct audio_source_info
{
	int num_channels = 0;
	int samplerate = 0;
	int64_t m_length_frames = 0;
	int num_bits = 0;
	bool is_floating_point = false;
	double get_length_seconds() const
	{
		if (m_length_frames == 0 || samplerate == 0)
			return 0.0;
		return (double)m_length_frames / samplerate;
	}
	String m_format_name;
};

class AudioFileInfoCache
{
public:
	AudioFileInfoCache() {}
	static AudioFileInfoCache* instance();
	audio_source_info getInfo(String fn, bool bypasscache=false);
	static audio_source_info get(String fn);
	void refreshAllInfos();
	void refreshInfoFor(String fn);
	void clearAllInfos();
	int size() const { return m_info_cache.size(); }
private:
	std::map<String, audio_source_info> m_info_cache;
	std::mutex m_mutex;
};

audio_source_info get_audio_source_info(String fn);
audio_source_info get_audio_source_info_cached(String fn);

template<typename Cont, typename F>
inline void remove_from_container(Cont& container, F predicate)
{
	container.erase(std::remove_if(container.begin(), container.end(), predicate), container.end());
}

template<typename Cont, typename T>
inline void addToVectorIfNotAlreadyThere(Cont& c, T&& x)
{
	if (std::count(c.begin(),c.end(),x)==0)
		c.emplace_back(x);
}

class juce_audio_file
{
public:
	juce_audio_file() {}
	juce_audio_file(String fn, AudioFormatManager* mgr)
	{
		m_file = new File(fn);
		m_reader = mgr->createReaderFor(*m_file);
		if (m_reader != nullptr)
		{
			m_source = new AudioFormatReaderSource(m_reader, true);
		}
		else Logger::writeToLog("Could not create audio file of " + fn);
	}
	~juce_audio_file()
	{
		delete m_source;
		delete m_file;
	}
	juce_audio_file(const juce_audio_file&) = delete;
	juce_audio_file& operator=(const juce_audio_file&) = delete;
	juce_audio_file(juce_audio_file&& other)
	{
		m_file = other.m_file;
		other.m_file = nullptr;
		m_source = other.m_source;
		other.m_source = nullptr;
		m_reader = other.m_reader;
		other.m_reader = nullptr;
	}
	juce_audio_file& operator=(juce_audio_file&& other)
	{
		std::swap(m_file, other.m_file);
		std::swap(m_source, other.m_source);
		std::swap(m_reader, other.m_reader);
		return *this;
	}
	AudioFormatReaderSource* get_source() const { return m_source; }
	AudioFormatReader* get_reader() const { return m_reader; }
	File* get_file() const { return m_file; }
	void reset()
	{
		delete m_source;
		m_source = nullptr;
		delete m_file;
		m_file = nullptr;
	}

private:
	File* m_file = nullptr;
	AudioFormatReader* m_reader = nullptr;
	AudioFormatReaderSource* m_source = nullptr;
};



// Returns valid File if the operation succeeded
File convertAudioFileToWav(const File& infile, String outfilename, int numchans, double new_sr, int bitdepth,
                           routing_matrix rm=routing_matrix());

// Returns false immediately if starting the operation failed.
// The completion callback is called with a valid File if the operation succeeded and an invalid File
// if something went wrong
bool convertAudioFileToWavAsync(const File& infile, String outfilename, int numchans, double new_sr, int bitdepth, routing_matrix rm,
                                std::function<void(File)> completion_callback);

class TempFileContainer
{
public:
    TempFileContainer() {}
    void add(File f)
    {
        m_files.push_back(f);
        m_files_size+=f.getSize();
    }
    void remove(File f)
    {
        for (int i=0;i<m_files.size();++i)
        {
            if (m_files[i]==f)
            {
                m_files.erase(m_files.begin()+i);
                m_files_size-=f.getSize();
                return;
            }
        }
    }
    int64_t getSizeOfFiles() const { return m_files_size; }
    size_t getNumFiles() const { return m_files.size(); }
    int deleteFiles()
    {
        int deletefailcount = 0;
        for (auto& e : m_files)
        {
            if (e.deleteFile()==false)
            {
                ++deletefailcount;
            }
        }
        return deletefailcount;
    }
    static TempFileContainer& instance()
    {
        static TempFileContainer s_instance;
        return s_instance;
    }
private:
    std::vector<File> m_files;
    int64_t m_files_size = 0;
};

