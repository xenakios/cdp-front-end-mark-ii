#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <functional>

void closePresetsSqlite();

class PresetComponent : public Component, public ComboBox::Listener, 
	public Button::Listener
{
public:
	PresetComponent(String presettype);
	~PresetComponent();
	void comboBoxChanged(ComboBox* cb) override;
	void buttonClicked(Button* but) override;
	void resized() override;
	std::function<ValueTree(void)> GetPresetValueTree;
	std::function<void(ValueTree)> OnPresetChanged;
	String m_preset_type;
	ComboBox m_combo;
private:
	
	TextButton m_menu_but;
	
	void storePreset(String name);
	void removePreset(String name);
	void renamePreset(String oldname, String newname);
	void updatePreset(String name);
	void initComboFromDatabase();
};


