#include "lua_helpers.h"
#include <mutex>

extern std::unique_ptr<PropertiesFile> g_propsfile;

using mutlocker = std::lock_guard<std::mutex>;

String get_cdp_lua_defs_file()
{
	File luafile(g_propsfile->getValue("general/lua_defs_fn"));
	if (luafile.exists())
	{
		return luafile.getFullPathName();
	}
	File exedir = File::getSpecialLocation(File::currentExecutableFile).getParentDirectory();
	return exedir.getFullPathName() + "/CDP_definitions.lua";
}
