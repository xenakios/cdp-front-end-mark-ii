#pragma once

#include <vector>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <thread>
#include <mutex>
#include <future>
#include "../JuceLibraryCode/JuceHeader.h"
#include "jcdp_utilities.h"
#include "jcdp_envelope.h"
#include "childprocess_helpers.h"

class process_node;
class WDL_Resampler;

enum class GraphRenderState
{
	Idle,
	Rendering,
	FinishedOK,
	FinishedError,
	Unknown
};

inline bool does_file_exist(String fn)
{
	File temp(fn);
	return temp.existsAsFile();
}

String query_cdp_binaries_location();
String query_cdp_render_location();
File queryAudioFile();
bool is_valid_cdp_binaries_location(String dir);
String get_cdp_bin_location();

#ifndef WIN32
  #define CDP_DONT_GET_EXIT_CODE
#endif

// Hack needed to work around Juce's getExitCode bug in OS-X...
String cdp_process_result(ChildProcess& proc);

inline String add_quotes_to_filename_if_needed(String fn)
{
	if (fn.containsChar(' ') == true)
	{
#ifdef WIN32
        return "\"" + fn + "\"";
#else
        return "\"" + fn + "\"";
#endif
	}
	return fn;
}

inline int& get_num_used_processes_ref()
{
	static int count = 0;
	return count;
}


template<typename T>
inline void combine_hash(size_t& seed, const T& x)
{
	// Borrowed from Boost
	seed ^= hash_helper(x) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

String get_output_folder();

enum class node_io_format
{
	Invalid,
	Internal_Buffer,
	PCM_Audio,
	PVOC_Analysis,
	Binary_Breakpoints,
	Text_Breakpoints,
	CDP_Score,
	Lua_Table
};

class node_io_pin
{
public:
	node_io_pin() {}
	node_io_pin(int numchans) : m_num_chans(numchans) {}
private:
	int m_num_chans = 0;
	// String m_name;
};

class node_io_bus
{
public:
	node_io_bus() {}
	node_io_bus(String signal_format, bool is_input, int maxsignals, String name = String()) :
		m_signal_type(signal_format), m_is_input(is_input), m_max_signals(maxsignals), m_name(name) {}
	bool isInput() const { return m_is_input; }
	bool isFormatCompatibleWith(const node_io_bus& other) const
	{
		return other.m_signal_type == m_signal_type;
	}
	void setNumPins(int sigcount)
	{
		if (sigcount >= 0 && sigcount < 100000)
		{
			m_pins.resize(sigcount);
		}
	}
	void setPinNumChannels(int pin_index, int chan_count)
	{
		if (pin_index >= 0 && pin_index < m_pins.size())
		{
			m_pins[pin_index] = node_io_pin(chan_count);
		}
	}
	int getNumPins() const { return m_pins.size(); }
	const String& getSignalFormat() const { return m_signal_type; }
	const String& getName() const { return m_name; }
	void setName(String name) { m_name = name; }
private:
	bool m_is_input = false; 
	// hmm...maybe should finally start using an enum for these. but for now for compatibility, use a string identifier
	String m_signal_type; 
	std::vector<node_io_pin> m_pins;
	int m_max_signals = 0;
	String m_name;
};

class node_buses_layout
{
public:
	node_buses_layout() {}
	void addInputBus(String format)
	{
		if (m_input_buses.size() > 0)
		{
			if (format != m_input_buses.back().getSignalFormat())
			{
				m_input_buses.emplace_back(format, true, 0, format+" input bus");
			}
		}
		else
		{
			m_input_buses.emplace_back(format, true, 0, format+" input bus");
		}
	}
	void addOutputBus(String format)
	{
		m_output_buses.emplace_back(format, false, 0, format+" output bus");
	}
	int getNumInputBuses() const { return m_input_buses.size(); }
	int getNumOutputBuses() const { return m_output_buses.size(); }
	node_io_bus* getInputBusMutable(int index)
	{
		if (index>=0 && index<m_input_buses.size())
			return &m_input_buses[index];
		return nullptr;
	}
	node_io_bus* getOutputBusMutable(int index)
	{
		if (index >= 0 && index<m_output_buses.size())
			return &m_output_buses[index];
		return nullptr;
	}
private:
	std::vector<node_io_bus> m_input_buses;
	std::vector<node_io_bus> m_output_buses;
};

enum class parameterType
{
	Unknown,
	FloatingPoint,
	Integer,
	Bool,
	Choice,
	String,
	StringFile,
	FileName,
	Custom
};

class node_parameter
{
public:
	node_parameter() {}
	node_parameter(String name, String argtoken,var minvalue, var maxvalue, var defaultvalue, bool automatable, double stepsize, double skew)
		: m_name(name),m_value(defaultvalue), m_min_value(minvalue), m_max_value(maxvalue), m_can_be_automated(automatable),
		m_step_size(stepsize), m_skew(skew), m_arg_token(argtoken), m_default_value(defaultvalue)
	{
		m_param_type = parameterType::FloatingPoint;
	}
	node_parameter(String name, String argtoken, var defaultvalue, StringArray combo_entries, Array<var> combo_values)
		: m_name(name), m_arg_token(argtoken), m_value(defaultvalue), m_choice_strings(combo_entries), m_choice_values(combo_values)
	{
		m_param_type = parameterType::Choice;
	}
	node_parameter(String name, String argtoken, var defaultvalue) :
		m_name(name), m_arg_token(argtoken), m_value(defaultvalue) {}
	node_parameter(bool, String name, String argtoken, bool defaultvalue) :
		m_name(name), m_arg_token(argtoken), m_value(defaultvalue), m_default_value(defaultvalue) {}
	
	//node_parameter(node_parameter&&) = delete;
	//node_parameter& operator = (node_parameter&&) = delete;
	parameterType m_param_type = parameterType::Unknown;
	
	Value m_default_value;
	Value m_min_value;
	Value m_max_value;
	bool m_custom_range_set = false;
	String m_name;
	String m_arg_token;
	StringArray m_choice_strings;
	Array<var> m_choice_values;
	double m_step_size = 0.0;
	double m_skew = 1.0;
	bool m_sym_skew = false;
	bool m_midpoint_skew = false;
	double m_midpoint = 0.0;
	bool m_can_be_automated = false;
	bool m_automation_enabled = false;
	std::function<void(node_parameter& param, double)> FileLengthChanged;
	String m_lua_argument;
	std::shared_ptr<breakpoint_envelope> get_envelope(bool init_default_nodes=true);
	bool hasEnvelope() const { return m_envelope != nullptr; }
	int m_envelope_change_count = 0;
	double m_env_time_scale = 1.0;
	process_node* m_parent_node = nullptr;
	String m_control_type;
    int m_gui_order = -1;
	String m_tip_text;
	bool m_enabled_in_gui = true;
    var getValue() const { return m_value.getValue(); }
	void setValue(var v);
    void addValueListener(Value::Listener* listener)
    {
        m_value.addListener(listener);
    }
    void removeValueListener(Value::Listener* listener)
    {
        m_value.removeListener(listener);
    }
    bool refersToSameSourceAs(Value& v)
    {
        return m_value.refersToSameSourceAs(v);
    }
private:
	Value m_value;
    std::shared_ptr<breakpoint_envelope> m_envelope;
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(node_parameter)
};

std::unique_ptr<Slider> make_parameter_helper_slider(node_parameter* p);

String generateBreakpointFile(node_parameter* parameter, double maxtime);

using parameter_ref = std::shared_ptr<node_parameter>;
using parameter_vector = std::vector<parameter_ref>;

inline NormalisableRange<double> make_range_from_parameter(node_parameter* p)
{
	return NormalisableRange<double>(p->m_min_value.getValue(), p->m_max_value.getValue(), 0.0, p->m_skew, p->m_midpoint_skew);
}

class NodeComponent;
class process_graph;

enum class TopoRenderState
{
    Idle,
    NeedsRendering,
    Rendering,
    FinishedOK,
    FinishedWithError
};

class process_node : public ActionBroadcaster
{
public:
	enum ProcessingState
	{
		Idle,
		Busy,
		FinishedOK,
		FinishedWithError
	};
	enum BypassMode
	{
		NotBypassed,
		Bypassed,
		BypassNotSupported
	};
	enum CustomGUIType
	{
		NoCustomEditor,
		InspectorCustomEditor,
		WindowCustomEditor
	};
	enum class SourcesState
	{
		NotFinished,
		FinishedOK,
		FinishedWithError
	};
	process_node();
	process_node(String procname);
	virtual ~process_node();
	BypassMode get_bypass_mode() const;
	virtual void set_bypass_mode(BypassMode mode);
	virtual void connections_changed() {}

	virtual void update_params();
	NodeComponent* m_component = nullptr;
	virtual bool usesCommandLine() const { return true; }

	virtual CustomGUIType getCustomEditorType2() { return NoCustomEditor; };
	virtual Component* getCustomEditor() { return nullptr; }

	std::vector<std::shared_ptr<process_node>> m_sources;
	std::vector<process_node*> m_destinations;
	virtual StringArray get_output_filenames() = 0;
	virtual String get_supported_input_type() = 0;
	virtual String get_output_type() = 0;
	ProcessingState get_processing_state() const;
	void set_idle();
	virtual void tick() = 0;
	String get_name() const { return m_proc_name; }
	void set_name(String name);
	virtual String getShortName();
	String get_type_name() const { return m_type_name; }
	virtual StringArray get_arguments();
	virtual void set_arguments(StringArray ar) {};
	virtual String get_error_text();
	std::function<void(process_node*)> ProcessFinishedFunc;
	std::function<void(process_node*)> ProcessErrorFunc;
	parameter_vector& get_parameters() { return m_parameters; }
	void set_parameter(int index, var v);
	
	ValueTree getStateValueTree();
	virtual ValueTree getAdditionalStateValueTree();
	void restoreStateFromValueTree(ValueTree tree);
	virtual void restoreAdditionalStateFromValueTree(ValueTree tree);
	MD5 get_state_hash();
	virtual MD5 extra_state_hash() { return MD5(); }
	double get_render_elapsed_milliseconds() const;
	int get_num_supported_input_files() const;
	int getNumSupportedInputChannels() const { return m_num_supported_input_channels; }
	int getCurrentNumOutputChannels() const { return m_num_current_output_channels; }
	virtual int getNumOutChansForNumInChans(int inchans) { return 0; }
	String getSourceFileName(int src, int index);

	virtual bool isOfflineOnly() { return true; }
	virtual void removeLastOutputFile() {}
    virtual void prepareToPlay(int expectedbufsize, int numchans, double sr);
    virtual void releaseResources();
    virtual void seek(double s);
	virtual void processAudio(float** buf, int numchans, int numframes, double sr);
	virtual double getPlaybackPosition();
	std::mutex* getPlaybackMutex() { return &m_playback_mutex; }
    
	NamedValueSet& get_dynamic_properties() { return m_dyn_props; }
    
	int getUniqueID() const { return m_unique_id; }
	void setUniqueID(int id) { m_unique_id=id; }
	
	void sendActionMessageTrampoline(const String& str);
    void setConnectionsChangedEnabled(bool b) { m_enable_connections_changed=b; }
    friend class node_factory;
    process_graph* m_owner_graph = nullptr;
	virtual void sourcesHaveChanged() {}
    
	// New render stuff
	
    TopoRenderState m_topo_render_state = TopoRenderState::Idle;
	// return true if already rendered with current settings
	virtual bool prepareToRender();
	virtual TopoRenderState performRender();
    int countReadySources();
	std::atomic<bool> m_test_thread_finished{ false };
    virtual bool supportsCancellation() { return false; }
    // -1.0 for indeterminate progress
    virtual double renderProgress() { return m_render_progress; }
    std::atomic<double> m_render_progress{-1.0};

	double getTailLength() const { return m_tail_length; };
	virtual void setTailLength(double len);

	bool isDirty() const { return m_is_dirty; }
	void setDirty(bool b) { m_is_dirty = b;  }

	node_buses_layout& getBussesLayout() { return m_buses_layout; }
protected:
	SharedChildProc m_cp;
	node_buses_layout m_buses_layout;
	StringArray m_arguments;
	String m_proc_name;
	String m_type_name;
    int m_unique_id=-1;
	bool m_is_dirty = false;
	parameter_vector m_parameters;
	NamedValueSet m_dyn_props;
	int m_num_supported_input_files = 1;
	int m_num_supported_input_channels = 0;
	int m_num_current_output_channels = 0;
	double m_tail_length = 0.0;
	void init_child_process();
	BypassMode m_bypassmode = BypassNotSupported;
	ProcessingState m_pstate = Idle;
	int m_iter_count = 0;
	double m_render_start_time = 0.0;
	double m_render_elapsed = 0.0;
	void start_benchmark();
	void end_benchmark();
	SourcesState are_sources_finished();
	void tick_sources();
	void output_errors();
	
    std::unique_ptr<MemoryMappedAudioFormatReader> m_mmap_reader;
    int64_t m_file_readpos = 0;
    std::unique_ptr<WDL_Resampler> m_resampler;
    std::vector<double> m_resampler_outbuf;
	std::mutex m_playback_mutex;
    String m_error_text;
    bool m_enable_connections_changed = true;
	void setAudioFileToPlay(String fn);
    virtual std::pair<String,String> initOutputFile(String prefix,String ext);
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(process_node)
};

using node_ref=std::shared_ptr<process_node>;
using node_ref_vec = std::vector<node_ref>;



inline bool does_connection_exist(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b)
{
	if (a == nullptr || b == nullptr)
		return false;
	auto it1 = std::find(b->m_sources.begin(), b->m_sources.end(), a);
	auto it2 = std::find(a->m_destinations.begin(), a->m_destinations.end(), b.get());
	return (it1 != b->m_sources.end()) && (it2 != a->m_destinations.end());
}

inline void connect_nodes(
	std::shared_ptr<process_node> a, 
	std::shared_ptr<process_node> b, 
	bool skipexistingcheck=false)
{
	if (a == nullptr || b == nullptr)
		return;
	if (skipexistingcheck == false && does_connection_exist(a, b) == true)
		return;
	//if (does_connection_exist(a, b) == false)
	{
		b->m_sources.push_back(a);
		b->connections_changed();
		a->m_destinations.push_back(b.get());
		a->connections_changed();
		b->setDirty(true);
	}
}

inline void has_feedback_recur(std::shared_ptr<process_node> node, 
							   std::unordered_set<std::shared_ptr<process_node>>& visited, bool& detected)
{
	if (detected == true)
		return;
	if (visited.count(node))
	{
		detected = true;
		return;
	}
	visited.insert(node);
	for (auto& e : node->m_sources)
		has_feedback_recur(e, visited, detected);
}

inline bool does_connection_have_feedback(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b)
{
	a->m_destinations.push_back(b.get());
	b->m_sources.push_back(a);
	std::unordered_set<std::shared_ptr<process_node>> visited;
	bool detected = false;
	has_feedback_recur(a, visited, detected);
	a->m_destinations.erase(a->m_destinations.end() - 1);
	b->m_sources.erase(b->m_sources.end() - 1);
	return detected;
}

inline void disconnect_nodes(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b)
{
	if (does_connection_exist(a, b) == false)
	{
		return;
	}
	a->m_destinations.erase(std::remove(a->m_destinations.begin(), a->m_destinations.end(), b.get()),
		a->m_destinations.end());
	a->connections_changed();
	b->m_sources.erase(std::remove(b->m_sources.begin(), b->m_sources.end(), a),
		b->m_sources.end());
	b->connections_changed();
}

inline void disconnect_nodes(std::shared_ptr<process_node> a, process_node* b)
{
	a->m_destinations.erase(std::remove(a->m_destinations.begin(), a->m_destinations.end(), b),
		a->m_destinations.end());
	a->connections_changed();
	b->m_sources.erase(std::remove(b->m_sources.begin(), b->m_sources.end(), a),
		b->m_sources.end());
	b->connections_changed();
}

inline void disconnect_node_inputs(std::shared_ptr<process_node> a)
{
	for (auto& e : a->m_sources)
	{
		auto it = std::remove(e->m_destinations.begin(), e->m_destinations.end(), a.get());
		if (it != e->m_destinations.end())
		{
			e->m_destinations.erase(it);
			e->connections_changed();
		}
	}
	a->m_sources.clear();
	a->connections_changed();
}

inline void disconnect_node_outputs(std::shared_ptr<process_node> a)
{
	for (auto& e : a->m_destinations)
	{
		auto it = std::remove(e->m_sources.begin(), e->m_sources.end(), a);
		if (it != e->m_sources.end())
		{
			e->m_sources.erase(it);
			e->connections_changed();
		}
	}
	a->m_destinations.clear();
	a->connections_changed();
}

inline void disconnect_node_from_graph(std::shared_ptr<process_node> a)
{
	if (a==nullptr)
        return;
    for (auto& e : a->m_sources)
	{
		auto it = std::remove(e->m_destinations.begin(), e->m_destinations.end(), a.get());
		if (it != e->m_destinations.end())
		{
			e->m_destinations.erase(it);
			e->connections_changed();
		}
	}
	for (auto& e : a->m_destinations)
	{
		auto it = std::remove(e->m_sources.begin(), e->m_sources.end(), a);
		if (it != e->m_sources.end())
		{
			e->m_sources.erase(it);
			e->connections_changed();
		}
	}
	a->m_destinations.clear();
	a->m_sources.clear();
	a->connections_changed();
}

inline void dep_resolve_det_circ(const std::shared_ptr<process_node>& node,
	std::vector<std::shared_ptr<process_node>>& result,
	std::unordered_set<std::shared_ptr<process_node>>& resolved,
	std::unordered_set<std::shared_ptr<process_node>>& unresolved)
{
	unresolved.insert(node);
	for (auto& edge : node->m_sources)
	{
		if (resolved.count(edge) == 0)
			//if (is_in(resolved,edge)==false)
		{
			if (unresolved.count(edge)>0)
				//if (is_in(unresolved,edge)==true)
				throw std::invalid_argument("Graph circular dependency detected");
			dep_resolve_det_circ(edge, result, resolved, unresolved);
		}
	}
	resolved.insert(node);
	result.emplace_back(node);
	unresolved.erase(node);
}

inline void dep_resolve_det_circ2(process_node* node,
	std::vector<process_node*>& result,
	std::unordered_set<process_node*>& resolved,
	std::unordered_set<process_node*>& unresolved)
{
	unresolved.insert(node);
	for (auto& edge : node->m_sources)
	{
		if (resolved.count(edge.get()) == 0)
			//if (is_in(resolved,edge)==false)
		{
			if (unresolved.count(edge.get())>0)
				//if (is_in(unresolved,edge)==true)
				throw std::invalid_argument("Graph circular dependency detected");
			dep_resolve_det_circ2(edge.get(), result, resolved, unresolved);
		}
	}
	resolved.insert(node);
	result.emplace_back(node);
	unresolved.erase(node);
}

inline bool has_circular_dependency(std::shared_ptr<process_node> start_node)
{
	std::vector<std::shared_ptr<process_node>> result;
	try
	{
		std::unordered_set<std::shared_ptr<process_node>> resolved;
		std::unordered_set<std::shared_ptr<process_node>> unresolved;
		//std::vector<NodeRef> already_seen;
		//already_seen.reserve(64);
		dep_resolve_det_circ(start_node, result, resolved, unresolved);
		
	}
	catch (std::exception&)
	{
		result.clear();
		return true;
	}
	return false;
}

inline std::vector<process_node*> topoSort(process_node* start_node)
{
	std::vector<process_node*> result;
	try
	{
		std::unordered_set<process_node*> resolved;
		std::unordered_set<process_node*> unresolved;
		dep_resolve_det_circ2(start_node, result, resolved, unresolved);
	}
	catch (std::exception&)
	{
		result.clear();
	}
	return result;
}

inline void visit_targets_recur(process_node* node,
	std::unordered_set<process_node*>& visited,
	std::vector<process_node*>& order)
{
	if (visited.count(node) == 0)
	{
		visited.insert(node);
		for (auto& e : node->m_destinations)
			visit_targets_recur(e, visited, order);
		order.emplace_back(node);
	}
}

template<typename F>
inline void visit_targets(process_node* startnode, F f)
{
	std::vector<process_node*> order;
	std::unordered_set<process_node*> visited;
	visit_targets_recur(startnode, visited, order);
	std::reverse(order.begin(), order.end());
	for (auto& e : order)
		f(e);
}

inline void visit_sources_recur(process_node* node,
	std::unordered_set<process_node*>& visited,
	std::vector<process_node*>& order)
{
	if (visited.count(node) == 0)
	{
		visited.insert(node);
		for (auto& e : node->m_sources)
			visit_sources_recur(e.get(), visited, order);
		order.emplace_back(node);
	}
}

template<typename F>
inline void visit_sources(process_node* startnode, F f)
{
	std::vector<process_node*> order;
	std::unordered_set<process_node*> visited;
	visit_sources_recur(startnode, visited, order);
	//std::reverse(order.begin(), order.end());
	for (auto& e : order)
		f(e);
}

inline bool is_node_connection_compatible(process_node* a, process_node* b)
{
    if (a==nullptr || b==nullptr)
        return false;
    // This might need to be implemented in a more complicated fashion at some point...
    if (a->get_output_type()=="wav" && b->get_supported_input_type()=="arrwav")
        return true;
	if (b->get_supported_input_type() == "any" || a->get_output_type()=="any")
		return true;
	return b->get_supported_input_type().containsIgnoreCase(a->get_output_type()); //==b->get_supported_input_type();
}

class process_graph
{
public:
	process_graph() {}
	node_ref_vec& getNodes() { return m_nodes; }
	GraphRenderState getRenderState() const { return m_render_state; }
	void setRenderState(GraphRenderState state) { m_render_state = state; }
	bool nodeBelongsToGraph(process_node* node);
	String renderNode(process_node* node, bool blockcurrentthread);
	node_ref createNode(String node_type);
	node_ref duplicateNode(node_ref src, bool with_connections);
	bool removeNode(node_ref node);
	bool removeNode(process_node* node);
	template<typename F>
	inline void removeNodesConditionally(F&& cond)
	{
		remove_from_container(m_nodes, cond);
	}
private:
	node_ref_vec m_nodes;
	GraphRenderState m_render_state = GraphRenderState::Idle;
};
