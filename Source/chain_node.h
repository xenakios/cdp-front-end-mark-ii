#ifndef chain_node_h
#define chain_node_h

#include "cdp_graph.h"

class chain_process_node_component;
class NodeView;

class chain_process_node : public process_node
{
public:
    chain_process_node(String procname);
    ~chain_process_node() {}
    StringArray get_output_filenames() override;
    String get_supported_input_type() override;
    String get_output_type() override;
    void tick() override;
    String get_error_text() override { return m_error_text; }
    MD5 extra_state_hash() override;
    //void valueChanged(Value& v) override;
    void connections_changed() override;
    void buildInternalGraph();
    node_ref_vec m_user_nodes;
    std::vector<node_ref_vec> m_internal_nodes;
	process_node::CustomGUIType getCustomEditorType2() override;
	Component* getCustomEditor() override;
	bool usesCommandLine() const override { return false; }
	std::vector<chain_process_node_component*> m_active_editors;
private:
    std::shared_ptr<chain_process_node_component> m_editor;
    
    String m_out_fn;
};

class chain_process_node_component : public Component, public ListBoxModel, public DragAndDropTarget
{
public:
    chain_process_node_component(chain_process_node* node);
	~chain_process_node_component();
	int getNumRows() override;
    void paintListBoxItem (int rowNumber, Graphics &g, int width, int height, bool rowIsSelected) override;
    void resized() override;
    bool isInterestedInDragSource(const SourceDetails &dragSourceDetails) override;
    void itemDragMove(const SourceDetails &dragSourceDetails) override;
    void itemDropped(const SourceDetails &dragSourceDetails) override;
    var getDragSourceDescription(const SparseSet< int > &currentlySelectedRows) override;
    void paint(Graphics& g) override;
	std::unique_ptr<NodeView> m_node_view;
private:
    chain_process_node* m_node = nullptr;
    ListBox m_listbox;
	
	int m_cur_drag_row = -1;
};


#endif /* chain_node_h */
