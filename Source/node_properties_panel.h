#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"
#include <memory>
#include <functional>

class CustomParameterComponent;
class ParameterLinker;
class PresetComponent;
class NodeView;

class parameter_component : public Component, 
							private Slider::Listener,
							private Value::Listener,
							private Button::Listener,
							private TextEditor::Listener,
							private ComboBox::Listener,
							public ChangeBroadcaster,
							public SettableTooltipClient,
							public FilenameComponentListener
{
public:
	parameter_component(parameter_ref param);
	~parameter_component();
	void setParameterLinker(ParameterLinker* lp) 
	{ 
		m_par_linker = lp; 
	}
	void resized() override;
	int getSuggestedHeigth() const { return m_heigth; }
	
	void setSliderEnabledFromParameter()
	{
		if (m_slider != nullptr)
		{
			m_slider->setEnabled(m_param->m_enabled_in_gui);
		}
	}
private:
	void buttonClicked(Button* but) override;
	void sliderValueChanged(Slider* slid) override;
	void sliderDragEnded(Slider* slid) override;
	void textEditorTextChanged(TextEditor& ed) override;
	void comboBoxChanged(ComboBox* cb) override;
	void valueChanged(Value& v) override;
	void filenameComponentChanged(FilenameComponent* fileComponentThatHasChanged) override;
	parameter_ref m_param;
	std::unique_ptr<Label> m_name_label;
	std::unique_ptr<Slider> m_slider;
	std::unique_ptr<ComboBox> m_combo;
	std::unique_ptr<FilenameComponent> m_filenamecomponent;
	std::unique_ptr<ToggleButton> m_toggle_but;
	std::unique_ptr<CustomParameterComponent> m_custom_component;
	std::unique_ptr<TextEditor> m_text_edit;
	std::unique_ptr<TextButton> m_param_props_but;
	std::unique_ptr<TextButton> m_env_enable_but;
	std::unique_ptr<TextButton> m_show_env_but;
    TextEditor* m_parlinkscript_edit = nullptr;
	int m_heigth = 20;
	ParameterLinker* m_par_linker = nullptr;
	void renderParentNode();
	NodeView* getNodeView();
};

void showEnvelopeCalloutBox(node_parameter* par, Component* parentComponent);
node_parameter* getLastEditedEnvelopeParameter(bool settonull);

class parameter_panel : public Component, public ChangeListener
{
public:
	parameter_panel(node_ref_vec nodes, ParameterLinker* lp);
	void resized() override;
	int get_content_height() const;
	void set_show_procname(bool b) { m_show_proc_name = b; resized(); }
	void changeListenerCallback(ChangeBroadcaster* cb) override;
	void updateParameterComponentsEnabled()
	{
		for (auto& e : m_param_comps)
		{
			e->setSliderEnabledFromParameter();
		}
	}
	void setParameterLinker(ParameterLinker* lp) 
	{ 
		m_param_linker = lp; 
	}
private:
	int get_top_margin() const;
	bool m_show_proc_name = true;
	node_ref_vec m_nodes;
	std::vector<std::unique_ptr<parameter_component>> m_param_comps;
	ParameterLinker* m_param_linker = nullptr;
};

class ConnectionsListBox : public ListBox, 
						   public ListBoxModel,
						   public DragAndDropTarget
{
public:
	ConnectionsListBox();
	void set_node(std::shared_ptr<process_node> node);
	bool isInterestedInDragSource(const SourceDetails &dragSourceDetails) override;
	void itemDragMove(const SourceDetails &dragSourceDetails) override;
	void itemDropped(const SourceDetails &dragSourceDetails) override;
	int getNumRows() override;
	var getDragSourceDescription(const SparseSet< int > &currentlySelectedRows) override;
	void paintListBoxItem(int rowNumber, Graphics &g, int width, int height, bool rowIsSelected) override;
private:
	std::shared_ptr<process_node> m_node;
	int m_drag_row = -1;
};

class node_inspector : 
	public Component, 
	private Value::Listener, 
	private MultiTimer, 
	public DragAndDropContainer,
	public ChangeBroadcaster
{
public:
	node_inspector(bool is_node_component_inspector);
	void setNodes(node_ref_vec n);
	void removeNode(process_node* n);
	void resized();
	void paint(Graphics& g);
	void valueChanged(Value& v);
	void timerCallback(int id);
	std::function<void(node_inspector*)> StateChangedFunc;
	void build_props_panel();
	void set_show_procname(bool b);
	void updateConnectionsList();
	void setParameterLinker(ParameterLinker* lp) { m_param_linker = lp; }
private:
	node_ref_vec m_nodes;
	std::unique_ptr<Viewport> m_view_port;
	std::unique_ptr<parameter_panel> m_param_panel;
	std::unique_ptr<Component> m_custom_editor;
	std::unique_ptr<ConnectionsListBox> m_file_listbox;
	std::unique_ptr<TextEditor> m_args_editor;
	std::unique_ptr<PresetComponent> m_preset_component;
	bool m_show_procname = true;
	var m_last_changed_value;
    bool m_is_node_component_inspector = false;
	ParameterLinker* m_param_linker = nullptr;
};
