#include "lua_process_node.h"
#include <map>
#include "waveform_component.h"
#include "envelope_component.h"

#ifdef WIN32
__pragma(warning(disable:4127))
#endif

sol::state* g_luanodestate=nullptr;
bool g_luanodescriptexecuted = false;

void destroy_lua_node_state()
{
	delete g_luanodestate;
	g_luanodestate = nullptr;
}

template<typename T>
inline String stringFromOptional(sol::optional<T>& opt)
{
	if (opt)
		return String(*opt);
	return String();
}

int get_cycle_count_cached(String fn)
{
	static std::map<String, int> s_cache;
	if (s_cache.count(fn) > 0)
	{
		return s_cache[fn];
	}
	String exe = get_cdp_bin_location() + "/distort";
	StringArray args;
	args.add(exe);
	args.add("cyclecnt");
	args.add(fn);
	ChildProcess proc;
	proc.start(args);
	proc.waitForProcessToFinish(10000);
	String progout = proc.readAllProcessOutput();
	//Logger::writeToLog("cyclecnt output is : " + progout);
	StringArray outputlines = StringArray::fromLines(progout);
	if (outputlines.size() > 2)
	{
		//Logger::writeToLog("output has enough lines");
		//Logger::writeToLog(outputlines[1]);
		StringArray resultlinetokens = StringArray::fromTokens(outputlines[1], " ");
		if (resultlinetokens.size() > 0)
		{
			int numcycles = resultlinetokens[0].getIntValue();
			s_cache[fn] = numcycles;
			return numcycles;
		}
	}
	return 0;
}

luacdpnode::luacdpnode(String name) 
{
	m_proc_name = name;
	m_type_name = name;
	ProcessFinishedFunc = [](process_node*) {};
	if (init_lua_processor(m_type_name) == false)
		throw std::runtime_error("Lua defined node could not be created");
	
	m_bypassmode = NotBypassed;
}

void init_choices(sol::table& tab, StringArray& string_arr, Array<var>& val_arr)
{
	sol::table choicetable = tab.get<sol::table>("choices");
	if (choicetable.valid() == true)
	{
		for (auto& q : choicetable)
		{
			sol::table foo = q.second.as<sol::table>();
			if (foo.valid() == true)
			{
				sol::optional<std::string> baz = foo[1];
				sol::optional<int> zaz = foo[2];
				if (baz && zaz)
				{
					string_arr.add(*baz);
					val_arr.add(*zaz);
				}
			}

		}
	}
}

bool luacdpnode::init_lua_processor(String procname)
{
	try
	{
		if (m_params_inited == false)
		{
			m_num_input_files_required = 0;
			m_num_input_files_supported = 0;
		}
		
		String luadefs_fn = get_cdp_lua_defs_file();
		if (g_luanodestate == nullptr)
			g_luanodestate = new sol::state;
		sol::state& lua = *g_luanodestate;
		std::string luafn = luadefs_fn.toStdString();
		lua.set_function("get_srate", [this]() { return m_sound_file_info.sr; });
		lua.set_function("get_cycles", [this]() { return m_sound_file_info.cycles; });
		lua.set_function("get_length", [this]() { return m_sound_file_info.length; });
		if (g_luanodescriptexecuted == false)
		{
			lua.script_file(luafn);
			g_luanodescriptexecuted = true;
		}
		sol::table dsp_entries = lua["dsp"];
		sol::table dsp_entry = dsp_entries[std::string(procname.getCharPointer())];
		if (dsp_entry.empty() == true)
			Logger::writeToLog("nope");
		sol::table args = dsp_entry["cmds"];
		m_exe = args.get_or("exe", std::string());
		m_mode = args.get_or("mode", std::string());
		m_channel_conf = args.get_or("channels", std::string());
		m_custom_editor_type = args.get_or("customeditor", std::string());
		//Logger::writeToLog("exe name is " + m_exe + ", mode is " + m_mode);
		int i = 1;
		int param_count = 0;
		int num_input_files_required = 0;
		int num_input_files_supported = 0;
		int num_output_files_required = 0;
		if (m_params_inited == true)
		{
			updateFloatParameters(dsp_entry);
		}
		while (true)
		{
			std::string argfieldname = "arg" + std::to_string(i);
			sol::table argtable = dsp_entry.get<sol::table>(argfieldname);
			if (argtable.valid()==true)
			{
				auto arg_type = detectArgumentType(argtable);
				if (m_params_inited == false)
				{
					if (arg_type == ArgumentType::InputFile)
						addInputFileArgument(argtable, false);
					if (arg_type == ArgumentType::InputFileArray)
						addInputFileArgument(argtable, true);
					if (arg_type == ArgumentType::OutputFile)
						addOutputFileArgument(argtable);
					if (arg_type == ArgumentType::FloatParameter)
						addFloatParameter(argtable);
					if (arg_type == ArgumentType::BoolParameter)
						addBoolParameter(argtable);
					if (arg_type == ArgumentType::StringParameter)
						addStringParameter(argtable);
					if (arg_type == ArgumentType::ChoiceParameter)
						addChoiceParameter(argtable);
				}
				
            }
			else
			{
				if (m_params_inited == false)
				{
					m_params_inited = true;
					m_io_types_inited = true;
					m_num_input_files_supported = std::max(m_num_input_files_required, m_num_input_files_supported);
					//Logger::writeToLog("num input files required for " + procname + " is " + String(m_num_input_files_required));
                    //Logger::writeToLog("input type is "+m_input_type);
					//Logger::writeToLog("num input files supported for " + procname + " is " + String(m_num_input_files_supported));
					
				}
				break;
			}
			++i;
		}
		//m_num_input_files_required = num_input_files_required;
		
		
		//Logger::writeToLog("num output file names required for " + procname + " is " + String(num_output_files_required));
		return true;

	} 
	catch (std::exception& excep)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"CDP Frontend Lua error",
			excep.what(), "OK",
			nullptr);
		return false;
	}
}

ArgumentType luacdpnode::detectArgumentType(sol::table& argtable)
{
    sol::optional<std::string> arg_input = argtable["input"];
    sol::optional<std::string> arg_output = argtable["output"];
    sol::optional<std::string> arg_switch = argtable["switch"];
    sol::optional<double> arg_min_val = argtable["min"];
    sol::optional<double> arg_max_val = argtable["max"];
    sol::table arg_choices = argtable["choices"];
    if (arg_input && (*arg_input=="wav" || *arg_input=="ana" || *arg_input=="env" || *arg_input=="txt"))
        return ArgumentType::InputFile;
    if (arg_input && *arg_input=="arrwav")
        return ArgumentType::InputFileArray;
	if (arg_output && (*arg_output=="wav" || *arg_output=="ana" || *arg_output=="env" || *arg_output=="txt"))
        return ArgumentType::OutputFile;
    if (arg_min_val && arg_max_val)
        return ArgumentType::FloatParameter;
    if ((!arg_choices) && arg_switch && (!arg_min_val))
        return ArgumentType::BoolParameter;
    if (arg_input && *arg_input=="string")
        return ArgumentType::StringParameter;
    sol::optional<bool> is_string = argtable["isstring"];
    if (is_string && *is_string==true)
        return ArgumentType::StringParameter;
    if (arg_choices.valid()==true)
        return ArgumentType::ChoiceParameter;
    return ArgumentType::Unknown;
}

String getTipText(sol::table& table)
{
	String result;
	sol::optional<const char*> tip = table["tip"];
	if (tip)
		result = String(CharPointer_UTF8(*tip));
	return result;
}

void initFloatParameterProperties(sol::table& argtable,
	double& minval, double &maxval, double& defval, double& skew, bool& use_midpoint, double& midpoint)
{
	double min_val = argtable["min"];
	double max_val = argtable["max"];
	sol::optional<double> def_val = argtable["def"];
	if (!def_val)
		def_val = average(min_val, max_val);
	minval = min_val;
	maxval = max_val;
	defval = *def_val;
	sol::optional<double> mid_point = argtable["skewm"];
	if (mid_point)
	{
		midpoint = *mid_point;
		use_midpoint = true;
	}
	else use_midpoint = false;
}

void luacdpnode::addFloatParameter(sol::table& argtable)
{
    sol::optional<const char*> name = argtable["name"];
	sol::optional<const char*> arg_switch = argtable["switch"];
	sol::optional<std::string> arg_input = argtable["input"];
	sol::optional<int> gui_order = argtable["ord"];
	double min_val = 0.0; 
	double max_val = 0.0; 
	double def_val = 0.0;
	double skew = 1.0;
	double midpoint = 0.0;
	bool usemidpoint = false;
	initFloatParameterProperties(argtable, min_val, max_val, def_val, skew, usemidpoint, midpoint);
    //Logger::writeToLog("Float parameter : "+String(*name)+ " "+String(min_val)+" "+String(max_val)+" "+
    //                   String(*def_val));
	bool automatable = false;
	if (arg_input && *arg_input == "brk")
		automatable = true;
	parameter_ref param=std::make_shared<node_parameter>(CharPointer_UTF8(*name), "", min_val, max_val, def_val, automatable, 0.0, 1.0);
	if (arg_switch)
		param->m_lua_argument = *arg_switch;
	param->m_parent_node = this;
	param->m_midpoint_skew = usemidpoint;
	param->m_midpoint = midpoint;
	/*
	param.m_skew = skew;
	if (symskew)
		param.m_sym_skew = true;
	*/
	sol::optional<const char*> custom_control = argtable["customcontrol"];
	if (custom_control)
		param->m_control_type = CharPointer_UTF8(*custom_control);
	param->m_tip_text = getTipText(argtable);
	if (gui_order)
		param->m_gui_order = *gui_order;
	m_parameters.push_back(param);
	m_argument_pattern.push_back(ArgumentType::FloatParameter);
}

void luacdpnode::addBoolParameter(sol::table &argtable)
{
    sol::optional<const char*> name = argtable["name"];
    sol::optional<const char*> _switch = argtable["switch"];
    if (name && _switch)
    {
        //Logger::writeToLog("Bool parameter : "+String(*name)+" "+String(*_switch));
        parameter_ref par(new node_parameter(true, CharPointer_UTF8(*name), "", false));
        par->m_lua_argument = *_switch;
        par->m_parent_node = this;
        par->m_tip_text = getTipText(argtable);
		par->m_param_type = parameterType::Bool;
        m_parameters.push_back(par);
		m_argument_pattern.push_back(ArgumentType::BoolParameter);
    }
}

void luacdpnode::addStringParameter(sol::table &argtable)
{
    sol::optional<const char*> name = argtable["name"];
    sol::optional<const char*> deftxt = argtable["def"];
    if (name && deftxt)
    {
        //Logger::writeToLog("String parameter : "+String(*name)+" "+String(*deftxt));
        
        parameter_ref param(new node_parameter(String(CharPointer_UTF8(*name)), "", String(CharPointer_UTF8(*deftxt))));
        param->m_parent_node = this;
        //param.m_gui_order = gui_order;
        param->m_tip_text = getTipText(argtable);
		param->m_param_type = parameterType::String;
        m_parameters.push_back(param);
		m_argument_pattern.push_back(ArgumentType::StringParameter);
    }
}

void luacdpnode::addChoiceParameter(sol::table& argtable)
{
    sol::optional<const char*> name = argtable["name"];
	sol::optional<const char*> _switch = argtable["switch"];
	sol::optional<int> gui_order = argtable["ord"];
	//if (name)
    //    Logger::writeToLog("ChoiceParameter "+String(*name));
	StringArray choice_strings;
	Array<var> choice_values;
	init_choices(argtable, choice_strings, choice_values);
	int def_x = argtable.get_or("def", 0);
	parameter_ref param(new node_parameter(CharPointer_UTF8(*name), "", def_x, choice_strings, choice_values));
	param->m_parent_node = this;
	if (gui_order)
		param->m_gui_order = *gui_order;
	param->m_tip_text = getTipText(argtable);
	if (_switch)
		param->m_lua_argument = *_switch;
	m_parameters.push_back(param);
	m_argument_pattern.push_back(ArgumentType::ChoiceParameter);
}

void luacdpnode::updateFloatParameters(sol::table & dsp_entry)
{
	int i = 1;
	int param_count = 0;
	while (true)
	{
		std::string argfieldname = "arg" + std::to_string(i);
		sol::table argtable = dsp_entry.get<sol::table>(argfieldname);
		if (argtable.valid() == true)
		{
			auto arg_type = detectArgumentType(argtable);
			if (arg_type == ArgumentType::FloatParameter)
			{
				double minv = 0.0;
				double maxv = 0.0;
				double defv = 0.0;
				double skew = 0.0;
				double midpt = 0.0;
				bool usempt = false;
				initFloatParameterProperties(argtable, minv, maxv, defv, skew, usempt, midpt);
				if (param_count < m_parameters.size())
				{
					//Logger::writeToLog("Updating props for param " + m_parameters[param_count].m_name+" "+
					//String(minv)+" "+String(maxv));
					m_parameters[param_count]->m_min_value = minv;
					m_parameters[param_count]->m_max_value = maxv;
				}
				++param_count;
			}
			else if (arg_type == ArgumentType::BoolParameter || arg_type == ArgumentType::ChoiceParameter ||
				arg_type == ArgumentType::IntParameter || arg_type == ArgumentType::StringParameter
				|| arg_type == ArgumentType::TextFileParameter)
			{
				++param_count;
			}
		}
		else
			break;
		++i;
	}
	//Logger::writeToLog("Finished updating Lua node parameters");
}

String stringFromTableKey(sol::table& table, const char* key)
{
	sol::optional<const char*> temp = table[key];
	if (temp)
		return String(*temp);
	return String();
}

void luacdpnode::addInputFileArgument(sol::table& argtable, bool is_array)
{
	String input_type = stringFromTableKey(argtable,"input");
	if (input_type.isNotEmpty())
		m_buses_layout.addInputBus(input_type);
	if (is_array == false)
	{
		++m_num_input_files_required;
		if (input_type.isNotEmpty())
		{
			if (input_type.containsIgnoreCase("ana"))
				m_num_supported_input_channels = 1;
			m_input_type += input_type;
			m_argument_pattern.push_back(ArgumentType::InputFile);
		}
	}
	else
	{
		m_num_input_files_supported = 1024;
		if (input_type.isNotEmpty())
		{
			if (input_type.containsIgnoreCase("ana"))
				m_num_supported_input_channels = 1;
			m_input_type = input_type;
			m_argument_pattern.push_back(ArgumentType::InputFileArray);
		}
	}
}

void luacdpnode::addOutputFileArgument(sol::table& argtable)
{
	sol::optional<const char*> output_type = argtable["output"];
	if (output_type)
	{
		m_output_type = *output_type;
		m_buses_layout.addOutputBus(m_output_type);
		if (m_output_type == "ana")
			m_num_current_output_channels = 1;
		m_argument_pattern.push_back(ArgumentType::OutputFile);
	}
}


String luacdpnode::get_error_text()
{
    return m_error_text;
}

void luacdpnode::removeLastOutputFile()
{
	if (m_pstate == Busy)
		return;
	File file(m_out_fn);
	if (file.existsAsFile() == true)
	{
		setAudioFileToPlay(String());
		if (file.deleteFile() == false)
		{
			Logger::writeToLog(m_out_fn + " could not be deleted");
		}
	}
}

StringArray luacdpnode::get_arguments()
{
	StringArray arguments;
	arguments.add(get_cdp_bin_location() + "/" + m_exe);
	if (m_mode.isEmpty() == false)
	{
		StringArray temparr = StringArray::fromTokens(m_mode, false);
		arguments.addArray(temparr);
	}
	int outputfilecount = 0;
	int inputfilecount = 0;
	int paramcount = 0;
	for (auto& arg : m_argument_pattern)
	{
		if (arg == ArgumentType::InputFile)
		{
			if (inputfilecount < m_sources.size())
			{
				arguments.add(m_sources[inputfilecount]->get_output_filenames()[0]);
			}
			++inputfilecount;
		}
		if (arg == ArgumentType::InputFileArray)
		{
			for (int i = 0; i < m_sources.size(); ++i)
			{
				if (inputfilecount < m_sources.size())
				{
					arguments.add(m_sources[inputfilecount]->get_output_filenames()[0]);
				}
				++inputfilecount;
			}
		}
		if (arg == ArgumentType::OutputFile)
		{
			arguments.add(m_out_fn);
			++outputfilecount;
		}
		if (arg == ArgumentType::BoolParameter)
		{
			if (paramcount < m_parameters.size())
			{
				node_parameter* par = m_parameters[paramcount].get();
				if (par->m_lua_argument.isEmpty() == false)
				{
					if ((bool)par->getValue() == true)
						arguments.add(par->m_lua_argument);
				}
			}
			++paramcount;
		}
		if (arg == ArgumentType::FloatParameter || arg == ArgumentType::IntParameter 
			|| arg == ArgumentType::ChoiceParameter || arg == ArgumentType::StringParameter)
		{
			if (paramcount < m_parameters.size())
			{
				node_parameter* par = m_parameters[paramcount].get();
				
				if (par->m_lua_argument.isEmpty() == false)
					arguments.add(par->m_lua_argument + par->getValue().toString());
				else
				{
					if (par->m_automation_enabled == false)
						arguments.add(par->getValue().toString());
					else
					{
						String bkfn = generateBreakpointFile(par, par->m_env_time_scale);
						arguments.add(bkfn);
					}
				}
			}
			++paramcount;
		}
	}
	return arguments;
}

int luacdpnode::getNumOutChansForNumInChans(int inchans)
{
	if (m_channel_conf == "any")
		return inchans;
	if (m_channel_conf == "1")
		return 1;
	if (m_output_type == "ana")
		return 1;
	if (m_exe == "distort")
		return 1;
	if (inchans == 1 && m_channel_conf == "1in2out")
		return 2;
	if (inchans == 2 && m_channel_conf == "2in1out")
		return 1;
	return 0;
}

void luacdpnode::valueChanged(Value & v)
{

}

process_node::CustomGUIType luacdpnode::getCustomEditorType2()
{
	if (m_custom_editor_type == "WaveformWithEditor")
		return process_node::InspectorCustomEditor;
	return process_node::NoCustomEditor;
}

Component * luacdpnode::getCustomEditor()
{
	if (m_custom_editor_type == "WaveformWithEditor")
		return new WaveformWithEditor(this);
	return nullptr;
}

void luacdpnode::tick()
{
	tick_sources();
	if (m_pstate == Busy && m_cp != nullptr && m_cp->isRunning() == false)
	{
		end_benchmark();
		//Logger::writeToLog(get_name() + " has finished, returning child process to pool");
		
		--get_num_used_processes_ref();
        m_error_text=cdp_process_result(*m_cp);
		if (m_error_text.isEmpty() == true)
		{
			File tempf(m_out_fn);
			if (tempf.existsAsFile() == false)
			{
				m_error_text = "Process didn't produce the expected output file";
				m_pstate = FinishedWithError;
				if (ProcessErrorFunc)
					ProcessErrorFunc(this);
			}
		}
		if (m_error_text.isEmpty()==true)
		{
			m_pstate = FinishedOK;
			TempFileContainer::instance().add(m_out_fn);
			setAudioFileToPlay(m_out_fn);
			if (ProcessFinishedFunc)
                ProcessFinishedFunc(this);
		}
		else
		{
			m_pstate = FinishedWithError;
			if (ProcessErrorFunc)
				ProcessErrorFunc(this);
		}

		childprocess_pool::get_instance()->release_child_process(m_cp);
		if (m_cp != nullptr)
			Logger::writeToLog("Pool wasn't able to null the pooled member");
	}
	if (m_pstate == Idle)
	{
		if (m_sources.size() < m_num_input_files_required)
		{
			m_pstate = FinishedWithError;
			m_error_text = "No source node";
			if (ProcessErrorFunc)
				ProcessErrorFunc(this);
			return;
		}
		auto finish_state = are_sources_finished();
		if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
		{
			m_error_text = "Error in source(s)";
			m_pstate = FinishedWithError;
			ProcessErrorFunc(this);
		}
		if (finish_state == SourcesState::FinishedOK)
		{
            auto r = initOutputFile("lua_node", m_output_type);
			if (r.second == "*")
			{
				m_pstate = FinishedOK;
				m_out_fn = r.first;
				setAudioFileToPlay(m_out_fn);
				if (ProcessFinishedFunc)
					ProcessFinishedFunc(this);
				
				return;
			}
            if (r.first.isEmpty())
                return;
            m_out_fn = r.first;
            update_params();
			if (m_bypassmode == Bypassed)
			{
				m_out_fn = m_sources[0]->get_output_filenames()[0];
				m_pstate = FinishedOK;
				setAudioFileToPlay(m_out_fn);
				ProcessFinishedFunc(this);
				return;
			}
			init_child_process();
			if (m_cp == nullptr)
			{
				return;
			}
			//remove_file_if_exists(outfn);
			//m_out_fn = outfn;
			StringArray arguments  = get_arguments();
			String temp = arguments.joinIntoString(" ");
			Logger::writeToLog(temp);
			start_benchmark();
			m_cp->start(arguments);
			++get_num_used_processes_ref();
			m_pstate = Busy;
		}
	}
}

StringArray luacdpnode::get_output_filenames()
{
	return StringArray({ m_out_fn });
}

String luacdpnode::get_supported_input_type()
{
	return m_input_type;
}

String luacdpnode::get_output_type()
{
	return m_output_type;
}

void luacdpnode::update_params()
{
	connections_changed();
}

int luacdpnode::guessNumOutChans(int numinchans)
{
	if (m_exe == "distort")
		return 1;
	if (m_input_type == "ana")
		return 1;
	if (m_sources.size())
		return m_sources[0]->getCurrentNumOutputChannels();
	return 0;
}

void luacdpnode::connections_changed()
{
	if (m_enable_connections_changed==false)
        return;
    if (m_sources.size() > 0 && m_sources[0]->get_output_filenames().size()>0)
	{
		String infn = m_sources[0]->get_output_filenames()[0];
		auto info = get_audio_source_info_cached(infn);
		//Logger::writeToLog("connections were changed and first source length is " + String(info.get_length_seconds()));
		int cycles = get_cycle_count_cached(m_sources[0]->get_output_filenames()[0]);
		//Logger::writeToLog("file seems to have " + String(cycles) + " cycles");
		m_sound_file_info = sound_file_info_t(info.samplerate,cycles,info.get_length_seconds()*1000.0);
		m_num_current_output_channels =  guessNumOutChans(info.num_channels);
		init_lua_processor(m_type_name);
		for (int i = 0; i < m_parameters.size(); ++i)
			m_parameters[i]->m_env_time_scale = info.get_length_seconds();
	}
	sendActionMessage("conns_changed");
}
