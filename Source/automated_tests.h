#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <atomic>

enum class TestState
{
	Idle,
	Running,
	CancelRequested,
	Finished
};

String get_test_audio_file_name();

void run_all_nodes_test(std::atomic<TestState>& state);

