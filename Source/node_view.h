#ifndef node_view_h
#define node_view_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"

class IJCDPreviewPlayback;
class node_inspector;
class DragTestComponent;

using document_t = std::vector<ValueTree>;

class UndoEntry
{
public:
	UndoEntry() {}
	String m_desc;
	document_t m_document;
	ValueTree m_connections;
};

class NodeView : public Component, public MultiTimer
{
public:
    NodeView(std::shared_ptr<IJCDPreviewPlayback> previewplayback, node_inspector* inspector);
    ~NodeView() {}
    void paint(Graphics&) override;
    void resized() override;
    void mouseDown(const MouseEvent& ev) override;
    void mouseUp(const MouseEvent& ev) override;
    void mouseDrag(const MouseEvent& ev) override;
	void mouseMove(const MouseEvent& ev) override;
    void timerCallback(int id) override;
    void add_node_to_view(std::shared_ptr<process_node> node, int xcor, int ycor, int w, int h);
    void connect_nodes_if_no_feedback(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b);
    std::shared_ptr<process_node> add_node_with_component(String procname, int xcor, int ycor, int w, int h, bool skiperrordlg);
    void node_menu_requested(std::shared_ptr<process_node> node);
    //node_ref getLastRenderedNode() { return m_rendering_node; }
    node_ref getSelectedNode() { return m_selected_node; }
    node_ref_vec getSelectedNodes();
    int numSelectedNodes() const;
    GraphRenderState getGraphRenderState() const { return m_graph_render_state; }
    void setGraphRenderState(GraphRenderState s) { m_graph_render_state = s; }
    
	void removeSelectedNodes();
	void removeNodes(const node_ref_vec& nodes);
	
	void updateParametersOfSelectedNodes();
    node_ref_vec& getNodes() { return m_graph.getNodes(); }
    String load_graph_query_filename();
    String save_graph_query_filename();
    String loadGraphFromXmlFile(File file);
    String saveGraphToXmlFile(File file);
	ValueTree getGraphValueTree();
	void restoreGraphFromValueTree(ValueTree tree);
	ValueTree getNodeValueTree(process_node* node);
	void restoreNodeFromTree(process_node* node, ValueTree tree);
	
	ValueTree getConnectionsValueTree();
	void restoreConnectionsFromTree(ValueTree tree);
	document_t getDocument();
	void restoreGraphFromDocument(const document_t& doc);
	void restoreGraphFromUndoEntry(UndoEntry& entry);

    String renderNode(process_node* node, bool blockcurrentthread, bool renderdestination);
    String renderSelected(bool blockcurrentthread);
    void renderNodeDeferred(process_node* node, int delayamount=2000);
    
    void forceRenderSelected();
    void rotateSources();
    void permutateSources();
    void duplicateNodes(bool withConnections, bool withrelativeConnections);
    void showLastEnvelope();
    void renameSelectedNode();
    void disconnectSelectedNodeIO(bool inputs, bool outputs);
    void connectOutputToClosestInput(bool below);
    
    void setAllNodesSelectedState(bool b);
    void showNodeList(int x, int y, bool replace);
    void doNodeCreateMenu(int x, int y);
    void replaceNodes();
    Viewport* m_viewport = nullptr;
    void setNodeInspector(node_inspector* inspec);
    std::function<void(process_node*)> NodeDeletedFunc;
    process_graph* getGraph() { return &m_graph; }
    void addDefaultNodes();
    void showNodeInfoText();
    void removeNodeComponent(process_node* node);
	void testTopologicalSort(process_node* node);
    std::vector<std::shared_ptr<DragTestComponent>> m_drag_tests;
    ComponentDragger m_dragger;
	bool isAutoRenderEnabled() const { return m_auto_render_enabled; }
	void setAutoRenderEnabled(bool b);

	void addUndoState(String desc);
	void recallUndoState(int index);
	void doUndo();
	void doRedo();

private:
    struct render_job
    {
        render_job() {}
        render_job(process_node* node, double start_time) : m_node(node), m_start_time(start_time) {}
        process_node* m_node = nullptr;
        double m_start_time = 0.0;
        bool m_is_running = false;
    };
    std::vector<std::unique_ptr<NodeComponent>> m_node_components;
    process_graph m_graph;
    std::shared_ptr<process_node> m_selected_node;
    std::vector<render_job> m_render_jobs;
    std::vector<process_node*> m_destinations_to_render;
    node_inspector* m_node_inspector = nullptr;
    bool m_auto_connect_nodes = true;
    bool m_auto_disconnect_nodes = true;
    GraphRenderState m_graph_render_state = GraphRenderState::Idle;
    std::shared_ptr<IJCDPreviewPlayback> m_audio_player;
    Path m_selection_path;
    void selectNodesWithinPath();
    bool m_drag_duplicate_was_done = false;
    void nodeManipulated(std::shared_ptr<process_node> node, MouseAction act, int x_par, int y_par);
    void doForSelectedNodes(std::function<void(node_ref)> f);
    void setSelectedNodesForInspector();
    double m_render_start_clock_time = 0.0;
    double m_render_elapsed_clock_time = 0.0;
	std::vector<process_node*> m_topological_render_order;
	bool m_auto_render_enabled = false;
	
	std::vector<UndoEntry> m_undo_history;
	int m_undo_level = 0;
	int m_last_mouse_move_recalled = -1;
};


#endif /* node_view_h */
