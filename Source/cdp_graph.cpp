
#include "cdp_graph.h"
#include <map>
#include <numeric>
#include "node_factory.h"
#include <random>
#include "resample.h"

extern String g_cdp_bin_location;
extern String g_cdp_render_location;
extern std::unique_ptr<AudioFormatManager> g_format_manager;
extern AudioDeviceManager* g_audiodevice_manager;
extern std::unique_ptr<PropertiesFile> g_propsfile;

#undef min
#undef max

bool is_valid_cdp_binaries_location(String dir)
{
	File temp(dir);
#ifdef WIN32
	auto test_files = StringArray({ "pvoc.exe","modify.exe","sfedit.exe" });
#else
	auto test_files = StringArray({ "pvoc","modify","sfedit" });
#endif

	int cnt = 0;
	for (int i = 0; i<test_files.size(); ++i)
		if (does_file_exist(temp.getFullPathName() + "/" + test_files[i]))
			++cnt;
	return cnt == test_files.size();
}

String query_cdp_binaries_location()
{
	FileChooser chooser("Choose CDP binaries location");
	bool result = chooser.browseForDirectory();
	if (result == true)
	{
		bool valid = is_valid_cdp_binaries_location(chooser.getResult().getFullPathName());
		if (valid == false)
		{
			AlertWindow::showNativeDialogBox("Warning", "The location does not appear to have the CDP binaries", false);
			return String();
		}
		else
		{
			return chooser.getResult().getFullPathName();
		}

	}
	
	return String();
}

String query_cdp_render_location()
{
	FileChooser chooser("Choose CDP audio render location");
	bool result = chooser.browseForDirectory();
	if (result == true)
	{
		return chooser.getResult().getFullPathName();
	}
	return String();
}

File queryAudioFile()
{
	File initdir = File(g_propsfile->getValue("general/last_sound_import_dir"));
	FileChooser myChooser("Please select wav-file...",
		initdir,
		"*.wav",true);
	if (myChooser.browseForFileToOpen() == true)
	{
		File file = myChooser.getResult();
		g_propsfile->setValue("general/last_sound_import_dir", file.getParentDirectory().getFullPathName());
		return file;
	}
	return File();
}

String get_cdp_bin_location()
{
    return g_cdp_bin_location;
}



// Hack needed to work around Juce's getExitCode bug in macOS...
// Was changed later to ignore the exitcode on Windows too
String cdp_process_result(ChildProcess & proc)
{
	String output = proc.readAllProcessOutput();
	if (output.contains("ERROR") || output.contains("Application doesn't work")
		|| output.contains("Insufficient") || output.contains("is out of range"))
		return output;
	if (output.isEmpty() == true)
		return "Could not get program output, probably crashed";
	return String();
}

String get_output_folder()
{
	return g_cdp_render_location;
}

std::unique_ptr<Slider> make_parameter_helper_slider(node_parameter * parameter)
{
	auto slid = std::make_unique<Slider>();
	slid->setRange(parameter->m_min_value.getValue(),
		parameter->m_max_value.getValue(), parameter->m_step_size);
	slid->setSkewFactor(parameter->m_skew, parameter->m_sym_skew);
	if (parameter->m_midpoint_skew == true)
		slid->setSkewFactorFromMidPoint(parameter->m_midpoint);
	return slid;
}

template<typename T>
inline void appendToBlock(MemoryBlock& block, T& x)
{
	block.append((void*)&x, sizeof(T));
}

MD5 getEnvelopeHash(breakpoint_envelope* env)
{
	MemoryBlock block;
	for (int i = 0; i < env->GetNumNodes(); ++i)
	{
		auto& pt = env->GetNodeAtIndex(i);
		appendToBlock(block, pt.Time);
		appendToBlock(block, pt.Value);
		appendToBlock(block, pt.Shape);
		appendToBlock(block, pt.ShapeParam1);
		appendToBlock(block, pt.ShapeParam2);
	}
	MD5 result(block);
	return result;
}

String generateBreakpointFile(node_parameter * parameter, double maxtime)
{
	auto env = parameter->get_envelope();
	MD5 h = getEnvelopeHash(env.get());
	String outfn = g_cdp_render_location + "/breakpoints_" + h.toHexString() + ".txt";
	File file(outfn);
	if (file.exists() == true)
	{
		return outfn;
	}
	auto stream = file.createOutputStream();
	if (stream != nullptr)
	{
		NormalisableRange<double> rangehelper(parameter->m_min_value.getValue(), 
			parameter->m_max_value.getValue(), 0.0, parameter->m_skew, parameter->m_sym_skew);
		for (int i = 0; i < env->GetNumNodes(); ++i)
		{
			env->resamplePointToLinearSegments(i, 0.0, 1.0, 0.0, 1.0, [&stream,&rangehelper,maxtime](double pt_x0, double pt_y0, double pt_x1, double pt_y1)
			{
				double foo_x0 = jmap<double>(pt_x0, 0.0, 1.0, 0.0, maxtime);
				double scaled_v = rangehelper.convertFrom0to1(pt_y0);
				(*stream) << foo_x0 << " " << scaled_v << "\n";
			}, [maxtime](double xdiff)
			{
				return std::max((int)(xdiff*maxtime / 16), 8);
			});
			
		}
		delete stream;
	}
	TempFileContainer::instance().add(outfn);
	return outfn;
}

void node_parameter::setValue(var v)
{
	m_value = v;
	if (m_parent_node)
		m_parent_node->setDirty(true);
}

process_node::process_node()
{
	ProcessFinishedFunc = [](process_node*) {};
}

process_node::process_node(String procname) : m_proc_name(procname), m_type_name(procname)
{
	ProcessFinishedFunc = [](process_node*) {};
}

process_node::~process_node()
{
	//Logger::writeToLog("node " + m_proc_name +" destroyed");
}

process_node::BypassMode process_node::get_bypass_mode() const
{
	return m_bypassmode;
}

void process_node::set_bypass_mode(BypassMode mode)
{
	if (m_bypassmode != BypassNotSupported)
	{
		m_bypassmode = mode;
	}
}

std::pair<String,String> process_node::initOutputFile(String prefix, String ext)
{
    MD5 h = get_state_hash();
    //Logger::writeToLog(h.toHexString());
    String outfn(get_output_folder() + "/" + prefix+"-" + h.toHexString() + "." + ext);
    //m_out_fn = outfn;
    File tempfile(outfn);
    if (tempfile.exists() == true)
    {
        if (tempfile.getSize()>=4096)
        {
            return {outfn,"*"};
        } else
        {
            // File was likely a junk file left on disk after a crash or other error...
            Logger::writeToLog("Deleting small likely junk file...");
            if (tempfile.deleteFile()==false)
            {
                m_error_text = "Could not delete "+outfn;
                m_pstate = FinishedWithError;
                if (ProcessErrorFunc)
                    ProcessErrorFunc(this);
                return {String(), "Failed to delete "+outfn};
            }
        }
    }
    return {outfn,String()};
}

void process_node::update_params()
{
	if (m_sources.size() == 0)
		return;
	if (m_sources[0]->get_output_filenames().size() == 0)
		return;
	audio_source_info info = get_audio_source_info_cached(m_sources[0]->get_output_filenames()[0]);
	for (auto& e : m_parameters)
	{
		e->m_env_time_scale = info.get_length_seconds();
		if (e->FileLengthChanged)
		{
			if (info.get_length_seconds() > 0.0)
			{
				//Logger::writeToLog(e.m_name + " " + m_sources[0]->get_output_filenames()[0] + " is " + String(info.get_length_seconds()) + "seconds long");
				e->FileLengthChanged(*e, info.get_length_seconds());
			}
		}
	}
}

process_node::ProcessingState process_node::get_processing_state() const
{
	return m_pstate;
}

void process_node::set_idle() 
{ 
	if (m_pstate == Busy)
		return;
	m_pstate = Idle; 
	m_iter_count = 0; 
}

void process_node::set_name(String name) 
{ 
	m_proc_name = name; 
	m_is_dirty = true;
}

String process_node::getShortName()
{
	String longname = get_name();
	if (longname.length() < 16)
		return longname;
	return longname.upToFirstOccurrenceOf(" - ", false, false);
}

StringArray process_node::get_arguments() 
{ 
	return{ "Command line not used" }; 
}

String process_node::get_error_text()
{
	if (m_cp != nullptr)
	{
		String temp = m_cp->readAllProcessOutput();
		if (temp.length() > 2048)
			return "CDP error output too long";
		if (temp.length()>0)
			return temp;
		return "Could not get CDP program error output";
	}
	return "Child process does not exist";
}

void process_node::set_parameter(int index, var v)
{
	if (index >= 0 && index < m_parameters.size())
	{
		m_parameters[index]->setValue(v);
		m_is_dirty = true;
	}
}

ValueTree process_node::getStateValueTree()
{
	ValueTree result("proc_state");
	if (m_bypassmode != BypassNotSupported)
    {
		if (m_bypassmode == Bypassed)
			result.setProperty("bypass", true, nullptr);
		else result.setProperty("bypass", false, nullptr);
    }
	if (m_dyn_props.contains("taillen") == true)
		result.setProperty("taillen", m_dyn_props["taillen"], nullptr);
	ValueTree parameters_tree("parameters");
	for (int i = 0; i < m_parameters.size(); ++i)
	{
		ValueTree param_tree("param_"+String(i));
		param_tree.setProperty("name", m_parameters[i]->m_name, nullptr);
		param_tree.setProperty("val", m_parameters[i]->getValue(), nullptr);
		if (m_parameters[i]->hasEnvelope() == true)
		{
			ValueTree env_tree("envelope");
			param_tree.addChild(env_tree, -1, nullptr);
			env_tree.setProperty("enab", m_parameters[i]->m_automation_enabled, nullptr);
			auto env = m_parameters[i]->get_envelope();
			if (env->m_script.isEmpty() == false)
				env_tree.setProperty("script", env->m_script, nullptr);
			for (int j = 0; j < env->GetNumNodes(); ++j)
			{
				ValueTree point_tree("pt");
				auto& env_pt = env->GetNodeAtIndex(j);
				point_tree.setProperty("x", env_pt.Time, nullptr);
				point_tree.setProperty("y", env_pt.Value, nullptr);
				env_tree.addChild(point_tree, -1, nullptr);
			}
		}
		parameters_tree.addChild(param_tree, -1, nullptr);
	}
	result.addChild(parameters_tree, -1, nullptr);
	ValueTree customstate = getAdditionalStateValueTree();
	if (customstate.isValid()==true)
		result.addChild(customstate, -1, nullptr);
	return result;
}

ValueTree process_node::getAdditionalStateValueTree()
{
	return ValueTree();
}

void process_node::restoreStateFromValueTree(ValueTree tree)
{
	if (m_bypassmode != BypassNotSupported)
	{
		if ((bool)tree.getProperty("bypass") == true)
			m_bypassmode = Bypassed;
		else m_bypassmode = NotBypassed;
	}
	if (m_dyn_props.contains("taillen") == true)
		m_dyn_props.set("taillen", tree.getProperty("taillen"));
	ValueTree paramstree = tree.getChildWithName("parameters");
	for (int i = 0; i < paramstree.getNumChildren(); ++i)
	{
		ValueTree param = paramstree.getChild(i);
		if (i >= m_parameters.size())
		{
			Logger::writeToLog("Stored parameter " + param.getProperty("name").toString() + " index out of current parameters range");
			continue;
		}
		if (m_parameters[i]->getValue().isDouble()==true)
			m_parameters[i]->setValue((double)param.getProperty("val", m_parameters[i]->m_default_value));
		else if (m_parameters[i]->getValue().isInt() == true)
			m_parameters[i]->setValue((int)param.getProperty("val", m_parameters[i]->m_default_value));
		else if (m_parameters[i]->getValue().isBool() == true)
			m_parameters[i]->setValue((bool)param.getProperty("val", m_parameters[i]->m_default_value));
		else if (m_parameters[i]->getValue().isString() == true)
			m_parameters[i]->setValue(param.getProperty("val", m_parameters[i]->m_default_value));
		ValueTree envtree = param.getChildWithName("envelope");
		if (envtree.isValid() == true)
		{
			auto env = m_parameters[i]->get_envelope();
			env->ClearAllNodes();
			m_parameters[i]->m_automation_enabled = envtree.getProperty("enab");
			var script_txt = envtree.getProperty("script");
			if (script_txt.isString() == true)
				env->m_script = script_txt;
			int numpoints = envtree.getNumChildren();
			for (int j = 0; j < numpoints; ++j)
			{
				ValueTree pt = envtree.getChild(j);
				double x = pt.getProperty("x");
				double y = pt.getProperty("y");
				env->AddNode({ x,y });
			}
			env->SortNodes();
		}
	}
	ValueTree customstate = tree.getChildWithName("customstate");
	if (customstate.isValid() == true)
		restoreAdditionalStateFromValueTree(customstate);
	m_is_dirty = true;
}

void process_node::restoreAdditionalStateFromValueTree(ValueTree)
{
}

MD5 process_node::get_state_hash()
{
	MemoryBlock block;
	for (int i = 0; i < m_parameters.size(); ++i)
	{
		block.append((void*)&m_parameters[i]->m_envelope_change_count, sizeof(int));
		var val = m_parameters[i]->getValue();
		if (val.isBool() == true)
		{
			bool x = val;
			block.append((void*)&x, sizeof(bool));
		}
		if (val.isDouble() == true)
		{
			double x = val; 
			block.append((void*)&x, sizeof(double));
		}
        if (val.isInt() == true)
        {
			int x = val;
            block.append((void*)&x, sizeof(int));
        }
		if (val.isString() == true)
		{
			block.append(val.toString().getCharPointer(), val.toString().length());
		}
	}
	for (auto& src : m_sources)
	{
		for (const auto& fn : src->get_output_filenames())
		{
			block.append(fn.getCharPointer(), fn.getNumBytesAsUTF8());
		}
	}
	block.append(m_type_name.getCharPointer(), m_type_name.getNumBytesAsUTF8());
	double tail = m_dyn_props["taillen"];
	block.append((void*)&tail, sizeof(double));
	block.append((void*)&m_unique_id, sizeof(int));
	MD5 h = extra_state_hash();
	if (h!=MD5())
		block.append(h.getChecksumDataArray(),16);
	MD5 hashresult(block);
	return hashresult;
}

double process_node::get_render_elapsed_milliseconds() const
{
	return m_render_elapsed;
}

int process_node::get_num_supported_input_files() const
{
	return m_num_supported_input_files;
}

String process_node::getSourceFileName(int src, int index)
{
	if (src >= m_sources.size())
		return String();
	if (index >= m_sources[src]->get_output_filenames().size())
		return String();
	return m_sources[src]->get_output_filenames()[index];
}

void process_node::seek(double s) 
{
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	if (m_mmap_reader != nullptr)
	{
        m_file_readpos = s*m_mmap_reader->sampleRate;
	}
}

void process_node::processAudio(float ** outputChannelData, int numOutputChannels, int numSamples, double sr)
{
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	if (numSamples*numOutputChannels>m_resampler_outbuf.size())
        m_resampler_outbuf.resize(numSamples*numOutputChannels);
    if (m_mmap_reader == nullptr)
	{
		for (int i = 0; i < numSamples; ++i)
		{
			for (int j = 0; j < numOutputChannels; ++j)
				outputChannelData[j][i] = 0.0f;
		}
		return;
	}
    int chans_in_reader = m_mmap_reader->numChannels;
    m_resampler->SetRates(m_mmap_reader->sampleRate, sr);
    double* inbuf = nullptr;
    int wanted = m_resampler->ResamplePrepare(numSamples, chans_in_reader, &inbuf);
    float mmreadbuf[64];
    int64_t readerlen = m_mmap_reader->lengthInSamples;
    for (int i=0;i<wanted;++i)
    {
        m_mmap_reader->getSample(m_file_readpos, mmreadbuf);
        for (int j=0;j<chans_in_reader;++j)
            inbuf[i*chans_in_reader+j]=mmreadbuf[j];
        ++m_file_readpos;
        if (m_file_readpos>=readerlen)
            m_file_readpos = 0;
    }
    m_resampler->ResampleOut(m_resampler_outbuf.data(), wanted, numSamples, chans_in_reader);
    if (chans_in_reader == 1)
	{
		for (int i = 0; i<numSamples; ++i)
		{
            outputChannelData[0][i] = m_resampler_outbuf[i];
            outputChannelData[1][i] = m_resampler_outbuf[i];
        }
	}
	if (chans_in_reader>1)
	{
		int chans_to_play = chans_in_reader;
		if (chans_to_play > numOutputChannels)
			chans_to_play = numOutputChannels;
		for (int i = 0; i < numSamples; ++i)
		{
            for (int j = 0; j < chans_to_play; ++j)
			{
                outputChannelData[j][i] = m_resampler_outbuf[i*chans_in_reader+j];
			}
		}
	}
}

double process_node::getPlaybackPosition()
{
	if (m_mmap_reader != nullptr)
        return (double)m_file_readpos/m_mmap_reader->sampleRate;
	return 0.0;
}

void process_node::releaseResources()
{
    std::lock_guard<std::mutex> locker(m_playback_mutex);
    m_resampler = nullptr;
    m_resampler_outbuf = std::vector<double>();
}

void process_node::prepareToPlay(int expectedbufsize, int numchans , double sr)
{
    std::lock_guard<std::mutex> locker(m_playback_mutex);
    if (m_resampler==nullptr)
        m_resampler = std::make_unique<WDL_Resampler>();
    m_resampler_outbuf.resize(4096);
}

void process_node::sendActionMessageTrampoline(const String & str)
{
	sendActionMessage(str);
}

std::mt19937 g_rand_gen;

std::set<String> g_test_cache;

int process_node::countReadySources()
{
    int result = 0;
    for (auto& e: m_sources)
        if (e->m_topo_render_state==TopoRenderState::FinishedOK)
            ++result;
    return result;
}

void process_node::setTailLength(double len)
{
	if (len != m_tail_length)
	{
		m_tail_length = jlimit(0.0, 600.0, len);
		m_is_dirty = true;
	}
}

bool process_node::prepareToRender()
{
    m_topo_render_state = TopoRenderState::NeedsRendering;
    //for (int i = 0; i < m_destinations.size(); ++i)
    //    --m_destinations[i]->m_ready_sources=0;
    return false;
    MD5 hash = get_state_hash();
	if (g_test_cache.count(hash.toHexString()))
	{
		Logger::writeToLog(get_name() + " was cached!");
		m_pstate = FinishedOK;
		//for (int i = 0; i < m_destinations.size(); ++i)
		//	++m_destinations[i]->m_ready_sources;
		return true;
	}
    return false;
}

TopoRenderState process_node::performRender()
{
    if (m_topo_render_state == TopoRenderState::NeedsRendering
        && countReadySources()==m_sources.size())
    {
        m_topo_render_state = TopoRenderState::Rendering;
        m_test_thread_finished = false;
        std::thread th([this]()
                       {
                           //Logger::writeToLog(get_name() + " does mock processing...");
                           std::uniform_int_distribution<int> dist(1, 10);
                           volatile double acc = 0.0;
                           int64_t iterations = dist(g_rand_gen)*40000000;
                           for (int i=0;i<iterations;++i)
                           {
                               acc+=sin(i*0.0001);
                               //m_render_progress = 1.0/iterations*i;
                           }
                           Logger::writeToLog(String(acc));
                           //Thread::sleep(dist(g_rand_gen)*200);
                           //Logger::writeToLog(get_name() + " finished mock processing...");
                           m_test_thread_finished = true;
                       });
        th.detach();
        return TopoRenderState::Rendering;
    }
    if (m_topo_render_state == TopoRenderState::Rendering &&
        m_test_thread_finished == true)
    {
        Logger::writeToLog(get_name()+" finishes processing OK");
        m_topo_render_state = TopoRenderState::FinishedOK;
        return TopoRenderState::FinishedOK;
    }
    if (m_topo_render_state == TopoRenderState::Rendering &&
        m_test_thread_finished == false)
    {
        return TopoRenderState::Rendering;
    }
    return TopoRenderState::Idle;
    /*
    if (m_topo_render_state == FinishedOK)
        return TopoRenderState::Idle;
	if (m_pstate == Busy && m_test_thread_finished==true)
	{
		for (int i = 0; i < m_destinations.size(); ++i)
			++m_destinations[i]->m_ready_sources;
		Logger::writeToLog(get_name() + " finished rendering");
		//MD5 hash = get_state_hash();
		//g_test_cache.insert(hash.toHexString());
		m_pstate = FinishedOK;
		return FinishedOK;
	}
	if (m_pstate == Busy && m_test_thread_finished == false)
		return Busy;
	if (m_ready_sources >= m_sources.size())
	{
		
		m_test_thread_finished = false;
		m_pstate = Busy;
		std::thread th([this]() 
		{
			Logger::writeToLog(get_name() + " does mock processing...");
			std::uniform_int_distribution<int> dist(1, 10);
			Thread::sleep(dist(g_rand_gen)*200);
			Logger::writeToLog(get_name() + " finished mock processing...");
			m_test_thread_finished = true;
		});
		th.detach();
		return Busy;
		
	}
	m_pstate = Idle;
	return Idle;
	*/
}

void process_node::init_child_process()
{
	m_cp = childprocess_pool::get_instance()->obtain_child_process();
	++m_iter_count;

}

void process_node::start_benchmark()
{
	m_render_start_time = Time::getMillisecondCounterHiRes();
	m_render_elapsed = 0.0;
}

void process_node::end_benchmark()
{
	m_render_elapsed = Time::getMillisecondCounterHiRes() - m_render_start_time;
}

process_node::SourcesState process_node::are_sources_finished()
{
	int okcount = 0;
	int errcount = 0;
	for (auto& e : m_sources)
	{
		if (e->get_processing_state() == FinishedOK)
			++okcount;
		if (e->get_processing_state() == FinishedWithError)
			++errcount;
	}
	if (okcount == m_sources.size())
		return SourcesState::FinishedOK;
	if (errcount > 0)
		return SourcesState::FinishedWithError;
	return SourcesState::NotFinished;
}

void process_node::tick_sources()
{
	for (auto& e : m_sources)
		e->tick();
}

void process_node::output_errors()
{
	if (m_cp != nullptr && m_cp->getExitCode() != 0)
	{
		Logger::writeToLog(m_cp->readAllProcessOutput());
		//std::cout << m_cp->readAllProcessOutput().toStdString() << "\n";
	}
}

void process_node::setAudioFileToPlay(String fn)
{
	if (fn.isEmpty() == true)
	{
        m_num_current_output_channels = 0;
        m_playback_mutex.lock();
        m_mmap_reader = nullptr;
        m_playback_mutex.unlock();
		return;
	}
	if (fn.containsIgnoreCase(".wav") == false)
		return;
    WavAudioFormat wavformat;
    auto reader = wavformat.createMemoryMappedReader(File(fn));
    if (reader!=nullptr)
    {
        reader->mapEntireFile();
        int64_t oldplaypos = m_file_readpos;
        m_playback_mutex.lock();
        m_mmap_reader = std::unique_ptr<MemoryMappedAudioFormatReader>(reader);
        if (oldplaypos<m_mmap_reader->lengthInSamples)
            m_file_readpos = oldplaypos;
        else m_file_readpos = 0;
        m_num_current_output_channels = m_mmap_reader->numChannels;
        m_playback_mutex.unlock();
    }
}

bool process_graph::nodeBelongsToGraph(process_node* node)
{
	for (auto& e : m_nodes)
		if (e.get() == node)
			return true;
	return false;
}

String process_graph::renderNode(process_node* node, bool blockcurrentthread)
{
	if (node == nullptr)
		return "Node is null";
	if (nodeBelongsToGraph(node) == false)
		return "Node does not belong to this graph";
	m_render_state = GraphRenderState::Idle;
	double t0 = Time::getMillisecondCounterHiRes();
	while (true)
	{
		node->tick();
		Thread::sleep(10);
		if (m_render_state == GraphRenderState::FinishedError)
			return "Blocking render failed with error";
		if (node->get_processing_state() == process_node::FinishedOK)
		{
			return String();
		}
		double t1 = Time::getMillisecondCounterHiRes();
		if (t1 - t0 > 20000.0)
		{
			return "Node rendering took too long";
		}
	}
	return String();
}

node_ref process_graph::createNode(String node_type)
{
	auto new_node = node_factory::instance()->create_from_name(node_type);
	if (new_node == nullptr)
		return nullptr;
	new_node->ProcessErrorFunc = [this](process_node* node)
	{
		m_render_state = GraphRenderState::FinishedError;
	};
	new_node->ProcessFinishedFunc = [this](process_node* node)
	{
		m_render_state = GraphRenderState::FinishedOK;
	};
    new_node->m_owner_graph = this;
	m_nodes.push_back(new_node);
	return new_node;
}

node_ref process_graph::duplicateNode(node_ref src, bool with_connections)
{
	auto dupl_node = node_factory::instance()->duplicate_node(src, with_connections);
	if (dupl_node == nullptr)
		return nullptr;
	dupl_node->ProcessErrorFunc = [this](process_node* node)
	{
		m_render_state = GraphRenderState::FinishedError;
	};
	dupl_node->ProcessFinishedFunc = [this](process_node* node)
	{
		m_render_state = GraphRenderState::FinishedOK;
	};
	dupl_node->m_owner_graph = this;
	m_nodes.push_back(dupl_node);
	return dupl_node;
}

bool process_graph::removeNode(node_ref node)
{
	for (int i=0;i<m_nodes.size();++i)
		if (m_nodes[i] == node)
		{
			disconnect_node_from_graph(node);
			m_nodes.erase(m_nodes.begin() + i);
			return true;
		}
	return false;
}

bool process_graph::removeNode(process_node* node)
{
	for (int i = 0; i<m_nodes.size(); ++i)
		if (m_nodes[i].get() == node)
		{
			//disconnect_node_from_graph(node);
			m_nodes.erase(m_nodes.begin() + i);
			return true;
		}
	return false;
}
