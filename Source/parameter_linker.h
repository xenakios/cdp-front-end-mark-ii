#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"
#include "sol.hpp"

class ParameterLinker
{
public:
	ParameterLinker();
	void addMasterParameter(node_parameter* par);
	void addSlaveParameter(node_parameter* master, node_parameter* slave, String mappingcode="return x");
	void setSlaveMappingScript(node_parameter* master, node_parameter* slave, String code);
private:
	struct parameter_slave_t : public Value::Listener
	{
		parameter_slave_t() {}
		parameter_slave_t(String mapping)
		{
			setMappingScript(mapping);
		}
		~parameter_slave_t()
		{
            m_master_par->removeValueListener(this);
		}
		void setMappingScript(String txt)
		{
			m_script = txt;
		}
		node_parameter* m_par = nullptr;
		node_parameter* m_master_par = nullptr;
		sol::state* m_lua = nullptr;
		String m_script;
		void valueChanged(Value& v) override;
	};
	struct parameter_master_t
	{
		node_parameter* m_par = nullptr;
		std::vector<std::shared_ptr<parameter_slave_t>> m_slave_pars;
	};
	std::vector<std::shared_ptr<parameter_master_t>> m_links;
	sol::state m_lua;
	int findMaster(node_parameter* par);
};


