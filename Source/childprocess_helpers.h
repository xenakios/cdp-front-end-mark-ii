#pragma once

#include <memory>
#include <future>
#include "../JuceLibraryCode/JuceHeader.h"

using SharedChildProc = std::shared_ptr<ChildProcess>;

class childprocess_pool
{
public:
	childprocess_pool() : m_child_processes(2)
	{
		// At the moment can't do a proper automatic initial count on a hyperthreaded CPU
		//Logger::writeToLog("There are " + String(SystemStats::getNumCpus()) + " CPU cores/threads");
	}
	size_t getNumAvailable() { return m_child_processes.size(); }
	void setNumAvailable(size_t sz)
	{
		if (m_child_processes.size() > 0 && sz == m_child_processes.size())
			return;
		// 8 seems like a reasonable maximum concurrency, the file IO will likely be the biggest bottleneck anyway
		sz = jlimit(size_t(1), size_t(8), sz);
		// Hopefully this method is not called while the processes are running to begin with...
		// But in case that happens, let's do a nasty thing like this...
		for (auto& e : m_child_processes)
			if (e.m_proc->isRunning() == true)
			{
				e.m_proc->waitForProcessToFinish(1000);
				e.m_is_free = true;
			}
		m_child_processes.resize(sz);
	}
	SharedChildProc obtain_child_process()
	{
		for (auto& e : m_child_processes)
		{
			if (e.m_is_free == true)
			{
				e.m_is_free = false;
				//Logger::writeToLog("child process pool returns " + String((uint64)e.m_proc.get()));
				return e.m_proc;
			}
		}
		return nullptr;
	}
	void release_child_process(SharedChildProc& ob)
	{
		for (auto& e : m_child_processes)
		{
			if (e.m_proc == ob)
			{
				e.m_is_free = true;
				ob = nullptr;
				//Logger::writeToLog("pool got back " + String((uint64)e.m_proc.get()));
				return;
			}
		}
		Logger::writeToLog("Could not release a child process to pool!");
	}
	static childprocess_pool* get_instance(bool destroy = false)
	{
		static childprocess_pool* inst = nullptr;
		if (destroy == true)
		{
			delete inst;
			inst = nullptr;
			return nullptr;
		}
		if (inst == nullptr)
			inst = new childprocess_pool;
		return inst;
	}
private:
	struct entry_t
	{
		entry_t()
		{
			m_proc = std::make_shared<ChildProcess>();
		}
		SharedChildProc m_proc;
		bool m_is_free = true;
		//entry_t(const entry_t&) = delete;
		//entry_t& operator=(const entry_t&) = delete;
	};
	std::vector<entry_t> m_child_processes;
};

/* Runs a ChildProcess with the given arguments asynchronously and when finished, calls the given callback
on the message thread.
*/
inline std::future<void> runChildProcessAsync(StringArray args, int timeout, std::function<void(String)> completion_callback)
{
	auto r = std::async(std::launch::async, [args, timeout, completion_callback]()
	{
		ChildProcess proc;
		proc.start(args);
		bool b = proc.waitForProcessToFinish(timeout);
		auto exitcode = proc.getExitCode();
		MessageManager::callAsync([completion_callback, b, exitcode]()
		{
			String completionmsg;
			if (b == false)
				completionmsg = "Timeout failed";
			if (exitcode != 0)
				completionmsg = "Process returned error " + String(exitcode);
			completion_callback(completionmsg);
		});
	});
	return r;
}

inline void test_thread(Label* lab)
{
	std::thread th([lab]()
	{
		Thread::sleep(2000);
		MessageManager::callAsync([lab]() 
		{
			static int cnt = 0;
			lab->setText("Thread has finished "+String(cnt), dontSendNotification);
			++cnt;
		});
	});
	th.detach();
}
