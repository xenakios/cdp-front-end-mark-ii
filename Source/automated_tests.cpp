#include "automated_tests.h"
#include "cdp_graph.h"
#include "node_factory.h"

#include <thread>
#ifdef WIN32
#include "windows.h"
#endif

String get_test_audio_file_name()
{
#ifdef WIN32
	return "C:\\MusicAudio\\reaper_temp\\cdp_batch_test\\count_10sec.wav";
#else
	return "/Users/teemu/AudioProjects/cdp_test_files1/count_10sec.wav";
#endif
}

struct test_result_t
{
    test_result_t() {}
    test_result_t(String nodename, String errmsg, double outlen, double renderelapsed, double initelapsed) :
    m_nodename(nodename), m_errmsg(errmsg), m_outlen(outlen),
    m_render_elapsed(renderelapsed), m_init_elapsed(initelapsed) {}
    String m_nodename;
    String m_errmsg;
    double m_outlen = 0.0;
    double m_render_elapsed = 0.0;
    double m_init_elapsed = 0.0;
};

void run_all_nodes_test(std::atomic<TestState>& state)
{
#ifdef WIN32
	DWORD dwMode = SetErrorMode(SEM_NOGPFAULTERRORBOX);
	SetErrorMode(dwMode | SEM_NOGPFAULTERRORBOX);
#else
	//TODO: Suppress crash report dialog on macOS too
#endif

	auto testfunc = [&state]() mutable
	{
		state = TestState::Running;
        std::vector<test_result_t> results;
        double t0 = Time::getMillisecondCounterHiRes();
		int num_pcm_nodestested = 0;
		int num_pvoc_nodestested = 0;
		int numnodesrenderedok = 0;
		int num_node_render_errors = 0;
		int num_param_errors = 0;
		double out_len_accum = 0.0;
		try
		{
			process_graph graph;
			auto node_in_pcm = graph.createNode("Input Sound File");
			node_in_pcm->set_parameter(0, get_test_audio_file_name());
			double infilelen = get_audio_source_info_cached(get_test_audio_file_name()).get_length_seconds();
			auto pvoc_anal_node = graph.createNode("PVOC ANAL - Create PVOC analysis file.");
			if (pvoc_anal_node == nullptr)
			{
				state = TestState::Finished;
				Logger::writeToLog("pvoc anal null");
				return;
			}
			connect_nodes(node_in_pcm, pvoc_anal_node);
			auto pvoc_resynth_node = graph.createNode("PVOC RESYNTH - Resynthesize .ana file to .wav file");
			if (pvoc_resynth_node == nullptr)
			{
				state = TestState::Finished;
				Logger::writeToLog("pvoc resynth null");
				return;
			}
			for (auto& e : node_factory::instance()->m_entries)
			{
				if (state == TestState::CancelRequested)
				{
					state = TestState::Finished;
					Logger::writeToLog("Cancel requested, finishing...");
					return;
				}
				if (e.m_is_plugin == false && e.m_name.containsIgnoreCase("selfsim") == false)
				{
                    double init_t0 = Time::getMillisecondCounterHiRes();
                    auto node_to_test = graph.createNode(e.m_name);
                    double init_t1 = Time::getMillisecondCounterHiRes();
					if (node_to_test == nullptr)
					{
						Logger::writeToLog("Could not create " + e.m_name);
						continue;
					}
					node_to_test->ProcessFinishedFunc = [&graph, &numnodesrenderedok](process_node* nodeptr)
					{
						++numnodesrenderedok;
						graph.setRenderState(GraphRenderState::FinishedOK);
					};
					node_to_test->ProcessErrorFunc = [&graph, &num_node_render_errors, &num_param_errors](process_node* nodeptr)
					{
						graph.setRenderState(GraphRenderState::FinishedError);
						String errtxt = nodeptr->get_error_text();
						if (errtxt.containsIgnoreCase("param"))
							++num_param_errors;
						else
							Logger::writeToLog(nodeptr->get_error_text());
						++num_node_render_errors;
					};
					if (node_to_test->get_supported_input_type() == "wav" && node_to_test->get_output_type() == "wav")
					{
						connect_nodes(node_in_pcm, node_to_test);
						//Logger::writeToLog("Rendering " + node_to_test->get_name());
						visit_sources(node_to_test.get(), [](process_node* node) { node->set_idle(); });
                        double render_t0 = Time::getMillisecondCounterHiRes();
                        String err = graph.renderNode(node_to_test.get(), true);
                        double render_t1 = Time::getMillisecondCounterHiRes();
						if (err.isEmpty() == false)
                        {
                            err = node_to_test->get_error_text();
                            results.emplace_back(node_to_test->get_name(),err,0.0,render_t1-render_t0,init_t1-init_t0);
                            //Logger::writeToLog(err);
                        }
						else
						{
							
                            if (node_to_test->get_output_filenames().size()>0)
                            {
								double outlen = get_audio_source_info_cached(node_to_test->get_output_filenames()[0]).get_length_seconds();
                                results.emplace_back(node_to_test->get_name(),err,outlen,render_t1-render_t0,init_t1-init_t0);
                                out_len_accum += outlen;
                            }
						}

						graph.removeNode(node_to_test);
						++num_pcm_nodestested;
					}
					if (node_to_test->get_supported_input_type() == "ana" && node_to_test->get_output_type() == "ana")
					{
						connect_nodes(pvoc_anal_node, node_to_test);
						connect_nodes(node_to_test, pvoc_resynth_node);
						//Logger::writeToLog("Rendering pvoc " + node_to_test->get_name());
						visit_sources(pvoc_resynth_node.get(), [](process_node* node) { node->set_idle(); });
						double render_t0 = Time::getMillisecondCounterHiRes();
                        String err = graph.renderNode(pvoc_resynth_node.get(), true);
                        double render_t1 = Time::getMillisecondCounterHiRes();
						if (err.isEmpty() == false)
                        {
							//Logger::writeToLog(err);
                            err = node_to_test->get_error_text();
                            results.emplace_back(node_to_test->get_name(),err,0.0,render_t1-render_t0,init_t1-init_t0);
                        }
						else
						{
							
                            if (pvoc_resynth_node->get_output_filenames().size()>0)
                            {
								double outlen = get_audio_source_info_cached(pvoc_resynth_node->get_output_filenames()[0]).get_length_seconds();
                                results.emplace_back(node_to_test->get_name(),err,outlen, render_t1-render_t0,init_t1-init_t0);
                                out_len_accum += outlen;
                            }
						}
						graph.removeNode(node_to_test);
						++num_pvoc_nodestested;
					}
				}

			}
		}
		catch (std::exception& excep)
		{
			Logger::writeToLog(excep.what());
		}
		double t1 = Time::getMillisecondCounterHiRes();
		Logger::writeToLog(String::formatted("Finished test for %d PCM and %d pvoc nodes. Elapsed time %f milliseconds. %d errors.",
			num_pcm_nodestested, num_pvoc_nodestested, t1 - t0, num_node_render_errors));
		Logger::writeToLog(String::formatted("%d parameter related errors", num_param_errors));
		Logger::writeToLog(String::formatted("%f seconds of output audio generated", out_len_accum));
        String resultstxt;
        for (auto& e : results)
        {
            resultstxt << e.m_nodename.replaceCharacter(',', ' ') << "," << e.m_render_elapsed << "," << e.m_outlen;
            String errtxt = e.m_errmsg.replaceCharacters(",\n", "  ");
            resultstxt << "," << errtxt << "\n";
        }
        MessageManager::callAsync([resultstxt]()
        {
            SystemClipboard::copyTextToClipboard(resultstxt);
        });
        state = TestState::Finished;
	};
	std::thread th(testfunc);
	th.detach();
}
