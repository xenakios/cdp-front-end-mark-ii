#include "script_editor.h"

ScriptTextEditor::ScriptTextEditor(ValueTree data) : TextEditor(), m_data(data)
{
	setMultiLine(true);
	setReturnKeyStartsNewLine(true);
	setFont(Font(Font::getDefaultMonospacedFontName(), 14.0f, 0));
	if (m_data.isValid() && m_data.hasProperty("script"))
	{
		m_data.addListener(this);
		setText(m_data.getProperty("script"), dontSendNotification);
	}
	addListener(this);
}

bool ScriptTextEditor::keyPressed(const KeyPress & ev)
{
	bool r = TextEditor::keyPressed(ev);
	//Logger::writeToLog(String(r));
	if (r == false)
	{
		if (ev == KeyPress::F3Key)
		{
			if (OnExecuteScript)
				OnExecuteScript(getText());
			return true;
		}
	}
	return r;
}

void ScriptTextEditor::valueTreePropertyChanged(ValueTree & treeWhosePropertyHasChanged, const Identifier & property)
{
	if (property == Identifier("script"))
	{
		const String& txt = treeWhosePropertyHasChanged.getProperty("script");
		if (txt!=getText())
			setText(txt, dontSendNotification);
	}
}

void ScriptTextEditor::textEditorTextChanged(TextEditor & ed)
{
	if (&ed == this)
	{
		m_data.setProperty("script", getText(), nullptr);
	}
}
