#include "plugin_gui.h"
#include "cdp_graph.h"

static Array <PluginWindow*> activePluginWindows;

PluginWindow::PluginWindow(Component* const pluginEditor,
	process_node* o,
	WindowFormatType t)
	: DocumentWindow(o->get_name(), Colours::lightblue,
		DocumentWindow::minimiseButton | DocumentWindow::closeButton),
	m_owner(o),
	type(t)
{
	//setUsingNativeTitleBar(false);
	setSize(400, 300);

	m_content_component = new PluginWindowContent(pluginEditor);
	m_content_component->m_tail_slider.get()->addListener(this);
	setContentOwned(m_content_component, true);

	setTopLeftPosition(50, 50);

	setVisible(true);

	m_content_component->m_num_inputs_combo.get()->addListener(this);
	m_content_component->m_num_outputs_combo.get()->addListener(this);

	activePluginWindows.add(this);
	startTimer(100);
    setAlwaysOnTop(true);
}

void PluginWindow::closeCurrentlyOpenWindowsFor(process_node* ptr)
{
	for (int i = activePluginWindows.size(); --i >= 0;)
		if (activePluginWindows.getUnchecked(i)->m_owner == ptr)
			delete activePluginWindows.getUnchecked(i);
}

PluginWindow* PluginWindow::getWindowFor(process_node* ptr)
{
	for (int i = activePluginWindows.size(); --i >= 0;)
		if (activePluginWindows.getUnchecked(i)->m_owner == ptr)
			return activePluginWindows.getUnchecked(i);
	return nullptr;
}

void PluginWindow::closeAllCurrentlyOpenWindows()
{
	if (activePluginWindows.size() > 0)
	{
		for (int i = activePluginWindows.size(); --i >= 0;)
			delete activePluginWindows.getUnchecked(i);

		Component dummyModalComp;
		dummyModalComp.enterModalState();
		MessageManager::getInstance()->runDispatchLoopUntil(50);
	}
}

PluginWindow::~PluginWindow()
{
	activePluginWindows.removeFirstMatchingValue(this);
	delete m_content_component;
	clearContentComponent();
}

void PluginWindow::moved()
{

}

void PluginWindow::closeButtonPressed()
{
	//owner->properties.set(getOpenProp(type), false);
	delete this;
}

void PluginWindow::sliderValueChanged(Slider * slid)
{
	if (slid == m_content_component->m_tail_slider.get())
	{
		m_owner->get_dynamic_properties().set("taillen", slid->getValue());
	}
}

void PluginWindow::timerCallback()
{
	if (m_inited == false)
	{
		double tail_len = m_owner->get_dynamic_properties()["taillen"];
		m_content_component->m_tail_slider.get()->setValue(tail_len);
		int numchans = m_owner->get_dynamic_properties()["num_ins_to_use"];
		m_content_component->m_num_inputs_combo.get()->setSelectedId(numchans, dontSendNotification);
		numchans = m_owner->get_dynamic_properties()["num_outs_to_use"];
		m_content_component->m_num_outputs_combo.get()->setSelectedId(numchans, dontSendNotification);
		m_inited = true;
	}
}

void PluginWindow::comboBoxChanged(ComboBox * c)
{
	if (c == m_content_component->m_num_inputs_combo.get())
	{
		m_owner->get_dynamic_properties().set("num_ins_to_use", c->getSelectedId());
	}
	if (c == m_content_component->m_num_outputs_combo.get())
	{
		m_owner->get_dynamic_properties().set("num_outs_to_use", c->getSelectedId());
	}
}

void PluginWindow::resized()
{
	DocumentWindow::resized();
	//if (m_content_component!=nullptr)
	//	m_content_component->resized();
}

PluginWindowContent::PluginWindowContent(Component * comp) : m_plugin_editor(comp),
m_flex1(FlexBox::Direction::row, FlexBox::Wrap::noWrap,FlexBox::AlignContent::stretch,
                FlexBox::AlignItems::flexStart,FlexBox::JustifyContent::flexStart),
m_num_inputs_combo("Num inputs",100),
m_num_outputs_combo("Num outputs",100),
m_tail_slider("Tail length",70,Slider::RotaryVerticalDrag,Slider::TextBoxRight)
{
	addAndMakeVisible(&m_menu_but);
	m_menu_but.setButtonText("Menu...");
    //m_flex1.items.add(FlexItem(80.0f,22.0f,m_menu_but).withMargin(3));
	addAndMakeVisible(&m_tail_slider);
	m_tail_slider.get()->setRange(0.0, 60.0, 1.0);
	m_tail_slider.get()->setValue(0.0);
    m_flex1.items.add(FlexItem(160.0f,22.0f,m_tail_slider).withMargin(3));
	addAndMakeVisible(&m_num_inputs_combo);
	m_flex1.items.add(FlexItem(160.0f,22.0f,m_num_inputs_combo).withMargin(3));
	addAndMakeVisible(&m_num_outputs_combo);
	m_flex1.items.add(FlexItem(160.0f,22.0f,m_num_outputs_combo).withMargin(3));
	addAndMakeVisible(comp);
	comp->setTopLeftPosition(0, 26);
	for (int i = 1; i < 33; ++i)
	{
		m_num_inputs_combo.get()->addItem(String(i), i);
		m_num_outputs_combo.get()->addItem(String(i), i);
	}
	m_num_inputs_combo.get()->setSelectedId(2, dontSendNotification);
	m_num_outputs_combo.get()->setSelectedId(2, dontSendNotification);
	setSize(100, 26);
	startTimer(50);
}

void PluginWindowContent::timerCallback()
{
	if (m_plug_w != m_plugin_editor->getWidth() || m_plug_h != m_plugin_editor->getHeight())
	{
		m_plug_w = m_plugin_editor->getWidth();
		m_plug_h = m_plugin_editor->getHeight();
		setSize(std::max(m_plug_w,500), m_plugin_editor->getHeight() + 30);
	}
}

void PluginWindowContent::resized() 
{ 
	m_flex1.performLayout(juce::Rectangle<int>{0, 0, getWidth(), 25}); 
}
