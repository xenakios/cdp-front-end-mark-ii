#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "sqlite3.h"

inline void sqlitebind(sqlite3_stmt* stmt, int index, int i)
{
	if (sqlite3_bind_int(stmt, index, i) != SQLITE_OK)
		throw std::runtime_error("failed sqlite3_bind_int");
}
/*
inline void sqlitebind(sqlite3_stmt* stmt, int index, const std::string& str)
{
if (sqlite3_bind_text(stmt, index, str.c_str(), str.length(), SQLITE_TRANSIENT) != SQLITE_OK)
throw std::runtime_error("failed sqlite3_bind_text with std::string");
}
*/
inline void sqlitebind(sqlite3_stmt* stmt, int index, const String& str)
{
	if (sqlite3_bind_text(stmt, index, str.toRawUTF8(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
		throw std::runtime_error("failed sqlite3_bind_text with juce::String");
}

inline void sqlitebind(sqlite3_stmt* stmt, int index, const std::vector<uint8_t>& x)
{
	if (sqlite3_bind_blob(stmt, index, x.data(), x.size(), SQLITE_STATIC) != SQLITE_OK)
		throw std::runtime_error("failed sqlite3_bind_blob with std::vector<uint8_t>");
}

inline void sqlitebind(sqlite3_stmt* stmt, int index, const ValueTree& vt)
{
	juce::MemoryOutputStream stream;
	vt.writeToStream(stream);
	if (sqlite3_bind_blob(stmt, index, stream.getData(), stream.getDataSize(), SQLITE_TRANSIENT) != SQLITE_OK)
		throw std::runtime_error("failed sqlite3_bind_blob with ValueTree");
}

template <typename... Ts>
inline void BindAllParamPack(sqlite3_stmt* stmt, const Ts& ... args)
{
	int index = 0;
	(void)std::initializer_list<int>{(sqlitebind(stmt, ++index, args), 0)...};
}

class SQLStatement
{
public:
	SQLStatement() {}
	SQLStatement(sqlite3* db, const char* sql)
	{
		sqlite3_prepare_v2(db, sql, -1, &m_stmt, NULL);
	}
	~SQLStatement()
	{
		if (m_stmt != nullptr)
		{
			sqlite3_clear_bindings(m_stmt);
			sqlite3_reset(m_stmt);
			sqlite3_finalize(m_stmt);
		}
	}
	SQLStatement(const SQLStatement&) = delete;
	SQLStatement& operator=(const SQLStatement&) = delete;
	SQLStatement(SQLStatement&& other) : m_stmt(other.m_stmt) { other.m_stmt = nullptr; }
	SQLStatement& operator=(SQLStatement&& other) { std::swap(other.m_stmt, m_stmt); return *this; }
	sqlite3_stmt* get() noexcept { return m_stmt; }
	bool isValid() const noexcept { return m_stmt != nullptr; }
private:
	sqlite3_stmt* m_stmt = nullptr;
};

template<typename F, typename... Args>
void executeSQL(sqlite3* db, const char* sql, F&& cb, Args&&... args)
{
	try
	{
		SQLStatement statement(db, sql);
		if (statement.isValid())
		{
			BindAllParamPack(statement.get(), args...);
			while (true)
			{
				int rc = sqlite3_step(statement.get());
				if (rc == SQLITE_ERROR)
				{
					std::cout << sqlite3_errmsg(db) << "\n";
					break;
				}
				if (rc == SQLITE_DONE)
				{
					break;
				}
				if (rc == SQLITE_ROW)
				{
					cb(statement.get());
				}
			}
		}
		else
			std::cout << sqlite3_errmsg(db) << "\n";
	}
	catch (std::exception& excep)
	{
		Logger::writeToLog(excep.what());
	}
}

template<typename... Args>
void executeSQLWithoutCallback(sqlite3* db, const char* sql, Args&&... args)
{
	auto empty_cb = [](sqlite3_stmt*) {};
	executeSQL(db, sql, empty_cb, args...);
}

