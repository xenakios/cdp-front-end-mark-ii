#pragma once

#include "cdp_graph.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include <vector>
#include <memory>
#include "sol.hpp"

class IAudioView
{
public:
	virtual ~IAudioView() {}
	virtual bool isValid() const = 0;
	virtual double getLength() const = 0;
	virtual void getAudioSamples(int64_t pos, float* buf, int nframes, int chans, double sr) = 0;
};

class LuaAudioFileView : public IAudioView
{
public:
	LuaAudioFileView(String fn) 
	{
		WavAudioFormat wavformat;
		auto* reader = wavformat.createMemoryMappedReader(File(fn));
		if (reader != nullptr)
		{
			m_mmreader = std::unique_ptr<MemoryMappedAudioFormatReader>(reader);
			m_mmreader->mapEntireFile();
		}
	}
	bool isValid() const override { return m_mmreader!=nullptr; }
	double getLength() const override
	{
		return 0.0;
	}
	void getAudioSamples(int64_t pos, float* buf, int nframes, int chans, double sr) override
	{
		if (m_mmreader == nullptr)
			return;
		int64_t readerlen = m_mmreader->lengthInSamples;
		float mmreaderbuf[128];
		int chansto_output = std::min<int>(chans,m_mmreader->numChannels);
		for (int i = 0; i < nframes; ++i)
		{
			int64_t postoread = pos + i;
			if (postoread >= 0 && postoread < readerlen)
			{
				m_mmreader->getSample(postoread, mmreaderbuf);
				for (int j = 0; j < chansto_output; ++j)
					buf[i*chansto_output + j] = mmreaderbuf[j];
			}
			else
			{
				for (int j = 0; j < chansto_output; ++j)
					buf[i*chansto_output + j] = 0.0f;
			}
		}
	}
private:
	std::unique_ptr<MemoryMappedAudioFormatReader> m_mmreader;
};

class LuaSubAudioView : public IAudioView
{
public:
	LuaSubAudioView(IAudioView* srcview, double start, double length) 
		: m_source_view(srcview), m_src_start(start), m_length(length)
	{

	}
	bool isValid() const override { return m_source_view != nullptr && m_length > 0.0; }
	double getLength() const override
	{
		return m_length;
	}
	void getAudioSamples(int64_t pos, float* buf, int nframes, int chans, double sr) override
	{
		if (isValid()==false)
			return;
	}
private:
	IAudioView* m_source_view = nullptr;
	double m_src_start = 0.0;
	double m_length = 0.0;
	breakpoint_envelope m_vol_env;
};

class LuaDSPNode : public process_node
{
public:
	LuaDSPNode(String procname);
	~LuaDSPNode() {}
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	void tick() override;
	String get_error_text() override { return m_error_text; }
	MD5 extra_state_hash() override;
	ValueTree getAdditionalStateValueTree() override;
	void restoreAdditionalStateFromValueTree(ValueTree tree) override;
	void connections_changed() override;
	void update_params() override;
	void sourcesHaveChanged() override;
	bool usesCommandLine() const override { return false; }
	String runScript(String scriptcode);
    Component* getCustomEditor() override;
    CustomGUIType getCustomEditorType2() override { return InspectorCustomEditor; };
private:
	ValueTree m_script_state;
    AudioBuffer<float> m_script_output_buf;
    int m_buf_sr = 0;
    std::unique_ptr<std::thread> m_thread;
    std::atomic<int> m_thread_state{ 0 };
    String m_output_fn;
    
};
