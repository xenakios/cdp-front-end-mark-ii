#pragma once

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

#include "../JuceLibraryCode/JuceHeader.h"

class LuaStateWrapper
{
public:
	LuaStateWrapper() { m_lua = luaL_newstate(); }
	~LuaStateWrapper() { if (m_lua != nullptr) lua_close(m_lua); }
	LuaStateWrapper(const LuaStateWrapper&) = delete;
	LuaStateWrapper& operator=(const LuaStateWrapper&) = delete;
	LuaStateWrapper(LuaStateWrapper&& other) : m_lua(other.m_lua) { other.m_lua = nullptr; }
	LuaStateWrapper& operator=(LuaStateWrapper&& other) { std::swap(m_lua, other.m_lua); return *this; }
	lua_State* get() { return m_lua; }
private:
	lua_State* m_lua = nullptr;
};

template<typename F>
inline void iterate_lua(lua_State *L, int index, F&& action)
{

	// Push another reference to the table on top of the stack (so we know
	// where it is, and this function can work for negative, positive and
	// pseudo indices
	lua_pushvalue(L, index);
	// stack now contains: -1 => table
	lua_pushnil(L);
	// stack now contains: -1 => nil; -2 => table
	while (lua_next(L, -2))
	{
		// stack now contains: -1 => value; -2 => key; -3 => table
		// copy the key so that lua_tostring does not modify the original
		lua_pushvalue(L, -2);
		// stack now contains: -1 => key; -2 => value; -3 => key; -4 => table
		action(L);
		// pop value + copy of key, leaving original key
		lua_pop(L, 2);
		// stack now contains: -1 => key; -2 => table
	}
	// stack now contains: -1 => table (when lua_next returns 0 it pops the key
	// but does not push anything.)
	// Pop table
	lua_pop(L, 1);
	// Stack is now the same as it was on entry to this function

}

String get_cdp_lua_defs_file();

template<typename T>
inline std::string make_string(T* str)
{
	if (str == nullptr)
		return std::string();
	return std::string(str);
}

struct sound_file_info_t
{
	sound_file_info_t() {}
	sound_file_info_t(double _sr, int _cycles, double _length)
		: sr(_sr), cycles(_cycles), length(_length) {}
	double sr = 44100.0;
	int cycles = 1;
	double length = 1000.0;
};

void set_sound_file_info(const sound_file_info_t& info);
sound_file_info_t get_sound_file_info();

template<typename T,typename F>
// T must be copy assignable
// F must be a callable with signature void(T&)
inline void run_with_and_revert(T& value, F&& f)
{
	static_assert(std::is_copy_assignable<T>::value, "T has to be copy assignable");
	T saved = value;
	f(value);
	value = saved;
}