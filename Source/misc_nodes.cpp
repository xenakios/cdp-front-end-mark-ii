#include "misc_nodes.h"
#include <memory>
#include "node_component.h"
#include "node_view.h"

inputfile_node::inputfile_node(String procname, String fn, int nameindex) : process_node(procname),
m_name_index(nameindex)
{
	m_type_name = m_proc_name;
	m_proc_name = "!No source file!";
#ifdef WIN32
	//String fn = g_propsfile->getValue("general/input_node_default", "");

#else
	//String fn = g_propsfile->getValue("general/input_node_default", "");

#endif
	m_parameters.emplace_back(std::make_shared<node_parameter>("File name", "$par0", fn));
	m_parameters.back()->m_param_type = parameterType::StringFile;
	m_parameters.back()->m_parent_node = this;
    m_parameters.back()->addValueListener(this);
	m_buses_layout.addOutputBus("wav");
}

inputfile_node::~inputfile_node()
{
    m_parameters[0]->removeValueListener(this);
}

StringArray inputfile_node::get_output_filenames()
{
	if (m_name_index == -1)
	{
		if (m_file_compatible == true)
			return StringArray{ m_parameters[0]->getValue().toString() };
		return StringArray();
	}
	return StringArray{ m_sources[0]->get_output_filenames()[m_name_index] };
}

String inputfile_node::get_supported_input_type()
{
	return String();
}

String inputfile_node::get_output_type()
{
	return "wav";
}

void inputfile_node::set_arguments(StringArray ar)
{
	m_arguments = ar;
	ProcessFinishedFunc(this);
}

int inputfile_node::getNumOutChansForNumInChans(int)
{
	auto info = get_audio_source_info_cached(m_parameters[0]->getValue().toString());
	return info.num_channels;
}

bool inputfile_node::prepareToRender()
{
    auto files = get_output_filenames();
    if (files.size() > 0)
    {
        File temp(files[0]);
        if (temp.existsAsFile() == false)
        {
            m_error_text = "File does not exist";
            m_topo_render_state = TopoRenderState::FinishedWithError;
            return true;
        }
        setAudioFileToPlay(files[0]);
        m_topo_render_state = TopoRenderState::Rendering;
        //updateDestinationsReadyCount();
    }
    return true;
}

TopoRenderState inputfile_node::performRender()
{
    if (m_topo_render_state == TopoRenderState::Rendering)
    {
        m_topo_render_state = TopoRenderState::FinishedOK;
        Logger::writeToLog(get_name()+" finishes OK");
    }
    return m_topo_render_state;
}

void inputfile_node::tick()
{
	if (m_pstate == Idle)
	{
		auto files = get_output_filenames();
		if (files.size() > 0)
		{
			File temp(files[0]);
			if (temp.existsAsFile() == false)
			{
				m_error_text = "File does not exist";
				m_pstate = FinishedWithError;
				ProcessErrorFunc(this);
				return;
			}
			setAudioFileToPlay(files[0]);
			m_pstate = FinishedOK;
			ProcessFinishedFunc(this);
		}
		else
		{
			m_error_text = "No files";
			m_pstate = FinishedWithError;
			ProcessErrorFunc(this);
		}

	}
}

void inputfile_node::valueChanged(Value& v)
{
	String fn = m_parameters[0]->getValue().toString();
	File file(fn);
	if (file.existsAsFile() == true)
	{
		auto info = get_audio_source_info_cached(fn);
		//Logger::writeToLog(info.m_format_name);
		if (info.m_format_name.startsWithIgnoreCase("wav"))
		{
			m_file_compatible = true;
			m_proc_name = file.getFileName();
			m_num_current_output_channels = info.num_channels;
			if (m_component && m_component->m_node_view)
			{
				m_component->m_node_view->repaint();
			}
		}
		else
		{
			m_file_compatible = false;
			// Conversion business...
			Logger::writeToLog("File " + fn + " not compatible, needs to be converted...");
		}

	}
}



void inputfile_node::connections_changed()
{
	auto info = get_audio_source_info_cached(m_parameters[0]->getValue().toString());
	m_num_current_output_channels = info.num_channels;
	sendActionMessage("conns_changed");
}

StringArray envelope_generator_node::get_output_filenames()
{
	return m_output_filenames;
}

String envelope_generator_node::get_supported_input_type()
{
	return String();
}

String envelope_generator_node::get_output_type()
{
	return "cdptextenvelope";
}

void envelope_generator_node::tick()
{
	tick_sources();
	if (m_pstate == Idle)
	{
		m_output_filenames.clear();
		for (int i = 0; i < m_destinations.size(); ++i)
		{
			int num_params = (int)m_destinations[i]->get_parameters().size();
			for (int j = 0; j < num_params; ++j)
			{
				auto par = m_destinations[i]->get_parameters()[j];
				if (par->m_can_be_automated == true)
				{
					Logger::writeToLog(m_destinations[i]->get_name() + "/" + par->m_name);
				}
			}
		}
		m_pstate = FinishedOK;
	}
}

std::shared_ptr<breakpoint_envelope> node_parameter::get_envelope(bool init_default_nodes)
{
	if (m_can_be_automated == false)
		return nullptr;
	if (m_envelope == nullptr)
	{
		m_envelope = std::make_shared<breakpoint_envelope>();
		++m_envelope_change_count;
	}
	if (init_default_nodes == true && m_envelope->GetNumNodes() == 0)
	{
		double normalized = jmap((double)m_default_value.getValue(),
			(double)m_min_value.getValue(), (double)m_max_value.getValue(), 0.0, 1.0);
		m_envelope->AddNode(envelope_node(0.0, normalized));
		m_envelope->AddNode(envelope_node(1.0, normalized));
	}
	return m_envelope;
}

fileconverter_node::fileconverter_node(String procname)
{
	m_proc_name = procname;
	m_type_name = procname;
	parameter_ref par=std::make_shared<node_parameter>("Samplerate", "", 
		44100, StringArray{ "44100","48000","88200","96000" }, Array<var>{ 44100,48000,88200,96000 });
	m_parameters.push_back(par);
	parameter_ref par2(new node_parameter("Bit depth", "", 32, { "PCM 16","PCM 24","Float 32" }, { 16,24,32 }));
	m_parameters.push_back(par2);
	parameter_ref par3(new node_parameter("Channel count", "", 1000,
	{ "Original","1","2","3","4","5","6","7","8","9","10","12","14","16","18","20","24","28","32","40","48","56","64" },
	{ 1000,1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,40,48,56,64 }));
	m_parameters.push_back(par3);
    par3->addValueListener(this);
	parameter_ref par4(new node_parameter("Routing matrix", "", var()));
	par4->m_control_type = "routingmatrix";
	m_dyn_props.set("routingmatrixptr", (int64)&m_routing_matrix);
	m_parameters.push_back(par4);
	for (auto& e : m_parameters)
		e->m_parent_node = this;
	m_parameters.back()->addValueListener(this);

}

fileconverter_node::~fileconverter_node()
{
	if (m_thread!=nullptr && m_thread->joinable() == true)
		m_thread->join();
}

MD5 fileconverter_node::extra_state_hash()
{
	MemoryBlock block;
	for (int i = 0; i<m_routing_matrix.getMaxNumInputs(); ++i)
		for (int j = 0; j<m_routing_matrix.getMaxNumOutputs(); ++j)
		{
			bool temp = m_routing_matrix.is_connected(i, j) == connection_state::Connected;
			block.append(&temp, sizeof(bool));
		}
	MD5 result(block);
	return result;
}

void fileconverter_node::valueChanged(Value & v)
{
	if (m_parameters[2]->refersToSameSourceAs(v))
	{
		Logger::writeToLog("Converter node chan count changed to " + v.toString());
		updateRoutingMatrixFromSource();
		sendActionMessage("chancount");
	}
}

void fileconverter_node::connections_changed()
{
	updateRoutingMatrixFromSource();
	sendActionMessage("chancount");
}

void fileconverter_node::update_params()
{
}

void fileconverter_node::sourcesHaveChanged()
{
	Logger::writeToLog("fileconverter_node::sources have changed");
	updateRoutingMatrixFromSource();
	sendActionMessage("chancount");
}

void fileconverter_node::updateRoutingMatrixFromSource()
{
	if (m_sources.size() == 0)
		return;
	auto sourcefns = m_sources[0]->get_output_filenames();
	if (sourcefns.size() == 0)
		return;
	auto sndfileinfo = get_audio_source_info_cached(sourcefns[0]);
	int numouts = m_parameters[2]->getValue();
	int numins = sndfileinfo.num_channels;
	if (numins < 0 || numins>999)
		return;
	if (numouts == 1000)
		numouts = numins;
	m_routing_matrix.initDefault(numins, numouts);
	Logger::writeToLog("updated routing matrix " + String(m_routing_matrix.getMaxNumInputs()) 
		+ " " + String(m_routing_matrix.getMaxNumOutputs()));
	
}

StringArray fileconverter_node::get_output_filenames()
{
	return StringArray{ m_output_fn };
}

String fileconverter_node::get_supported_input_type()
{
	return "wav";
}

String fileconverter_node::get_output_type()
{
	return "wav";
}

void fileconverter_node::tick()
{
	tick_sources();
	if (m_pstate == Busy && m_thread_state == 1)
	{
		m_thread->join();
		m_thread.reset();
		end_benchmark();
		TempFileContainer::instance().add(m_output_fn);
		m_pstate = FinishedOK;
		setAudioFileToPlay(m_output_fn);
		ProcessFinishedFunc(this);
		return;
	}
	if (m_pstate == FinishedWithError && m_thread_state == 1)
	{
		m_thread->join();
		m_thread.reset();
		end_benchmark();
		ProcessErrorFunc(this);
		return;
	}
	auto finish_state = are_sources_finished();
	if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
	{
		m_error_text = "Error in source(s)";
		m_pstate = FinishedWithError;
		ProcessErrorFunc(this);
	}
	if (finish_state == SourcesState::FinishedOK)
	{
		if (m_pstate == Idle)
		{

			MD5 h = get_state_hash();
			//Logger::writeToLog(h.toHexString());
			String outfn(get_output_folder() + "/converter_node-" + h.toHexString() + ".wav");
			File tempfile(outfn);
			if (tempfile.exists() == true)
			{
				m_pstate = FinishedOK;
				setAudioFileToPlay(outfn);
				ProcessFinishedFunc(this);
				return;
			}
			auto taskfunc = [this, outfn]()
			{
				WavAudioFormat wavformat;
				String infn = this->getSourceFileName(0, 0);
				if (infn.isEmpty() == true)
				{
					m_error_text = "No inputs";
					m_pstate = FinishedWithError;
					m_thread_state = 1;
					return;
				}
				File infile(infn);
				auto instream = infile.createInputStream();
				if (instream != nullptr)
				{
					auto wavreader = wavformat.createReaderFor(instream, true);
					if (wavreader != nullptr)
					{
						File outfile(outfn);
						auto outstream = outfile.createOutputStream();
						if (outstream != nullptr)
						{
							double sr = m_parameters[0]->getValue();
							int bitdepth = m_parameters[1]->getValue();
							int numoutchans = m_parameters[2]->getValue();
							if (numoutchans == 1000)
								numoutchans = wavreader->numChannels;
							auto wavwriter = wavformat.createWriterFor(outstream, sr, numoutchans, bitdepth, StringPairArray(), 0);
							if (wavwriter != nullptr)
							{
								AudioFormatReaderSource readersource(wavreader, false);
								AudioTransportSource transporsource;
								int foo = std::max((int)wavreader->numChannels, numoutchans);
								transporsource.setSource(&readersource, 0, nullptr, wavreader->sampleRate, foo);
								transporsource.prepareToPlay(65536, sr);
								transporsource.start();

								double sr_ratio = sr / wavreader->sampleRate;
								AudioSampleBuffer readbuffer(wavreader->numChannels, wavreader->lengthInSamples*sr_ratio);
								AudioSampleBuffer writebuffer(numoutchans, wavreader->lengthInSamples*sr_ratio);
								AudioSourceChannelInfo asci(readbuffer);
								transporsource.getNextAudioBlock(asci);
								m_routing_matrix.process(readbuffer, writebuffer, true);
								wavwriter->writeFromAudioSampleBuffer(writebuffer, 0, wavreader->lengthInSamples*sr_ratio);
								wavwriter->flush();
								delete wavwriter;
								delete wavreader;
								m_output_fn = outfn;
								m_thread_state = 1;
								return;
							} m_error_text = "Could not create wav writer";
						} m_error_text = "Could not create output stream";
					} m_error_text = "Could not create wav reader";
				}
				else m_error_text = "Could not create input stream";

				m_pstate = FinishedWithError;
				m_thread_state = 1;


			};
			start_benchmark();
			m_pstate = Busy;
			m_thread = std::make_unique<std::thread>(taskfunc);
		}
	}

}

textfile_node::textfile_node(String procname)
{
	m_proc_name = procname;
	m_type_name = procname;
	m_text = "Foo foo text\n";
	m_buses_layout.addOutputBus("txt");
}

StringArray textfile_node::get_output_filenames()
{
	return StringArray{ m_out_fn };
}

String textfile_node::get_supported_input_type()
{
	return String();
}

String textfile_node::get_output_type()
{
	return "txt";
}

int textfile_node::getNumOutChansForNumInChans(int inchans)
{
	return 0;
}

void textfile_node::tick()
{
	if (m_pstate == Idle)
	{
		MD5 h = get_state_hash();
		String outfn(get_output_folder() + "/text_node-" + h.toHexString() + ".txt");
		File tempfile(outfn);
		if (tempfile.exists() == true)
		{
			m_pstate = FinishedOK;
			if (ProcessFinishedFunc)
				ProcessFinishedFunc(this);
			return;
		}
		FileOutputStream* ostream = tempfile.createOutputStream();
		if (ostream != nullptr)
		{
			ostream->writeText(m_text, false, false);
			ostream->flush();
			delete ostream;
			m_out_fn = outfn;
			TempFileContainer::instance().add(m_out_fn);
			m_pstate = FinishedOK;
			if (ProcessFinishedFunc)
				ProcessFinishedFunc(this);
		}
		else
		{
			m_pstate = FinishedWithError;
			m_error_text = "Could not create output stream for text";
			if (ProcessErrorFunc)
				ProcessErrorFunc(this);
		}
		
	}
}

void textfile_node::connections_changed()
{
}

MD5 textfile_node::extra_state_hash()
{
	MD5 result(m_text.toRawUTF8(),m_text.length());
	return result;
}

Component * textfile_node::getCustomEditor()
{
	TextEditor* ed = new TextEditor;
	ed->setMultiLine(true);
	ed->setReturnKeyStartsNewLine(true);
	ed->setText(m_text, dontSendNotification);
	ed->addListener(this);
	ed->getProperties().set("maxh", -1);
	return ed;
}

void textfile_node::textEditorTextChanged(TextEditor & ed)
{
	m_text = ed.getText();
}

dummy_node::dummy_node(String procname)
{
	m_proc_name = procname;
	m_type_name = procname;
	m_buses_layout.addInputBus("any");
	m_buses_layout.addOutputBus("any");
}

StringArray dummy_node::get_output_filenames()
{
	return m_out_filenames;
}

String dummy_node::get_supported_input_type()
{
	return "any";
}

String dummy_node::get_output_type()
{
	return "any";
}

void dummy_node::tick()
{
	tick_sources();
	auto finish_state = are_sources_finished();
	if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
	{
		m_error_text = "Error in source(s)";
		m_pstate = FinishedWithError;
		ProcessErrorFunc(this);
		return;
	}
	if (finish_state == SourcesState::FinishedOK)
	{
		if (m_pstate == Idle)
		{
			m_out_filenames.clear();
			for (auto& e : m_sources)
			{
				auto outnames = e->get_output_filenames();
				if (outnames.size()>0)
					m_out_filenames.add(outnames[0]);
			}
			m_pstate = FinishedOK;
			setAudioFileToPlay(m_out_filenames[0]);
			ProcessFinishedFunc(this);
		}
	}
}

void dummy_node::connections_changed()
{
}

MD5 dummy_node::extra_state_hash()
{
	return MD5();
}
