function generate_events()
	local mixnode = getNodeByName("SequencerMixer")
	if mixnode == nil then msgbox("Node not found") return end
	mixnode:setNumMixOutChans(2)
	mixnode:clearEvents()
	local numevents = 16
	local halfnumevents = numevents / 2
	for i=0,numevents do
		local gain = 1.0/halfnumevents*i
		if i>=halfnumevents then gain = 1.0 - (1.0/halfnumevents*(i-halfnumevents)) end
		local outchan = 0 -- i % 4
		mixnode:addEvent(i*2.0,"$0",2.0,0.0,outchan,gain, 0.01, 0.01, math.random())
		--mixnode:addEvent(i*0.6,"$1",1.0,0.0,outchan,gain, 0.01, 0.01, math.random())
		--mixnode:addEvent(i*0.7,"$2",1.0,0.0,outchan,gain, 0.01, 0.01, math.random())
		--mixnode:addEvent(i*0.8,"$3",1.0,0.6,outchan,gain, 0.01, 0.01, math.random())
	end
end

function generate_events2()
	local mixnode = getNodeByName("SequencerMixer")
	if mixnode == nil then msgbox("Node not found") return end
	mixnode:clearEvents()
	events = { { 0.0, "/Users/teemu/AudioProjects/sourcesamples/MUNNI_perus21.wav", 0.0,0.0,0,1.0,nil,nil,0.01},
			   { 2.0, "/Users/teemu/AudioProjects/sourcesamples/MUNNI_perus21.wav"},
			   { 2.9, "/Users/teemu/AudioProjects/sourcesamples/MUNNI_perus21.wav"},
			   { 4.5, "/Users/teemu/AudioProjects/sourcesamples/MUNNI_perus21.wav", 0.5,0.5},
			   { 6.5, "/Users/teemu/AudioProjects/sourcesamples/MUNNI_perus21.wav", 2.5,0.6}
			    }
	mixnode:addEvents(events)
end

--generate_events2()
generate_events()
--numchans, lenseconds, sr, formatname = getSoundFileInfo("C:/MusicAudio/sourcesamples/count_01.ana")
--if numchans>0 then msgbox(numchans.." "..lenseconds.." "..sr.." "..formatname.."\n") end


--local node1 = getNodeByName("είναι")
--msgbox(node1.name.." just testing")
--val = node1:getParameter(1)
--node1:setParameter(1,val-0.1)
--node1.name = node1.name.." είναι απλά"
--msgbox("είναι απλά juuh")




