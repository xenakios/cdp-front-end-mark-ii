#include "jcdp_utilities.h"
#include <future>

extern std::unique_ptr<AudioFormatManager> g_format_manager;

audio_source_info get_audio_source_info(String fn)
{
	audio_source_info result;
	File file(fn);
	auto reader = std::unique_ptr<AudioFormatReader>(g_format_manager->createReaderFor(file));
	if (reader != nullptr)
	{
		result.num_channels = reader->numChannels;
		result.m_length_frames = reader->lengthInSamples;
		result.samplerate = (int)reader->sampleRate;
		result.num_bits = reader->bitsPerSample;
		result.is_floating_point = reader->usesFloatingPointData;
	}
	return result;
}

audio_source_info get_audio_source_info_cached(String fn)
{
	return AudioFileInfoCache::get(fn);
}

File convertAudioFileToWav(const File& infile, String outfilename, int numchans, double new_sr, int bitdepth, routing_matrix rm)
{
    AudioFormatReader* reader = g_format_manager->createReaderFor(infile);
	if (reader != nullptr)
	{
		WavAudioFormat wavformat;
		File outfile(outfilename);
		FileOutputStream* fos = outfile.createOutputStream();
		if (fos != nullptr)
		{
			AudioFormatWriter* writer = wavformat.createWriterFor(fos, new_sr, numchans, bitdepth, StringPairArray(), 0);
			if (writer != nullptr)
			{
				int bufsize = 65536;
				int64_t infilecounter = 0;
				while (infilecounter < reader->lengthInSamples)
				{
					infilecounter += bufsize;
				}
			}
			
		}
		
	}
    return File();
}

bool convertAudioFileToWavAsync(const File& infile, String outfilename, int numchans, double new_sr, int bitdepth, routing_matrix rm,
                                std::function<void(File)> completion_callback)
{
    if (infile.existsAsFile()==false)
        return false;
    std::thread th([infile,outfilename, numchans,new_sr,bitdepth,rm,completion_callback]()
    {
        File outfile = convertAudioFileToWav(infile, outfilename,  numchans, new_sr, bitdepth, rm);
        MessageManager::callAsync([completion_callback,outfile](){ completion_callback(outfile); });
    });
    th.detach();
    return true;
}



AudioFileInfoCache* g_audiofileinfocache = nullptr;

AudioFileInfoCache * AudioFileInfoCache::instance()
{
	if (g_audiofileinfocache == nullptr)
		g_audiofileinfocache = new AudioFileInfoCache;
	return g_audiofileinfocache;
}

audio_source_info AudioFileInfoCache::getInfo(String fn, bool bypasscache)
{
	if (bypasscache == false)
	{
		auto it = m_info_cache.find(fn);
		if (it != m_info_cache.end())
			return it->second;
	}
	audio_source_info result;
	File file(fn);
	auto reader = std::unique_ptr<AudioFormatReader>(g_format_manager->createReaderFor(file));
	if (reader != nullptr)
	{
		result.m_format_name = reader->getFormatName();
		result.num_channels = reader->numChannels;
		result.m_length_frames = reader->lengthInSamples;
		result.samplerate = (int)reader->sampleRate;
		result.num_bits = reader->bitsPerSample;
		result.is_floating_point = reader->usesFloatingPointData;
		m_info_cache[fn] = result;
		//Logger::writeToLog("Sound file info cache has " + String(m_info_cache.size()) + " entries");
	}
	return result;
}

audio_source_info AudioFileInfoCache::get(String fn)
{
	return AudioFileInfoCache::instance()->getInfo(fn);
}

void AudioFileInfoCache::refreshAllInfos()
{
	for (auto& e : m_info_cache)
	{
		auto temp = getInfo(e.first, true);
	}
}

void AudioFileInfoCache::refreshInfoFor(String fn)
{
	auto temp = getInfo(fn, true);
}

void AudioFileInfoCache::clearAllInfos()
{
	m_info_cache.clear();
}
