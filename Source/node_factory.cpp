#include "node_factory.h"
#include "lua_helpers.h"

#include "lua_process_node.h"
#include "plugin_process_node.h"
#include "chain_node.h"
#include "misc_nodes.h"
#include "mixersequencer_node.h"
#include "minihourglass_node.h"
#include "lua_dsp_node.h"
#include "sol.hpp"

extern std::unique_ptr<PropertiesFile> g_propsfile;
extern String g_cdp_render_location;

node_factory::node_factory()
{
	auto sndlenchangedfunc = [](node_parameter& par, double len)
	{
		par.m_max_value = len;
	};
	entry_t entry;

	entry.m_name = "Input Sound File";
    entry.m_tiptext = "Starting point for node graphs.";
	entry.m_exe = "Internal";
    entry.CreateFunc = [entry]()
	{
		auto node = std::make_shared<inputfile_node>(entry.m_name, String(), -1);
		return node;
	};
	m_entries.push_back(entry);

	entry.m_name = "Text file";
	entry.m_tiptext = "Can be used as source node for nodes that require text file (.txt) inputs";
	entry.m_exe = "Internal";
	entry.CreateFunc = [entry]()
	{
		auto node = std::make_shared<textfile_node>(entry.m_name);
		return node;
	};
	m_entries.push_back(entry);

	entry.m_name = "Montage";
    entry.m_tiptext = "Time sequence and mix sounds";
	entry.m_exe = "Internal";
    entry.CreateFunc = [entry]()
	{
		auto node = std::make_shared<filemixer_node>(entry.m_name);
		return node;
	};
	m_entries.push_back(entry);

	entry.m_name = "Convert file";
    entry.m_tiptext = "Changes sound file bit depths, sample rates and channel counts";
    entry.m_exe = "Internal";
    entry.CreateFunc = [entry]()
    {
        return std::make_shared<fileconverter_node>(entry.m_name);
    };
    m_entries.push_back(entry);

	
    entry.m_name = "Processing chain node";
    entry.m_tiptext="Contains other nodes in a chain\nUseful when quick reordering of the processing steps is desired.";
    entry.m_exe = "Internal";
    entry.CreateFunc = [entry]()
    {
        return std::make_shared<chain_process_node>(entry.m_name);
    };
    m_entries.push_back(entry);
	
    entry.m_name = "Mini HourGlass";
    entry.m_tiptext="Sound fragmenter/granulator with sound processing taken from the HourGlass application.";
    entry.m_exe = "Internal";
    entry.CreateFunc = [entry]()
    {
        return std::make_shared<minihourglass_node>(entry.m_name);
    };
    m_entries.push_back(entry);
    
	entry.m_name = "Lua audio DSP";
	entry.m_tiptext = "Lua script audio processing node";
	entry.m_exe = "Internal";
	entry.CreateFunc = [entry]()
	{
		return std::make_shared<LuaDSPNode>(entry.m_name);
	};
	m_entries.push_back(entry);

	entry.m_name = "Dummy node";
	entry.m_tiptext = "Node that passes its inputs to its output";
	entry.m_exe = "Internal";
	entry.CreateFunc = [entry]()
	{
		return std::make_shared<dummy_node>(entry.m_name);
	};
	m_entries.push_back(entry);

    init_lua_defined_programs();
	init_plugins(false);
}

AudioPluginFormatManager* g_audio_plugin_manager = nullptr;

void node_factory::init_plugins(bool forgetpreviousresults)
{
	if (forgetpreviousresults == true)
	{
		remove_from_container(m_entries, [](const entry_t& e) 
		{ 
			return e.m_is_plugin == true;
		});
	}
	else
	{
		XmlElement* elem = g_propsfile->getXmlValue("general/plugin_scan_results");
		if (elem != nullptr)
		{
			m_pluginlist.recreateFromXml(*elem);
			delete elem;
		}
	}
	if (m_pluginlist.getNumTypes() > 0)
	{
		//Logger::writeToLog("previous plugin scan results loaded");
	}
	int count = 0;
	for (auto& plug : m_pluginlist)
	{
		if (plug->isInstrument == true)
		{
			++count;
			continue;
		}
		entry_t entry;
		String prefix = plug->pluginFormatName+" : ";
		entry.m_name = prefix+plug->name;
		entry.m_is_plugin = true;
		entry.m_plugin_id = count;
        entry.m_exe = "Plugin";
		entry.m_tiptext = "VST2/VST3/AU plugins do not have info texts :(";
		String foo = entry.m_name;
		entry.CreateFunc = [this,foo, count]()->node_ref
		{
            if (g_audio_plugin_manager==nullptr)
            {
                g_audio_plugin_manager = new AudioPluginFormatManager;
                g_audio_plugin_manager->addDefaultFormats();
            }
            PluginDescription* desc = m_pluginlist.getType(count);
			String errmsg;
            AudioPluginInstance* pluginst = g_audio_plugin_manager->createPluginInstance(*desc, 44100.0, 512, errmsg);
			if (pluginst != nullptr)
			{
				//Logger::writeToLog("Managed to create plugin " + pluginst->getName());
				auto node = std::make_shared<plugin_process_node>(foo, pluginst);
				return node;
            } else Logger::writeToLog(errmsg);
			return nullptr;
		};
		++count;
		m_entries.push_back(entry);
	}
	
	XmlElement* xml = m_pluginlist.createXml();
	g_propsfile->setValue("general/plugin_scan_results", xml);
	delete xml;
	
}

node_factory::~node_factory()
{
    delete g_audio_plugin_manager;
    g_audio_plugin_manager = nullptr;
}

node_factory* g_node_factory_instance = nullptr;

node_factory* node_factory::instance(bool destroy)
{
	if (destroy == true)
	{
		delete g_node_factory_instance;
		g_node_factory_instance = nullptr;
		return nullptr;
	}
	if (g_node_factory_instance == nullptr)
		g_node_factory_instance = new node_factory;
	return g_node_factory_instance;
}

void node_factory::init_lua_defined_programs()
{
	try
	{
		String luadefs_fn = get_cdp_lua_defs_file();
		std::vector<entry_t> entries;
		entries.reserve(850);
		sol::state lua;
		lua.set_function("get_srate", []() { return 44100.0; });
		lua.set_function("get_cycles", []() { return 1000.0; });
		lua.set_function("get_length", []() { return 5000.0; });
		std::string def_fn = luadefs_fn.toStdString();
		double t0 = Time::getMillisecondCounterHiRes();
		lua.script_file(def_fn);
		sol::table dsp_entries = lua["dsp"];
		dsp_entries.for_each([&entries](sol::object& key, sol::object& val)
		{
			entry_t entry;
			entry.m_lua_defined = true;
			entry.m_name = CharPointer_UTF8(key.as<const char*>());
			auto args = val.as<const sol::table&>();
			args.for_each([&entry](sol::object& key, sol::object& val)
			{
				if (key.as<const std::string&>() == "cmds")
				{
					auto cmd_fields = val.as<const sol::table&>();
                    sol::optional<const char*> temp_tip = cmd_fields["tip"];
                    if (temp_tip)
                        entry.m_tiptext = CharPointer_UTF8(*temp_tip);
					sol::optional<const char*> temp_exename = cmd_fields["exe"];
                    if (temp_exename)
                        entry.m_exe = CharPointer_UTF8(*temp_exename);
                    entry.m_supported = cmd_fields.get_or("jcdp", false);
				}
			});
			entries.emplace_back(entry);
		});
		double t1 = Time::getMillisecondCounterHiRes();
		Logger::writeToLog("Initing Lua entries with sol2 took " + String(t1 - t0) + " ms");
		std::sort(entries.begin(), entries.end(), [](entry_t& a, entry_t&b) { return a.m_name < b.m_name; });
		remove_from_container(m_entries, [](const entry_t& e)
		{
			return e.m_lua_defined == true;
		});
		m_entries.insert(m_entries.end(), entries.begin(), entries.end());

	}
	catch (std::exception& excep)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"CDP Frontend Lua error",
			excep.what(), "OK",
			nullptr);
	}

}

node_ref node_factory::create_lua_processor_from_name(String name)
{
	return std::make_shared<luacdpnode>(name);
}

node_ref node_factory::duplicate_node(node_ref src, bool duplicate_connections)
{
	auto node = create_from_name(src->get_type_name());
	if (node == nullptr)
		return nullptr;
	ValueTree state_tree = src->getStateValueTree();
	node->restoreStateFromValueTree(state_tree);
	if (duplicate_connections == true)
	{
		node->m_sources = src->m_sources;
		node->m_destinations = src->m_destinations;
		for (auto& e : node->m_destinations)
		{
			e->m_sources.push_back(node);
		}
		for (auto& e : node->m_sources)
		{
			e->m_destinations.push_back(node.get());
		}
	}
	return node;
}

node_ref node_factory::create_from_name(String name)
{
    ++m_instance_counter;
	for (auto& e : m_entries)
	{
		if (e.m_lua_defined == true && e.m_name.containsIgnoreCase(name))
		{
			try
			{
				auto node = create_lua_processor_from_name(e.m_name);
				++e.m_instance_count;
				String orig_name = node->get_name();
				node->set_name(orig_name + "_" + String(e.m_instance_count));
                node->m_unique_id = m_instance_counter;
                return node;
			}
			catch (std::exception& ex)
			{
				Logger::writeToLog(String(ex.what()));
				return nullptr;
			}
		}
		if (e.m_name.containsIgnoreCase(name))
		{
			++e.m_instance_count;
			auto node = e.CreateFunc();
			if (node!=nullptr)
            {
                String orig_name = node->get_name();
                String temp = orig_name.replaceCharacter('/', '_');
                node->set_name(temp + "_" + String(e.m_instance_count));
            }
			node->m_unique_id = m_instance_counter;
            return node;
		}
	}
	
	return nullptr;
}
