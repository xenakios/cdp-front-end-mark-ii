#include "envelope_component.h"
#include "cdp_graph.h"

extern std::unique_ptr<AudioThumbnailCache> g_thumb_cache;
extern std::unique_ptr<AudioFormatManager> g_format_manager;
extern std::unique_ptr<TooltipWindow> g_tooltipwindow;
extern std::unique_ptr<PropertiesFile> g_propsfile;

EnvelopeComponent::EnvelopeComponent()
{
	OnEnvelopeEdited = [](breakpoint_envelope*) {};
	setWantsKeyboardFocus(true);
	ValueFromNormalized = [](double x) { return x; };
	TimeFromNormalized = [](double x) { return x; };
	addChildComponent(&m_bubble);
    setOpaque(true);
}

EnvelopeComponent::~EnvelopeComponent()
{
	delete m_thumb;
}

void EnvelopeComponent::show_bubble(int x, int y, const envelope_node& node)
{
	double scaledtime = TimeFromNormalized(node.Time);
	double scaledvalue = ValueFromNormalized(node.Value);
	x -= 50;
	if (x < 0)
		x = 0;
	if (x + 100 > getWidth())
		x = getWidth() - 100;
	if (y < 0)
		y = 0;
	if (y + 20 > getHeight())
		y = getHeight() - 20;
	m_bubble.showAt({ x,y,100,20 }, AttributedString(String::formatted("%.2f %.2f", scaledtime, scaledvalue)), 5000);
}


void EnvelopeComponent::paint(Graphics& g)
{
	if (!EnvelopeUnderlayDraw)
	{
		g.fillAll(Colours::black);
		g.setColour(Colours::white.darker());
		juce::Rectangle<int> rect(0, 0, getWidth(), getHeight());
        if (m_thumb!=nullptr)
			m_thumb->drawChannels(g, rect, 0.0, m_thumb->getTotalLength(), 1.0f);
		g.setFont(15.0);
		
	}
	else
	{
		g.saveState();
		EnvelopeUnderlayDraw(this, g);
		g.restoreState();
	}
	
	if (m_envelope == nullptr)
	{
		g.drawText("No envelope set", 10, 10, getWidth(), getHeight(), Justification::centred);
		return;
	}
	if (m_envelope.unique() == true)
	{
		g.drawText("Envelope is orphaned (may be a bug)", 10, 10, getWidth(), getHeight(), Justification::centred);
		return;
	}
	String name = m_name;
	if (name.isEmpty() == true)
		name = "Untitled envelope";
	g.drawText(name, 10, 10, getWidth(), getHeight(), Justification::topLeft);
	const float linethickness = 1.0f;
	for (int i = 0; i < m_envelope->GetNumNodes(); ++i)
	{
		const envelope_node& pt = m_envelope->GetNodeAtIndex(i);
		double xcor = jmap(pt.Time, m_view_start_time, m_view_end_time, 0.0, (double)getWidth());
		double ycor = (double)getHeight()-jmap(pt.Value, m_view_start_value, m_view_end_value, 0.0,(double)getHeight());
		g.setColour(Colours::white);
		if (pt.Status==0)
			g.drawRect((float)xcor - 4.0f, (float)ycor - 4.0f, 8.0f, 8.0f, 1.0f);
		else g.fillRect((float)xcor - 4.0f, (float)ycor - 4.0f, 8.0f, 8.0f);
		m_envelope->resamplePointToLinearSegments(i,0.0, 1.0, 0.0, 1.0, [&g,linethickness,this](double pt_x0, double pt_y0, double pt_x1, double pt_y1)
		{
			double foo_x0 = jmap<double>(pt_x0, m_view_start_time, m_view_end_time, 0.0, getWidth());
			double foo_x1 = jmap<double>(pt_x1, m_view_start_time, m_view_end_time, 0.0, getWidth());
			double foo_y0 = (double)getHeight() - jmap<double>(pt_y0, m_view_start_value, m_view_end_value, 0.0, getHeight());
			double foo_y1 = (double)getHeight() - jmap<double>(pt_y1, m_view_start_value, m_view_end_value, 0.0, getHeight());
			g.setColour(m_env_color);
			g.drawLine(foo_x0, foo_y0, foo_x1, foo_y1, linethickness);
			g.setColour(Colours::white);
			g.drawLine(foo_x0, foo_y0 - 8.0, foo_x0, foo_y0 + 8.0, 1.0);
		}, [this](double xdiff) 
		{ 
			return std::max((int)(xdiff*getWidth() / 16), 8);
		});
		
		/*
		envelope_node pt1;
		if (i + 1 < m_envelope->GetNumNodes())
		{
			g.setColour(m_env_color);
			pt1 = m_envelope->GetNodeAtIndex(i + 1);
			double xcor1 = jmap(pt1.Time, m_view_start_time, m_view_end_time, 0.0, (double)getWidth());
			double ycor1 = (double)getHeight() - jmap(pt1.Value, m_view_start_value, m_view_end_value, 0.0, (double)getHeight());
			g.drawLine((float)xcor, (float)ycor, (float)xcor1, (float)ycor1, linethickness);
		}
		if (i == 0 && pt.Time >= 0.0)
		{
			g.setColour(m_env_color);
            g.drawLine(0.0f, (float)ycor, (float)xcor, float(ycor), linethickness);
		}
		if (i == m_envelope->GetNumNodes()-1 && pt.Time < 1.0)
		{
			g.setColour(m_env_color);
            g.drawLine((float)xcor, (float)ycor, (float)getWidth(), float(ycor), linethickness);
		}
		*/
	}
	g.setColour(Colours::green);
	double prevderiv = derivative([this](double xx) { return m_envelope->GetInterpolatedNodeValue(xx); }, 0.0);
	for (int i = 0; i < getWidth()/8; ++i)
	{
		double x = 1.0 / getWidth()*(i*8);
		double derv = derivative([this](double xx) { return m_envelope->GetInterpolatedNodeValue(xx); }, x);
		double y = getHeight()-jmap<double>(derv, -10.0, 10.0, 0.0, getHeight());
		g.fillEllipse(i*8, y, 5.0f, 5.0f);
	}
}

void EnvelopeComponent::changeListenerCallback(ChangeBroadcaster*)
{
	repaint();
}

void EnvelopeComponent::timerCallback(int)
{
	
}

void EnvelopeComponent::initThumbNail(String fn)
{
	File thumbfile(fn);
	m_thumb_source = new FileInputSource(thumbfile);
	delete m_thumb;
	m_thumb = new AudioThumbnail(64, *g_format_manager, *g_thumb_cache);
	m_thumb->setSource(m_thumb_source);
	m_thumb->addChangeListener(this);
}

/*
OK, so this is a horrible hack to show a waveform guide for nodes that output .ana files. The .ana file
is resynthesized to .wav using pvoc.exe and that file is then used to build the waveform thumb.
Maybe it wouldn't be so awful if the EnvelopeComponent didn't have to do this...
Should probably implement the waveform drawing in a separate class that could deal with the .ana files too.
Another possibility would be to attempt to draw the .ana files directly.
*/

void EnvelopeComponent::initResynth(String fn, MD5 h)
{
	if (m_proc_node != nullptr)
	{
		String outfn = get_output_folder() + "/" + h.toHexString() + ".wav";
		File temp(outfn);
		if (temp.exists() == true)
		{
			initThumbNail(outfn);
			return;
		}
		StringArray args;
		args.add(get_cdp_bin_location() + "/pvoc");
		args.add("synth");
		args.add(fn);
		args.add("-f" + outfn);
		m_resynth_fut = runChildProcessAsync(args, 15000, [this,outfn](String msg) 
		{
			if (msg.isEmpty() == true)
			{
				initThumbNail(outfn); 
				TempFileContainer::instance().add(outfn);
			}
			else Logger::writeToLog(msg);
		});
	}
}

void EnvelopeComponent::set_envelope(std::shared_ptr<breakpoint_envelope> env, process_node* node, String name)
{
	m_envelope = env;
	m_name = name;
	m_proc_node = node;
	if (m_proc_node != nullptr)
	{
		if (m_proc_node->m_sources.size() > 0 && m_proc_node->m_sources[0]->get_output_filenames().size() > 0)
		{
			String fn = m_proc_node->m_sources[0]->get_output_filenames()[0];
			if (fn.containsIgnoreCase(".wav") == false)
			{
				if (fn.containsIgnoreCase(".ana") == true)
				{
					initResynth(fn, m_proc_node->m_sources[0]->get_state_hash());
				}
				return;
			}
			initThumbNail(fn);
		}
	}
	repaint();
}

void EnvelopeComponent::mouseDrag(const MouseEvent& ev)
{
	if (m_envelope == nullptr)
		return;
	if (m_segment_drag_info.first >= 0 && ev.mods.isAltDown())
	{
		double dist = jmap<double>(ev.getDistanceFromDragStartX(), -300.0, 300.0, -1.0, 1.0);
		m_envelope->performRelativeTransformation([dist, this](int index, envelope_node& point) 
		{ 
			if (index == m_segment_drag_info.first)
			{
				point.ShapeParam1 += dist;
				m_segment_drag_info.second = true;
			}
		});
		repaint();
		return;
	}
	if (m_segment_drag_info.first >= 0)
	{
		double dist = jmap<double>(ev.getDistanceFromDragStartY(), -getHeight(), getHeight(), -1.0, 1.0);
		m_envelope->adjustEnvelopeSegmentValues(m_segment_drag_info.first, -dist);
		repaint();
		return;
	}
	if (m_node_to_drag >= 0)
	{
		//Logger::writeToLog("trying to move pt " + String(m_node_to_drag));
		envelope_node& pt = m_envelope->GetNodeAtIndex(m_node_to_drag);
		double left_bound = m_view_start_time;
		double right_bound = m_view_end_time;
		if (m_node_to_drag > 0 )
		{
			left_bound = m_envelope->GetNodeAtIndex(m_node_to_drag - 1).Time;
		}
		if (m_node_to_drag < m_envelope->GetNumNodes() - 1)
		{
			right_bound = m_envelope->GetNodeAtIndex(m_node_to_drag + 1).Time;
		}
		double normx = jmap((double)ev.x, 0.0, (double)getWidth(), m_view_start_time, m_view_end_time);
		double normy = jmap((double)getHeight() - ev.y, 0.0, (double)getHeight(), m_view_start_value, m_view_end_value);
		pt.Time=jlimit(left_bound+0.001, right_bound - 0.001, normx);
		pt.Value=jlimit(0.0,1.0,normy);
		m_last_tip = String(pt.Time, 2) + " " + String(pt.Value, 2);
		show_bubble(ev.x, ev.y, pt);
		m_node_that_was_dragged = m_node_to_drag;
		repaint();
		return;
	}
}

void EnvelopeComponent::mouseMove(const MouseEvent & ev)
{
	if (m_envelope == nullptr)
		return;
	m_node_to_drag = find_hot_envelope_point(ev.x, ev.y);
	if (m_node_to_drag >= 0)
	{
		if (m_mouse_down == false)
		{
			show_bubble(ev.x, ev.y, m_envelope->GetNodeAtIndex(m_node_to_drag));
			setMouseCursor(MouseCursor::PointingHandCursor);
		}
	}
	else
	{
		setMouseCursor(MouseCursor::NormalCursor);
		m_bubble.setVisible(false);
	}
}

void EnvelopeComponent::mouseDown(const MouseEvent & ev)
{
	if (m_envelope == nullptr)
		return;
	m_node_to_drag = find_hot_envelope_point(ev.x, ev.y);
	m_mouse_down = true;
	m_segment_drag_info = { findHotEnvelopeSegment(ev.x, ev.y, true),false };
	if (m_segment_drag_info.first >= 0)
	{
		m_envelope->beginRelativeTransformation();
		return;
	}
	if (m_node_to_drag >= 0 && ev.mods.isAltDown() == true)
	{
		if (m_envelope->GetNumNodes() < 2)
		{
			m_bubble.showAt({ ev.x,ev.y, 0,0 }, AttributedString("Can't remove last node"), 3000, false, false);
			return;
		}
		m_envelope->DeleteNode(m_node_to_drag);
		m_node_to_drag = -1;
		OnEnvelopeEdited(m_envelope.get());
		repaint();
		return;
	}
	if (m_node_to_drag >= 0 && ev.mods.isShiftDown()==true)
	{
		int oldstatus = m_envelope->GetNodeAtIndex(m_node_to_drag).Status;
		if (oldstatus==0)
			m_envelope->GetNodeAtIndex(m_node_to_drag).Status=1;
		else m_envelope->GetNodeAtIndex(m_node_to_drag).Status=0;
		repaint();
		return;
	}
	if (m_node_to_drag == -1)
	{
		double normx = jmap((double)ev.x, 0.0, (double)getWidth(), m_view_start_time, m_view_end_time);
		double normy = jmap((double)getHeight() - ev.y, 0.0, (double)getHeight(), m_view_start_value, m_view_end_value);
		m_envelope->AddNode ({ normx,normy, 0.5});
		m_envelope->SortNodes();
		m_mouse_down = false;
		OnEnvelopeEdited(m_envelope.get());
		repaint();
	}
}

void EnvelopeComponent::mouseUp(const MouseEvent &ev)
{
	if (ev.mods == ModifierKeys::noModifiers)
		m_bubble.setVisible(false);
	if (m_node_that_was_dragged >= 0 || m_segment_drag_info.second==true)
	{
		OnEnvelopeEdited(m_envelope.get());
	}
	m_mouse_down = false;
	m_node_that_was_dragged = -1;
	m_node_to_drag = -1;
	if (m_segment_drag_info.second == true)
	{
		m_segment_drag_info = { -1,false };
		m_envelope->endRelativeTransformation();
	}
}

bool EnvelopeComponent::keyPressed(const KeyPress & ev)
{
	if (ev == KeyPress::deleteKey && m_envelope!=nullptr)
	{
		m_node_to_drag = -1;
		//m_envelope->ClearAllNodes();
		m_envelope->removePointsConditionally([](const envelope_node& pt) { return pt.Status == 1; });
		if (m_envelope->GetNumNodes()==0)
			m_envelope->AddNode({ 0.0,0.5 });
		repaint();
		OnEnvelopeEdited(m_envelope.get());
		
		return true;
	}
	return false;
}

int EnvelopeComponent::find_hot_envelope_point(double xcor, double ycor)
{
	if (m_envelope == nullptr)
		return -1;
	for (int i = 0; i < m_envelope->GetNumNodes(); ++i)
	{
		const envelope_node& pt = m_envelope->GetNodeAtIndex(i);
		double ptxcor = jmap(pt.Time, m_view_start_time, m_view_end_time, 0.0, (double)getWidth());
		double ptycor = (double)getHeight() - jmap(pt.Value, m_view_start_value, m_view_end_value, 0.0, (double)getHeight());
		juce::Rectangle<double> target(ptxcor - 4.0, ptycor - 4.0, 8.0, 8.0);
		if (target.contains(xcor, ycor) == true)
		{
			return i;
		}
	}
	return -1;
}

int EnvelopeComponent::findHotEnvelopeSegment(double xcor, double ycor, bool detectsegment)
{
	if (m_envelope == nullptr)
		return -1;
	for (int i = 0; i < m_envelope->GetNumNodes()-1; ++i)
	{
		const envelope_node& pt0 = m_envelope->GetNodeAtIndex(i);
		const envelope_node& pt1 = m_envelope->GetNodeAtIndex(i+1);
		float xcor0 = jmap<double>(pt0.Time, m_view_start_time, m_view_end_time, 0.0, getWidth());
		float xcor1 = jmap<double>(pt1.Time, m_view_start_time, m_view_end_time, 0.0, getWidth());
		float segwidth = xcor1 - xcor0;
		juce::Rectangle<float> segrect(xcor0+8.0, 0.0f, segwidth-16.0, getHeight());
		if (segrect.contains(xcor, ycor))
		{
			if (detectsegment == false)
				return i;
			else
			{
				double normx = jmap<double>(xcor, 0.0, getWidth(), m_view_start_time, m_view_end_time);
				double yval = m_envelope->GetInterpolatedNodeValue(normx);
				float ycor0 = getHeight()-jmap<double>(yval, 0.0, 1.0, 0.0, getHeight());
				juce::Rectangle<float> segrect2(xcor - 5, ycor - 10, 10, 20);
				if (segrect2.contains(xcor, ycor0))
					return i;
			}
		}
		
		
	}
	return -1;
}

void zoom_scrollbar::mouseDown(const MouseEvent &e)
{
	m_drag_start_x = e.x;
}

void zoom_scrollbar::mouseMove(const MouseEvent &e)
{
	auto ha = get_hot_area(e.x, e.y);
	if (ha == ha_left_edge || m_hot_area == ha_right_edge)
		setMouseCursor(MouseCursor::LeftRightResizeCursor);
	else
		setMouseCursor(MouseCursor::NormalCursor);
	if (ha != m_hot_area)
	{
		m_hot_area = ha;
		repaint();
	}
}

void zoom_scrollbar::mouseDrag(const MouseEvent &e)
{
	if (m_hot_area == ha_none)
		return;
	if (m_hot_area == ha_left_edge)
	{
		double new_left_edge = 1.0 / getWidth()*e.x;
		m_start = jlimit(0.0, m_end - 0.01,new_left_edge);
		repaint();
	}
	if (m_hot_area == ha_right_edge)
	{
		double new_right_edge = 1.0 / getWidth()*e.x;
		m_end = jlimit(m_start + 0.01, 1.0, new_right_edge);
		repaint();
	}
	if (m_hot_area == ha_handle)
	{
		double delta = 1.0 / getWidth()*(e.x - m_drag_start_x);
		//double old_start = m_start;
		//double old_end = m_end;
		double old_len = m_end - m_start;
		m_start = jlimit(0.0, 1.0 - old_len, m_start + delta);
		m_end = jlimit(old_len, m_start + old_len, m_end + delta);
		m_drag_start_x = e.x;
		repaint();
	}
	if (RangeChanged)
		RangeChanged(m_start, m_end);
}

void zoom_scrollbar::mouseEnter(const MouseEvent & event)
{
	m_hot_area = get_hot_area(event.x, event.y);
	repaint();
}

void zoom_scrollbar::mouseExit(const MouseEvent &)
{
	m_hot_area = ha_none;
	repaint();
}

void zoom_scrollbar::paint(Graphics &g)
{
	g.setColour(Colours::darkgrey);
	g.fillRect(0, 0, getWidth(), getHeight());
	int x0 = (int)(getWidth()*m_start);
	int x1 = (int)(getWidth()*m_end);
	if (m_hot_area != ha_none)
		g.setColour(Colours::white);
	else g.setColour(Colours::lightgrey);
	g.fillRect(x0, 0, x1 - x0, getHeight());
}

void zoom_scrollbar::setRange(double t0, double t1)
{
	m_start = jlimit(0.0,1.0, t0);
	m_end = jlimit(0.0, 1.0, t1);
	if (m_start > m_end)
		std::swap(m_start, m_end);
	repaint();
}

zoom_scrollbar::hot_area zoom_scrollbar::get_hot_area(int x, int)
{
	int x0 = (int)(getWidth()*m_start);
	int x1 = (int)(getWidth()*m_end);
	if (is_in_range(x, x0 - 5, x0 + 5))
		return ha_left_edge;
	if (is_in_range(x, x1 - 5, x1 + 5))
		return ha_right_edge;
	if (is_in_range(x, x0 + 5, x1 - 5))
		return ha_handle;
	return ha_none;
}

EnvelopeContainerComponent::EnvelopeContainerComponent() : 
	m_resizer(this,nullptr), m_script_editor(ValueTree("envelopescriptstate"))
	
{
	m_lua.open_libraries(sol::lib::base, sol::lib::math);
	m_lua.set_function("get_slider_value", [this](int index) 
	{ 
		if (index>=0 && index<m_sliders.size())
			return m_sliders[index]->m_slider.getValue(); 
		return 0.0;
	});
	m_lua.set_function("add_slider", [this](const char* name, double minval, double maxval, double val)
	{
		EnvScriptSlider* slid = new EnvScriptSlider(this, name, minval, maxval, val);
		slid->m_slider.addListener(this);
		m_sliders.push_back(std::unique_ptr<EnvScriptSlider>(slid));
	});
	m_env_comp = std::make_unique<EnvelopeComponent>();
    addAndMakeVisible(&m_resizer);
    addAndMakeVisible(m_env_comp.get());
	addAndMakeVisible(&m_script_editor);
	
	m_script_editor.OnExecuteScript = [this](String txt) { runScript(txt); };
	int w = g_propsfile->getIntValue("general/lastenvw",800);
    int h = g_propsfile->getIntValue("general/lastenvh",450);
    setSize(w,h);
	setWantsKeyboardFocus(true);
	
}

void EnvelopeContainerComponent::resized()
{
	int ed_h = 200;
	m_env_comp->setBounds(0,0,getWidth(),getHeight()-ed_h-2);
	m_script_editor.setBounds(0, getHeight() - ed_h+2, getWidth() - 500, ed_h);
	int edright = m_script_editor.getRight();
	int edbot = m_env_comp->getBottom();
	int sliders_w = getWidth() - m_script_editor.getWidth() - 10;
	int sliders_h = 115;
	int num_cols = 2;
	int num_rows = 4;
	for (int i = 0; i < m_sliders.size(); ++i)
	{
		int col = i % num_cols;
		int row = i / num_cols;
		int slid_x = edright + 5 + col*(sliders_w / num_cols);
		int slid_y = edbot + row * 30;
		m_sliders[i]->m_label.setBounds(slid_x, slid_y, 80, 25);
		m_sliders[i]->m_slider.setBounds(slid_x + 90, slid_y, 135, 25);
	}
	m_resizer.setBounds(getWidth()-20,getHeight()-20,20,20);
    g_propsfile->setValue("general/lastenvw", getWidth());
    g_propsfile->setValue("general/lastenvh", getHeight());
}

bool EnvelopeContainerComponent::keyPressed(const KeyPress & ev)
{
	return false;
}

void EnvelopeContainerComponent::runScript(String txt)
{
	try
	{
		m_lua.script(txt.toStdString());
		int numnodes = jlimit(1,1000, (int)m_lua.get_or("num_points", 64));
		sol::function genfun = m_lua["generate_value"];
		sol::function genenvfun = m_lua["generate_envelope"];
		sol::function transformfun = m_lua["transform_value"];
       
        auto env = m_env_comp->get_envelope();
        auto backup = env->get_all_nodes();
        if (genfun.valid() && transformfun.valid())
		{
			Logger::writeToLog("Can not run both transform and generate function");
			return;
		}
		
		if (genenvfun.valid())
		{
			sol::table envtable = genenvfun();
			env->ClearAllNodes();
			envtable.for_each([&env](sol::object& key, sol::object& val) 
			{
				sol::table pt_table = val;
				double pt_x = pt_table[1];
				double pt_y = pt_table[2];
				pt_y = jlimit(0.0, 1.0, pt_y);
				if (pt_x>=0.0 && pt_x<=1.0)
				{
					env->AddNode({ pt_x,pt_y });
				}
			});
			if (env->GetNumNodes() > 0)
			{
				env->SortNodes();
				m_env_comp->repaint();
				m_env_comp->OnEnvelopeEdited(env.get());
			}

		}
		if (genfun.valid())
		{
			
			env->ClearAllNodes();
			
			for (int i = 0; i < numnodes; ++i)
			{
				double t = 1.0 / (numnodes - 1) * i;
				sol::optional<double> r = genfun(t);
				if (r)
				{
					*r = jlimit(0.0, 1.0, *r);
					env->AddNode({ t,*r });
				}
			}
			m_env_comp->repaint();
			//m_env_comp->OnEnvelopeEdited(env.get());
			env->m_script = txt;
			//Logger::writeToLog(String(r.first) + " : " + String(r.second));
		}
		if (transformfun.valid() && m_stored_env.size()>0)
		{
			for (int i = 0; i < m_stored_env.size(); ++i)
			{
				envelope_node& node = m_stored_env[i];
				sol::optional<double> r = transformfun(node.Time, node.Value);
				if (r)
				{
					node.Value = jlimit(0.0, 1.0, *r);
				}
			}
			m_env_comp->repaint();
			//m_env_comp->OnEnvelopeEdited(env.get());
			env->m_script = txt;
		}
	}
	catch (std::exception& excep)
	{
		Logger::writeToLog(excep.what());
	}
}

void EnvelopeContainerComponent::sliderDragStarted(Slider *slid)
{
	auto env = m_env_comp->get_envelope();
	if (env)
		m_stored_env = env->get_all_nodes();
}

void EnvelopeContainerComponent::sliderValueChanged(Slider *s)
{
	for (auto& e : m_sliders)
	{
		if (&e->m_slider == s)
		{
			runScript(m_script_editor.getText());
			return;
		}
	}
}

void EnvelopeContainerComponent::sliderDragEnded(Slider *slid)
{
	
	return;
	if (m_stored_env.size() > 0)
	{
		m_env_comp->get_envelope()->set_all_nodes(m_stored_env);
		repaint();
		m_env_comp->OnEnvelopeEdited(m_env_comp->get_envelope().get());
		m_stored_env.clear();
	}
}

void EnvelopeContainerComponent::updateScriptText()
{
	auto env = m_env_comp->get_envelope();
	if (env!=nullptr)
	{
		String txt = env->m_script;
		if (txt.isEmpty() == true)
			txt = R"(
num_points = 64
function on_init()
	add_slider("rate a",0.5,16.0,2.0)
	add_slider("amp a",0.0,1.0,0.45)
	add_slider("center a",0.0,1.0,0.5)
	add_slider("phase a",0.0,6.2831853071795865,0.0)
	add_slider("rate b",0.5,16.0,2.0)
	add_slider("amp b",0.0,1.0,0.45)
	add_slider("center b",0.0,1.0,0.5)
	add_slider("phase b",0.0,6.2831853071795865,0.0)
end

function transform_valuex(t,v)
	return v+get_slider_value(2)
end
function fun_a(t)
  local rate, amp, cent, phase = get_slider_value(0),get_slider_value(1),get_slider_value(2),get_slider_value(3)
  return cent+amp*math.sin(2*3.141593*t*rate+phase)
end
function fun_b(t)
  local rate, amp, cent, phase = get_slider_value(4),get_slider_value(5),get_slider_value(6),get_slider_value(7)
  return cent+amp*math.sin(2*3.141593*t*rate+phase)
end
function generate_value(t)
	--if t>=0.4 and t<=0.6 then return nil end
	return fun_a(t)+fun_b(t)
end

function generate_envelopex()
  points = {}
  local rate = get_slider_value(0)
  local cent = get_slider_value(2)
  local amp = get_slider_value(1)
  local phase = get_slider_value(3)
  for i=0,100 do
    local t = 1.0/100*i
    points[i]={t,fun_a(t)+fun_b(t)}
  end
  return points
end

)";
		m_script_editor.setText(txt);
		try
		{
			m_lua.script(txt.toStdString());
			sol::function initfun = m_lua["on_init"];
			if (initfun.valid())
			{
				m_sliders.clear();
				initfun();
				resized();
			}
		}
		catch (std::exception& excep)
		{
			Logger::writeToLog(excep.what());
		}
	}
}

