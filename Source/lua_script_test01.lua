function doit()
	--node = find_node("count")
	--set_parameter(node,0,"C:\\MusicAudio\\sourcesamples\\there was a time .wav")
	--node = find_node("Modify")
	--set_parameter(node,0,666.0)
	nodes = get_nodes()
	
	txt = ""
	for k,v in pairs(nodes) do
		txt=txt..k.." "..get_name(v).."\n"
	end
	msgbox("Document has nodes :\n"..txt)
end

function doit4()
avail_nodes={ 
{name="Input",chans=4,spec=false},
{name="Reverse",chans=4,spec=false},
{name="Reverb",chans=4,spec=false},
{name="DistRepeat",chans=1,spec=false},
{name="Stretch",chans=1,spec=true}
}

local nodes={}
nodes[1]=make_node("Input Sound File")
nodes[2]=make_node("Modify Radical - 1 Reverse")
nodes[3]=make_node("Distort Repeat - Ti")
nodes[4]=make_node("Modify Radical - 5 Ring Modulate")
local result_nodes={}
local result_conns={}
local paralchains = 1
local cnt = 2
local concnt = 1
result_nodes[1]=nodes[1]
for i=2,#nodes do
  if paralchains<2 and nodes[i]["chans"]==1 then
    local foo = cnt-1
    paralchains = result_nodes[foo]["chans"]
    for j=1,paralchains do
      result_nodes[cnt]={name="split "..j,chans=1,spec=false}
      local indx = cnt-1
      if indx>0 then
        result_conns[concnt]={result_nodes[foo].name,result_nodes[cnt].name}
        concnt = concnt + 1
      end
      cnt=cnt+1
    end
  end
  if paralchains>1 and nodes[i]["chans"]>1 then
    result_nodes[cnt]={name="merge",chans=paralchains}
    for j=1,paralchains do
      local indx = cnt-paralchains+(j-1)
      if indx>0 then
        result_conns[concnt]={result_nodes[indx].name,result_nodes[cnt].name}
        concnt = concnt + 1
      end
    end
    cnt=cnt+1    
    paralchains = 1
    
  end
  local foo = cnt
  for j=1,paralchains do
    local numchans = nodes[i].chans
    if paralchains>1 then numchans = 1 end
    local n = {name=nodes[i]["name"].." "..j,chans=numchans}
    result_nodes[cnt]=n
    local indx = (foo-paralchains)+(j-1)
    if indx>0 then
      result_conns[concnt]={result_nodes[indx].name,n.name}
      concnt = concnt + 1
    end
    cnt=cnt+1
  end
end
--[[
print("nodes for graph:")
for i=1,#result_nodes do
  print ("\t"..result_nodes[i]["name"])
end
print("connections for graph:")
for i=1,#result_conns do
  print("\t"..result_conns[i][1].." -> "..result_conns[i][2])
end
--]]

end

function doit2()
	mixer = find_node("SequencerMixer")
	clear_events(mixer)
	for i=0,31 do
		add_event(mixer,i*0.2,"C:/MusicAudio/Samples from net/Berklee44v4/piano_Eb5.wav")
		add_event(mixer,i*0.2+0.1,"C:/MusicAudio/Samples from net/Berklee44v4/prep_pianoG#0.wav")
	end
    for i=0,7 do
		add_event(mixer,10.0+i*0.2,"C:/MusicAudio/Samples from net/Berklee44v4/piano_Eb5.wav")
		add_event(mixer,10.0+i*0.2+0.1,"C:/MusicAudio/Samples from net/Berklee44v4/prep_pianoG#0.wav")
	end
	add_event(mixer,11.0,"C:/MusicAudio/Samples from net/NilsVanOttorloo44/NilsVanOttorloo44/bassClarinet1.wav")
	add_event(mixer,26.0,"C:/MusicAudio/Samples from net/NilsVanOttorloo44/NilsVanOttorloo44/bassClarinet1.wav")
	add_event(mixer,14.5,"C:/MusicAudio/Samples from net/NilsVanOttorloo44/NilsVanOttorloo44/bassClarinet3.wav")
end


function doit3()      
	--msgbox("scripting rocks!")
	clear_nodes()
	--n0 = make_node("monty","python", 5,95,80,30)
	--n1 = make_node("1 reverse", "takaperin",  210, 5, 80,30)
	n1 = make_node("Input Sound File", "file", 305,5,100,50)
	---[[
	n2 = {}
	n3 = find_node("count")
	if n3==nil then msgbox("Node not found, aborting.") return end
	mm = make_node("join multiple files","join",305,200,200,125)
	coroutine.yield(0);	
	for i=0,9 do
		n2 = make_node("stack", "stack"..i, 5+i*90, 100, 85, 35)
		coroutine.yield(i+1);
		connect_nodes(n3,n2)
		set_parameter(n2,0,-3.0+4.0/10*i)
		set_parameter(n2,3,0.1+1.9/10*i)
		connect_nodes(n2,mm)
			
	end
end
