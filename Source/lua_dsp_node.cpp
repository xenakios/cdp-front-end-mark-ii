#include "lua_dsp_node.h"
#include "script_editor.h"
#include "node_component.h"
#include "node_view.h"

LuaDSPNode::LuaDSPNode(String procname) : m_script_state("luadspscriptstate")
{
	m_proc_name = procname;
	m_type_name = procname;
#ifndef WIN32
	m_script_state.setProperty("script",
    R"(function renderx(opts)
    local buffer = makeAudioBuffer(4,44100*10)
    for i=0,44100*10 do
        local s = 0.5+0.4*math.sin(2*3.141592654/44100*i*440.0)
        buffer:setSample(i,0,s*math.sin(2*3.141592654/44100*i*0.4))
        buffer:setSample(i,1,s*math.sin(2*3.141592654/44100*i*1.9))
        buffer:setSample(i,2,s*math.sin(2*3.141592654/44100*i*7.5))
        buffer:setSample(i,3,s*math.sin(2*3.141592654/44100*i*9.0))
        end
        
        return 44100, buffer
        end
    function render(opts)
        local reader = makeAudioFileReader("/Users/teemu/AudioProjects/sourcesamples/count.wav")
        local outbuffer = makeAudioBuffer(2,44100)
        local readbuffer = makeAudioBuffer(1,44100)
        reader:getAudio(0,readbuffer,0,44100)
        outbuffer:copyFrom(0,readbuffer,0,44100,0,false)
        reader:getAudio(22050,readbuffer,0,44100)
        outbuffer:copyFrom(1,readbuffer,0,44100,0,false)
        return 44100, outbuffer
    end

)",nullptr);
#else
	m_script_state.setProperty("script",
		R"(function renderx(opts)
    local buffer = makeAudioBuffer(4,44100*10)
    for i=0,44100*10 do
        local s = 0.5+0.4*math.sin(2*3.141592654/44100*i*440.0)
        buffer:setSample(i,0,s*math.sin(2*3.141592654/44100*i*0.4))
        buffer:setSample(i,1,s*math.sin(2*3.141592654/44100*i*1.9))
        buffer:setSample(i,2,s*math.sin(2*3.141592654/44100*i*7.5))
        buffer:setSample(i,3,s*math.sin(2*3.141592654/44100*i*9.0))
        end
        
        return 44100, buffer
        end
    function render(opts)
        local reader = makeAudioFileReader("C:\\MusicAudio\\sourcesamples\\count.wav")
        local outbuffer = makeAudioBuffer(2,44100)
        local readbuffer = makeAudioBuffer(1,44100)
        reader:getAudio(0,readbuffer,0,44100)
        outbuffer:copyFrom(0,readbuffer,0,44100,0,false)
        reader:getAudio(22050,readbuffer,0,44100)
        outbuffer:copyFrom(1,readbuffer,0,44100,0,false)
        return 44100, outbuffer
    end

)",nullptr);
#endif
	//runScript(m_script);
}

StringArray LuaDSPNode::get_output_filenames()
{
    return StringArray{m_output_fn};
}

String LuaDSPNode::get_supported_input_type()
{
	return "wav";
}

String LuaDSPNode::get_output_type()
{
	return "wav";
}

void LuaDSPNode::tick()
{
    tick_sources();
    if (m_pstate == Busy && m_thread_state == 1)
    {
        m_thread->join();
        m_thread.reset();
        end_benchmark();
        TempFileContainer::instance().add(m_output_fn);
        m_pstate = FinishedOK;
        setAudioFileToPlay(m_output_fn);
        ProcessFinishedFunc(this);
        return;
    }
    if (m_pstate == FinishedWithError && m_thread_state == 1)
    {
        m_thread->join();
        m_thread.reset();
        ProcessErrorFunc(this);
        return;
    }
    auto finish_state = are_sources_finished();
    if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
    {
        m_error_text = "Error in source(s)";
        m_pstate = FinishedWithError;
        ProcessErrorFunc(this);
    }
    if (finish_state == SourcesState::FinishedOK)
    {
        if (m_pstate == Idle)
        {
            
            MD5 h = get_state_hash();
            //Logger::writeToLog(h.toHexString());
            String outfn(get_output_folder() + "/luadsp_node-" + h.toHexString() + ".wav");
            File tempfile(outfn);
            if (tempfile.exists() == true)
            {
                m_pstate = FinishedOK;
                setAudioFileToPlay(outfn);
                if (ProcessFinishedFunc)
                    ProcessFinishedFunc(this);
                return;
            }
            start_benchmark();
            String err = runScript(m_script_state.getProperty("script").toString().toStdString());
            if (err.isEmpty()==false)
            {
                m_error_text = err;
                m_pstate = FinishedWithError;
                ProcessErrorFunc(this);
                return;
            }
            m_output_fn = outfn;
            WavAudioFormat wavformat;
            File outfile(outfn);
            FileOutputStream* outstream = outfile.createOutputStream();
            if (outstream!=nullptr)
            {
                int numoutchans = m_script_output_buf.getNumChannels();
                StringPairArray metadata;
                metadata.set(WavAudioFormat::bwavOriginator, CharPointer_UTF8("λ Lua DSP node"));
                auto writer = wavformat.createWriterFor(outstream, m_buf_sr, numoutchans, 32, metadata, 0);
                writer->writeFromAudioSampleBuffer(m_script_output_buf, 0, m_script_output_buf.getNumSamples());
                writer->flush();
                delete writer;
                end_benchmark();
                
                m_pstate = FinishedOK;
                TempFileContainer::instance().add(m_output_fn);
                setAudioFileToPlay(m_output_fn);
                ProcessFinishedFunc(this);
            }
            
        }
    }

}

MD5 LuaDSPNode::extra_state_hash()
{
	String script = m_script_state.getProperty("script");
	MD5 result(script.toRawUTF8(),script.length());
    return result;
}

ValueTree LuaDSPNode::getAdditionalStateValueTree()
{
	ValueTree result("customstate");
	result.setProperty("script", m_script_state.getProperty("script"), nullptr);
	return result;
}

void LuaDSPNode::restoreAdditionalStateFromValueTree(ValueTree tree)
{
	m_script_state.setProperty("script",tree.getProperty("script"), nullptr);
	
}

void LuaDSPNode::connections_changed()
{
}

void LuaDSPNode::update_params()
{
}

void LuaDSPNode::sourcesHaveChanged()
{
}

String LuaDSPNode::runScript(String scriptcode)
{
	try
	{
		sol::state lua;
		lua.open_libraries(sol::lib::base, sol::lib::math);
        lua.set_function("makeAudioBuffer", [](int chans, int len)
		{
            AudioBuffer<float> result(chans,len);
            result.clear();
            return result;
        });
        WavAudioFormat wavformat;
		auto func_makeReader = [this, &wavformat](const char* fn)
		{
			String temp{ CharPointer_UTF8(fn) };
			if (temp.startsWithChar('$'))
			{
				temp = temp.substring(1);
				int sourceindex = temp.getIntValue();
				temp = getSourceFileName(sourceindex, 0);
			}
			if (File::isAbsolutePath(temp) == true)
			{
				auto reader = wavformat.createMemoryMappedReader(File(temp));
				if (reader != nullptr)
				{
					reader->mapEntireFile();
					return std::shared_ptr<MemoryMappedAudioFormatReader>(reader);
				}
			}
			return std::shared_ptr<MemoryMappedAudioFormatReader>();
		};
		lua.set_function("makeAudioFileReader", func_makeReader);
        auto func_numChannels=[](MemoryMappedAudioFormatReader& reader){ return reader.numChannels; };
        auto func_sampleRate=[](MemoryMappedAudioFormatReader& reader){ return reader.sampleRate; };
        auto func_lengthSamples=[](MemoryMappedAudioFormatReader& reader){ return reader.lengthInSamples; };
		auto func_lengthSeconds = [](MemoryMappedAudioFormatReader& reader) 
		{ 
			if (reader.sampleRate == 0)
				return 0.0;
			return (double)reader.lengthInSamples / reader.sampleRate;
		};
		auto func_getSamples=[](MemoryMappedAudioFormatReader& reader,
                                int64_t srcpos,
                                AudioBuffer<float>& dest,
                                int64_t destpos,
                                int destlen)
        {
            int chans_to_out = std::min<int>(dest.getNumChannels(),reader.numChannels);
            float** writepointers = dest.getArrayOfWritePointers();
            float readbuf[64];
            for (int j=0;j<destlen;++j)
            {
                if (srcpos+j>=0 && srcpos+j<reader.lengthInSamples && destpos+j>=0 && destpos+j<dest.getNumSamples())
                {
                    reader.getSample(srcpos+j, readbuf);
                    for (int i=0;i<chans_to_out;++i)
                    {
                        writepointers[i][destpos+j]=readbuf[i];
                    }
                    
                }
            }
        };
        lua.new_usertype<MemoryMappedAudioFormatReader>("AudioReader",
                                                        "numChannels", func_numChannels,
                                                        "sampleRate", func_sampleRate,
                                                        "lengthSamples",func_lengthSamples,
                                                        "getAudio",func_getSamples,
														"lengthSeconds",func_lengthSeconds);
		lua.new_usertype<AudioBuffer<float>>("AudioBuffer",
			"setSample", [](AudioBuffer<float>& buf, int pos, int chan, double value) 
		{ 
			if (pos>=0 && pos<buf.getNumSamples())
				buf.setSample(chan, pos, value); 
        }, "getSample",[](AudioBuffer<float>& buf, int pos, int chan)
        {
            if (pos>=0 && pos<buf.getNumSamples())
            {
                return (double)buf.getSample(chan, pos);
            }
            return 0.0;
        }, "copyFrom",[](AudioBuffer<float>& destbuf, int destchan, AudioBuffer<float>& srcbuf, int pos, int len, int src_chan, bool mix)
        {
            if (mix == false)
                destbuf.copyFrom(destchan, pos, srcbuf, src_chan, 0, len);
        });
		lua.script(scriptcode.toStdString());
		sol::function render_func = lua["render"];
		if (render_func.valid())
		{
			std::pair<int,AudioBuffer<float>> result = render_func();
			//Logger::writeToLog("returned buffer address is " + String((uint64_t)result.second.getWritePointer(0)));
            //Logger::writeToLog("returned buffer size is " + String(result.second.getNumSamples()));
			//Logger::writeToLog("returned buffer sr is " + String(result.first));
            m_script_output_buf = result.second;
            m_buf_sr = jlimit(1,384000,result.first);
            return String();
		}
	}
	catch (std::exception& excep)
	{
		//Logger::writeToLog(excep.what());
        return excep.what();
	}
    return String();
}

Component * LuaDSPNode::getCustomEditor()
{
    ScriptTextEditor* ed = new ScriptTextEditor(m_script_state);
    ed->setMultiLine(true);
    ed->setReturnKeyStartsNewLine(true);
    ed->OnExecuteScript=[this](String)
    {
        m_component->m_node_view->renderSelected(false);
    };
	ed->getProperties().set("maxh", -1);
	return ed;
}
