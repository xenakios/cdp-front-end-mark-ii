#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include <functional>
#include <future>

class zoom_scrollbar;
class process_node;

class WaveformComponentBase : 
	public Component, 
	public ChangeListener,
	public MultiTimer
{
public:
	WaveformComponentBase(bool withzoomscrollbar);
	void mouseDown(const MouseEvent& ev) override;
	void setAudioFile(String fn);
	void paint(Graphics& g) override;
	virtual void paintOverlay(Graphics& /*g*/) {}
	void timerCallback(int) override {}
	void changeListenerCallback(ChangeBroadcaster*) override;
	void resized() override;
	std::function<void(double)> OnClicked;
	void setPlayCursorPosition(double tpos);
protected:
	std::unique_ptr<AudioThumbnail> m_thumb;
	String m_fn;
	double m_view_start = 0.0;
	double m_view_end = 1.0;
	std::unique_ptr<zoom_scrollbar> m_zsbar;
	double m_playcurpos = -1.0;
	Image m_spectral_image;
	bool m_is_dirty = false;
};

class WaveformWithEditor : public WaveformComponentBase, public ValueListener, public ActionListener
{
public:
	WaveformWithEditor(process_node* n);
	~WaveformWithEditor();
	void paintOverlay(Graphics& g) override;
	void mouseDown(const MouseEvent& ev) override;
	void mouseUp(const MouseEvent& ev) override;
	void mouseDrag(const MouseEvent& ev) override;
	void mouseMove(const MouseEvent& ev) override;
	void timerCallback(int id) override;
	void valueChanged(Value& var) override;
	void actionListenerCallback(const String& msg) override;
private:
	int m_hot_area = -1;
	int m_edge_to_drag = -1;
	double m_new_time_sel_start = 0.0;
	int findHotArea(int x, int y);
	Value m_time_sel_start_val;
	Value m_time_sel_end_val;
	process_node* m_node=nullptr;
	void sanitizeTimeSelection();
};
