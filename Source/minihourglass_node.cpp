#include "minihourglass_node.h"
#ifdef BUILD_HOURGLASS_NODE


#include "db2val.h"

double c_pi = 3.1415926536;

template<typename T>
inline T qBound(T lowest, T value, T highest)
{
    return jlimit(lowest,highest,value);
}

SoundSourcesManager::SoundSourcesManager(minihourglass_node * node) : m_minihg(node)
{
	for (int i = 0; i<8; ++i)
		m_sources.push_back(std::make_shared<CAudioSource>());
}

CAudioSource * SoundSourcesManager::getSource(int index)
{
	updateSource();
	return m_sources[index].get();
}

CAudioSource * SoundSourcesManager::focusedSound()
{
	updateSource();
	if (m_sources.size() == 0)
		return nullptr;
	return m_sources[0].get();
}

int SoundSourcesManager::getRandomSourceIndex()
{
	return m_dist(m_randgen);
}

void SoundSourcesManager::updateSource()
{
    if (m_minihg->m_sources.size()>0)
    {
        m_active_sources = m_minihg->m_sources.size();
        m_dist = std::uniform_int_distribution<int>(0,m_active_sources-1);
        if (m_minihg->m_sources.size()>m_active_sources)
        {
            //m_sources.resize(m_minihg->m_sources.size());
            //for (int i=0;i<m_minihg->m_sources.size();++i)
            //    m_sources[i]=std::make_shared<CAudioSource>();
        }
        
        for (int i=0;i<m_minihg->m_sources.size();++i)
        {
            
            if (m_minihg->m_sources[i]->get_output_filenames().size()>0)
            {
                String src_fn = m_minihg->m_sources[i]->get_output_filenames()[0];
                if (src_fn!=m_sources[i]->m_src_fn)
                {
                    m_sources[i]->loadAudioFile(src_fn);
                }
            }
        }
		int temp = 0;
		for (auto& e : m_sources)
		{
			if (e->m_mmareader != nullptr)
				++temp;
		}
		m_active_sources = temp;
    }
}

FragmentVoice::FragmentVoice()
{
    m_per_fragment_pan=false;
    m_outputBufferLenFrames=512;
    m_volumeEnvelopeErrors=0;
    m_voiceIsPlaying=false;
    m_voiceLengthCounter=-1;
    m_gatherPlayPosInfo=false;
    m_playPosInfo=0;
    m_playPosInfoIndex=0;
    m_pitchEnvelopeIsPlayrate=false;
    m_pitchEnvelopeGranularity=64;
    m_envMorph=1.0;
    m_elapsedCounter=0;
    m_elapsedAccum=0.0;
    m_indexToResampledOut=-1;
    
    m_previousSrcFact=0.0;
    m_graintoplay.Audiosource=0;
    m_graintoplay.Timepos=-1;
    m_graintoplay.sendsEnabled[0]=false;
    m_graintoplay.sendsEnabled[1]=false;
    m_audbuffer.resize(4096);
    m_samplerate=44100;
    m_srcOutBuf.resize(4096);
    m_streamFramepos=0;
    //m_GrainPlays=false;
    m_offsettedFramePos=0;
    m_resamplerOutLen=1024;
    m_outputPosition=0;
    m_VoicePlayState=0; // 0 free, 1 started grain, 2 grain ended in this buffer
    m_resampler.SetMode(true,1,false);
    m_audioSourcePlayPos=0;
    m_numOutChans=2;
    m_pannedoutputsgains.resize(256);
}

FragmentVoice::~FragmentVoice()
{
    
}

void FragmentVoice::setResamplerQuality(int nqual)
{
    if (nqual==0)
        m_resampler.SetMode(false,0,false);
    else if (nqual==1)
        m_resampler.SetMode(true,0,false);
    else if (nqual==2)
        m_resampler.SetMode(true,2,false);
    else if (nqual==3)
        m_resampler.SetMode(true,1,true,16,32);
    else if (nqual==4)
        m_resampler.SetMode(true,1,true,64,64);
    else if (nqual==5)
        m_resampler.SetMode(true,4,true,192);
    else
        m_resampler.SetMode(true,1,false);
    
}

void FragmentVoice::resetBuffers(int newbufsize, int channels)
{
    //qDebug() << "FragmentVoice::resetBuffers"<<m_ID<<newbufsize;
    m_numOutChans=channels;
    if (newbufsize*m_numOutChans>=(int)m_audbuffer.size())
    {
        m_audbuffer.resize(newbufsize*m_numOutChans);
    }
    //m_srcOutBuf.resize(newbufsize*m_numOutChans+32);
    
    //for (int i=0;i<m_sends.size();i++)
    //    m_sends[i].audiobuf.resize(newbufsize*2);
    
    
    m_resamplerOutLen=newbufsize;
    std::fill(m_srcOutBuf.begin(),m_srcOutBuf.end(),0.0);
    m_resampler.Reset();
    //m_resamplerQueue.Clear();
}
#ifdef USE_MEM_MAPPED_WAV_READER
void FragmentVoice::fillResampledBuffer(int offset,int frames)
{
    float readBuf[64];
    CAudioSource *audSource=m_graintoplay.Audiosource;
	if (audSource->m_mmareader == nullptr)
	{
		for (int i = 0; i < m_numOutChans*m_resamplerOutLen; ++i)
			m_srcOutBuf[i] = 0.0;
		return;
	}
	int startframe=m_graintoplay.Audiosource->mediaStart()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    int endFrame=m_graintoplay.Audiosource->mediaEnd()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    int64_t srcFullLenFrames=m_graintoplay.Audiosource->processedAudioLengthFrames();
    double compens=(double)audSource->origSampleRate()/m_samplerate;
    int lastFrame=endFrame+m_graintoplay.Length*m_samplerate;
    if (lastFrame>m_graintoplay.Audiosource->processedAudioLengthFrames())
        lastFrame=m_graintoplay.Audiosource->processedAudioLengthFrames();
    //const float** audioSourceBuf=audSource->audioBuffer();
    
    double* resamplerIn=nullptr;
    int src_chans=audSource->processedAudioNumChans();
    int resamplerGranularity=m_pitchEnvelopeGranularity;
    int fraglensamples=m_graintoplay.Length*m_samplerate;
    double lenenvtable=m_volumeEnvelopes->tableSize();
    double envtablestep=(double)(lenenvtable-1)/fraglensamples;
    int numFramesReadyFromResampler=0;
    int sanityCounter=0;
    while (numFramesReadyFromResampler<m_resamplerOutLen)
    {
        sanityCounter++;
        if (sanityCounter>1024) // hopefully this is a good limit...
        {
            //qDebug() << "fragmentvoice resampler loop sanity failure"<<m_ID;
            break;
        }
        int maxtableindex=m_pitchEnvelopes->numEnvelopes()-1;
        double z=m_graintoplay.pitchEnvMorph*maxtableindex;
        int tabindex1=m_graintoplay.pitchEnvMorph*maxtableindex;
        int tabindex2=m_graintoplay.pitchEnvMorph*maxtableindex+1;
        if (tabindex2>maxtableindex)
            tabindex2=maxtableindex;
        //m_pitchEnvelopes->tableMutex()->Enter();
        float* pitchenvptr1=m_pitchEnvelopes->tableDataPtr(tabindex1);
        float* pitchenvptr2=m_pitchEnvelopes->tableDataPtr(tabindex2);
        //m_pitchEnvelopes->tableMutex()->Leave();
        double fract=z-(int)z;
        double morphfact1=1.0-fract;
        double morphfact2=fract;
        
        int indextopitchtable=m_pitchEnvelopePosition;
        if (indextopitchtable>65535)
            indextopitchtable=65535;
        double envelopedeltanormalized=pitchenvptr1[indextopitchtable]*morphfact1+pitchenvptr2[indextopitchtable]*morphfact2;
        double envelopedeltasemitones=-24.0+48.0*envelopedeltanormalized;
        envelopedeltasemitones = 0.0;
        //qDebug() << m_ID << "envelope pitch delta"<<envelopedeltasemitones << m_pitchEnvelopePosition;
        double infilesrcCompens=(double)m_samplerate/(double)m_graintoplay.Audiosource->origSampleRate();
        double finalsrcfact=1.0; //(1.0/infilesrcCompens)*(pow(2.0,(m_graintoplay.Transpose+envelopedeltasemitones)/12.0));
        double playrate=1.0;
        if (m_pitchEnvelopeIsPlayrate==false)
            finalsrcfact=(1.0/infilesrcCompens)*(pow(2.0,(m_graintoplay.Transpose+envelopedeltasemitones)/12.0));
        else
        {
            
            if (envelopedeltanormalized<0.25)
                playrate=-16.0+(15.0/0.25)*envelopedeltanormalized;
            else if (envelopedeltanormalized>=0.25 && envelopedeltanormalized<0.5)
                playrate=-1.0+(1.0/0.25)*(envelopedeltanormalized-0.25);
            else if (envelopedeltanormalized>=0.5 && envelopedeltanormalized<0.75)
                playrate=0.0625+((1.0-0.0625)/0.25)*(envelopedeltanormalized-0.5);
            else if (envelopedeltanormalized>=0.75)
                playrate=1.0+(15.0/0.25)*(envelopedeltanormalized-0.75);
            finalsrcfact=(1.0/infilesrcCompens)*(pow(2.0,m_graintoplay.Transpose/12.0)*std::abs(playrate));
        }
        if (m_pitchEnvelopeIsPlayrate==true)
        {
            finalsrcfact=jlimit(0.01,64.0,finalsrcfact);
        }
        m_resampler.SetRates(m_graintoplay.Audiosource->origSampleRate(),m_samplerate*(compens*(1.0/finalsrcfact)));
        int wanted=m_resampler.ResamplePrepare(resamplerGranularity,src_chans,&resamplerIn);
        if (playrate>0.0) // reverse playback would need to touch samples in reverse order, not implemented yet
        {
            /*
			for (int i=0;i<wanted;++i)
            {
                if (m_audioSourcePlayPos+i<audSource->m_mmareader->lengthInSamples)
                    audSource->m_mmareader->touchSample(m_audioSourcePlayPos+i);
            }
			*/
			if (m_audioSourcePlayPos + wanted < audSource->m_mmareader->lengthInSamples)
				audSource->m_mmareader->touchSample(m_audioSourcePlayPos + wanted);
        }
        if (m_pitchEnvelopeIsPlayrate==false)
        {
            for (int i=0;i<wanted;i++)
            {
                if (m_audioSourcePlayPos<srcFullLenFrames)
                {
                    audSource->m_mmareader->getSample(m_audioSourcePlayPos, readBuf);
                    for (int j=0;j<src_chans;++j)
                    {
                        resamplerIn[i*src_chans+j]=readBuf[j];
                    }
                } else
                {
                    for (int j=0;j<src_chans;++j)
                    {
                        resamplerIn[i*src_chans+j]=0.0;
                    }
                }
                
                ++m_audioSourcePlayPos;
                
            }
        } else
        {
            for (int i=0;i<wanted;++i)
            {
                for (int j=0;j<src_chans;++j)
                {
                    //resamplerIn[i*src_chans + j] = audioSourceBuf[j][m_audioSourcePlayPos]; //[m_audioSourcePlayPos*src_chans + j];
                }
                if (playrate<0.0)
                {
                    --m_audioSourcePlayPos;
                    if (m_audioSourcePlayPos<startframe)
                        m_audioSourcePlayPos=endFrame-1;
                } else
                {
                    ++m_audioSourcePlayPos;
                    if (m_audioSourcePlayPos>endFrame-1)
                        m_audioSourcePlayPos=startframe;
                }
            }
        }
        int srcoutbufindex=src_chans*numFramesReadyFromResampler;
        
        double* temp=&m_srcOutBuf.data()[srcoutbufindex];
        int produced=m_resampler.ResampleOut(temp,wanted,resamplerGranularity,src_chans);
        //if (m_ID==0 && produced!=resamplerGranularity)
        //    qDebug() << m_ID << "WDL Resampler didn't produce asked number of frames"<<resamplerGranularity<<produced;
        numFramesReadyFromResampler+=produced;
        m_pitchEnvelopePosition+=envtablestep*resamplerGranularity;
    }
    
    //qDebug() << "resampler latency is"<<m_resampler.GetCurrentLatency()*m_samplerate;
    double* temp=m_srcOutBuf.data();
    if (m_volumeEnvelopes->numEnvelopes()>0)
    {
        
        int maxtableindex=m_volumeEnvelopes->numEnvelopes()-1;
        double z=m_graintoplay.volumeEnvMorph*maxtableindex;
        int tabindex1=m_graintoplay.volumeEnvMorph*maxtableindex;
        int tabindex2=m_graintoplay.volumeEnvMorph*maxtableindex+1;
        if (tabindex2>maxtableindex)
            tabindex2=maxtableindex;
        //m_volumeEnvelopes->tableMutex()->Enter();
        float* env1=m_volumeEnvelopes->tableDataPtr(tabindex1);
        float* env2=m_volumeEnvelopes->tableDataPtr(tabindex2);
        //m_volumeEnvelopes->tableMutex()->Leave();
        double fract=z-(int)z;
        double morphfact1=1.0-fract;
        double morphfact2=fract;
        for (int i=0;i<m_resamplerOutLen;++i)
        {
            double frac=m_volumeEnvelopePosition-(int)m_volumeEnvelopePosition;
            int tablepos1=(int)m_volumeEnvelopePosition;
            //if (tablepos1>lenenvtable-1) m_volumeEnvelopeErrors++;
            int tablepos2=tablepos1+1;
            if (tablepos2>lenenvtable-1)
                tablepos2=lenenvtable-1;
            double tablegain1a=env1[tablepos1];
            double tablegain1b=env1[tablepos2];
            double tablegain1delta=tablegain1b-tablegain1a;
            double tablegain1interpolated=tablegain1a+tablegain1delta*frac;
            double tablegain2a=env2[tablepos1];
            double tablegain2b=env2[tablepos2];
            double tablegain2delta=tablegain2b-tablegain2a;
            double tablegain2interpolated=tablegain2a+tablegain2delta*frac;
            double interp=tablegain1interpolated*morphfact1+tablegain2interpolated*morphfact2;
            for (int j=0;j<src_chans;++j)
                temp[i*src_chans+j]*=interp;
            m_volumeEnvelopePosition+=envtablestep;
            if (m_volumeEnvelopePosition>=lenenvtable)
                m_volumeEnvelopePosition=0.0;
        }
        return;
    }
    
}
#else

void FragmentVoice::fillResampledBuffer(int offset,int frames)
{
    CAudioSource *audSource=m_graintoplay.Audiosource;
    int startframe=m_graintoplay.Audiosource->mediaStart()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    int endFrame=m_graintoplay.Audiosource->mediaEnd()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    int64_t srcFullLenFrames=m_graintoplay.Audiosource->processedAudioLengthFrames();
    double compens=(double)audSource->origSampleRate()/m_samplerate;
    int lastFrame=endFrame+m_graintoplay.Length*m_samplerate;
    if (lastFrame>m_graintoplay.Audiosource->processedAudioLengthFrames())
        lastFrame=m_graintoplay.Audiosource->processedAudioLengthFrames();
    const float** audioSourceBuf=audSource->audioBuffer();
    double* resamplerIn=nullptr;
    int src_chans=audSource->processedAudioNumChans();
    int resamplerGranularity=m_pitchEnvelopeGranularity;
    int fraglensamples=m_graintoplay.Length*m_samplerate;
    double lenenvtable=m_volumeEnvelopes->tableSize();
    double envtablestep=(double)(lenenvtable-1)/fraglensamples;
    int numFramesReadyFromResampler=0;
    int sanityCounter=0;
    while (numFramesReadyFromResampler<m_resamplerOutLen)
    {
        sanityCounter++;
        if (sanityCounter>1024) // hopefully this is a good limit...
        {
            //qDebug() << "fragmentvoice resampler loop sanity failure"<<m_ID;
            break;
        }
        int maxtableindex=m_pitchEnvelopes->numEnvelopes()-1;
        double z=m_graintoplay.pitchEnvMorph*maxtableindex;
        int tabindex1=m_graintoplay.pitchEnvMorph*maxtableindex;
        int tabindex2=m_graintoplay.pitchEnvMorph*maxtableindex+1;
        if (tabindex2>maxtableindex)
            tabindex2=maxtableindex;
        //m_pitchEnvelopes->tableMutex()->Enter();
        float* pitchenvptr1=m_pitchEnvelopes->tableDataPtr(tabindex1);
        float* pitchenvptr2=m_pitchEnvelopes->tableDataPtr(tabindex2);
        //m_pitchEnvelopes->tableMutex()->Leave();
        double fract=z-(int)z;
        double morphfact1=1.0-fract;
        double morphfact2=fract;
        
        int indextopitchtable=m_pitchEnvelopePosition;
        if (indextopitchtable>65535)
            indextopitchtable=65535;
        double envelopedeltanormalized=pitchenvptr1[indextopitchtable]*morphfact1+pitchenvptr2[indextopitchtable]*morphfact2;
        double envelopedeltasemitones=-24.0+48.0*envelopedeltanormalized;
        envelopedeltasemitones = 0.0;
        //qDebug() << m_ID << "envelope pitch delta"<<envelopedeltasemitones << m_pitchEnvelopePosition;
        double infilesrcCompens=(double)m_samplerate/(double)m_graintoplay.Audiosource->origSampleRate();
        double finalsrcfact=1.0; //(1.0/infilesrcCompens)*(pow(2.0,(m_graintoplay.Transpose+envelopedeltasemitones)/12.0));
        double playrate=1.0;
        if (m_pitchEnvelopeIsPlayrate==false)
            finalsrcfact=(1.0/infilesrcCompens)*(pow(2.0,(m_graintoplay.Transpose+envelopedeltasemitones)/12.0));
        else
        {
            
            if (envelopedeltanormalized<0.25)
                playrate=-16.0+(15.0/0.25)*envelopedeltanormalized;
            else if (envelopedeltanormalized>=0.25 && envelopedeltanormalized<0.5)
                playrate=-1.0+(1.0/0.25)*(envelopedeltanormalized-0.25);
            else if (envelopedeltanormalized>=0.5 && envelopedeltanormalized<0.75)
                playrate=0.0625+((1.0-0.0625)/0.25)*(envelopedeltanormalized-0.5);
            else if (envelopedeltanormalized>=0.75)
                playrate=1.0+(15.0/0.25)*(envelopedeltanormalized-0.75);
            finalsrcfact=(1.0/infilesrcCompens)*(pow(2.0,m_graintoplay.Transpose/12.0)*std::abs(playrate));
        }
        if (m_pitchEnvelopeIsPlayrate==true)
        {
            finalsrcfact=jlimit(0.01,64.0,finalsrcfact);
        }
        m_resampler.SetRates(m_graintoplay.Audiosource->origSampleRate(),m_samplerate*(compens*(1.0/finalsrcfact)));
        int wanted=m_resampler.ResamplePrepare(resamplerGranularity,src_chans,&resamplerIn);
        if (m_pitchEnvelopeIsPlayrate==false)
        {
            for (int i=0;i<wanted;i++)
            {
                for (int j=0;j<src_chans;++j)
                {
                    if (m_audioSourcePlayPos<srcFullLenFrames)
                    {
                        int temp_pos=m_audioSourcePlayPos*src_chans+j;
                        resamplerIn[i*src_chans+j]=audioSourceBuf[j][m_audioSourcePlayPos];
                    }
                    else
                    {
                        resamplerIn[i*src_chans+j]=0.0;
                    }
                }
                ++m_audioSourcePlayPos;
                
            }
        } else
        {
            for (int i=0;i<wanted;++i)
            {
                for (int j=0;j<src_chans;++j)
                {
					resamplerIn[i*src_chans + j] = audioSourceBuf[j][m_audioSourcePlayPos]; //[m_audioSourcePlayPos*src_chans + j];
                }
                if (playrate<0.0)
                {
                    --m_audioSourcePlayPos;
                    if (m_audioSourcePlayPos<startframe)
                        m_audioSourcePlayPos=endFrame-1;
                } else
                {
                    ++m_audioSourcePlayPos;
                    if (m_audioSourcePlayPos>endFrame-1)
                        m_audioSourcePlayPos=startframe;
                }
            }
        }
        int srcoutbufindex=src_chans*numFramesReadyFromResampler;
        
        double* temp=&m_srcOutBuf.data()[srcoutbufindex];
        int produced=m_resampler.ResampleOut(temp,wanted,resamplerGranularity,src_chans);
        //if (m_ID==0 && produced!=resamplerGranularity)
        //    qDebug() << m_ID << "WDL Resampler didn't produce asked number of frames"<<resamplerGranularity<<produced;
        numFramesReadyFromResampler+=produced;
        m_pitchEnvelopePosition+=envtablestep*resamplerGranularity;
    }
    
    //qDebug() << "resampler latency is"<<m_resampler.GetCurrentLatency()*m_samplerate;
    double* temp=m_srcOutBuf.data();
    if (m_volumeEnvelopes->numEnvelopes()>0)
    {
        
        int maxtableindex=m_volumeEnvelopes->numEnvelopes()-1;
        double z=m_graintoplay.volumeEnvMorph*maxtableindex;
        int tabindex1=m_graintoplay.volumeEnvMorph*maxtableindex;
        int tabindex2=m_graintoplay.volumeEnvMorph*maxtableindex+1;
        if (tabindex2>maxtableindex)
            tabindex2=maxtableindex;
        //m_volumeEnvelopes->tableMutex()->Enter();
        float* env1=m_volumeEnvelopes->tableDataPtr(tabindex1);
        float* env2=m_volumeEnvelopes->tableDataPtr(tabindex2);
        //m_volumeEnvelopes->tableMutex()->Leave();
        double fract=z-(int)z;
        double morphfact1=1.0-fract;
        double morphfact2=fract;
        for (int i=0;i<m_resamplerOutLen;++i)
        {
            double frac=m_volumeEnvelopePosition-(int)m_volumeEnvelopePosition;
            int tablepos1=(int)m_volumeEnvelopePosition;
            //if (tablepos1>lenenvtable-1) m_volumeEnvelopeErrors++;
            int tablepos2=tablepos1+1;
            if (tablepos2>lenenvtable-1)
                tablepos2=lenenvtable-1;
            double tablegain1a=env1[tablepos1];
            double tablegain1b=env1[tablepos2];
            double tablegain1delta=tablegain1b-tablegain1a;
            double tablegain1interpolated=tablegain1a+tablegain1delta*frac;
            double tablegain2a=env2[tablepos1];
            double tablegain2b=env2[tablepos2];
            double tablegain2delta=tablegain2b-tablegain2a;
            double tablegain2interpolated=tablegain2a+tablegain2delta*frac;
            double interp=tablegain1interpolated*morphfact1+tablegain2interpolated*morphfact2;
            for (int j=0;j<src_chans;++j)
                temp[i*src_chans+j]*=interp;
            m_volumeEnvelopePosition+=envtablestep;
            if (m_volumeEnvelopePosition>=lenenvtable)
                m_volumeEnvelopePosition=0.0;
        }
        return;
    }
    
    
}
#endif
void FragmentVoice::initVoiceParams()
{
    
    m_voiceIsPlaying=true;
    m_voiceLengthCounter=m_fragmentLengthSamples-1;
    m_playPosInfoIndex=0;
    m_volumeEnvelopePosition=0.0;
    m_pitchEnvelopePosition=0.0;
    m_indexToResampledOut=m_resamplerOutLen;
    resetBuffers(m_resamplerOutLen,m_numOutChans);
    //m_resamplerOutLen=qBound((uint)1,m_resamplerOutLen,(uint)(m_srcOutBuf.size()*2+8));
    int startFrame=m_graintoplay.Audiosource->mediaStart()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    int endFrame=m_graintoplay.Audiosource->mediaEnd()*m_graintoplay.Audiosource->processedAudioLengthFrames();
    m_audioSourcePlayPos=startFrame+m_graintoplay.PosInSource*(endFrame-startFrame); //(double)m_graintoplay.PosInSource*(double)m_graintoplay.Audiosource->lengthFrames();
    //qDebug() << m_ID << "inited to source pos"<<m_audioSourcePlayPos;
    if (m_graintoplay.PosInSource>1.0 || m_graintoplay.PosInSource<0.0)
    {
        //qDebug() << "corrupt grain offset"<<m_graintoplay.PosInSource;
        m_graintoplay.PosInSource=0.0;
    }
    
    
    double infilesrcCompens=(double)m_samplerate/(double)m_graintoplay.Audiosource->origSampleRate();
    m_srcfact=(1.0/infilesrcCompens)*(pow(2.0,m_graintoplay.Transpose/12.0));
    
    //fillResampledBuffer();
}

//#define USE_LEGACY_STEREO_VOICE

void FragmentVoice::processAudio(int frames)
{
#ifdef BENCH_VOICE_PERF
    XenElapsedTimer bench;
    bench.start();
#endif
    if (m_VoicePlayState==0)
    {
        //qDebug() << "FragmentVoice::processAudio : called when voice play state free (should not happen)"<<m_ID<<m_VoicePlayState;
        return;
    }
    if (m_graintoplay.Audiosource==0)
    {
        //qDebug() << "FragmentVoice::processAudio : called with no audio source assigned!";
        return;
    }
    
    
    m_fragmentLengthSamples=(double)m_graintoplay.Length*m_samplerate;
    
    m_outputPosition=std::round((double)m_graintoplay.Timepos*m_samplerate);
    m_resamplerOutLen=frames;
    int source_nch=m_graintoplay.Audiosource->processedAudioNumChans();
    if (source_nch>m_last_source_chancount)
    {
        m_last_source_chancount=source_nch;
        if (m_srcOutBuf.size()<m_last_source_chancount*(m_resamplerOutLen+m_pitchEnvelopeGranularity))
            m_srcOutBuf.resize((m_pitchEnvelopeGranularity+m_resamplerOutLen)*m_last_source_chancount);
    }
    double normalizedPanX=qBound(0.0,(m_graintoplay.PanX+1.0)*0.5,1.0);
    double normalizedPanY=qBound(0.0,(m_graintoplay.PanY+1.0)*0.5,1.0);
    double panningGainLeft=1.0-normalizedPanX;
    double panningGainRight=normalizedPanX;
    double panningGainFront=1.0-normalizedPanY;
    double panningGainBack=normalizedPanY;
    if (m_panModeMono==PanMode_SquareRoot || m_panModeStereo==PanMode_SquareRoot)
    {
        panningGainLeft=sqrt(1.0-normalizedPanX);
        panningGainRight=sqrt(normalizedPanX);
        panningGainFront=sqrt(1.0-normalizedPanY);
        panningGainBack=sqrt(normalizedPanY);
    } else if (m_panModeMono==PanMode_WeirdCosine || m_panModeStereo==PanMode_WeirdCosine)
    {
        panningGainRight=(m_graintoplay.PanX*0.5+0.5)*(1.0-0.292*cos(c_pi*m_graintoplay.PanX*0.5));
        panningGainLeft=(-m_graintoplay.PanX*0.5+0.5)*(1.0-0.292*cos(c_pi*-m_graintoplay.PanX*0.5));
    }
    
    
    int out_chans_to_use=m_numOutChans;
    // panning is now supported for only 4 output channels
    if (m_panning_enabled==true && out_chans_to_use>4)
        out_chans_to_use=4;
    if (m_pannedoutputsgains.size()<out_chans_to_use*source_nch)
        m_pannedoutputsgains.resize(out_chans_to_use*source_nch);
    if (m_panning_enabled==true)
    {
        if (m_per_fragment_pan==false)
        {
            for (int i=0;i<m_pannedoutputsgains.size();++i)
                m_pannedoutputsgains[i]=0.0;
            for (int i=0;i<out_chans_to_use;++i)
            {
                for (int j=0;j<source_nch;++j)
                {
                    double temp_pan_x=qBound(0.0,(1.0+m_graintoplay.PanPositions[j*2+0])*0.5,1.0);
                    double temp_pan_y=qBound(0.0,(1.0+m_graintoplay.PanPositions[j*2+1])*0.5,1.0);
                    double temp_out_gain;
                    if (i==0)
                        temp_out_gain=sqrt(1.0-temp_pan_x)*sqrt(1.0-temp_pan_y);
                    if (i==1)
                        temp_out_gain=sqrt(temp_pan_x)*sqrt(1.0-temp_pan_y);
                    if (i==2)
                        temp_out_gain=sqrt(1.0-temp_pan_x)*sqrt(temp_pan_y);
                    if (i==3)
                        temp_out_gain=sqrt(temp_pan_x)*sqrt(temp_pan_y);
                    m_pannedoutputsgains[i*source_nch+j]=temp_out_gain;
                }
            }
        }
    }
    
    double cos_coef = cos((c_pi/6)*m_graintoplay.PanX);
    double sin_coef = sin((c_pi/6)*m_graintoplay.PanX);
    double grainGain=0.0;
    m_resamplerOutLen=frames;
    for (int i=0;i<frames*m_numOutChans;i++)
    {
        m_audbuffer[i]=0.0f;
    }
    if (m_per_fragment_pan==true)
    {
        m_graintoplay.PanPositions[0]=0.0;
        m_graintoplay.PanPositions[1]=1.0;
        m_graintoplay.PanEndPositions[0]=0.0;
        m_graintoplay.PanEndPositions[1]=-1.0;
    }
    for (int i=0;i<frames;i++)
    {
        int pos=i+m_streamFramepos;
        if (pos==m_outputPosition)
        {
            //qDebug() << "voice is at start position"<<m_ID<< m_streamFramepos+i<<m_outputPosition;
            if (m_VoicePlayState==1)
            {
                //qDebug() << "initing voice params...";
                initVoiceParams();
            } //else Logger::wr"inconsistent state:voice at playpos start but voice not active!"<<m_VoicePlayState;
            
        }
        //if (m_streamFramepos+i==m_fragmentLengthSamples+m_outputPosition)
        if (m_voiceLengthCounter==0)
            //if (m_voiceLengthCounter==0 && m_VoicePlayState==1)
        {
            //qDebug() << "voice"<<m_ID<<"ended";
            m_voiceLengthCounter=-1;
            //g_voiceStatemutex.lock();
            m_VoicePlayState=2;
            m_voiceIsPlaying=false;
            //m_graintoplay.Audiosource->m_lastFragmentParams.voice=-1;
            //g_voiceStatemutex.unlock();
            //break;
        }
        
        if (m_VoicePlayState==1 && m_voiceLengthCounter>0)
            //if (m_VoicePlayState==1 && m_voiceLengthCounter>0)
            //if (m_streamFramepos+i>=m_outputPosition && m_streamFramepos+i<m_fragmentLengthSamples+m_outputPosition)
        {
            m_voiceLengthCounter--;
            if (m_indexToResampledOut==m_resamplerOutLen)
            {
                fillResampledBuffer(0,0);
                m_indexToResampledOut=0;
            }
            grainGain=m_graintoplay.Volume;
            // For legacy stereo playback
#ifdef USE_LEGACY_STEREO_VOICE
            if (out_chans_to_use==2)
            {
                if (source_nch==1)
                {
                    m_audbuffer[i*2+0]=m_srcOutBuf[m_indexToResampledOut]*panningGainLeft;
                    m_audbuffer[i*2+1]=m_srcOutBuf[m_indexToResampledOut]*panningGainRight;
                }
                if (source_nch>1)
                {
                    if (source_nch==2 && m_panModeStereo==PanMode_Rotate)
                    {
                        double in_left=m_srcOutBuf[m_indexToResampledOut*2+0];
                        double in_right=m_srcOutBuf[m_indexToResampledOut*2+1];
                        m_audbuffer[i*2+0] = in_left * cos_coef - in_right * sin_coef;
                        m_audbuffer[i*2+1] = in_left * sin_coef + in_right * cos_coef;
                    }
                    if (source_nch==2 && m_panModeStereo!=PanMode_Rotate)
                    {
                        m_audbuffer[i*2+0] = m_srcOutBuf[m_indexToResampledOut*2+0]*panningGainLeft;
                        m_audbuffer[i*2+1] = m_srcOutBuf[m_indexToResampledOut*2+1]*panningGainRight;
                    }
                }
                ++m_indexToResampledOut;
            }
#endif
            // for new surround playback
            if (out_chans_to_use>=2)
            {
                if (m_panning_enabled==true)
                {
                    if (m_per_fragment_pan==false)
                    {
                        for (int j=0;j<source_nch;++j)
                        {
                            for (int k=0;k<out_chans_to_use;++k)
                            {
                                double temp_out_gain=m_pannedoutputsgains[k*source_nch+j];
                                int indextoresbuf=m_indexToResampledOut*source_nch+j;
                                double outsample;
                                //if (indextoresbuf<m_srcOutBuf.size())
                                int outbufindex=i*m_numOutChans+k;
                                //if (outbufindex<outbufsize)
                                outsample=temp_out_gain*m_srcOutBuf[indextoresbuf];
                                m_audbuffer[outbufindex]+=outsample;
                            }
                            for (int k=out_chans_to_use;k<m_numOutChans;++k)
                            {
                                m_audbuffer[i*m_numOutChans+k]=0.0;
                            }
                        }
                    } else // fragment has moving pan path
                    {
                        for (int j=0;j<source_nch;++j)
                        {
                            double pan_start_x=qBound(0.0,(1.0+m_graintoplay.PanPositions[j*2+0])*0.5,1.0);
                            double pan_start_y=qBound(0.0,(1.0+m_graintoplay.PanPositions[j*2+1])*0.5,1.0);
                            double pan_end_x=qBound(0.0,(1.0+m_graintoplay.PanEndPositions[j*2+0])*0.5,1.0);
                            double pan_end_y=qBound(0.0,(1.0+m_graintoplay.PanEndPositions[j*2+1])*0.5,1.0);
                            double pan_x_delta=pan_end_x-pan_start_x;
                            double pan_y_delta=pan_end_y-pan_start_y;
                            int playpos=m_fragmentLengthSamples-m_voiceLengthCounter;
                            double interp_x=pan_start_x+(pan_x_delta/m_fragmentLengthSamples)*playpos;
                            double interp_y=pan_start_y+(pan_y_delta/m_fragmentLengthSamples)*playpos;
                            for (int k=0;k<out_chans_to_use;++k)
                            {
                                double temp_out_gain=0.0;
                                if (k==0)
                                    temp_out_gain=sqrt(1.0-interp_x)*sqrt(1.0-interp_y);
                                if (k==1)
                                    temp_out_gain=sqrt(interp_x)*sqrt(1.0-interp_y);
                                if (k==2)
                                    temp_out_gain=sqrt(1.0-interp_x)*sqrt(interp_y);
                                if (k==3)
                                    temp_out_gain=sqrt(interp_x)*sqrt(interp_y);
                                double outsample=temp_out_gain*m_srcOutBuf[m_indexToResampledOut*source_nch+j];
                                m_audbuffer[i*out_chans_to_use+k]+=outsample*temp_out_gain;
                            }
                        }
                        
                    }
                } else
                {
                    for (int j=0;j<out_chans_to_use;++j)
                    {
                        double outsample=0.0;
                        int indextoresbuf=m_indexToResampledOut*source_nch+j;
                        if (j<source_nch) // && indextoresbuf<m_srcOutBuf.size())
                        {
                            outsample=m_srcOutBuf[indextoresbuf];
                        }
                        m_audbuffer[i*out_chans_to_use+j]=outsample;
                    }
                }
                ++m_indexToResampledOut;
            }
            
        }
        
        
    }
    //if (m_voiceLengthCounter<0)
    //    qDebug() << "voice len counter under zero"<<m_voiceLengthCounter;
#ifdef BENCH_VOICE_PERF
    m_elapsedCounter++;
    m_elapsedAccum+=bench.elapsed();
    if (m_elapsedCounter>1000)
    {
        //qDebug() << "last 100 voice processAudio calls took avs"<<m_elapsedAccum/m_elapsedCounter<<"ms";
        m_elapsedCounter=0;
        m_elapsedAccum=0.0;
    }
#endif
}

void FragmentStream::setSourcePosQuantizedToWaveCycles(bool b)
{
    m_srcPosQuantizedToWavecycles=b;
}

void FragmentStream::setPitchEnvelopeGranularity(int samples)
{
    std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_pitchEnvelopeGranularity=qBound(16,samples,1024);
    //qDebug() << "FragmentStream : setPitchEnvelopegranularity to"<<m_pitchEnvelopeGranularity;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->m_pitchEnvelopeGranularity=m_pitchEnvelopeGranularity;
    }
}

void FragmentStream::setTextureLoopStart(double t)
{
    return;
    m_textureLoopStart=qBound(0.0,t,m_textureLoopEnd-0.05);
}

void FragmentStream::setTextureLoopEnd(double t)
{
    return;
    m_textureLoopEnd=qBound(m_textureLoopStart+0.05,t,m_cloudDur);
}

void FragmentStream::setPitchEnvelopeAsPlayrate(bool b)
{
    /*
    std::lock_guard<std::recursive_mutex> locker(m_wdlmutex);
    std::vector<double> v;
    for (int i=0;i<5;i++)
        v.push_back(i*0.25);// << 0.00 << 0.25 << 0.5 << 0.75 << 1.0;
    for (uint i=0;i<m_pitchEnvelopes->numEnvelopes();i++)
    {
        HGParam* par=m_pitchEnvelopes->envelopeParameter(i);
        par->setDirty(true);
        if (b==false)
        {
            par->setAbsoluteValueLimits(-24.0,24.0);
            par->setDefaultValue(0.5);
            par->setGridDivNormalized(1.0/12);
            par->setStaticValueShapingKind(Shaping_Linear);
        } else
        {
            par->setAbsoluteValueLimits(-16.0,16.0);
            par->setDefaultValue(0.75);
            par->setValueGrid(v,true);
            par->setStaticValueShapingKind(Shaping_BipolarPlaySpeed);
        }
    }
    m_pitchEnvelopeIsPlayrate=b;
    for (uint i=0;i<m_Voices.size();i++)
        m_Voices.at(i)->m_pitchEnvelopeIsPlayrate=b;
    */
}


FragmentStream::FragmentStream(minihourglass_node* mhg) :
m_unidist(0.0,1.0), m_hgnode(mhg)
{
    m_pan_history_length=256;
    m_pan_history_counter=0;
    
    m_numOutChans=2;
    m_recordingActive=false;
    m_pitchEnvelopeIsPlayrate=false;
    
    m_offlineProcessingActive=false;
    m_shouldXFadeVoiceBuffers=false;
    
    m_lastInputPosSeconds=0.0;
    m_fragmentSyncMode=FSM_Sequential;
    
    m_fragmentStartPositionsCounter=0;
    m_voiceStarvationCounter=0;
    
    m_playposInfoVoiceIndex=-1;
    
    m_bufferSizeIsHardwareBufferSize=true;
    m_cloudDur=10.0;
    m_textureLoopStart=0.0;
    m_textureLoopEnd=m_cloudDur;
    m_pitchEnvelopeGranularity=64;
    m_srcPosQuantizedToWavecycles=false;
    
    m_panSpreadMode=0;
    m_muteAtEnd=true;
    m_textureDurCacheRefreshCnt=99;
    m_cachedTextureDuration=0.0;
    m_timeBehavior=TimeBehav_Classic;
    m_bpm=60.0;
    m_fragmentRate=1.0;
    m_previousFragmentRate=-1.0;
    
    
    m_currentGrainParams.Audiosource=0;
    m_currentGrainParams.Length=0.1;
    m_currentGrainParams.PosInSource=0.0;
    m_currentGrainParams.Transpose=0.0;
    //m_currentGrainParams.
    m_mtVoicesEnabled=true;
    m_wasSeeked=true;
    
    m_ssm=std::make_unique<SoundSourcesManager>(m_hgnode);
    
    m_outputBufSize=512;
    m_outputBuffer.resize(m_outputBufSize*m_numOutChans);
    m_loopTexture=false;
    
    m_setTextureDurToSoundFileDur=true;
    
    m_GrainFadePercent=0.1;
    m_delayIncounter=0;
    m_delayOutCounter=4096;
    m_GrainLen=0.1;
    m_resamplerQ=2;
    m_GrainCounter=0;
    m_EnvelopeListDirty=true;
    
    
    m_parameters.emplace_back(std::make_unique<HGParam>("Source position", "SRC_POS", 0.0, 0.0, 1.0)); // 0
	m_parameters.emplace_back(std::make_unique<HGParam>("Fragment rate", "DENSITY", 32.0, 2.0, 500.0)); // 1
	m_parameters.emplace_back(std::make_unique<HGParam>("Fragment rate randomization", "GRPOS_RND", 0.0, 0.0, 1.0)); // 2
	m_parameters.emplace_back(std::make_unique<HGParam>("Fragment length", "GR_LENGTH", 0.070, 0.005, 1.0)); // 3
	m_parameters.emplace_back(std::make_unique<HGParam>("Transposition centre", "TRANSP_MEAN", 0.0, -24.0, 24.0)); // 4
	m_parameters.emplace_back(std::make_unique<HGParam>("Transposition spread", "TRANSP_SPREAD", 0.0, 0.0, 24.0)); // 5
	m_parameters.emplace_back(std::make_unique<HGParam>("X Panning centre", "PAN_MEAN", 0.0, -1.0, 1.0)); // 6
	m_parameters.emplace_back(std::make_unique<HGParam>("X Panning spread", "PAN_SPREAD", 0.9, 0.0, 1.0)); // 7
	m_parameters.emplace_back(std::make_unique<HGParam>("Y Panning centre", "PAN_MEAN_Y", 0.0, -1.0, 1.0)); // 8
	m_parameters.emplace_back(std::make_unique<HGParam>("Y Panning spread", "PAN_SPREAD_Y", 0.0, 0.0, 1.0)); // 9
	m_parameters.emplace_back(std::make_unique<HGParam>("Fragment volume", "GRAIN_AMP", -1.0, -48.0, 12.0)); // 10
	m_parameters.emplace_back(std::make_unique<HGParam>("Playback speed", "PLAYSPEED", 1.0, 0.01, 10.0)); // 11
	m_parameters.emplace_back(std::make_unique<HGParam>("Envelope morph", "MORPHENVE", 0.0, 0.0, 1.0)); // 12
	m_parameters.emplace_back(std::make_unique<HGParam>("Pitch Envelope morph", "MORPHPITCHENVE", 0.0, 0.0, 1.0)); // 13
	m_parameters.emplace_back(std::make_unique<HGParam>("Texture position", "TEXPOS", 0.0, 0.0, 1.0)); // 14
	m_parameters.emplace_back(std::make_unique<HGParam>("Src pos randomization", "SRCPOSRAND", 0.0, 0.0, 1.0)); // 15
	m_parameters.emplace_back(std::make_unique<HGParam>("Sound source select", "SRCSELECT", 0.05, 0.0, 1.0)); // 16
    
    m_outputPosFramesMapped=0;
    
    m_grainScriptProgramEnabled=false;
    m_grainScriptHasError=false;
    m_scripthelper_x0=0.0;
    m_scripthelper_x1=0.0;
    m_Density=2.0;
    m_GrainLen=0.05;
    m_posinsrc=0.0;
    allocate_voices(512);
    setPitchEnvelopeAsPlayrate(false);
    m_panModeMono=PanMode_SquareRoot;
    m_panModeStereo=PanMode_Rotate;
    m_nextFragmentTime=0.0;
    m_outputPosFrames=0;
    m_samplerate=44100;
    
    m_streamVolume=0.5;
    m_currentVoice=0;
    
    m_inputStart=0.0;
    m_inputEnd=1.0;
    m_inputPosSeconds=0.0;
#ifdef USE_QT_CONCURRENT
    //TODO: Maybe need to "wake up" the Qt thread pool somehow here...
#endif
    calculateEnvelopeTables(0);
    calculateEnvelopeTables(1);
    //qDebug() << "waking threadpool threads took"<<elap.elapsed();
    
}

FragmentStream::~FragmentStream()
{
    
    int numv=m_Voices.size();
    int voiceVolumeErrors=0;
    for (int i=0;i<numv;i++)
    {
        voiceVolumeErrors+=m_Voices[i]->m_volumeEnvelopeErrors;
        delete m_Voices[i];
    }
    //if (voiceVolumeErrors>0)
    //    qDebug() << voiceVolumeErrors << "errors detected in voice volume envelopes";
}

void FragmentStream::allocate_voices(int num_voices)
{
    //qDebug() << "mem use with old voice count"<<(double)process_memory_usage()/1024/1024<<"MB";
    std::vector<FragmentVoice*> temp;
    for (int i=0;i<num_voices;i++)
    {
        FragmentVoice *newvoice=new FragmentVoice;
        newvoice->m_ID=i;
        newvoice->m_streamFramepos=0;
        newvoice->m_graintoplay.Audiosource=0;
        newvoice->m_panModeMono=PanMode_SquareRoot;
        newvoice->m_panModeStereo=PanMode_Rotate;
        newvoice->m_volumeEnvelopes=&m_volumeEnvelopes;
        newvoice->m_pitchEnvelopes=&m_pitchEnvelopes;
        temp.push_back(newvoice);
    }
    m_Voices = temp;
    
    //swap_under_mutex(m_Voices,temp,m_wdlmutex);
    //qDebug() << "mem use with new voice count"<<(double)process_memory_usage()/1024/1024<<"MB";
}

void FragmentStream::setSampleRate(int sr) noexcept
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_samplerate=qBound(11025,sr,192000);
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices.at(i)->m_samplerate=m_samplerate;
    }
    
}

void FragmentStream::setNumOutChans(int count)
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
	m_numOutChans=qBound(2,count,64);
    setOutputBufferLength(m_outputBufSize);
}

void FragmentStream::setOutputBufferLength(int len)
{
    //qDebug() << "FragmentStream num out chans set to"<<m_numOutChans;
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_outputBufSize=qBound(16,len,65536);
    m_outputBuffer.resize(m_outputBufSize*m_numOutChans);
    
    flushVoices(true);
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->resetBuffers(m_outputBufSize,m_numOutChans);
    }
    //qDebug() << "changed fragmentstream buffer size"<<m_outputBufSize;
}

int64_t FragmentStream::playbackPositionFrames(bool mapped) const noexcept
{
    //if (mapped==false || m_parameters[14]->envelopeEnabled()==false)
    //    return m_outputPosFrames;
    return m_outputPosFramesMapped;
}

void FragmentStream::setMuteAtEnd(bool b)
{
    m_muteAtEnd=b;
}

HGParam* FragmentStream::GetEnvFromName(String name) const noexcept
{
    for (int i=0;i<m_parameters.size();i++)
    {
        if (name==m_parameters[i]->m_name)
            return m_parameters[i].get();
    }
    return 0;
}

void FragmentStream::setTextureDuration(double dur)
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_cloudDur=qBound(0.1,dur,3600.0);
    if (m_textureLoopEnd>m_cloudDur)
        m_textureLoopEnd=m_cloudDur;
    m_textureLoopStart=0.0; //qBound(0.0,m_textureLoopStart,m_textureLoopEnd-0.01);
    m_textureLoopEnd=m_cloudDur;
    
}

int FragmentStream::resamplerQuality() const noexcept
{
    return m_resamplerQ;
}

void FragmentStream::setResamplerQuality(int nq) noexcept
{
    if (nq!=m_resamplerQ)
    {
		std::lock_guard<std::recursive_mutex> locker(m_mutex);
        m_resamplerQ=nq;
        for (size_t i=0;i<m_Voices.size();i++)
        {
            m_Voices[i]->setResamplerQuality(nq);
        }
        //qDebug() << "changed resampler quality to"<<nq;
    }
}

void FragmentStream::resetAllEnvelopes(bool setstaticvaluetodefault)
{
    /*
    WDL_MutexLock locker(m_wdlmutex);
    for (int i=0;i<m_parameters.size();i++)
    {
        m_parameters[i]->envelope()->resetEnvelopeUsingDefaultState();
        m_parameters[i]->setEnvelopeEnabled(false);
        if (m_parameters[i]->m_isSourcePositionParameter==true)
        {
            m_parameters[i]->setEnvelopeEnabled(true);
        }
        if (setstaticvaluetodefault==true)
            m_parameters.at(i)->setNormalizedValue(m_parameters.at(i)->defaultValue());
    }
    m_ssm->m_morphParam->envelope()->resetEnvelopeUsingDefaultState();
    m_ssm->m_morphParam->setEnvelopeEnabled(false);
    if (setstaticvaluetodefault==true)
        m_ssm->m_morphParam->setNormalizedValue(m_ssm->m_morphParam->defaultValue());
     */
}

int FragmentStream::GetNumUsedVoices() const noexcept
{
    int voices=0;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        if (m_Voices[i]->m_VoicePlayState==1)
            voices++;
    }
    return voices;
}

void FragmentStream::SeekTo_s(double pos,bool flushvoices)
{
    //qDebug() << "seeking to"<<pos<<"flush voices"<<flushvoices;
    
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
	m_ssm->updateSource();
	m_voiceStarvationCounter=0;
    m_outputPosFrames=pos*m_samplerate;
    m_inputPosSeconds=calculateInputDrivenTimePos(pos);
    m_nextFragmentTime=pos;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        //m_Voices[i]->m_streamFramepos=m_outputPosFrames;
        //m_Voices[i]->m_graintoplay.Timepos=pos;
    }
    if (flushvoices)
        flushVoices();
        m_wasSeeked=true;
}

double FragmentStream::calculateInputDrivenTimePos(double input)
{
    if (m_timeBehavior==TimeBehav_Classic)
        return input;
	return input;
	/*
    double timeaccum=0.0;
    CAudioSource* as=m_ssm->focusedSound();
    if (as)
    {
        quint64 cnt=0;
        quint64 sndlen=as->processedAudioLengthFrames();
        double sndlenseconds=(double)sndlen/as->origSampleRate();
        HGParam* playpar=m_parameters.at(11);
        while (timeaccum<input)
        {
            double timetoenv=1.0/sndlenseconds*timeaccum;
            double speednorm=playpar->getEnvelopeOrStaticValue(timetoenv);
            double playspeed=playpar->scaledValueFromNormalized(speednorm);
            
            timeaccum+=((double)m_outputBufSize/as->origSampleRate())*playspeed; //*srcompens;
            cnt++;
            if (cnt>1000000)
            {
                qDebug() << "FragmentStream::calculateInputDrivenTimePos : sanity failed";
                break;
            }
        }
        //qDebug() << "calculated input driven time position"<<input<<timeaccum;
        return timeaccum;
    }
    return input;
     */
}

void FragmentStream::calculateEnvelopeTables(int which)
{
#ifdef OLDFRAGMENTENVELOPECODE
    //WDL_MutexLock locker(m_wdlmutex);
    
    int tablesize=0;
    if (which==0)
        tablesize=g_fragmentVolEnvInfos.at(0)->tablesize;
    else if (which==1)
        tablesize=g_fragmentPitchEnvInfos.at(0)->tablesize;
    int numtablestoupdate=0;
    if (which==0)
        numtablestoupdate=g_fragmentVolEnvInfos.size();
    else if (which==1)
        numtablestoupdate=g_fragmentPitchEnvInfos.size();
    std::vector<float> temp; temp.resize(tablesize);
    for (int i=0;i<numtablestoupdate;i++)
    {
        HGParam* par=0;
        float* buf=temp.data();
        if (which==0)
        {
            //qDebug() << __FUNCTION__ << "updates volume tables";
            //buf=g_fragmentVolEnvInfos.at(i)->table.data();
            //g_fragmentVolEnvInfos.at(i)->table.back()=0.5;
            //qFill(g_fragmentVolEnvInfos.at(i)->table,0.0);
            par=g_fragmentVolEnvInfos.at(i)->param;;
        }
        else if (which==1)
        {
            //qDebug() << __FUNCTION__ << "updates pitch tables";
            //buf=g_fragmentPitchEnvInfos.at(i)->table.data();
            par=g_fragmentPitchEnvInfos.at(i)->param;
            /*
             if (m_pitchEnvelopeIsPlayrate==true)
             qFill(g_fragmentPitchEnvInfos.at(i)->table,0.75);
             else qFill(g_fragmentPitchEnvInfos.at(i)->table,0.5);
             */
        }
        int envnodeindex=0;
        int bkcounter=0;
        
        int invalidcount=0;
        for (int j=0;j<tablesize;j++)
        {
            XenEnvNode* node1=par->envelope()->getNodeAtIndex(envnodeindex);
            
            double v1=node1->Value;
            double v2=v1;
            double t1=node1->Time;
            double t2=t1+0.0001;
            if (envnodeindex+1<par->envelope()->GetNumNodes())
            {
                XenEnvNode* node2=0;
                node2=par->envelope()->getNodeAtIndex(envnodeindex+1);
                v2=node2->Value;
                t2=node2->Time;
            }
            double valuedelta=v2-v1;
            double timedeltanormalized=(t2-t1);
            double timedeltasamples=(double)timedeltanormalized*tablesize;
            double shaped=node1->getShapedValue(1.0/timedeltasamples*bkcounter);
            double interpolated=v1+valuedelta*shaped;
            if (qIsNaN(interpolated) || qIsInf(interpolated))
            {
                interpolated=0.0;
                invalidcount++;
            }
            buf[j]=qBound(0.0,interpolated,1.0);
            bkcounter++;
            if (bkcounter>=timedeltasamples)
            {
                bkcounter=0;
                envnodeindex++;
            }
        }
        if (which==0)
            buf[tablesize-1]=0.0;
        if (invalidcount>0)
            qDebug() << __FUNCTION__ << invalidcount << "invalid values were calculated for table"<<which<<i;
        par->setDirty(false);
        QElapsedTimer timer;
        timer.start();
        g_envTablesMutex->Enter();
        if (which==0)
        {
            g_fragmentVolEnvInfos.at(i)->table.swap(temp);
            //for (int j=0;j<tablesize;j++)
            //    g_fragmentVolEnvInfos.at(i)->table[j]=buf[j];
        }
        if (which==1)
        {
            g_fragmentPitchEnvInfos.at(i)->table.swap(temp);
            //for (int j=0;j<tablesize;j++)
            //    g_fragmentPitchEnvInfos.at(i)->table[j]=buf[j];
        }
        g_envTablesMutex->Leave();
        //qDebug() << "copying table"<<which<<i<<"took"<<(double)timer.nsecsElapsed()/1000000<<"ms";
    }
#endif
}

int FragmentStream::GetFreeVoice(int buflen)
{
    //for (int i=0;i<m_Voices.size();i++)
    //    m_Voices[i]->m_gatherPlayPosInfo=false;
    
    for (int i=m_Voices.size()-1;i>=0;--i)
    {
        if (m_Voices[i]->m_VoicePlayState==0)
        {
            /*
             if (m_playposInfoVoiceIndex==-1)
             {
             //qDebug() << "voice"<<i<<"gathers playpos data";
             m_Voices[i]->m_gatherPlayPosInfo=true;
             m_Voices[i]->m_playPosInfo=(float*)m_fragmentPlayPosInfo.constData();
             m_playposInfoVoiceIndex=i;
             }
             */
            //qDebug() << "returning voice"<<i<<"for playback";
            return i;
        }
    }
    return -1;
}

void FragmentStream::flushVoices(bool freeVoices)
{
    //return;
    bool dovoiceFadeOut=true;
    //qDebug() << "flushing voices, voice fadeout"<<dovoiceFadeOut;
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    //double timetoInterp=(1.0/m_cloudDur)*m_nextGrainTime;
    //HGParam *pEnv=GetEnvFromName("SRC_POS");
    //double posinsrc=pEnv->getEnvelopeOrStaticValue(timetoInterp);
    m_posinsrc=0.0;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->m_graintoplay.Audiosource=0;
        
        m_Voices[i]->m_VoicePlayState=0;
        m_Voices[i]->m_numOutChans=m_numOutChans;
        if (m_Voices[i]->m_audbuffer.size()<m_numOutChans*m_outputBufSize)
            m_Voices[i]->m_audbuffer.resize(m_numOutChans*m_outputBufSize);
        //m_Voices[i]->m_streamFramepos=m_outputPosFrames;
        //m_Voices[i]->m_graintoplay.PosInSource=0.0;
        //m_Voices[i]->m_voiceLengthCounter=0;
        //m_Voices[i]->resetBuffers(m_outputBufSize);
        if (dovoiceFadeOut)
        {
            for (int j=0;j<m_outputBufSize;j++)
            {
                double gain=1.0-(1.0/m_outputBufSize)*j;
                for (int k=0;k<m_numOutChans;++k)
                    m_Voices[i]->m_audbuffer[j*m_numOutChans+k]*=gain;
            }
        }
    }
    m_currentVoice=0;
    
}

void FragmentStream::setMTVoicesEnabled(bool b)
{
    m_mtVoicesEnabled=b;
}

void FragmentStream::generateNextFragmentParams(int voiceIndex)
{
    
    HGParam *pEnv_SourcePos=m_parameters[0].get();
    HGParam *pEnv_GrainTimeRand=m_parameters[2].get();
    HGParam *pEnv_Length=m_parameters[3].get();
    HGParam *pEnv_Transpose=m_parameters[4].get();
    HGParam *pEnv_TransposeSpread=m_parameters[5].get();
    HGParam *pEnv_PanMean=m_parameters[6].get();
    HGParam *pEnv_PanSpread=m_parameters[7].get();
	HGParam *pEnv_PanMean_Y = m_parameters[8].get();
	HGParam *pEnv_PanSpread_Y = m_parameters[9].get();
	HGParam *pEnv_GrainVolume=m_parameters[10].get();
    //HGParam *pEnv_Meta_A=m_parameters[9];
    //HGParam *pEnv_Meta_B=m_parameters[10];
    HGParam* pEnv_EnvMorph=m_parameters[12].get();
    HGParam* pEnv_PitchEnvMorph=m_parameters[13].get();
    //HGParam* pEnv_Texturepos=m_parameters[14];
    HGParam* pEnv_SrcPosRand=m_parameters[15].get();
    HGParam* pEnv_SrcSelect=m_parameters[16].get();
    double normzVal;
    double timetoInterp=0.0;
    double soundlenseconds=0.0001;
    if (m_timeBehavior==TimeBehav_Classic)
    {
        //if (pEnv_Texturepos->envelopeEnabled()==true)
        //{
        //    double originalTexPos=(1.0/m_cloudDur)*m_nextFragmentTime;
        //    timetoInterp=pEnv_Texturepos->getEnvelopeOrStaticValue(originalTexPos);
        //    m_outputPosFramesMapped=timetoInterp*(m_cloudDur*m_samplerate);
        //} else
        {
            timetoInterp=(1.0/m_cloudDur)*m_nextFragmentTime;
            m_outputPosFramesMapped=m_outputPosFrames;
        }
    }
    else
    {
        
        CAudioSource* as=m_ssm->focusedSound();
        if (as)
        {
            double fullsoundlenseconds=(double)as->processedAudioLengthFrames()/as->origSampleRate();
            soundlenseconds=(as->mediaEnd()-as->mediaStart())*fullsoundlenseconds;
            timetoInterp=1.0/soundlenseconds*m_inputPosSeconds;
        }
        
    }
    double timeposdelta=m_fragmentRate;
    
    double z;
    if (voiceIndex>=0)
    {
        m_Voices[voiceIndex]->m_voiceLengthCounter=-1;
        int sampleIndex=0;
        if (m_ssm)
        {
            if (m_ssm->numSources()==0)
                m_ssm->updateSource();
            if (m_ssm->numSources()>0)
            {
                
                double source_select=pEnv_SrcSelect->getEnvelopeOrStaticValue(timetoInterp)+0.00001;
                if (m_fragmentSyncMode==FSM_Sequential && source_select<0.01)
                    sampleIndex=m_ssm->getRandomSourceIndex();
                if (m_fragmentSyncMode==FSM_Sequential && source_select>=0.01)
                    sampleIndex=qBound(0,(int)(m_ssm->numSources()*source_select),m_ssm->numSources()-1);
            //if (m_fragmentSyncMode==FSM_Mix)
            //    sampleIndex=m_GrainCounter % m_ssm->numSources();
            }
        }
        double grposrnddepth=pEnv_GrainTimeRand->getEnvelopeOrStaticValue(timetoInterp);
        z=m_unidist(m_mt_rand);
        double nextFragmentTime=m_nextFragmentTime;
        //if (pEnv_GrainTimeRand->m_rateRandomMode==RateRandomization_AddRandomUnipolar && m_fragmentSyncMode==FSM_Sequential)
        //    nextFragmentTime+=(z*(1.0/timeposdelta)*grposrnddepth);
        //if (pEnv_GrainTimeRand->m_rateRandomMode==RateRandomization_AddRandomUnipolar && m_fragmentSyncMode==FSM_Mix && (m_GrainCounter % m_ssm->numSources()==0))
        {
            
            nextFragmentTime+=(z*(1.0/timeposdelta)*grposrnddepth);
            //qDebug() << "advancing next fragment time pos in sync mix mode to"<<nextFragmentTime;
        }
        m_Voices[voiceIndex]->m_graintoplay.volumeEnvMorph=pEnv_EnvMorph->getEnvelopeOrStaticValue(timetoInterp);
        m_Voices[voiceIndex]->m_graintoplay.pitchEnvMorph=pEnv_PitchEnvMorph->getEnvelopeOrStaticValue(timetoInterp);
        m_Voices[voiceIndex]->m_graintoplay.Timepos=nextFragmentTime;
        double posinsrc=0.0;
        if (m_timeBehavior==TimeBehav_Classic)
        {
            CAudioSource* audiosrc=m_ssm->getSource(sampleIndex);
            if (audiosrc)
            {
                posinsrc=pEnv_SourcePos->getEnvelopeOrStaticValue(timetoInterp);
				z = m_unidist(m_mt_rand);
                double pos_rand_amount=pEnv_SrcPosRand->getEnvelopeOrStaticValue(timetoInterp)*0.25;
                posinsrc=posinsrc+(-pos_rand_amount+(pos_rand_amount*2.0*z));
                
                //if (pEnv_SourcePos->envelopeEnabled())
                //    posinsrc=pEnv_SourcePos->scaledValueFromNormalized(posinsrc);
                //else posinsrc=pEnv_SourcePos->scaledValueFromNormalized(posinsrc,pEnv_SourcePos->smootherEnabled());
            }         }
        else
        {
            //double posdelta=
            posinsrc=timetoInterp;
        }
        m_posinsrc=posinsrc;
        double meanTranspose;
        z=m_unidist(m_mt_rand);
        
        normzVal=pEnv_Transpose->getEnvelopeOrStaticValue(timetoInterp);
        meanTranspose=normzVal; // pEnv_Transpose->scaledValueFromNormalized(normzVal);
        
        normzVal=pEnv_TransposeSpread->getEnvelopeOrStaticValue(timetoInterp);
        double trans_position_spread_semis=normzVal; //pEnv_TransposeSpread->scaledValueFromNormalized(normzVal);
        
        normzVal=pEnv_Length->getEnvelopeOrStaticValue(timetoInterp);
        double grainLen=normzVal;
        /*
        if (pEnv_Length->m_lenIsPercentOfRate==false)
            grainLen=pEnv_Length->scaledValueFromNormalized(normzVal);
        else
        {
            double foobar=1.0/m_fragmentRate;
            grainLen=foobar*pEnv_Length->scaledValueFromNormalized(normzVal);
            //qDebug() << "len linked to rate is"<<grainLen<<"seconds";
        }
        */
        
		normzVal = pEnv_GrainVolume->getEnvelopeOrStaticValue(timetoInterp);
		double grainGain = exp(((normzVal))*0.11512925464970228420089957273422);
		
		double panMean = pEnv_PanMean->getEnvelopeOrStaticValue(timetoInterp);
		double panMean_Y = pEnv_PanMean_Y->getEnvelopeOrStaticValue(timetoInterp);
		double panSpread = pEnv_PanSpread->getEnvelopeOrStaticValue(timetoInterp);
		double panSpread_Y = pEnv_PanSpread_Y->getEnvelopeOrStaticValue(timetoInterp);


        if (m_timeBehavior==TimeBehav_Classic)
            m_Voices[voiceIndex]->m_graintoplay.Audiosource=m_ssm->getSource(sampleIndex);
        else
        {
            m_Voices[voiceIndex]->m_graintoplay.Audiosource=m_ssm->focusedSound();
        }
        
        double wcquantizedpos=m_posinsrc;
        
        m_Voices[voiceIndex]->m_graintoplay.PosInSource=qBound(0.0,wcquantizedpos,1.0);
        m_Voices[voiceIndex]->m_graintoplay.Length=grainLen;
        z=m_unidist(m_mt_rand);
        m_Voices[voiceIndex]->m_graintoplay.Transpose=meanTranspose+(-trans_position_spread_semis+(trans_position_spread_semis*2.0*z));
        if (m_panSpreadMode==0)
        {
			int num_src_chans = m_Voices[voiceIndex]->m_graintoplay.Audiosource->processedAudioNumChans();
			
			for (int ch = 0; ch < num_src_chans; ++ch)
			{
				z = m_unidist(m_mt_rand);
				if (m_numOutChans == 2)
				{
					m_Voices[voiceIndex]->m_graintoplay.PanPositions[ch*2+0] = panMean + (-panSpread + (panSpread*2.0*z));
					//z = m_unidist(m_mt_rand);
					//m_Voices[voiceIndex]->m_graintoplay.PanPositions[ch*2+1] = panMean_Y + (-panSpread_Y + (panSpread_Y*2.0*z));	
				}
				if (m_numOutChans == 4)
				{
					double rotspread = 2 * 3.141592654*panSpread;
					double rotpos = 3.141592654*panMean + rotspread*z;
					double xcor = std::cos(rotpos);
					double ycor = std::sin(rotpos);
					m_Voices[voiceIndex]->m_graintoplay.PanPositions[ch * 2 + 0] = xcor;
					m_Voices[voiceIndex]->m_graintoplay.PanPositions[ch * 2 + 1] = ycor;
				}
				
			}
			
			
			//m_Voices[voiceIndex]->m_graintoplay.PanX=panMean +(-panSpread+(panSpread*2.0*z));
            
            //m_Voices[voiceIndex]->m_graintoplay.PanY=panMean +(-panSpread+(panSpread*2.0*z));
            
        } else if (m_panSpreadMode==1)
        {
            z=m_unidist(m_mt_rand);
            if (z<0.5)
                m_Voices[voiceIndex]->m_graintoplay.PanX=panMean-panSpread;
            else
                m_Voices[voiceIndex]->m_graintoplay.PanX=panMean+panSpread;
        }
        else
        {
            //panpos_t pantemp;
            //if (m_mono_panpath.m_positions.size()>0)
            //    pantemp=m_mono_panpath.getNext();
            //m_Voices[voiceIndex]->m_graintoplay.PanX=pantemp.m_x0;
            //m_Voices[voiceIndex]->m_graintoplay.PanY=pantemp.m_y0;
            //m_Voices[voiceIndex]->m_graintoplay.PanX=*m_eel.m_eelvar_pan;
            //m_Voices[voiceIndex]->m_graintoplay.PanY=*m_eel.m_eelvar_pany;
            //const double pan_x_positions[4]={-1.0,1.0,1.0,-1.0};
            //const double pan_y_positions[4]={-1.0,-1.0,1.0,1.0};
            //m_Voices[voiceIndex]->m_graintoplay.PanX=sin(2.0*3.141592/32*m_GrainCounter);
            //m_Voices[voiceIndex]->m_graintoplay.PanY=cos(2.0*3.141592/32*m_GrainCounter);
            //m_Voices[voiceIndex]->m_graintoplay.PanX=pan_x_positions[m_GrainCounter % 4];
            //m_Voices[voiceIndex]->m_graintoplay.PanY=pan_y_positions[m_GrainCounter % 4];
            
            if (m_GrainCounter % 2==0)
                m_Voices[voiceIndex]->m_graintoplay.PanX=panMean-panSpread;
            else m_Voices[voiceIndex]->m_graintoplay.PanX=panMean+panSpread;
            
        }
        
        m_GrainLen=m_Voices[voiceIndex]->m_graintoplay.Length;
        m_posinsrc=m_Voices[voiceIndex]->m_graintoplay.PosInSource;
        m_Voices[voiceIndex]->m_graintoplay.Volume=grainGain*m_Voices[voiceIndex]->m_graintoplay.Audiosource->baseVolume();
        m_Voices[voiceIndex]->m_streamFramepos=m_outputPosFrames;
        //g_voiceStatemutex.lock();
        m_Voices[voiceIndex]->m_VoicePlayState=1;
        //qDebug() << "generated voice params for"<<voiceIndex<<m_Voices[voiceIndex]->m_streamFramepos;
        //g_voiceStatemutex.unlock();
        if (m_fragmentSyncMode==FSM_Sequential)
        {
            //if (pEnv_GrainTimeRand->m_rateRandomMode==RateRandomization_AccumRandom)
            //    m_nextFragmentTime+=(1.0/timeposdelta)+(z*(1.0/timeposdelta)*grposrnddepth);
            //else
                m_nextFragmentTime+=(1.0/timeposdelta);
        }
        
        if (m_Voices.at(voiceIndex)->m_graintoplay.Volume<0.000014)
            
            m_Voices[voiceIndex]->m_VoicePlayState=0;
        if (m_timeBehavior==TimeBehav_InputDriven)
        {
            HGParam* speedpar=m_parameters[11].get();
            double timetoenvel=1.0/soundlenseconds*m_inputPosSeconds;
            double playspeednormalized=speedpar->getEnvelopeOrStaticValue(timetoenvel);
            double playspeed=playspeednormalized;// speedpar->scaledValueFromNormalized(playspeednormalized);
            m_inputPosSeconds+=playspeed*(1.0/m_fragmentRate);
            m_lastInputPosSeconds=wcquantizedpos;
        }
        
    }
    
}

HGParam* FragmentStream::envFromDisplayName(const String &n)
{
    for (int i=0;i<m_parameters.size();i++)
        if (m_parameters.at(i)->m_name==n)
            return m_parameters[i].get();
    return 0;
}

void FragmentStream::initScriptEngine()
{
    
}

void FragmentStream::idleTask(int x)
{
    double s=0.0;
    for (int i=0;i<x;i++)
        s+=sin((double)i);
    m_idleTaskResult=s;
}

void VoicePlayFunction(FragmentVoice *v)
{
    if (v->m_VoicePlayState==1)
        v->processAudio(v->m_outputBufferLenFrames);
}

void FragmentStream::playVoicesParallel()
{
#ifdef FOO22
    QElapsedTimer timer;
    timer.start();
    for (uint i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->m_outputBufferLenFrames=m_outputBufSize;
        m_Voices[i]->m_streamFramepos=m_outputPosFrames;
    }
    parallel_for_each(m_Voices.begin(),m_Voices.end(),
                      [](FragmentVoice* v)
                      {
                          if (v->m_VoicePlayState==1)
                              v->processAudio(v->m_outputBufferLenFrames);
                      });
    return;
    double elapsed=(double)timer.nsecsElapsed()/1000000;
    static int counter=0;
    static double accum=0.0;
    ++counter;
    accum+=elapsed;
    if (counter==1000)
    {
        double avg=accum/1000;
        //qDebug() << "avg parallel voices play"<<avg<<"ms";
        counter=0;
        accum=0.0;
    }
#endif
}

void FragmentStream::playVoices()
{
    for (size_t i=0;i<m_Voices.size();i++)
    {
        //if ( m_Voices[i]->m_VoicePlayState>0 && m_Voices[i]->m_VoicePlayState<3)
        m_Voices[i]->m_streamFramepos=m_outputPosFrames;
        if (m_Voices.at(i)->m_VoicePlayState==1)
        {
            //qDebug() << "processing audio for voice"<<i;
            
            m_Voices[i]->processAudio(m_outputBufSize);
            
        }
    }
}

void FragmentStream::setSyncMode(FragmentStream::FragmentSyncMode mode)
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_fragmentSyncMode=mode;
}

void FragmentStream::setOfflineProcessingActive(bool b)
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_offlineProcessingActive=b;
}

void FragmentStream::crossFadeVoiceBuffers()
{
    m_shouldXFadeVoiceBuffers=true;
}


void FragmentStream::setRecordingActive(bool b)
{
    //WDL_MutexLock autolocker(m_wdlmutex);
    m_recordingActive=b;
}

bool FragmentStream::recordingActive() const
{
    return m_recordingActive;
}

void FragmentStream::processAudio()
{
#ifdef WIN32
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
#endif
    double* audiobuf=m_outputBuffer.data();
    int frames=m_outputBufSize;
    std::lock_guard<std::recursive_mutex> locker(m_mutex);
    if (audiobuf)
    {
        for (int i=0;i<m_outputBufSize*m_numOutChans;i++)
        {
            audiobuf[i]=0.0;
        }
    }
	if (m_ssm->numSources() == 0)
		return;
    if (m_offlineProcessingActive==true)
    {
        return;
    }
    int sanityCnt=0;
    m_textureDurCacheRefreshCnt++;
    if (m_textureDurCacheRefreshCnt==100)
    {
        m_textureDurCacheRefreshCnt=0;
        m_cachedTextureDuration=calculateInputDrivenTextureDuration();
    }
    
    //writeAutomations();
    //    if (m_loopTexture==false && m_muteAtEnd && m_timeBehavior==TimeBehav_InputDriven && m_samplepos>m_cachedTextureDuration*m_samplerate)
    //    {
    //        m_lock->unlock();
    //        qDebug() << "texture has ended in forwards mode"<<m_samplepos;
    //        return;
    //    }
    
    
    
    // we hope for now this can be run in just the audiothread, but might be worth it trying to use the QFutureSynchronizer approach
    double timeToRateEnvel=1.0/m_cloudDur*((double)m_outputPosFrames/m_samplerate);
    //if (m_ssm->m_morphParam->envelopeEnabled())
    //m_ssm->updateProbs(timeToRateEnvel);
    HGParam *pEnv_Density=m_parameters[1].get();
    
    
        double nonshaped=pEnv_Density->getEnvelopeOrStaticValue(timeToRateEnvel);
        m_fragmentRate=nonshaped; //pEnv_Density->scaledValueFromNormalized(nonshaped);
        //m_fragmentRate=2.0+498.0*pow(pEnv_Density->getEnvelopeOrStaticValue(timeToRateEnvel),3.0);
    
#ifdef FOO22
    if (pEnv_Density->m_rateJumpThreshold>0 && m_previousFragmentRate<pEnv_Density->m_rateJumpThreshold && m_fragmentRate>=pEnv_Density->m_rateJumpThreshold)
    {
        //qDebug() << "invoking special hack for switch from low density to high density"<<m_previousFragmentRate<<m_fragmentRate;
        m_nextFragmentTime=(double)m_outputPosFrames/m_samplerate;
    }
    if (m_fragmentRate!=m_previousFragmentRate)
    {
        for (int i=0;i<m_parameters.size();i++)
        {
            //double smslope=m_parameters.at(i)->smootherSlope()*normalizedrate;
            m_parameters.at(i)->setSmootherSlope(m_parameters.at(i)->smootherSlope(),m_fragmentRate);
            //if (m_parameters.at(i)->smootherEnabled())
            //{
            //qDebug() << "changing smoother slope of param"<<m_parameters.at(i)->pathToParam();
            //}
        }
    }
#endif
    m_previousFragmentRate=m_fragmentRate;
    bool nomorefragments=false;
    if (m_loopTexture==false && m_muteAtEnd && m_timeBehavior==TimeBehav_Classic && m_outputPosFrames>m_cloudDur*m_samplerate)
        nomorefragments=true;
    
    if (nomorefragments==false)
    {
        if (true)
        {
            while (m_nextFragmentTime<((double)m_outputPosFrames+m_outputBufSize)/(double)m_samplerate)
            {
                int voiceIndex=GetFreeVoice(m_outputBufSize);
                if (voiceIndex==-1)
                {
                    m_voiceStarvationCounter++;
                    {
                        m_voiceStarvationCounter=64;
                    }
                    voiceIndex=0;
                } else
                {
                    if (m_voiceStarvationCounter>0)
                        m_voiceStarvationCounter--;
                }
                generateNextFragmentParams(voiceIndex);
                sanityCnt++;
                m_GrainCounter++;
                if (sanityCnt>1000) // something is very likely wrong if more than 1000 grains per buffer
                    break;
            }
        }
        //if (sanityCnt>0)
        //    qDebug() << "for buffer starting at"<<m_outputPosFrames<<"generated"<<sanityCnt<<"new fragments";
    } //else
    //  qDebug() << "ended generating more fragments in classic mode";
    //if (m_GrainCounter>0)
    //    emit grainCountChanged(m_GrainCounter-1);
    if (m_shouldXFadeVoiceBuffers==true)
    {
        //qDebug() << "should crossfade voice source audio buffers at time pos"<<m_outputPosFrames;
        m_shouldXFadeVoiceBuffers=false;
    }
    if (m_mtVoicesEnabled==true)
    {
        playVoices();
        //playVoicesParallel();
    } else
    {
        playVoices();
    }
    
    for (size_t i=0;i<m_Voices.size();i++)
    {
        if (m_Voices[i]->m_VoicePlayState>0 && m_Voices[i]->m_VoicePlayState<3)
        {
            double graingain=m_Voices[i]->m_graintoplay.Volume;
            for (int j=0;j<frames;j++)
            {
                for (int k=0;k<m_numOutChans;++k)
                    audiobuf[j*m_numOutChans+k]+=m_Voices[i]->m_audbuffer[j*m_numOutChans+k]*graingain;
            }
            
        }
    }
    
    
    double fadegain=1.0;
    bool fadeatloopswitch=false;
    for (int i=0;i<frames;i++)
    {
        
        if (m_wasSeeked && fadeatloopswitch)
        {
            fadegain=1.0/frames*i;
        }
        for (int j=0;j<m_numOutChans;++j)
            audiobuf[i*m_numOutChans+j]=fadegain*audiobuf[i*m_numOutChans+j];
    }
    m_wasSeeked=false;
    
    for (size_t i=0;i<m_Voices.size();i++)
    {
        if (m_Voices[i]->m_VoicePlayState==2)
        {
            m_Voices[i]->m_VoicePlayState=0;
            
        }
    }
    CAudioSource* as=m_ssm->focusedSound();
    double soundlenseconds=m_cloudDur;
    if (as && m_timeBehavior==TimeBehav_InputDriven)
    {
        
        double fullsoundlenseconds=(double)as->processedAudioLengthFrames()/as->origSampleRate();
        soundlenseconds=(as->mediaEnd()-as->mediaStart())*fullsoundlenseconds;
    }
    
    m_outputPosFrames+=frames;
    //if (m_timeBehavior==TimeBehav_Classic && m_loopTexture && m_samplepos>=m_cloudDur*m_samplerate)
    if (m_timeBehavior==TimeBehav_Classic && m_loopTexture && m_outputPosFrames>=m_textureLoopEnd*m_samplerate)
    {
        m_outputPosFrames=m_samplerate*m_textureLoopStart; //m_samplepos=0;
        m_nextFragmentTime=m_textureLoopStart; //m_nextGrainTime=0.0;
        SeekTo_s(m_textureLoopStart,false);
        
        if (fadeatloopswitch)
        {
            for (int i=0;i<frames;i++)
            {
                double fadegain=1.0-(1.0/frames*i);
                for (int j=0;j<m_numOutChans;++j)
                    audiobuf[i*m_numOutChans+j]*=fadegain;
            }
        }
    }
    if (m_timeBehavior==TimeBehav_InputDriven && m_loopTexture && m_inputPosSeconds>=soundlenseconds)
    {
        m_outputPosFrames=0;
        m_inputPosSeconds=0.0;
        m_nextFragmentTime=0.0;
        SeekTo_s(0.0);
        for (int i=0;i<frames;i++)
        {
            double fadegain=1.0-(1.0/frames*i);
            for (int j=0;j<m_numOutChans;++j)
                audiobuf[i*m_numOutChans+j]*=fadegain;
        }
    }
    double buflen = (double)frames/m_samplerate*1000.0;
    //m_cpuload = 1.0/buflen*bench.elapsed();
    
}

void FragmentStream::processAudioQueued(double *buf, int nframes)
{
    /*
    qDebug() << "fragment stream queued process";
    while (m_adapterQueue.Available()<(int)(nframes*2*sizeof(double)))
    {
        processAudio();
        m_adapterQueue.Add((void*)m_outputBuffer.data(),m_outputBufSize*2*sizeof(double));
    }
    if (m_adapterQueue.Available()>16384*sizeof(double))
    {
        qDebug() << "fragment stream queue has"<<m_adapterQueue.Available()/2/8<<"frames";
        //m_adapterQueue.Clear();
    }
    m_adapterQueue.GetToBuf(0,(void*)buf,nframes*2*sizeof(double));
    m_adapterQueue.Advance(nframes*2*sizeof(double));
     */
}



void FragmentStream::setTimeBehaviorMode(TimeBehaviorModes mode)
{
	std::lock_guard<std::recursive_mutex> locker(m_mutex);
    m_timeBehavior=mode;
    if (m_timeBehavior==TimeBehav_InputDriven)
        calculateInputDrivenTextureDuration();
}



void FragmentStream::setMonoPanMode(PanModes mode) noexcept
{
    m_panModeMono=mode;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->m_panModeMono=mode;
    }
}

void FragmentStream::setStereoPanMode(PanModes mode) noexcept
{
    m_panModeStereo=mode;
    for (size_t i=0;i<m_Voices.size();i++)
    {
        m_Voices[i]->m_panModeStereo=mode;
    }
}


double FragmentStream::textureDuration()
{
    
    if (m_timeBehavior==TimeBehav_Classic)
        return m_cloudDur;
    if (m_ssm->numSources()==0)
        return m_cloudDur;
    if (m_timeBehavior==TimeBehav_InputDriven)
    {
        //return m_cachedTextureDuration;
        CAudioSource* as=m_ssm->focusedSound();
        if (as)
        {
            double fulllen=(double)as->processedAudioLengthFrames()/as->origSampleRate();
            return (as->mediaEnd()-as->mediaStart())*fulllen;
        }
        return m_cloudDur;
    }
    return m_cloudDur;
}

double FragmentStream::calculateInputDrivenTextureDuration()
{
    if (m_timeBehavior!=TimeBehav_InputDriven)
        return m_cloudDur;
	double theduration=1.0;
    double timeaccum=0.0;
    CAudioSource* as=m_ssm->focusedSound();
    if (as)
    {
        uint64 sndlen=as->processedAudioLengthFrames();
        double fullsndlenseconds=(double)sndlen/as->origSampleRate();
        double sndlenseconds=(as->mediaEnd()-as->mediaStart())*fullsndlenseconds;
        HGParam* playpar=m_parameters[11].get();
        int sanity=0;
        if (playpar->envelopeEnabled())
        {
            while (timeaccum<sndlenseconds)
            {
                
				// This code needs to simulate playing back the texture with automated
				// playspeed parameter. 
				double timetoenv=1.0/sndlenseconds*timeaccum;
                //double cach=playpar->lastAutomatedValue();
                //playpar->setLastAutomatedValue(cach);
                double playspeed= playpar->getEnvelopeOrStaticValue(timetoenv);
                timeaccum+=((double)m_outputBufSize/as->origSampleRate())*playspeed; //*srcompens;
                theduration+=(double)m_outputBufSize/as->origSampleRate();
                sanity++;
                if (sanity>1000000)
                {
                    break;
                }
                
            }
        } else
            theduration=sndlenseconds*(1.0/playpar->getEnvelopeOrStaticValue(0.0));
        m_textureDurationDirty=false;
        //qDebug() << "out dur calculation took"<<m_inputPosition<<"took"<<timer.elapsed()<<"ms"<<"result"<<theduration<<"iterations"<<sanity;
        //m_outdurPerfInfos[m_outDurPerfInfoCounter].time=m_inputPosition;
        //m_outdurPerfInfos[m_outDurPerfInfoCounter].elapsed=timer.elapsed();
        //m_outDurPerfInfoCounter++;
        //if (m_outDurPerfInfoCounter==m_outDurPerfInfosSize)
        //{
        //    m_outDurPerfInfoCounter=0;
        //    qDebug() << "gathered calculateInputDrivenTextureDuration() diagnostics";
        //}
        return theduration;
    }

    return m_cloudDur;

}

void FragmentStream::setPanSpreadMode(int mode)
{
    m_panSpreadMode=mode;
}

minihourglass_node::minihourglass_node(String procname)
{
    m_proc_name = procname;
    m_type_name = procname;
    
	m_buses_layout.addInputBus("wav");
	m_buses_layout.addOutputBus("wav");
	
	m_fragment_stream = std::make_unique<FragmentStream>(this);
    m_parameters.emplace_back(new node_parameter("Texture duration","",0.5,60.0,10.0,false,0.0,1.0));
    m_parameters.back()->m_parent_node = this;
    m_parameters.back()->addValueListener(this);
	m_parameters.emplace_back(new node_parameter("Number of output channels", "",
		2, StringArray{ "1","2","4" }, Array<var>{ 1, 2, 4 }));
	m_parameters.back()->m_parent_node = this;
	m_parameters.emplace_back(new node_parameter("Playback mode", "",
		1, StringArray{ "Free source position","Forward" }, Array<var>{ 1, 2 }));
	m_parameters.back()->m_parent_node = this;
    m_parameters.back()->addValueListener(this);
	m_hg_params_offset = m_parameters.size();
	for (int i=0;i<m_fragment_stream->m_parameters.size();++i)
    {
        HGParam* par = m_fragment_stream->m_parameters[i].get();
        double parval = par->m_value;
        double minval = par->m_lowlim;
        double maxval = par->m_hilim;
        parameter_ref param(new node_parameter(par->m_displayname, "", minval, maxval, parval, true, 0.0, 1.0));
        param->m_parent_node = this;
        m_parameters.push_back(param);
        m_parameters.back()->addValueListener(par);
		if (i == 0)
		{
			m_parameters.back()->m_automation_enabled = true;
			auto env = m_parameters.back()->get_envelope(false);
			if (env != nullptr)
			{
				env->AddNode({ 0.0,0.0 });
				env->AddNode({ 1.0,1.0 });
			}
		}
		if (i == 11) // forward playspeed
		{
			m_parameters.back()->m_midpoint_skew = true;
			m_parameters.back()->m_midpoint = 1.0;
			auto env = m_parameters.back()->get_envelope(false);
			if (env != nullptr)
			{
				env->AddNode({ 0.0,0.5 });
				env->AddNode({ 1.0,0.5 });
			}
		}
    }
	for (int i = 0; i < m_fragment_stream->m_parameters.size(); ++i)
	{
		m_fragment_stream->m_parameters[i]->setNodeParameter(m_parameters[i + m_hg_params_offset].get());
	}
}

minihourglass_node::~minihourglass_node()
{
	if (m_thread != nullptr && m_thread->joinable())
		m_thread->join();
}

StringArray minihourglass_node::get_output_filenames()
{
    return StringArray({m_out_fn});
}

String minihourglass_node::get_supported_input_type()
{
    return "wav";
}

String minihourglass_node::get_output_type()
{
    return "wav";
}

void minihourglass_node::tick()
{
    tick_sources();
    if (m_pstate == Busy && m_error_text.isEmpty() == false)
    {
        m_pstate = FinishedWithError;
        ProcessErrorFunc(this);
        return;
    }
    if (m_pstate == Busy && m_thread_state == 1)
    {
        m_thread->join();
        m_thread.reset();
        end_benchmark();
        m_audio_out_writer->flush();
        m_audio_out_writer.reset();
        TempFileContainer::instance().add(m_out_fn);
        m_pstate = FinishedOK;
        setAudioFileToPlay(m_out_fn);
        ProcessFinishedFunc(this);
        return;
    }
	auto finish_state = are_sources_finished();
	if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
	{
		m_error_text = "Error in source(s)";
		m_pstate = FinishedWithError;
		ProcessErrorFunc(this);
	}
    if (finish_state == SourcesState::FinishedOK)
    {
        if (m_pstate == Idle)
        {
            if (m_sources.size() == 0)
            {
                m_error_text = "No sources";
                m_pstate = Busy;
                return;
            }
            if (m_sources[0]->get_output_filenames().size() == 0)
            {
                m_error_text = "Source has no files to use";
                m_pstate = Busy;
                return;
            }
            if (m_bypassmode == Bypassed)
            {
                m_out_fn = m_sources[0]->get_output_filenames()[0];
                m_pstate = FinishedOK;
                setAudioFileToPlay(m_out_fn);
                ProcessFinishedFunc(this);
                return;
            }
            m_error_text = "";
            MD5 h = get_state_hash();
            //Logger::writeToLog(h.toHexString());
            String outfn(get_output_folder() + "/hourglass_node-" + h.toHexString() + ".wav");
            m_out_fn = outfn;
            File tempfile(outfn);
            if (tempfile.exists() == true)
            {
                m_pstate = FinishedOK;
                setAudioFileToPlay(m_out_fn);
                ProcessFinishedFunc(this);
                return;
            }
            
            String infn = m_sources[0]->get_output_filenames()[0];
            if (infn.containsIgnoreCase("wav") == false)
            {
                m_pstate = Busy;
                m_error_text = "Source has incompatible file format";
                return;
            }
            auto info = get_audio_source_info_cached(infn);
            double in_sr = 44100.0;
            int numoutchans = m_parameters[1]->getValue();
            int numinchans = 2;
            File audio_out_file(outfn);
            WavAudioFormat wavformat;
            OutputStream* outstream = audio_out_file.createOutputStream();
            StringPairArray metadata;
            metadata.set(WavAudioFormat::bwavOriginator, CharPointer_UTF8("λ MiniHourGlass node"));
            m_audio_out_writer = std::unique_ptr<AudioFormatWriter>(wavformat.createWriterFor(
                                                                                              outstream, in_sr, numoutchans, 32, metadata, 0));
            m_outcounter = 0;
            
			double taillen = m_dyn_props["taillen"];
            int playmode = m_parameters[2]->getValue();
            double texturedur = m_parameters[0]->getValue();
			if (playmode == 1)
				m_fragment_stream->setTimeBehaviorMode(TimeBehav_Classic);
			if (playmode == 2)
			{
				m_fragment_stream->setTimeBehaviorMode(TimeBehav_InputDriven);
				texturedur = m_fragment_stream->calculateInputDrivenTextureDuration();
			}
            m_outlen = (int64_t)(texturedur)*in_sr;
            m_proc_buffer = AudioSampleBuffer(64, m_proc_buf_size);
            
            start_benchmark();
			m_render_progress = 0.0;
			m_thread_state = 0;
            m_pstate = Busy;
            auto tfunc = [this, texturedur,numinchans,numoutchans]()
            {
                int64_t outcounter = 0;
                m_fragment_stream->setTextureDuration(texturedur);
                m_fragment_stream->setOutputBufferLength(m_proc_buf_size);
                m_fragment_stream->SeekTo_s(0.0);
                m_fragment_stream->setNumOutChans(numoutchans);
				while (outcounter < m_outlen)
                {
                    double normtime = 1.0 / m_outlen*outcounter;
					m_render_progress = normtime;
					m_fragment_stream->processAudio();
                    for (int i=0;i<numoutchans;++i)
                    {
                        float* outptr = m_proc_buffer.getWritePointer(i);
                        for (int j=0;j<m_proc_buf_size;++j)
                            outptr[j]=m_fragment_stream->m_outputBuffer[j*numoutchans+i];
                    }
                    m_audio_out_writer->writeFromAudioSampleBuffer(m_proc_buffer, 0, m_proc_buf_size);
                    outcounter += m_proc_buf_size;
                }
                m_thread_state = 1;
            };
            m_thread = std::make_unique<std::thread>(tfunc);
        }
    }
}

MD5 minihourglass_node::extra_state_hash()
{
    return MD5();
}

ValueTree minihourglass_node::getAdditionalStateValueTree()
{
    return ValueTree("HGSTATE");
}
void minihourglass_node::restoreAdditionalStateFromValueTree(ValueTree tree)
{
}

void minihourglass_node::removeLastOutputFile()
{
    
}

String minihourglass_node::get_error_text()
{
    return m_error_text;
}
int minihourglass_node::getNumOutChansForNumInChans(int inchans)
{
    return 2;
}

void minihourglass_node::valueChanged(Value & v)
{
	if (m_parameters[0]->refersToSameSourceAs(v))
	{
		m_fragment_stream->setTextureDuration(v.getValue());
	}
	if (m_parameters[2]->refersToSameSourceAs(v))
	{
		int mode = v.getValue();
		if (mode == 1)
		{
			m_parameters[3]->m_enabled_in_gui = true;
			m_parameters[14]->m_enabled_in_gui = false;
		}
		if (mode == 2)
		{
			m_parameters[3]->m_enabled_in_gui = false;
			m_parameters[14]->m_enabled_in_gui = true;
		}
	}
}

void minihourglass_node::prepareToPlay(int expectedbufsize, int numchans, double sr)
{
	if (isOfflineOnly() == true)
	{
		process_node::prepareToPlay(expectedbufsize, numchans, sr);
		return;
	}
	if (m_owner_graph != nullptr)
	{
		for (auto& e : m_sources)
			m_owner_graph->renderNode(e.get(), true);
	}
	int numoutchans = m_parameters[1]->getValue();
	m_fragment_stream->setSampleRate(sr);
	int numinchans = 2;
	int playmode = m_parameters[2]->getValue();
	double texturedur = m_parameters[0]->getValue();
	if (playmode == 1)
		m_fragment_stream->setTimeBehaviorMode(TimeBehav_Classic);
	if (playmode == 2)
	{
		m_fragment_stream->setTimeBehaviorMode(TimeBehav_InputDriven);
		texturedur = m_fragment_stream->calculateInputDrivenTextureDuration();
	}
	m_fragment_stream->setTextureDuration(texturedur);
	m_fragment_stream->setOutputBufferLength(expectedbufsize);
	m_fragment_stream->SeekTo_s(0.0);
	m_fragment_stream->setNumOutChans(numoutchans);
	m_fragment_stream->setLooped(true);
}

void minihourglass_node::seek(double s)
{
	if (isOfflineOnly() == true)
	{
		process_node::seek(s);
		return;
	}
	m_fragment_stream->SeekTo_s(s);
}

void minihourglass_node::processAudio(float ** buf, int numchans, int numframes, double sr)
{
	if (isOfflineOnly() == true)
	{
		process_node::processAudio(buf, numchans, numframes, sr);
		return;
	}
	if (m_sources.size() == 0)
	{
		return;
	}
	
	if (numframes > m_fragment_stream->outputBufferLength())
		m_fragment_stream->setOutputBufferLength(numframes);
	int numoutchans = m_parameters[1]->getValue();
	if (numoutchans > numchans)
		numoutchans = numchans;
	m_fragment_stream->processAudio();
	for (int i = 0; i < numoutchans; ++i)
	{
		for (int j = 0; j < numframes; ++j)
		{
			buf[i][j] = m_fragment_stream->m_outputBuffer[j*numoutchans + i];
		}
	}
}

double minihourglass_node::getPlaybackPosition()
{
	if (isOfflineOnly() == true)
		return process_node::getPlaybackPosition();
	return (double)m_fragment_stream->playbackPositionFrames()/m_fragment_stream->sampleRate();
}

bool minihourglass_node::isOfflineOnly()
{
	return true;
}


#endif

double HGParam::getEnvelopeOrStaticValue(double tpos)
{
	if (m_node_param == nullptr)
		return m_value;
	if (m_node_param->m_automation_enabled == true && m_node_param->hasEnvelope() == true)
	{
		double normvalue = m_node_param->get_envelope()->GetInterpolatedNodeValue(tpos);
		return m_dummy_slider.proportionOfLengthToValue(normvalue);
	}
	return m_value;
}

#ifdef USE_MEM_MAPPED_WAV_READER
CAudioSource::CAudioSource()
{
	
}

void CAudioSource::loadAudioFile(String fn)
{
    WavAudioFormat wavformat;
    MemoryMappedAudioFormatReader* mappedreader = wavformat.createMemoryMappedReader(File(fn));
    if (mappedreader)
    {
        delete m_mmareader;
        m_chans = mappedreader->numChannels;
        m_src_fn = fn;
        m_sr = mappedreader->sampleRate;
        m_length = mappedreader->lengthInSamples;
        m_mmareader = mappedreader;
        if (m_mmareader->mapEntireFile()==true)
        {
            Logger::writeToLog("Successfully mem mapped all of "+fn);
        } else
            Logger::writeToLog("Could not mem map "+fn);
    } else Logger::writeToLog("Could not create memmap reader for "+fn);
}

#else

CAudioSource::CAudioSource()
{
    m_buf = AudioSampleBuffer(m_chans, m_length);
    for (int i = 0; i<m_length; ++i)
        m_buf.setSample(0, i, 0.5*sin(2 * 3.141592 / 44100 * i*440.0));
}

void CAudioSource::loadAudioFile(String fn)
{
	Filea file(fn);
	InputStream* instream = file.createInputStream();
	WavAudioFormat wavformat;
	AudioFormatReader* reader = wavformat.createReaderFor(instream, true);
	if (reader != nullptr)
	{
		int numchans = reader->numChannels;
		int len = reader->lengthInSamples;
		Logger::writeToLog("Opened file " + fn);
		m_buf = AudioSampleBuffer(numchans,len);
		reader->read(&m_buf, 0, len, 0, true, true);
		m_chans = numchans;
		m_length = len;
		m_sr = reader->sampleRate;
		m_src_fn = fn;
		delete reader;
		Logger::writeToLog("File has been imported");
	}
}
#endif

FragmentEnvelopeModel::FragmentEnvelopeModel(int which)
{
	m_buf.resize(1024);
	for (int i = 0; i<1024; ++i)
	{
		if (which == 0)
			m_buf[i] = 0.5*(1.0 - std::cos((2 * 3.1415926536*i) / 1023));
		else if (which == 1)
			m_buf[i] = 0.5;
	}
}
