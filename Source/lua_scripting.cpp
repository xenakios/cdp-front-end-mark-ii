#include "lua_scripting.h"
#include "node_component.h"
#include "node_view.h"
#include "mixersequencer_node.h"

ScriptingEngine::ScriptingEngine(process_graph* graph, NodeView* view) :
	m_graph(graph), m_node_view(view)
{
	
}

void ScriptingEngine::initAPI()
{
	m_lua.set_function("msgbox", [this](const char* txt)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Message from Lua script",
			CharPointer_UTF8(txt), "OK",
			m_node_view);
	});
	m_lua.set_function("refreshui", [this]()
	{
		if (m_node_view) m_node_view->repaint();
	});
	m_lua.set_function("getNodeByName", [this](const char* txt)
	{
		StringRef temp(txt);
		for (auto& e : m_graph->getNodes())
		{
			if (e->get_name().containsIgnoreCase(temp))
			{
				return e;
			}
		}
		return node_ref();
	});
	m_lua.set_function("getSoundFileInfo", [](const char* fn) 
	{
		auto info = get_audio_source_info_cached(CharPointer_UTF8(fn));
		return std::make_tuple(info.num_channels, info.get_length_seconds(), info.samplerate, 
			info.m_format_name.toRawUTF8(), info.num_bits, info.m_length_frames);
	});
    auto get_name_f = [](process_node& node) { return node.get_name().toRawUTF8(); };
    auto set_name_f = [](process_node& node, const char* name) { node.set_name(CharPointer_UTF8(name)); };
	m_lua.new_usertype<process_node>("node",
		"setParameter", [](process_node& node, int index, double value)
	{
		node.set_parameter(index, value);
	}, "getParameter", [](process_node& node, int index)
	{
		auto& pars = node.get_parameters();
		if (index >= 0 && index < pars.size())
			return (double)pars[index]->getValue();
		return 0.0;
	}, "clearEvents", [](process_node& node)
	{
		if (node.get_type_name() == "SequencerMixer")
		{
			((filemixer_node&)node).clearMixEvents();
		}
	}, "addEvent", [](process_node& node, double tpos, const char* fn, double len, double offset, 
		int ouchoffset, double gain, double fadeinlen, double fadeoutlen, double monopan, int lane)
	{
		if (node.get_type_name() == "SequencerMixer")
		{
			((filemixer_node&)node).addMixEvent(tpos, fn, len, offset, ouchoffset, gain, fadeinlen, fadeoutlen,monopan,lane);
		}
    }, "addEvents", [](process_node& node, sol::table table)
                                    {
                                         if (node.get_type_name() == "SequencerMixer")
                                         {
                                             ((filemixer_node&)node).addMixEvents(table);
                                         }
                                     },
		"setNumMixOutChans", [](process_node& node, int chans)
	{
		if (node.get_type_name() == "SequencerMixer")
		{
			((filemixer_node&)node).setNumOutChans(chans);
		}
	},
        "name",sol::property(get_name_f, set_name_f));
}

String ScriptingEngine::executeScriptFromFile(String filename)
{
	m_lua = sol::state();
	m_lua.open_libraries(sol::lib::base, sol::lib::math);
	initAPI();
	try
	{
		m_lua.script_file(filename.toStdString());
	}
	catch (std::exception& excep)
	{
		return excep.what();
	}
	return String();
}

bool ScriptingEngine::executeScriptFromFileAsync(String filename, std::function<void(String)> completion_callback)
{
	return false;
}
