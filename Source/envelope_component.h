#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include <functional>
#include <future>
#include "jcdp_envelope.h"
#include "sol.hpp"
#include "script_editor.h"

class process_node;

template<typename T>
inline bool is_in_range(T val, T start, T end)
{
	return val >= start && val <= end;
}


class zoom_scrollbar : public Component
{
public:
	enum hot_area
	{
		ha_none,
		ha_left_edge,
		ha_right_edge,
		ha_handle
	};
	void mouseDown(const MouseEvent& e) override;
	void mouseMove(const MouseEvent& e) override;
	void mouseDrag(const MouseEvent& e) override;
	void mouseEnter(const MouseEvent &event) override;
	void mouseExit(const MouseEvent &event) override;
	void paint(Graphics &g) override;
	std::function<void(double, double)> RangeChanged;
	std::pair<double, double> get_range() const { return std::make_pair(m_start, m_end); }
	void setRange(double t0, double t1);
private:
	double m_start = 0.0;
	double m_end = 1.0;
	hot_area m_hot_area = ha_none;
	hot_area get_hot_area(int x, int y);
	int m_drag_start_x = 0;
};



class EnvelopeComponent : public Component, 
	public ChangeListener, 
	public MultiTimer
	//public TooltipClient
{
public:
	EnvelopeComponent();
	~EnvelopeComponent();
	void paint(Graphics& g) override;
	void set_envelope(std::shared_ptr<breakpoint_envelope> env, process_node* node, String name = String());
	std::shared_ptr<breakpoint_envelope> get_envelope() { return m_envelope; }
	String get_name() { return m_name; }
	void mouseMove(const MouseEvent& ev) override;
	void mouseDown(const MouseEvent& ev) override;
	void mouseDrag(const MouseEvent& ev) override;
	void mouseUp(const MouseEvent& ev) override;
	bool keyPressed(const KeyPress& ev) override;
	double get_view_start_time() const { return m_view_start_time; }
	double get_view_end_time() const { return m_view_end_time; }
	void set_view_start_time(double t) { m_view_start_time = t; repaint(); }
	void set_view_end_time(double t) { m_view_end_time = t; repaint(); }
	std::function<void(EnvelopeComponent*, Graphics&)> EnvelopeUnderlayDraw;
	std::function<void(EnvelopeComponent*, Graphics&)> EnvelopeOverlayDraw;
	std::function<void(breakpoint_envelope*)> OnEnvelopeEdited;
	std::function<double(double)> ValueFromNormalized;
	std::function<double(double)> TimeFromNormalized;
	void changeListenerCallback(ChangeBroadcaster*) override;
	void timerCallback(int id) override;
	//String getTooltip() override;
private:
	std::shared_ptr<breakpoint_envelope> m_envelope;
	String m_name;
	Colour m_env_color{ Colours::yellow };
	double m_view_start_time = 0.0;
	double m_view_end_time = 1.0;
	double m_view_start_value = 0.0;
	double m_view_end_value = 1.0;
	int find_hot_envelope_point(double xcor, double ycor);
	int findHotEnvelopeSegment(double xcor, double ycor, bool detectsegment);
	bool m_mouse_down = false;
	int m_node_to_drag = -1;
	std::pair<int, bool> m_segment_drag_info{ -1,false };
	int m_node_that_was_dragged = -1;
	process_node* m_proc_node = nullptr;
	AudioThumbnail* m_thumb = nullptr;
	FileInputSource* m_thumb_source = nullptr;
	void initThumbNail(String fn);
	void initResynth(String fn, MD5 h);
	std::future<void> m_resynth_fut;
	String m_last_tip;
	BubbleMessageComponent m_bubble;
	void show_bubble(int x, int y, const envelope_node &node);
};

struct EnvScriptSlider
{
	EnvScriptSlider(Component* parentcomp, String name, double minval, double maxval, double defval) :
		m_slider(Slider::LinearBar, Slider::TextBoxLeft)
	{
		parentcomp->addAndMakeVisible(&m_label);
		m_label.setText(name, dontSendNotification);
		m_label.setColour(Label::textColourId, Colours::white);
		parentcomp->addAndMakeVisible(&m_slider);
		m_slider.setRange(minval, maxval);
		m_slider.setValue(defval,dontSendNotification);
		m_slider.setColour(Slider::textBoxTextColourId, Colours::white);
	}
	Label m_label;
	Slider m_slider;
	
};

class EnvelopeContainerComponent : public Component, public Slider::Listener
{
public:
    EnvelopeContainerComponent();
    void resized() override;
	bool keyPressed(const KeyPress& ev) override;
    std::unique_ptr<EnvelopeComponent> m_env_comp;
	void runScript(String txt);
	void sliderDragStarted(Slider*) override;
	void sliderValueChanged(Slider*) override;
	void sliderDragEnded(Slider*) override;
	void updateScriptText();
private:
	ScriptTextEditor m_script_editor;
    ResizableCornerComponent m_resizer;
	std::vector<std::unique_ptr<EnvScriptSlider>> m_sliders;
	sol::state m_lua;
	nodes_t m_stored_env;
};
