#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include <functional>
#include "cdp_graph.h"

class CDPProgramsComponent;

class MyTextEditor : public TextEditor
{
public:
	std::function<void(TextEditor*)> OnArrowDownPressed;
	bool keyPressed(const KeyPress& press) override
	{
		if (press == KeyPress::downKey && OnArrowDownPressed)
			OnArrowDownPressed(this);
		return TextEditor::keyPressed(press);
	}
};

class NodeChooserTreeView : public TreeView
{
public:
	bool keyPressed(const KeyPress& ev) override;
};

class NodeChooserTreeItem : public TreeViewItem
{
public:
	NodeChooserTreeItem(CDPProgramsComponent* chooser, String txt, bool isleaf) : 
		m_txt(txt), m_isleaf(isleaf), m_chooser(chooser) {}
	bool mightContainSubItems() override;
	void paintItem(Graphics& g, int w, int h) override;
	void itemClicked(const MouseEvent & e) override;
	void itemDoubleClicked(const MouseEvent& e) override;
    void itemSelectionChanged (bool isNowSelected) override;
    
    String getNodeType()
	{
		if (m_isleaf == true)
			return m_txt;
		return String();
	}
	CDPProgramsComponent* m_chooser = nullptr;
private:
	String m_txt;
	bool m_isleaf = false;
	
};

class IJCDPreviewPlayback;
class node_inspector;

class CDPProgramsComponent :
	public Component,
	public TextEditor::Listener,
	public Button::Listener,
	public MultiTimer
{
public:
	CDPProgramsComponent(std::shared_ptr<IJCDPreviewPlayback> preview);
	~CDPProgramsComponent();
	void resized() override;
	void textEditorTextChanged(TextEditor& ed) override;
	void paint(Graphics& g) override;
	std::function<void(String)> OnProgramSelected;
	void buttonClicked(Button*) override;
	void treeItemClicked(NodeChooserTreeItem* item, bool doubleclick);
	bool keyPressed(const KeyPress& press) override;
	void timerCallback(int id) override;
private:
	std::unique_ptr<NodeChooserTreeView> m_treeview;
	std::unique_ptr<MyTextEditor> m_line_edit;
	std::unique_ptr<TextEditor> m_tip_line_edit;
	std::unique_ptr<ToggleButton> m_unsupported_toggle;
    std::unique_ptr<TextButton> m_preview_but;
    std::unique_ptr<TextButton> m_file_but;
	std::unique_ptr<node_inspector> m_node_inspector;
    node_ref m_node_inputfile;
    node_ref m_node_pvoc_anal;
    node_ref m_node_pvoc_resynth;
    node_ref m_node_preview;
    node_ref m_node_pvoc;
    node_ref m_node_pcm;
    std::shared_ptr<IJCDPreviewPlayback> m_playback;
    void updateTree();
    void buildPreviewGraph(String nodetype);
	void importPreviewFile();
	void start_preview();
	void stop_preview();
	String m_last_selected_node_type;
	process_graph m_preview_graph;
};

