#pragma once

#include "cdp_graph.h"
#include "routingmatrix.h"

class inputfile_node : public process_node, public Value::Listener
{
public:
	inputfile_node(String procname, String fn, int nameindex);
    ~inputfile_node();
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	void set_arguments(StringArray ar) override;
	int getNumOutChansForNumInChans(int inchans) override;
	void tick() override;
	void connections_changed() override;
	String get_error_text() override { return m_error_text; }
    void valueChanged(Value& v) override;
	bool usesCommandLine() const override { return false; }
    bool prepareToRender() override;
    TopoRenderState performRender() override;
private:
	int m_name_index = -1;
	bool m_file_compatible = false;
	String m_converted_filename;
};

class textfile_node : public process_node, public TextEditor::Listener
{
public:
	textfile_node(String procname);
	~textfile_node() {}
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	//void set_arguments(StringArray ar) override;
	int getNumOutChansForNumInChans(int inchans) override;
	void tick() override;
	void connections_changed() override;
	String get_error_text() override { return m_error_text; }
	bool usesCommandLine() const override { return false; }
	MD5 extra_state_hash() override;
	CustomGUIType getCustomEditorType2() override { return InspectorCustomEditor; };
	Component* getCustomEditor() override;
	void textEditorTextChanged(TextEditor& ed) override;
private:
	String m_text;
	String m_out_fn;
};

class dummy_node : public process_node
{
public:
	dummy_node(String procname);
	~dummy_node() {}
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	
	void tick() override;
	void connections_changed() override;
	String get_error_text() override { return m_error_text; }
	bool usesCommandLine() const override { return false; }
	MD5 extra_state_hash() override;
private:
	StringArray m_out_filenames;
};

class envelope_generator_node : public process_node
{
public:
	envelope_generator_node(String procname) : process_node(procname)
	{
	}
	void tick() override;
	bool usesCommandLine() const override { return false; }
private:


	// Inherited via process_node
	virtual StringArray get_output_filenames() override;

	virtual String get_supported_input_type() override;

	virtual String get_output_type() override;


	StringArray m_output_filenames;
};

class fileconverter_node : public process_node,
	public ValueListener
{
public:
	fileconverter_node(String procname);
	~fileconverter_node();
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	void tick() override;
	String get_error_text() override { return m_error_text; }
	MD5 extra_state_hash() override;
	void valueChanged(Value& v) override;
	void connections_changed() override;
	void update_params() override;
	void sourcesHaveChanged() override;
	bool usesCommandLine() const override { return false; }
private:
	String m_output_fn;
	
	routing_matrix m_routing_matrix;
	std::unique_ptr<std::thread> m_thread;
	std::atomic<int> m_thread_state{ 0 };
	void updateRoutingMatrixFromSource();
};
