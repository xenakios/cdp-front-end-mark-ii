#include "preset_component.h"
#include "sqlitehelpers.h"

extern std::unique_ptr<PropertiesFile> g_propsfile;

sqlite3* g_db_conn=nullptr;

void closePresetsSqlite()
{
	sqlite3_close(g_db_conn);
	g_db_conn = nullptr;
}

PresetComponent::PresetComponent(String presettype)
{
	if (g_db_conn == nullptr)
	{
		String presets_db_file(g_propsfile->getFile().getParentDirectory().getFullPathName() + "/CDP_FrontEnd_Presets.sqlitedb");
		sqlite3_open(presets_db_file.toRawUTF8(), &g_db_conn);
		char *errmsg = nullptr;
		sqlite3_exec(g_db_conn, "CREATE TABLE Presets(id INTEGER PRIMARY KEY, 'nodetype' TEXT,'presetname' TEXT,'statedata' BLOB);", nullptr, nullptr, &errmsg);
		if (errmsg != nullptr)
		{
			Logger::writeToLog(errmsg);
			sqlite3_free(errmsg);
		}
	}
	m_preset_type = presettype.replaceCharacter(' ', '_');
	initComboFromDatabase();
	addAndMakeVisible(&m_combo);
	m_combo.addListener(this);
	addAndMakeVisible(&m_menu_but);
	m_menu_but.addListener(this);
	m_menu_but.setButtonText("...");
	
}

PresetComponent::~PresetComponent()
{
	
}

void PresetComponent::comboBoxChanged(ComboBox * cb)
{
	if (cb == &m_combo)
	{
		if (g_db_conn != nullptr)
		{
			const char* sql = "SELECT statedata FROM Presets WHERE nodetype=? and presetname=?";
			auto callback = [this](sqlite3_stmt* stmt)
			{
				int numbytes = sqlite3_column_bytes(stmt, 0);
				ValueTree state = ValueTree::readFromData(sqlite3_column_blob(stmt, 0), numbytes);
				if (state.isValid())
					OnPresetChanged(state);
			};
			executeSQL(g_db_conn, sql, callback, m_preset_type, cb->getText());
		}
	}
}

void PresetComponent::buttonClicked(Button * but)
{
	if (but == &m_menu_but)
	{
		PopupMenu menu;
		menu.addItem(1, "Add preset...", true, false);
		menu.addItem(2, "Remove current", true, false);
		menu.addItem(3, "Rename current...", true, false);
		menu.addItem(4, "Update current", true, false);
		int r = menu.show();
		if (r == 1)
		{
			AlertWindow dlg(CharPointer_UTF8("λ"), "Enter new preset name", AlertWindow::InfoIcon, this);
			dlg.addTextEditor("name", "New preset", "Name");
			dlg.addButton("OK", 1, KeyPress(KeyPress::returnKey));
			dlg.addButton("Cancel", 2, KeyPress(KeyPress::escapeKey));
			dlg.getTextEditor("name")->selectAll();
			int dlg_result = dlg.runModalLoop();
			if (dlg_result == 1)
			{
				String new_name = dlg.getTextEditorContents("name");
				storePreset(new_name);
				initComboFromDatabase();
			}
		}
		if (r == 2)
		{
			removePreset(m_combo.getText());
			initComboFromDatabase();
		}
		if (r == 3)
		{
			String curname = m_combo.getText();
			AlertWindow dlg(CharPointer_UTF8("λ"), "Enter new preset name", AlertWindow::InfoIcon, this);
			dlg.addTextEditor("name", curname, "Name");
			dlg.addButton("OK", 1, KeyPress(KeyPress::returnKey));
			dlg.addButton("Cancel", 2, KeyPress(KeyPress::escapeKey));
			dlg.getTextEditor("name")->selectAll();
			int dlg_result = dlg.runModalLoop();
			if (dlg_result == 1)
			{
				String new_name = dlg.getTextEditorContents("name");
				renamePreset(curname, new_name);
				initComboFromDatabase();
			}
		}
		if (r == 4)
		{
			updatePreset(m_combo.getText());
		}
	}
}

void PresetComponent::updatePreset(String name)
{
	ValueTree state = GetPresetValueTree();
	const char* sql = "UPDATE Presets SET statedata=? WHERE nodetype=? AND presetname=?";
	executeSQLWithoutCallback(g_db_conn, sql, state, m_preset_type, name);
}

void PresetComponent::renamePreset(String oldname, String newname)
{
	const char* sql = "UPDATE Presets SET presetname=? WHERE nodetype=? AND presetname=?";
	executeSQLWithoutCallback(g_db_conn, sql, newname, m_preset_type, oldname);
}

void PresetComponent::storePreset(String name)
{
	ValueTree state = GetPresetValueTree();
	if (g_db_conn != nullptr)
	{
		const char* sql = "INSERT INTO Presets VALUES (NULL,?,?,?)";
		executeSQLWithoutCallback(g_db_conn, sql, m_preset_type, name, state);
	}
}

void PresetComponent::removePreset(String name)
{
	const char* sql = "DELETE FROM Presets WHERE presetname=? and nodetype=?";
	executeSQLWithoutCallback(g_db_conn, sql, name,m_preset_type);
}

void PresetComponent::resized()
{
	int butwid = 25;
	m_combo.setBounds(1, 1, getWidth() - butwid - 5, 20);
	m_menu_but.setBounds(m_combo.getRight() + 2, 1, butwid, 20);
}



void PresetComponent::initComboFromDatabase()
{
    if (g_db_conn != nullptr)
	{
		m_combo.clear();
		const char* sql = "SELECT presetname from Presets WHERE nodetype=?";
		int id = 1;
		auto callback = [&id,this](sqlite3_stmt* stmt)
		{
			String presname(CharPointer_UTF8((char*)sqlite3_column_text(stmt, 0)));
			m_combo.addItem(presname, id);
			++id;
		};
		executeSQL(g_db_conn, sql, callback, m_preset_type);
	}
}
