#ifndef H_GRAIN
#define H_GRAIN

#define BUILD_HOURGLASS_NODE

#ifdef BUILD_HOURGLASS_NODE

#include <math.h>
#include <vector>
#include <random>
#include "resample.h"
#include "fastqueue.h"
#include "cdp_graph.h"
#include "../JuceLibraryCode/JuceHeader.h"

class minihourglass_node;

#define USE_MEM_MAPPED_WAV_READER

#ifdef USE_MEM_MAPPED_WAV_READER
class CAudioSource
{
public:
    CAudioSource();
    ~CAudioSource() { delete m_mmareader; }
    double mediaStart() { return 0.0; }
    double mediaEnd() { return 1.0; }
    int64_t processedAudioLengthFrames() { return m_length; }
    double origSampleRate() { return m_sr; }
    int processedAudioNumChans() { return m_chans; }
    double baseVolume() { return 1.0; }
    void loadAudioFile(String fn);
    String m_src_fn;
    MemoryMappedAudioFormatReader* m_mmareader = nullptr;
private:
    int m_chans = 1;
    int64_t m_length = 44100;
    double m_sr = 44100.0;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CAudioSource)
};

#else
class CAudioSource
{
public:
	CAudioSource();
    double mediaStart() { return 0.0; }
    double mediaEnd() { return 1.0; }
    int64_t processedAudioLengthFrames() { return m_length; }
    double origSampleRate() { return m_sr; }
    int processedAudioNumChans() { return m_chans; }
    const float** audioBuffer() { return m_buf.getArrayOfReadPointers(); }
    double baseVolume() { return 1.0; }
	void loadAudioFile(String fn);
    String m_src_fn;
private:
	AudioSampleBuffer m_buf;
	int m_chans = 1;
    int m_length = 44100;
    double m_sr = 44100.0;
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CAudioSource)
};
#endif
class HGParam : public Value::Listener
{
public:
    HGParam(String displayname, String internalname, double defaultvalue, double lolim, double hilim) :
		m_displayname(displayname), m_name(internalname), 
		m_value(defaultvalue), m_lowlim(lolim), m_hilim(hilim) {}
    String m_name;
    String m_displayname;
    double m_value = 0.0;
    double m_lowlim = 0.0;
    double m_hilim = 1.0;
    bool envelopeEnabled() 
	{ 
		if (m_node_param != nullptr)
			return m_node_param->m_automation_enabled;
		return false;
	}
	double getEnvelopeOrStaticValue(double tpos);
    void valueChanged(Value& var) override
    {
        m_value = var.getValue();
    }
	void setNodeParameter(node_parameter* p)
	{
		//Logger::writeToLog(m_displayname + " " + p->m_name);
		m_node_param = p;
		m_dummy_slider.setRange(p->m_min_value.getValue(), p->m_max_value.getValue());
		if (p->m_midpoint_skew == true)
			m_dummy_slider.setSkewFactorFromMidPoint(p->m_midpoint);
	}
	node_parameter* m_node_param = nullptr;
private:
	Slider m_dummy_slider;
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(HGParam)
};

class SoundSourcesManager
{
public:
	SoundSourcesManager(minihourglass_node* node);
    CAudioSource* getSource(int index);
	CAudioSource* focusedSound();
    int numSources() { return m_active_sources; }
	int getRandomSourceIndex();
    void updateSource();
private:
    std::vector<std::shared_ptr<CAudioSource>> m_sources;
    minihourglass_node* m_minihg = nullptr;
    int m_active_sources = 0;
    std::mt19937 m_randgen{0};
    std::uniform_int_distribution<int> m_dist{0,1};
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SoundSourcesManager)
};

class FragmentEnvelopeModel
{
public:
	FragmentEnvelopeModel(int which);
    int tableSize()
    {
        return 1024;
    }
    int numEnvelopes()
    {
        return 1;
    }
    
    float* tableDataPtr(int index)
    {
        return m_buf.data();
    }
private:
    std::vector<float> m_buf;
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FragmentEnvelopeModel)
};

enum PanModes
{
    PanMode_Linear=0,
    PanMode_SquareRoot,
    PanMode_WeirdCosine,
    PanMode_Rotate
};

class SoundSourcesManager;
class FragmentVoice;

class Fragment_Info
{
public:
    Fragment_Info()
    {
        volumeEnvMorph=0.0; pitchEnvMorph=0.0;
        PanPositions.resize(256);
        PanEndPositions.resize(256);
        for (size_t i=0;i<PanPositions.size();++i)
        {
            PanPositions[i]=0.0;
            PanEndPositions[i]=0.0;
        }
        PanEndPositions[0]=0.0;
        PanEndPositions[1]=-1.0;
    }
    int State;
    CAudioSource *Audiosource=nullptr;
    double PosInSource;
    double Timepos;
    double Length;
    double Volume; // decibels
    double PanX;
    double PanY;
    std::vector<double> PanPositions;
    std::vector<double> PanEndPositions;
    double Transpose; // semitones
    double sendlevels[2];
    bool sendsEnabled[2];
    double volumeEnvMorph;
    double pitchEnvMorph;
    double panEnvMorph;
    
};

class FragmentVoice
{
public:
    FragmentVoice();
    ~FragmentVoice();
    void processAudio(int frames);
    void setResamplerQuality(int nqual);
    void resetBuffers(int newbufsize, int channels);
    Fragment_Info m_graintoplay;
    int m_VoicePlayState;
    
    
    std::vector<float> m_audbuffer;
    
    std::vector<double> m_pannedoutputsgains;
    int m_streamFramepos;
    int m_offsettedFramePos;
    int m_outputBufferLenFrames;
    
    int m_outputPosition;
    int m_fragmentLengthSamples;
    int m_pitchEnvelopeGranularity;
    double m_srcfact;
    double m_insourceCounter;
    PanModes m_panModeMono;
    PanModes m_panModeStereo;
    bool m_pitchEnvelopeIsPlayrate;
    
    
    bool m_gatherPlayPosInfo;
    float* m_playPosInfo;
    int m_playPosInfoIndex;
    int m_voiceLengthCounter;
    int m_volumeEnvelopeErrors;
    
    int m_samplerate;
    int m_numOutChans=0;
    double m_elapsedAccum;
    int m_elapsedCounter;
    int m_ID;
    bool m_per_fragment_pan=true;
    bool getPanningEnabled() const noexcept { return m_panning_enabled; }
    void setPanningEnabled(bool b) noexcept { m_panning_enabled=b; }
    FragmentEnvelopeModel* m_volumeEnvelopes;
    FragmentEnvelopeModel* m_pitchEnvelopes;
private:
    int m_resamplerOutLen;
    std::vector<double> m_srcOutBuf;
    
    double m_envMorph;
    double m_volumeEnvelopePosition;
    double m_pitchEnvelopePosition;
    void initVoiceParams();
    void fillResampledBuffer(int offset,int frames);
    double m_previousSrcFact;
    WDL_Resampler m_resampler;
    int m_audioSourcePlayPos;
    int m_indexToResampledOut;
    
    bool m_voiceIsPlaying;
    bool m_panning_enabled=true;
    int m_last_source_chancount=0;
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FragmentVoice)
};

class HGParam;

enum TimeBehaviorModes
{
        TimeBehav_Classic,
        TimeBehav_InputDriven,
        TimeBehav_Warp
};

class FragmentStream
{
    
public:
    enum FragmentSyncMode
    {
        FSM_Sequential,
        FSM_Mix
    };
    minihourglass_node* m_hgnode = nullptr;
    friend class GrooveWidget;
    FragmentStream(minihourglass_node* mhg);
    
    ~FragmentStream();
    HGParam* GetEnvFromName(String name) const noexcept;
    void setTextureDuration(double dur);
    double textureDuration();
    void SeekTo_s(double pos,bool flushVoices=true);
    int GetFreeVoice(int buflen);
    void setResamplerQuality(int nq) noexcept;
    int resamplerQuality() const noexcept;
    void flushVoices(bool freeVoices=false);
    void setMTVoicesEnabled(bool b);
    
    
    void resetAllEnvelopes(bool setstaticvaluetodefault=false);
    int GetNumUsedVoices() const noexcept;
    void processAudio();
    void processAudioQueued(double* buf,int nframes);
    std::vector<double> m_outputBuffer;
    void Lock_Enter()  { m_mutex.lock();}
    void Lock_Leave()  { m_mutex.unlock(); }
    
    bool getSendIsPreFader(int index) const;
    void setSendPrefader(int,bool);
    void setSendLevel(int index,double v);
    double getSendLevel(int index) noexcept;
    void setSendProbability(int index,double p);
    double getSendProbability(int index);
    
    
    
    void setSampleRate(int sr) noexcept;
    void setOutputBufferLength(int len);
    int outputBufferLength() const noexcept { return m_outputBufSize; }
    
    
    
    int64_t playbackPositionFrames(bool mapped=false) const noexcept;
    
    void setMuteAtEnd(bool b);
    bool muteAtEnd() { return m_muteAtEnd; }
    void setMonoPanMode(PanModes mode) noexcept;
    void setStereoPanMode(PanModes mode) noexcept;
    PanModes monoPanMode() const noexcept { return m_panModeMono; }
    PanModes stereoPanMode() const noexcept { return m_panModeStereo; }
    
    HGParam* envFromDisplayName(const String &n);
    bool m_setTextureDurToSoundFileDur;
    int m_currentVoice;
    double m_Density;
    double m_GrainLen;
    double m_GrainFadePercent;
    
    double m_posinsrc;
    double m_srcdur_s;
    double m_streamVolume;
    std::recursive_mutex m_mutex;
    int m_GrainCounter;
    std::vector<std::unique_ptr<HGParam>> m_parameters;
    double m_nextFragmentTime;
    std::vector<FragmentVoice*> m_Voices;
    
    bool isLooped() { return m_loopTexture; }
    void setLooped(bool b) { m_loopTexture=b; }
    double m_bpm;
    double m_inputPosSeconds;
    double m_lastInputPosSeconds;
    void setTimeBehaviorMode(TimeBehaviorModes mode);
    TimeBehaviorModes timeBehaviorMode() { return m_timeBehavior; }
    double m_cachedTextureDuration;
    double calculateInputDrivenTextureDuration();
    
    void setPanSpreadMode(int mode);
    int panSpreadMode() { return m_panSpreadMode; }
    void removeFragmentEnvelope(HGParam* par);
    
    
    void setSourcePosQuantizedToWaveCycles(bool b);
    bool sourcePosQuantizedToWaveCycles() { return m_srcPosQuantizedToWavecycles; }
    void setPitchEnvelopeGranularity(int samples);
    int pitchEnvelopeGranularity() { return m_pitchEnvelopeGranularity; }
    void setTextureLoopStart(double t);
    void setTextureLoopEnd(double t);
    double textureLoopStart() { return m_textureLoopStart; }
    double textureLoopEnd() { return m_textureLoopEnd; }
    bool m_bufferSizeIsHardwareBufferSize;
    void setSwingAmount(double v);
    double swingAmount() { return m_swingAmount; }
    void storeGlobalGrooveGrid();
    void restoreGlobalGrooveGrid();
    void setPitchEnvelopeAsPlayrate(bool b);
    bool pitchEnvelopeIsPlayrate() { return m_pitchEnvelopeIsPlayrate; }
    void playVoicesParallel();
    void playVoices();
    std::vector<double> m_fragmentStartPositions;
    FragmentSyncMode syncMode() { return m_fragmentSyncMode; }
    void setSyncMode(FragmentSyncMode mode);
    
    void setOfflineProcessingActive(bool);
    void crossFadeVoiceBuffers();
    
    void setRecordingActive(bool b);
    bool recordingActive() const;
    int voiceOverLoadCount() const { return m_voiceStarvationCounter; }
    int sampleRate() const { return m_samplerate; }
    
    void update_probability_table(HGParam*, bool update_all=false);
    int num_allocated_voices() const { return m_Voices.size(); }
    int getNumOutChans() const { return m_numOutChans; }
    void setNumOutChans(int count);
    
    int m_pan_history_counter;
    int m_pan_history_length;
    void set_spatialization_path(std::vector<std::pair<double,double>> path);
    
    
    
    double getCPULoad() const { return m_cpuload; }
private:
    void allocate_voices(int num_voices);
    FragmentEnvelopeModel m_pitchEnvelopes{1};
    FragmentEnvelopeModel m_volumeEnvelopes{0};
    void writeAutomations();
    bool m_shouldXFadeVoiceBuffers;
    bool m_offlineProcessingActive;
    
    int64_t m_outputPosFrames;
    int64_t m_outputPosFramesMapped;
    FragmentSyncMode m_fragmentSyncMode;
    int m_fragmentStartPositionsCounter;
    int m_fragmentParameterSamplesCounter;
    int m_playposInfoVoiceIndex;
    int m_samplerate;
    int m_numOutChans;
    void registerEelVariables();
    void calculateGrooveGridFactors();
    double m_swingAmount;
    double m_textureLoopStart;
    double m_textureLoopEnd;
    int m_pitchEnvelopeGranularity;
    bool m_pitchEnvelopeIsPlayrate;
    
    
    int m_panSpreadMode;
    double m_previousFragmentRate;
    double m_fragmentRate;
    double m_cloudDur;
    double m_idleTaskResult;
    double m_cpuload = 0.0;
    void idleTask(int x);
    
    bool m_mtVoicesEnabled;
    PanModes m_panModeMono;
    PanModes m_panModeStereo;
    
    int m_outputBufSize;
    bool m_loopTexture;
    std::unique_ptr<SoundSourcesManager> m_ssm;
    Fragment_Info m_currentGrainParams;
    
    void initScriptEngine();
    
    bool m_grainScriptHasError;
    bool m_grainScriptProgramEnabled;
    
    double m_scripthelper_x0;
    double m_scripthelper_x1;
    
    int m_fftSampleCounter;
    int m_resamplerQ;
    bool m_EnvelopeListDirty;
    double m_inputStart;
    double m_inputEnd;
    
    
    void generateNextFragmentParams(int voiceIndex);
    int m_delayIncounter;
    int m_delayOutCounter;
    
    bool m_wasSeeked;
    TimeBehaviorModes m_timeBehavior;
    
    double calculateInputDrivenTimePos(double input);
    bool m_textureDurationDirty;
    
    int m_textureDurCacheRefreshCnt;
    bool m_muteAtEnd;
    void calculateEnvelopeTables(int which);
    bool m_srcPosQuantizedToWavecycles;
    WDL_FastQueue m_adapterQueue;
    
    int m_voiceStarvationCounter;
    bool m_recordingActive;
    std::mt19937 m_mt_rand;
    std::uniform_real_distribution<double> m_unidist;
    
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FragmentStream)
};

class minihourglass_node : public process_node, public Value::Listener
{
public:
    minihourglass_node(String procname);
    
	~minihourglass_node();
    
    StringArray get_output_filenames() override;
    
    String get_supported_input_type() override;
    
    String get_output_type() override;
    
    void tick() override;
    MD5 extra_state_hash() override;
    
    ValueTree getAdditionalStateValueTree() override;
    void restoreAdditionalStateFromValueTree(ValueTree tree) override;
    void removeLastOutputFile() override;
    
    String get_error_text() override;
    int getNumOutChansForNumInChans(int inchans) override;

	void valueChanged(Value& v) override;

	void prepareToPlay(int expectedbufsize, int numchans, double sr) override;
	void seek(double s) override;
	void processAudio(float** buf, int numchans, int numframes, double sr) override;
	double getPlaybackPosition() override;
	bool isOfflineOnly() override;
private:
    int64_t m_outcounter = 0;
    int64_t m_outlen = 0;
    std::unique_ptr<FragmentStream> m_fragment_stream;
    String m_out_fn;
    std::unique_ptr<std::thread> m_thread;
    std::atomic<int> m_thread_state{ 0 };
    int m_proc_buf_size = 256;
    int m_hg_params_offset = 0;
    std::unique_ptr<AudioFormatWriter> m_audio_out_writer;
    AudioSampleBuffer m_proc_buffer;
};

#endif
#endif

