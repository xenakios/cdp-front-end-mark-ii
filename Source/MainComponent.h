/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "cdp_graph.h"
#include "node_properties_panel.h"
#include "misc_components.h"
#include "automated_tests.h"

class NodeComponent;
class NodeView;
class juce_audio_preview;

class NodeInspectorTabComponent : public TabbedComponent
{
public:
	NodeInspectorTabComponent() : TabbedComponent(TabbedButtonBar::TabsAtTop)
	{
		TabChangedFunc = [](int) {};
	}
	void currentTabChanged(int index, const String&) override
	{
		TabChangedFunc(index);
	}
	node_inspector* getInspector(int index)
	{
		return static_cast<node_inspector*>(getTabContentComponent(index));
	}
	void cleanUpRemovedNode(process_node* nodeptr)
	{
		for (int i = 0; i < getNumTabs(); ++i)
		{
			auto inspec = getInspector(i);
			inspec->removeNode(nodeptr);
		}
	}
	std::function<void(int)> TabChangedFunc;
};

class action_t
{
public:
	action_t() {}
	action_t(String sname, String desc, String categ, KeyPress shortcut, std::function<void(void)> func,
		bool changesdocument=true) :
		m_shortname(sname), m_description(desc), m_category(categ), m_shortcut(shortcut), 
		m_performfunc(func), m_changes_document(changesdocument) {}
	String m_shortname;
	String m_description;
	String m_category;
	KeyPress m_shortcut;
	std::function<void(void)> m_performfunc;
	bool m_changes_document = true;
};

class IJCDPreviewPlayback;
class TextFileParameterComponent;
class ScriptingEngine;
class ParameterLinker;

class MainContentComponent   : public Component, 
	                           private Button::Listener, 
							   private MultiTimer,
							   private Slider::Listener,
							   public ApplicationCommandTarget
{
public:
    //==============================================================================
    
    MainContentComponent(std::shared_ptr<IJCDPreviewPlayback>, ParameterLinker* lp);
    ~MainContentComponent();
	void mouseDown(const MouseEvent& ev) override;
	void buttonClicked(Button*) override;
    void paint (Graphics&) override;
    void resized() override;
	void timerCallback(int id) override;
	bool keyPressed(const KeyPress& ev) override;
	
	String startRender(bool blockcurrenthread =false);
	String exportNodeToFile(std::shared_ptr<process_node> n, String fn);
	void exportNodeToAutoNamedFile(std::shared_ptr<process_node> n);
	void exportNodeToFilePromptingName(std::shared_ptr<process_node> n);
#ifdef IS_REAPER_EXTENSION_PLUGIN
	void exportNodeToReaperTimeLine(node_ref n);
#endif
	
	bool queryAutoNameExportFolder();
	void togglePlayback();
	void showPreferences();
	void removeCurrentGraph();
	void updateWindowTitle();
	void addFavoriteNode(int index);
	void saveFavorites();
	void loadFavorites();
	void showAboutScreen();
	void addTabbedNodeInspector();
	void removeCurrentTabbedNodeInspector();
    void checkIfDocumentModifiedOutside();
	void deferredInit();
	void setParameterLinker(ParameterLinker* linker);
private:
    //==============================================================================
	std::shared_ptr<process_node> m_selected_node;
	std::shared_ptr<NodeView> m_active_node_view;
	Viewport m_node_view_port;
	std::unique_ptr<TextButton> m_render_button;
    std::unique_ptr<TextButton> m_show_param_manager;
	ParameterLinker* m_param_linker = nullptr;
	std::unique_ptr<Slider> m_volume_dial;
    //LabelledComponent<Slider> m_volume_dial_labelled;
    Label m_playbackstatuslabel;
    Label m_renderstatuslabel;
	NodeInspectorTabComponent m_inspector_tabs;
	double m_render_start_clock_time = 0.0;
	double m_render_elapsed_clock_time = 0.0;
	void connect_nodes_if_no_feedback(std::shared_ptr<process_node> a, std::shared_ptr<process_node> b);
	void save_document(String fn);
	void test_load();
    std::shared_ptr<IJCDPreviewPlayback> m_audio_player;
	void sliderValueChanged(Slider*) override;
	void test_heavy_graph();
	
	std::unique_ptr<ApplicationCommandManager> m_cmd_manager;
	ApplicationCommandTarget* getNextCommandTarget() override { return nullptr; }
	void getAllCommands(Array< CommandID > &commands) override;
	void getCommandInfo(CommandID commandID, ApplicationCommandInfo &result) override;
	bool perform(const InvocationInfo &info) override;
	std::vector<action_t> m_actions;
	void initActions();
	String m_current_doc_fn;
    Time m_cur_doc_last_saved;
	bool m_document_dirty = false;
	StringArray m_favorite_nodes;
	TextFileParameterComponent* m_txed = nullptr;
	void updateInspectorTabNames();
	FlexBox m_main_layout;
	FlexBox m_toprow_layout;
	SplitterLayout m_inspec_nodeview_layout;
	void buildFlexLayouts();
	void deleteUnusedTemporaryFiles();
	std::unique_ptr<ScriptingEngine> m_script_engine;
	std::atomic<TestState> m_test_state{ TestState::Idle };
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};



#endif  // MAINCOMPONENT_H_INCLUDED
