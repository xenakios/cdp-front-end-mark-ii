#include "mixersequencer_node.h"
#include <random>
#include <map>
#include "node_component.h"
#include "node_view.h"
#include "jcdp_utilities.h"

#undef min
#undef max

extern std::unique_ptr<AudioThumbnailCache> g_thumb_cache;
extern std::unique_ptr<AudioFormatManager> g_format_manager;

filemixer_node::filemixer_node(String procname)
{
	m_proc_name = procname;
	m_type_name = procname;
	m_eventstoplay.resize(128);
}

StringArray filemixer_node::get_output_filenames()
{
	return StringArray{ m_output_fn };
}

String filemixer_node::get_supported_input_type()
{
	return "wav";
}

String filemixer_node::get_output_type()
{
	return "wav";
}

void filemixer_node::tick()
{
	tick_sources();
	if (m_pstate == Busy && m_thread_state == 1)
	{
		m_thread->join();
		m_thread.reset();
		end_benchmark();
		TempFileContainer::instance().add(m_output_fn);
		m_pstate = FinishedOK;
		setAudioFileToPlay(m_output_fn);
		ProcessFinishedFunc(this);
		return;
	}
	if (m_pstate == FinishedWithError && m_thread_state == 1)
	{
		m_thread->join();
		m_thread.reset();
		ProcessErrorFunc(this);
		return;
	}
	auto finish_state = are_sources_finished();
	if (finish_state == SourcesState::FinishedWithError && ProcessErrorFunc)
	{
		m_error_text = "Error in source(s)";
		m_pstate = FinishedWithError;
		ProcessErrorFunc(this);
	}
	if (finish_state == SourcesState::FinishedOK)
	{
		if (m_pstate == Idle)
		{

			MD5 h = get_state_hash();
			//Logger::writeToLog(h.toHexString());
			String outfn(get_output_folder() + "/mixer_node-" + h.toHexString() + ".wav");
			File tempfile(outfn);
			if (tempfile.exists() == true)
			{
				m_pstate = FinishedOK;
				setAudioFileToPlay(outfn);
				if (ProcessFinishedFunc)
					ProcessFinishedFunc(this);
				return;
			}
			if (m_mix_events.size() == 0)
			{
				m_error_text = "No events to mix";
				m_pstate = FinishedWithError;
				if (ProcessErrorFunc)
					ProcessErrorFunc(this);
				return;
			}
			start_benchmark();
			performMix2(outfn);
			end_benchmark();
			m_output_fn = outfn;
			m_pstate = FinishedOK;
			TempFileContainer::instance().add(m_output_fn);
			setAudioFileToPlay(m_output_fn);
			ProcessFinishedFunc(this);
		}
	}
}

void filemixer_node::sortEvents()
{
    std::stable_sort(m_mix_events.begin(), m_mix_events.end(), [](const mixer_event_t& a, const mixer_event_t& b)
                     { return a.m_tpos<b.m_tpos; });
}

void filemixer_node::conformMixEvents()
{
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	for (auto& e : m_mix_events)
	{
		e.m_mono_pan = jlimit(0.0, 1.0, e.m_mono_pan);
		String fn = e.m_fn;
		if (fn.startsWithChar('$'))
		{
			String foo = fn.substring(1);
			int index = foo.getIntValue();
			if (index >= 0 && index<m_sources.size())
			{
				e.m_real_fn = getSourceFileName(index, 0);
				e.m_eventname = m_sources[index]->getShortName();
			}
		}
		else 
			e.m_real_fn = e.m_fn;
		if (e.m_real_fn.isEmpty() == false)
		{
			File file(e.m_real_fn);
			if (e.m_eventname.isEmpty())
				e.m_eventname = file.getFileName();
			auto info = get_audio_source_info_cached(e.m_real_fn);
			double srclensecs = info.get_length_seconds();
			if (srclensecs > 0.0)
				e.m_max_len = srclensecs;
			if (e.m_len < 0.00001)
			{
				e.m_len = srclensecs;
			}
			e.m_playpos = 0;
			e.m_outpos = 0;
			double sr = info.samplerate;
			if (sr > 0.0)
			{
				e.m_playpos = sr*e.m_src_offset; // +(m_mix_playpos - (e.m_tpos*m_out_sr));
				if (e.m_playpos < 0)
					e.m_playpos = 0;
				e.m_outpos = m_mix_playpos - (e.m_tpos*m_out_sr);
				if (e.m_outpos < 0)
					e.m_outpos = 0;
				//Logger::writeToLog("event playpos " + String(e.m_playpos) + " outpos " + String(e.m_outpos));
			}
		}
	}
}

void filemixer_node::addMixEvents(sol::table table)
{
	m_mix_events.clear();
	table.for_each([this](sol::object& key, sol::object& val)
    {
		try
		{
			sol::table ev_tab = val.as<sol::table>();
			double tpos = ev_tab[1];
			const char* fn = ev_tab[2].get<const char*>();
			double len = ev_tab[3].get_or(0.0);
			double src_offs = ev_tab[4].get_or(0.0);
			int ochanofs = ev_tab[5].get_or(0);
			double gain = ev_tab[6].get_or(1.0);
			double fadeinlen = ev_tab[7].get_or(0.002);
			double fadeoutlen = ev_tab[8].get_or(0.002);
			double monopan = ev_tab[9].get_or(0.5);
			int lane = ev_tab[10].get_or(0);
			addMixEvent(tpos, CharPointer_UTF8(fn), len, src_offs,
				ochanofs, gain, fadeinlen, fadeoutlen, monopan,lane);
			//Logger::writeToLog(String(tpos)+" "+fn);
		}
		catch (std::exception& excep)
		{
			Logger::writeToLog(excep.what());
		}
    });
    
}

void filemixer_node::addMixEvent(double tpos, String fn, double len, double offset, int outchoffset,
	double gain, double fadeinlen, double fadeoutlen, double monopan, int lane)
{
	tpos = jlimit(0.0, 600.0, tpos);
	outchoffset = jlimit(0, m_num_outchans-1, outchoffset);
	m_mix_events.emplace_back(tpos, fn, len, offset, outchoffset, gain, fadeinlen, fadeoutlen, monopan, lane);
}

void filemixer_node::addMixEvent(mixer_event_t ev)
{
	m_mix_events.emplace_back(ev);
}

void filemixer_node::clearMixEvents()
{
	m_mix_events.clear();
}

void filemixer_node::setNumOutChans(int chans)
{
	m_num_outchans = jlimit(1, 8, chans);
}

MD5 filemixer_node::extra_state_hash()
{
	MemoryBlock block;
	for (auto& e : m_mix_events)
	{
		block.append((void*)&e.m_tpos, sizeof(double));
		block.append((void*)&e.m_len, sizeof(double));
		block.append((void*)&e.m_src_offset, sizeof(double));
		block.append((void*)&e.m_outchan_offset, sizeof(int));
		block.append((void*)&e.m_gain, sizeof(double));
		block.append((void*)&e.m_fade_in_len, sizeof(double));
		block.append((void*)&e.m_fade_out_len, sizeof(double));
		block.append((void*)&e.m_mono_pan, sizeof(double));
		block.append(e.m_fn.getCharPointer(), e.m_fn.getNumBytesAsUTF8());
	}
	block.append((void*)&m_num_outchans, sizeof(int));
	block.append((void*)&m_mix_blocksize, sizeof(int));
	MD5 h(block);
	return h;
}

ValueTree filemixer_node::getAdditionalStateValueTree()
{
	ValueTree result("customstate");
	result.setProperty("numoutchans", m_num_outchans, nullptr);
	result.setProperty("scriptcode", m_script_code, nullptr);
	ValueTree mixeventstree("mixevents");
	for (auto& e : m_mix_events)
	{
		ValueTree mixevent("ev");
		mixevent.setProperty("tpos", e.m_tpos, nullptr);
		mixevent.setProperty("len", e.m_len, nullptr);
		mixevent.setProperty("offs", e.m_src_offset,nullptr);
		mixevent.setProperty("outchofs", e.m_outchan_offset, nullptr);
		mixevent.setProperty("gain", e.m_gain, nullptr);
		mixevent.setProperty("fadeinlen", e.m_fade_in_len, nullptr);
		mixevent.setProperty("fadeoutlen", e.m_fade_out_len, nullptr);
		mixevent.setProperty("monopan", e.m_mono_pan, nullptr);
		mixevent.setProperty("fn", e.m_fn,nullptr);
		mixevent.setProperty("lane", e.m_lane,nullptr);
        mixeventstree.addChild(mixevent, -1, nullptr);
	}
	result.addChild(mixeventstree, -1, nullptr);
    for (auto& e: m_components)
    {
        if ((bool)e->getProperties()["is_in_node_component"]==true)
        {
            ValueTree guistate = e->saveState();
            result.addChild(guistate, -1, nullptr);
        }
    }
    return result;
}

void filemixer_node::restoreAdditionalStateFromValueTree(ValueTree tree)
{
	if (tree.hasProperty("numoutchans"))
		m_num_outchans = tree.getProperty("numoutchans");
	if (tree.hasProperty("scriptcode"))
	{
		m_script_code = tree.getProperty("scriptcode");
		
	}
	ValueTree mixevents = tree.getChildWithName("mixevents");
	int num_events = mixevents.getNumChildren();
	m_mix_events.clear();
	for (int i = 0; i < num_events; ++i)
	{
		ValueTree mix_event = mixevents.getChild(i);
		double tpos = mix_event.getProperty("tpos");
		double len = mix_event.getProperty("len");
		double srcoffs = mix_event.getProperty("offs");
		int outchofs = mix_event.getProperty("outchofs");
		double gain = mix_event.getProperty("gain",1.0);
		double fadeinlen = mix_event.getProperty("fadeinlen");
		double fadeoutlen = mix_event.getProperty("fadeoutlen");
		double monopan = mix_event.getProperty("monopan");
        int lane = mix_event.getProperty("lane", -1);
        String fn = mix_event.getProperty("fn");
		m_mix_events.emplace_back(tpos, fn, len, srcoffs, outchofs,gain, fadeinlen, fadeoutlen,monopan, lane);
        
	}
    for (auto& e : m_components)
    {
        if ((bool)e->getProperties()["is_in_node_component"]==true)
        {
            e->restoreState(tree.getChildWithName("mixerseqguistate"));
        }
    }
}

process_node::CustomGUIType filemixer_node::getCustomEditorType2()
{
	return process_node::InspectorCustomEditor;
}

Component * filemixer_node::getCustomEditor()
{
	return new MixerSequencerComponent(this);
}

void filemixer_node::setScriptCode(String text)
{
	if (text != m_script_code)
	{
		m_script_code = text;
		sendActionMessage("scriptchanged");
	}
}

String filemixer_node::runScript()
{
	try
	{
		sol::state lua;
		lua.open_libraries(sol::lib::base, sol::lib::math);
		lua.script(m_script_code.toStdString());
		sol::table events_table = lua["events"];
		if (events_table.valid() == true)
		{
            m_num_outchans = jlimit<int>(1,32,lua.get_or("outchans", 2));
            addMixEvents(events_table);
            conformMixEvents();
            return String();
		}
	}
	catch (std::exception& excep)
	{
		Logger::writeToLog(excep.what());
		return String(excep.what());
	}
	return String();
}

ResamplerPool g_resamplerpool{ 32 };

void updateMixEventPlayStates(int64_t curblockstart, int nframes, double sr,std::vector<mixer_event_t>& events)
{
	for (auto& e : events)
	{
		int64_t eventpos = sr*e.m_tpos;
		int64_t eventlen = sr*e.m_len;
		int64_t eventend = eventpos + eventlen;
		int64_t sourceoffset = sr*e.m_src_offset;
		Range<int64_t> blockrange(curblockstart, curblockstart + nframes);
		Range<int64_t> eventrange(eventpos, eventend);
		//bool playing = blockrange.intersects(eventrange);
		bool playing = eventrange.intersects(blockrange);
		if (playing == true && e.m_playing == false)
		{
			//Logger::writeToLog("obtaining resampler for " + e.m_fn);
			if (e.m_resampler == nullptr)
			{
				e.m_resampler = g_resamplerpool.obtain();
				e.m_resampler->Reset();
			}
			e.m_playpos = -1;
		}
		if (playing == false && e.m_playing == true && e.m_resampler != nullptr)
		{
			//Logger::writeToLog("releasing resampler from " + e.m_fn);
			g_resamplerpool.release(e.m_resampler);
			//e.m_playpos = -1;
			e.m_resampler = nullptr;
			//if (e.m_resampler != nullptr)
			//	Logger::writeToLog("Pool couldn't set resampler to null");
		}
		e.m_playing = playing;
	}
}

void initMemoryMappedReaders(std::vector<mixer_event_t>& the_events, 
	MemoryMappedReaderCache& mmreaders)
{
	WavAudioFormat wavformat;
	for (auto& e : the_events)
	{
		if (e.m_real_fn.isEmpty() == true)
			continue;
		if (mmreaders.count(e.m_real_fn) == 0)
		{
			MemoryMappedAudioFormatReader* reader = wavformat.createMemoryMappedReader(File(e.m_real_fn));
			if (reader != nullptr)
			{
				if (reader->mapEntireFile() == true)
				{
					mmreaders[e.m_real_fn] = std::unique_ptr<MemoryMappedAudioFormatReader>(reader);
				}
				else
				{
					delete reader;
					Logger::writeToLog("Could not map " + e.m_real_fn);
				}
			}
			else Logger::writeToLog("Could not create mm reader for " + e.m_real_fn);
		}
	}
}

void mixEvent(mixer_event_t& ev, int64_t mixpos, double sr,int outchans, int nframes, 
	MemoryMappedReaderCache& readercache,
	AudioSampleBuffer& outbuf, double* resamplebuf)
{
	Range<int64_t> cursor_range(mixpos, mixpos + nframes);
	Range<int64_t> event_range(ev.m_tpos*sr, (ev.m_tpos + ev.m_len)*sr);
	auto render_range = event_range.getIntersectionWith(cursor_range);
	if (render_range.isEmpty() == true)
	{
		//ev.m_playpos = -1;
        //ev.m_outpos = 0;
		return;
	}
	int64_t xh = render_range.getStart()-mixpos;
	int64_t xi = render_range.getLength();
	float mmreaderbuf[64];
	memset(mmreaderbuf, 0, 64 * sizeof(float));
	MemoryMappedAudioFormatReader* reader = readercache[ev.m_real_fn].get();
	if (reader == nullptr)
		return;
	const double src_sr = reader->sampleRate;
	const int src_chans = reader->numChannels;
	
	//if (ev.m_playpos == -1)
	//	ev.m_playpos = ev.m_src_offset*src_sr;
	float** outbufpointers = outbuf.getArrayOfWritePointers();
	WDL_Resampler* resampler = ev.m_resampler.get();
	if (resampler == nullptr)
	{
		ev.m_resampler = g_resamplerpool.obtain();
		resampler = ev.m_resampler.get();
	}
	resampler->SetRates(src_sr, sr);
	double* resampleinbuf = nullptr;
	int wanted = resampler->ResamplePrepare(xi, src_chans, &resampleinbuf);
	bool release_resampler = false;
	int64_t readerlen = reader->lengthInSamples;
	for (int i = 0; i < wanted; ++i)
	{
		if (ev.m_playpos < readerlen)
		{
			reader->getSample(ev.m_playpos, mmreaderbuf);
			++ev.m_playpos;
		}
		for (int j = 0; j < src_chans; ++j)
		{
			resampleinbuf[i*src_chans + j] = mmreaderbuf[j];
		}
	}
	resampler->ResampleOut(resamplebuf, wanted, (int)xi, src_chans);
    int fadeinlen = ev.m_fade_in_len*sr;
    int fadeoutlen = ev.m_fade_out_len*sr;
    int64_t eventlen = ev.m_len*sr;
    bool mono_to_stereo = false;
	float pangains[2];
	pangains[0] = 1.0-ev.m_mono_pan;
	pangains[1] = ev.m_mono_pan;
	if (outchans>1 && src_chans==1)
        mono_to_stereo = true;
	double eventgain = ev.m_gain;
	for (int i = 0; i < xi; ++i)
	{
        double fadegain = 1.0;
        if (ev.m_outpos<fadeinlen)
            fadegain = 1.0/fadeinlen*ev.m_outpos;
        else if (ev.m_outpos>=eventlen-fadeoutlen)
            fadegain = 1.0 - (1.0 / fadeoutlen*(ev.m_outpos - eventlen + fadeoutlen));
        fadegain=jlimit(0.0,1.0,fadegain);
        if (mono_to_stereo==false)
        {
            for (int j = 0; j < src_chans; ++j)
            {
                if (j<outchans)
                    outbufpointers[j][i+xh] += eventgain*fadegain*resamplebuf[i*src_chans + j];
            }
        } else
        {
            for (int j = 0; j < 2; ++j)
            {
                outbufpointers[j][i+xh] += pangains[j]*eventgain*fadegain*resamplebuf[i];
            }
        }
        ++ev.m_outpos;
	}
	int64_t eventsource_end = (ev.m_src_offset + ev.m_len)*src_sr;
	if (ev.m_playpos>=eventsource_end)
	{
		g_resamplerpool.release(ev.m_resampler);
		ev.m_resampler = nullptr;
	}
}

void filemixer_node::performMix2(String outfn)
{
	int outsr = 44100;
	int outchans = m_num_outchans;
	if (m_mix_events.size() == 0)
		return;
	m_mix_playpos = 0;
	m_out_sr = outsr;
	conformMixEvents();
	double mix_end_time = maxEventTime();
	WavAudioFormat wavformat;
	File outfile(outfn);
	FileOutputStream* foutputstream = outfile.createOutputStream();
	AudioFormatWriter* writer = wavformat.createWriterFor(foutputstream, outsr, outchans, 32, StringPairArray(), 0);
	if (writer == nullptr)
	{
		Logger::writeToLog("Could not create mix output audio file");
		return;
	}
	MemoryMappedReaderCache mmreaders;
	initMemoryMappedReaders(m_mix_events, mmreaders);
	int64_t mixlenframes = outsr*mix_end_time;
	int64_t outcounter = 0;
	int mixblocksize = m_mix_blocksize;
	juce::AudioSampleBuffer diskwritebuf(outchans, mixblocksize);
	std::vector<double> resamplebuf(8*mixblocksize);
	while (outcounter < mixlenframes)
	{
		diskwritebuf.clear();
		int lentorender = std::min((int64_t)mixblocksize, mixlenframes - outcounter);
		for (auto& e : m_mix_events)
		{
			mixEvent(e, outcounter, outsr, outchans, lentorender, mmreaders, diskwritebuf, resamplebuf.data());
		}
		writer->writeFromAudioSampleBuffer(diskwritebuf, 0, lentorender);
		outcounter += lentorender;
	}
	writer->flush();
	delete writer;
}

double filemixer_node::maxEventTime()
{
	double maxtime = 0.0;
	for (auto& e : m_mix_events)
	{
		maxtime = std::max(maxtime, e.m_tpos + e.m_len);
	}
	return maxtime;
}

void filemixer_node::prepareToPlay(int expectedbufsize, int numchans, double sr)
{
	if (isOfflineOnly() == true)
	{
		process_node::prepareToPlay(expectedbufsize, numchans, sr);
		return;
	}
	Logger::writeToLog("mixerseq preparetoplay");
	conformMixEvents();
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	m_num_outchans = numchans;
	m_out_sr = sr;
	m_mix_playpos = 0;
	
	initMemoryMappedReaders(m_mix_events, m_mm_readers);
	m_resampler_out_buf.resize(numchans * 4096);
}

void filemixer_node::seek(double s)
{
	if (isOfflineOnly() == true)
	{
		process_node::seek(s);
		return;
	}
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	m_mix_playpos = s*m_out_sr;
}

void filemixer_node::processAudio(float ** buf, int numchans, int numframes, double sr)
{
	if (isOfflineOnly() == true)
	{
		process_node::processAudio(buf, numchans, numframes, sr);
		return;
	}
	AudioSampleBuffer outputbuf((float* const*)buf, numchans, numframes);
	outputbuf.clear();
	std::lock_guard<std::mutex> locker(m_playback_mutex);
	for (auto& e : m_mix_events)
	{
		mixEvent(e, m_mix_playpos, sr, numchans, numframes, m_mm_readers, outputbuf, m_resampler_out_buf.data());
	}
	m_mix_playpos += numframes;
}

double filemixer_node::getPlaybackPosition()
{
	if (isOfflineOnly() == true)
	{
		return process_node::getPlaybackPosition();
	}
	return (double)m_mix_playpos / m_out_sr;
}

MixerSequencerComponent::MixerSequencerComponent(filemixer_node * node)
	: m_node(node), m_layout(this,true), m_script_ed(ValueTree("montagescriptstate"))
{
    m_node->m_components.push_back(this);
    addAndMakeVisible(&m_zs);
	m_node->addActionListener(this);
	m_script_ed.addListener(this);
	addAndMakeVisible(&m_script_ed);
	m_script_ed.setMultiLine(true);
	m_script_ed.setReturnKeyStartsNewLine(true);
	if (m_node->getScriptCode().isEmpty() == true)
	{
		m_script_ed.setText(
			R"(events = 
{ 
{ 0.0, "$0", 0.0,0.0,0,1.0,nil,nil,0.01},
{ 2.0, "$0"},
{ 2.9, "$0"},
{ 4.5, "$0", 0.5,0.5},
{ 6.5, "$0", 2.5,0.6}
}
)");
	}
	else
		m_script_ed.setText(m_node->getScriptCode(), false);
	m_script_ed.OnExecuteScript = [this](String txt) 
	{
		runScript(txt);
		m_timelinecomp->repaint();
	};
	getProperties().set("maxh", 700);
	m_timelinecomp = std::make_unique<SequencerTimeLineComponent>(m_node,&m_zs);
	addAndMakeVisible(m_timelinecomp.get());
    
	m_layout.addComponent(&m_script_ed, -0.01, -0.9, -0.09, true);
	m_layout.addComponent(m_timelinecomp.get(), -0.1, -0.99, -0.90, false);
	m_layout.addComponent(&m_zs, 15.0, 15.0, 15.0, false);
}

MixerSequencerComponent::~MixerSequencerComponent()
{
	m_node->removeActionListener(this);
    m_node->m_components.erase(std::remove(m_node->m_components.begin(),
                          m_node->m_components.end(), this),
              m_node->m_components.end());
}

void MixerSequencerComponent::resized()
{
	m_layout.performLayout(0, 0, getWidth(), getHeight());
}

void MixerSequencerComponent::actionListenerCallback(const String & message)
{
	if (message == "scriptchanged")
		m_script_ed.setText(m_node->getScriptCode(),false);
}

void MixerSequencerComponent::textEditorTextChanged(TextEditor & ed)
{
	m_node->setScriptCode(ed.getText());
}

void MixerSequencerComponent::runScript(String txt)
{
	m_node->setScriptCode(txt);
	m_node->runScript();
	m_timelinecomp->setMaxEventTime(m_node->maxEventTime());
    
	m_node->m_component->m_node_view->renderSelected(false);
    m_timelinecomp->updateThumbNails();
}

ValueTree MixerSequencerComponent::saveState()
{
    ValueTree vt("mixerseqguistate");
    vt.addChild(m_layout.saveState(), -1, nullptr);
    return vt;
}

void MixerSequencerComponent::restoreState(ValueTree state)
{
    if (state.isValid()==false)
        return;
    ValueTree layoutstate = state.getChildWithName("sls");
    m_layout.restoreState(layoutstate);
    resized();
    if (m_timelinecomp)
        m_timelinecomp->updateThumbNails();
	m_script_ed.setText(m_node->getScriptCode(),false);
}

SequencerTimeLineComponent::SequencerTimeLineComponent(filemixer_node * node, zoom_scrollbar* zs)
	: m_node(node), m_zs(zs)
{
	setOpaque(true);
	setWantsKeyboardFocus(true);
	m_zs->RangeChanged = [this](double t0, double t1)
	{
		setViewRange(m_max_event_time*t0, m_max_event_time*t1);
	};
    m_lane_heights.resize(m_num_lanes);
    m_lane_ycoords.resize(m_num_lanes+1);
    std::fill(m_lane_heights.begin(), m_lane_heights.end(), m_default_lane_heigth);
    updateLaneYCoords();
}

void SequencerTimeLineComponent::updateLaneYCoords()
{
    m_lane_ycoords.resize(m_lane_heights.size()+1);
    float accum = 0.0;
    m_lane_ycoords[0]=accum;
    for (int i=0;i<m_lane_heights.size();++i)
    {
        accum+=m_lane_heights[i];
        m_lane_ycoords[i+1]=accum;
    }
}

void SequencerTimeLineComponent::paint(Graphics & g)
{
	g.fillAll(Colours::black);
	if (m_node->m_mix_events.size() == 0)
		return;
	
	double maxtime = m_max_event_time;
	if (maxtime < 0.0001)
		maxtime = m_node->maxEventTime();
	if (maxtime < 0.0001)
		return;
	g.setColour(Colours::cyan);
	g.drawText(String(m_view_start_time, 1),0,0,100,20,Justification::left);
	g.drawText(String(m_view_end_time, 1), getWidth()-100, 0, 100, 20, Justification::right);
	int numlanes = m_num_lanes;
	for (int i = 0; i < m_lane_ycoords.size(); ++i)
	{
		float y0 = m_lane_ycoords[i];
		//float y1 = m_lane_ycoords[i + 1];
		g.drawLine(0.0f, y0, getWidth(), y0);
	}
	Range<double> cursorrange(m_debug_cursor_pos, m_debug_cursor_pos + m_debug_cursor_len);
	for (int i = 0; i < m_node->m_mix_events.size(); ++i)
	{
		Path fadepath;
        auto& mixevent = m_node->m_mix_events[i];
		int eventlane = mixevent.m_lane;
		if (eventlane == -1)
			eventlane = i % numlanes;
		float xcor = jmap<float>(mixevent.m_tpos, m_view_start_time, m_view_end_time, 0, getWidth());
		float xcor1 = jmap<float>(mixevent.m_tpos+mixevent.m_len, m_view_start_time, m_view_end_time, 0, getWidth());
		float evwidth = xcor1 - xcor;
		float ycor = 0.0f;
		if (mixevent.m_lane>=0 && mixevent.m_lane<m_lane_ycoords.size())
			ycor = m_lane_ycoords[mixevent.m_lane];
        float evhei = 50.0f;
        if (mixevent.m_lane>=0 && mixevent.m_lane<m_lane_heights.size())
            evhei=m_lane_heights[mixevent.m_lane];
		if (mixevent.m_real_fn.isEmpty())
		{
			g.drawText("Rendering node...", xcor, ycor, 100.0f, evhei, Justification::centredLeft);
			continue;
		}
		Colour eventrectcolour = Colours::white;
		if (mixevent.m_selected == true)
			eventrectcolour = Colours::yellow;
        Range<double> eventrange(mixevent.m_tpos, mixevent.m_tpos + mixevent.m_len);
		auto union_range = eventrange.getIntersectionWith(cursorrange);
		if (union_range.isEmpty()==false)
		//if (eventrange.intersects(cursorrange))
		{
			if (union_range.isEmpty() == false)
			{
				g.setColour(Colours::red);
				float srcxcor0 = jmap<float>(union_range.getStart(), m_view_start_time, m_view_end_time, 0, getWidth());
				float srcxcor1 = jmap<float>(union_range.getEnd(),
					m_view_start_time, m_view_end_time, 0, getWidth());
				//if (srcxcor1 - srcxcor0>0.0f)
				//	g.fillRect(srcxcor0, ycor, srcxcor1 - srcxcor0, evhei);
			}
        }
		g.setColour(eventrectcolour);
		g.drawRect(xcor, ycor, evwidth, evhei, 1.5f);
		g.setColour(Colours::white);
        if (evwidth-30>0)
            g.drawText(mixevent.m_eventname, xcor+2, ycor+1, evwidth-30, 14, Justification::centredLeft);
        if (xcor+evwidth-30>0)
            g.drawText(String(mixevent.m_gain,2), xcor+evwidth-30, ycor+1, 30, 14, Justification::centredLeft);
        if (m_thumb_nails.count(mixevent.m_real_fn)==0)
            updateThumbNails();
        if (m_thumb_nails.count(mixevent.m_real_fn)>0)
		{
			g.setColour(Colours::white);
            juce::Rectangle<int> area(xcor+1, ycor+15, evwidth-1, evhei-16);
            double gain = mixevent.m_gain;
            m_thumb_nails[mixevent.m_real_fn]->drawChannels(g, area,
                                                            mixevent.m_src_offset, mixevent.m_src_offset + mixevent.m_len,
                                                            gain);
		}
        float fadeend = jmap<float>(mixevent.m_tpos+mixevent.m_fade_in_len,m_view_start_time,m_view_end_time,0,getWidth());
        g.setColour(Colours::cyan.withAlpha(0.5f));
        fadepath.addTriangle(xcor, ycor+evhei, fadeend, ycor+16, fadeend, ycor+evhei);
        float fadestart = jmap<float>(mixevent.m_tpos+mixevent.m_len-mixevent.m_fade_out_len,m_view_start_time,m_view_end_time,0,getWidth());
        //fadepath.startNewSubPath(fadestart, ycor+16);
        fadepath.addTriangle(fadestart, ycor+16, xcor+evwidth, ycor+evhei, fadestart, ycor+evhei);
        g.fillPath(fadepath);
        //fadepath.addLineSegment(<#const Line<float> &line#>, <#float lineThickness#>)
        //g.drawLine(xcor, ycor+evhei, xcor+fadelen, ycor+16);
        //jmap<float>(mixevent.m_fade_out_len,m_view_start_time,m_view_end_time,0,getWidth());
        //g.drawLine(xcor+evwidth-fadelen, ycor+15, xcor+evwidth, ycor+evhei);
	}
	float xcor = jmap<float>(m_debug_cursor_pos, m_view_start_time, m_view_end_time, 0, getWidth());
	float xcor1 = jmap<float>(m_debug_cursor_pos + m_debug_cursor_len, m_view_start_time, m_view_end_time, 0, getWidth());
	//g.setColour(Colours::green.withAlpha(0.5f));
	//g.fillRect(xcor, 0.0f, xcor1-xcor, (float)getHeight());
	g.setColour(Colours::white);
	xcor = jmap<float>(m_node->getPlaybackPosition(), m_view_start_time, m_view_end_time, 0, getWidth());
	g.drawLine(xcor, 0.0f, xcor, getHeight());
}

void SequencerTimeLineComponent::updateThumbNails()
{
    //m_thumb_nails.clear();
    for (auto& e : m_node->m_mix_events)
    {
        if (m_thumb_nails.count(e.m_real_fn)==0)
        {
            auto thumb = std::make_shared<AudioThumbnail>(256, *g_format_manager, *g_thumb_cache);
            if (thumb->setSource(new FileInputSource(File(e.m_real_fn)))==true)
            {
                m_thumb_nails[e.m_real_fn]=thumb;
                //Logger::writeToLog("Succesfully created thumb for "+e.m_real_fn);
            } else
                Logger::writeToLog("Could not create thumb for "+e.m_real_fn);
            
        }
    }
}

void SequencerTimeLineComponent::setViewRange(double t0, double t1)
{
	m_view_start_time = t0;
	m_view_end_time = t1;
	repaint();
}



int SequencerTimeLineComponent::laneFromYCoord(float y)
{
	/*
	double acc = 0.0;
	for (int i = 0; i < m_lane_heights.size(); ++i)
	{
		double y1 = acc + m_lane_heights[i];
		if (y >= acc && y < y1)
			return i;
		acc = y1;
	}
	return -1;
	*/
	
	for (int i=0;i<m_lane_heights.size();++i)
    {
        float lane_ycor0 = m_lane_ycoords[i];
        float lane_ycor1 = lane_ycor0 + m_lane_heights[i];
        if (y>=lane_ycor0 && y<lane_ycor1)
            return i;
    }
    return -1;
	
}

std::vector<float> SequencerTimeLineComponent::yCoordsFromLanes()
{
	std::vector<float> result(m_lane_heights.size() + 1);
	result[0] = 0.0f;
	double acc = 0.0;
	for (int i = 0; i < m_lane_heights.size(); ++i)
	{
		acc += m_lane_heights[i];
		result[i + 1] = acc;
	}
	return result;
}

void SequencerTimeLineComponent::showEventContextMenu()
{
	if (m_hot_event < 0)
		return;
	mixer_event_t& mixevent = m_node->m_mix_events[m_hot_event];
	PopupMenu menu;
	menu.addItem(1, "Pan left");
	menu.addItem(2, "Pan center");
	menu.addItem(3, "Pan right");
	std::array<double,3> panpositions={ 0.0,0.5,1.0 };
	int r = menu.show();
	if (r >= 1)
	{
		mixevent.m_mono_pan = panpositions[r - 1];
		m_node->conformMixEvents();
		renderNode();
	}
}

void SequencerTimeLineComponent::mouseDown(const MouseEvent & ev)
{
	m_debug_cursor_hot = isDebugCursorHot(ev.x, ev.y);
	m_events_were_changed = false;
    m_hot_event = findEventFromCoordinates(ev.x, ev.y);
	if (ev.mods.isLeftButtonDown())
	{
		for (auto& e : m_node->m_mix_events)
			e.m_selected = false;
		if (m_hot_event >= 0)
		{
			m_drag_start_time_pos = m_node->m_mix_events[m_hot_event].m_tpos;
			m_drag_start_len = m_node->m_mix_events[m_hot_event].m_len;
			m_drag_start_src_offset = m_node->m_mix_events[m_hot_event].m_src_offset;
			m_drag_start_gain = m_node->m_mix_events[m_hot_event].m_gain;
			m_drag_start_fade_in = m_node->m_mix_events[m_hot_event].m_fade_in_len;
			m_drag_start_fade_out = m_node->m_mix_events[m_hot_event].m_fade_out_len;
			m_node->m_mix_events[m_hot_event].m_selected = true;
		}
		repaint();
	}
	if (ev.mods.isRightButtonDown() && m_hot_event >= 0)
	{
		showEventContextMenu();
		return;
	}
	if (m_hot_event==-1)
	{
		if (ev.mods.isRightButtonDown() == true)
		{
			PopupMenu menu;
			menu.addItem(1, "Insert audio file...", true, false);
			PopupMenu nodemenu;
			int id = 2;
			for (auto& e : m_node->m_sources)
			{
				if (e->get_output_type() == "wav")
				{
					nodemenu.addItem(id, e->getShortName(), true, false);
					++id;
				}
			}
			menu.addSubMenu("Insert node from graph", nodemenu);
			id = 10000;
			PopupMenu chanmenu;
			for (int i = 1; i < 9; ++i)
			{
				chanmenu.addItem(id, String(i), true, i==m_node->m_num_outchans);
				++id;
			}
			menu.addSubMenu("Output channels", chanmenu);
			PopupMenu blocksizemenu;
			std::vector<int> sizes{ 64,256,1024,4096,16384,32768,65536 };
			id = 20000;
			for (int i = 0; i < sizes.size(); ++i)
			{
				int sz = sizes[i];
				blocksizemenu.addItem(id, String(sz), true, sz == m_node->m_mix_blocksize);
				++id;
			}
			menu.addSubMenu("Mixing block size",blocksizemenu);
            PopupMenu laneheigthmenu;
            std::vector<float> lanehs{25.0f,50.0f,100.0f,200.0f};
            std::vector<String> lanehnames{"Tiny","Small","Medium","Large"};
            id = 30000;
            for (int i=0;i<lanehs.size();++i)
            {
                laneheigthmenu.addItem(id,lanehnames[i],true,false);
                ++id;
            }
            menu.addSubMenu("Lane heigth", laneheigthmenu);
            int r = menu.show();
			if (r == 1)
			{
				File file = queryAudioFile();
                grabKeyboardFocus();
                if (file.existsAsFile())
				{
					double tpos = jmap<double>(ev.x, 0, getWidth(), m_view_start_time, m_view_end_time);
					int lane = jlimit<int>(0, m_lane_heights.size() - 1, laneFromYCoord(ev.y));
					m_node->addMixEvent(tpos, file.getFullPathName(), 0.0, 0.0, 0, 1.0, 0.1, 0.1, 0.5,lane);
					m_node->conformMixEvents();
					double oldmaxtime = m_max_event_time;
					m_max_event_time = m_node->maxEventTime();
					updateThumbNails();
					double zs_t0 = m_zs->get_range().first;
					double zs_t1 = jmap<double>(oldmaxtime, 0.0, m_max_event_time, 0.0, 1.0);
					m_zs->setRange(zs_t0, zs_t1);
					repaint();
                    m_events_were_changed = true;
                    renderNode();
				}
			}
			if (r >= 2 && r<10000)
			{
				double tpos = jmap<double>(ev.x, 0, getWidth(), m_view_start_time, m_view_end_time);
				int lane = jlimit<int>(0, m_lane_heights.size() - 1, laneFromYCoord(ev.y));
				m_node->addMixEvent(tpos, "$"+String(r-2), 0.0, 0.0, 0, 1.0, 0.1, 0.1, 0.5, lane);
				m_node->conformMixEvents();
				m_max_event_time = m_node->maxEventTime();
				updateThumbNails();
				m_events_were_changed = true;
                renderNode();
				repaint();
			}
			if (r >= 10000 && r<20000)
			{
				m_node->m_num_outchans = r - 9999;
				m_events_were_changed = true;
				renderNode();
			}
			if (r >= 20000 && r < 30000)
			{
				m_node->m_mix_blocksize = sizes[r - 20000];
				m_events_were_changed = true;
				renderNode();
			}
            if (r >= 30000 && r<40000)
            {
                float hei = lanehs[r-30000];
                int lane = laneFromYCoord(ev.y);
                if (lane>=0)
                {
                    m_lane_heights[lane]=hei;
                }
                
                updateLaneYCoords();
                repaint();
            }
		}
	}
}

bool SequencerTimeLineComponent::isDebugCursorHot(float x, float y)
{
	float xcor = jmap<float>(m_debug_cursor_pos, m_view_start_time, m_view_end_time, 0, getWidth());
	float xcor1 = jmap<float>(m_debug_cursor_pos+m_debug_cursor_len, m_view_start_time, m_view_end_time, 0, getWidth());
	float evwidth = xcor1 - xcor;
	juce::Rectangle<float> debugcursorarea(xcor, 0.0f, evwidth, getHeight());
	return debugcursorarea.contains(x, y);
}

void SequencerTimeLineComponent::mouseDrag(const MouseEvent & ev)
{
	if (m_max_event_time < 0.0001)
		return;
	if (m_debug_cursor_hot == true)
	{
		double new_time_pos = jmap<double>(ev.x, 0.0, getWidth(), m_view_start_time, m_view_end_time);
		m_debug_cursor_pos = new_time_pos;
		
		repaint();
		return;
	}
	if (m_hot_event >= 0)
	{
		auto& mixevent = m_node->m_mix_events[m_hot_event];
		double old_x = ev.x-ev.getDistanceFromDragStartX();
		double old_time_pos = jmap(old_x, 0.0, (double)getWidth(), m_view_start_time, m_view_end_time);
		double new_time_pos = jmap((double)ev.x, 0.0, (double)getWidth(), m_view_start_time, m_view_end_time);
		if (m_hot_event_control == 200)
        {
            mixevent.m_fade_in_len=jlimit<double>(0.0,2.0,m_drag_start_fade_in+(new_time_pos-old_time_pos));
            m_events_were_changed = true;
        }
        if (m_hot_event_control == 201)
        {
            mixevent.m_fade_out_len=jlimit<double>(0.0,2.0,m_drag_start_fade_out-(new_time_pos-old_time_pos));
            m_events_were_changed = true;
        }
        if (m_hot_event_control == 100)
        {
            double gaindelta = ev.getDistanceFromDragStartY()*0.01;
            mixevent.m_gain=jlimit(0.0,2.0,m_drag_start_gain-gaindelta);
            m_events_were_changed = true;
        }
        double maxlen = 1.0;
        
		maxlen = mixevent.m_max_len;
        if (m_hot_event_control == 0)
		{
            if (ev.mods.isAltDown()==false)
            {
                mixevent.m_tpos = m_drag_start_time_pos + (new_time_pos - old_time_pos);
                m_events_were_changed = true;
                if (mixevent.m_tpos + mixevent.m_len >= m_max_event_time)
                {
                    double new_max_time = mixevent.m_tpos + mixevent.m_len+1.0;
                    
                    double zs_t0 = m_zs->get_range().first;
                    double zs_t1 = jmap<double>(m_max_event_time, 0.0, new_max_time, 0.0, 1.0);
                    m_zs->setRange(zs_t0, zs_t1);
                    m_max_event_time = new_max_time;
                }
                if (mixevent.m_tpos < 0.0)
                    mixevent.m_tpos = 0.0;
                int new_lane = laneFromYCoord(ev.y);
                if (new_lane!=-1)
                    mixevent.m_lane = new_lane;
				else
				{
					m_lane_heights.push_back(m_default_lane_heigth);
					m_num_lanes = m_lane_heights.size();
					updateLaneYCoords();
				}
            
            }
            if (ev.mods.isAltDown())
            {
                double new_offset = m_drag_start_src_offset-(new_time_pos-old_time_pos);
                mixevent.m_src_offset = jlimit(0.0,maxlen,new_offset);
                m_events_were_changed = true;
            }
		}
		
		if (m_hot_event_control == -1 && m_drag_start_len > 0.0)
		{
			mixevent.m_src_offset = jlimit(0.0, maxlen, m_drag_start_src_offset + (new_time_pos - old_time_pos));
			mixevent.m_len = jlimit(0.1, maxlen- mixevent.m_src_offset, m_drag_start_len - (new_time_pos - old_time_pos));
			mixevent.m_tpos = jlimit(0.0,m_max_event_time, m_drag_start_time_pos + (new_time_pos - old_time_pos));
			m_events_were_changed = true;
			
		}
		if (m_hot_event_control == 1 && m_drag_start_len>0.0)
		{
            maxlen = jlimit(0.1,maxlen,maxlen-mixevent.m_src_offset);
			mixevent.m_len = jlimit(0.1, maxlen, m_drag_start_len + (new_time_pos - old_time_pos));
            m_events_were_changed = true;
		}
		repaint();
	}
}

void SequencerTimeLineComponent::mouseMove(const MouseEvent & ev)
{
	int hot = findEventFromCoordinates(ev.x, ev.y);
	if (hot >= 0)
	{
		//Logger::writeToLog("Mouse at event " + String(hot));
		m_hot_event_control = getHotEventControl(hot, ev.x, ev.y);
		if (m_hot_event_control == 0)
			setMouseCursor(MouseCursor::DraggingHandCursor);
		if (m_hot_event_control == -1 || m_hot_event_control==1)
            setMouseCursor(MouseCursor::LeftRightResizeCursor);
        if (m_hot_event_control==100)
            setMouseCursor(MouseCursor::UpDownResizeCursor);
        if (m_hot_event_control==200 || m_hot_event_control==201)
            setMouseCursor(MouseCursor::CrosshairCursor);
        
	}
	else setMouseCursor(MouseCursor::NormalCursor);
}

void SequencerTimeLineComponent::mouseUp(const MouseEvent & ev)
{
	if (m_events_were_changed == true)
    {
		m_node->conformMixEvents();
		renderNode();
		//m_node->prepareToPlay()
        m_events_were_changed = false;
		
    }
}

void SequencerTimeLineComponent::renderNode()
{
    if (m_node->m_component && m_node->m_component->m_node_view)
    {
        m_node->m_component->m_node_view->renderNodeDeferred(m_node,1000);
		//updateThumbNails();
		repaint();
    }
}

bool SequencerTimeLineComponent::keyPressed(const KeyPress & ev)
{
	if (ev == KeyPress::backspaceKey)
	{
		remove_from_container(m_node->m_mix_events, [](const mixer_event_t& a) { return a.m_selected; });
		m_hot_event = -1;
		m_hot_event_control = 0;
		repaint();
        renderNode();
        return true;
	}
	if (ev == '.' && m_hot_event>=0)
	{
		for (auto& e : m_node->m_mix_events)
			e.m_selected = false;
		mixer_event_t dupl = m_node->m_mix_events[m_hot_event];
		dupl.m_tpos += dupl.m_len;
		dupl.m_selected = true;
		m_node->addMixEvent(dupl);
		m_hot_event = (int)m_node->m_mix_events.size() - 1;
		repaint();
        renderNode();
        return true;
	}
    if (ev == 'X')
    {
        auto xy = getMouseXYRelative();
        int hot_event = findEventFromCoordinates(xy.getX(), xy.getY());
        if (hot_event<0)
            return true;
        mixer_event_t& orig = m_node->m_mix_events[hot_event];
        mixer_event_t dupl = m_node->m_mix_events[hot_event];
        
        double t0 = jmap<double>(xy.getX(),0,getWidth(),m_view_start_time,m_view_end_time)-dupl.m_tpos;
        double origlen = orig.m_len;
        orig.m_len=t0;
        dupl.m_tpos += t0;
        dupl.m_src_offset += t0;
        dupl.m_len = origlen-t0;
        dupl.m_selected = true;
        m_node->addMixEvent(dupl);
        m_hot_event = (int)m_node->m_mix_events.size() - 1;
        repaint();
        renderNode();
        return true;
    }
	return false;
}

int SequencerTimeLineComponent::findEventFromCoordinates(float x, float y)
{
	if (m_max_event_time < 0.0001)
		return -1;
	int numlanes = m_num_lanes;
	for (int i = 0; i < m_node->m_mix_events.size(); ++i)
	{
		auto& mixevent = m_node->m_mix_events[i];
		int eventlane = mixevent.m_lane;
		if (eventlane == -1)
            eventlane = 0;
		float xcor = jmap<float>(mixevent.m_tpos, m_view_start_time, m_view_end_time, 0, getWidth());
		float xcor1 = jmap<float>(mixevent.m_tpos + mixevent.m_len, m_view_start_time, m_view_end_time, 0, getWidth());
		float evwidth = xcor1 - xcor;
        float ycor = m_lane_ycoords[eventlane];
        float evhei = m_lane_heights[eventlane];
		juce::Rectangle<float> evarea(xcor, ycor, evwidth, evhei);
		if (evarea.contains(x, y) == true)
			return i;
	}
	return -1;
}

int SequencerTimeLineComponent::getHotEventControl(int hotevent, float x, float y)
{
    if (hotevent<0)
        return 0;
    int numlanes = m_num_lanes;
    auto& mixevent = m_node->m_mix_events[hotevent];
    int eventlane = mixevent.m_lane;
    if (eventlane == -1)
        eventlane = 0;
    float xcor = jmap<float>(mixevent.m_tpos, m_view_start_time, m_view_end_time, 0, getWidth());
    float xcor1 = jmap<float>(mixevent.m_tpos + mixevent.m_len, m_view_start_time, m_view_end_time, 0, getWidth());
    float evwidth = xcor1 - xcor;
    float ycor = m_lane_ycoords[eventlane];
    float evhei = m_lane_heights[eventlane];
    juce::Rectangle<float> evarea_gain(xcor+evwidth-30, ycor, 30, 14);
    if (evarea_gain.contains(x, y))
        return 100;
    juce::Rectangle<float> evarea_left(xcor-10, ycor, 20, evhei);
    if (evarea_left.contains(x, y))
        return -1;
    juce::Rectangle<float> evarea_right(xcor+evwidth - 10, ycor, 20, evhei);
    if (evarea_right.contains(x, y))
        return 1;
    float xcor2 = jmap<float>(mixevent.m_tpos + mixevent.m_fade_in_len, m_view_start_time, m_view_end_time, 0, getWidth());
    juce::Rectangle<float> evarea_left_fade(xcor2-10, ycor+16, 20, 10);
    if (evarea_left_fade.contains(x, y))
        return 200;
    float xcor3 = jmap<float>(mixevent.m_tpos + mixevent.m_len-mixevent.m_fade_out_len, m_view_start_time, m_view_end_time, 0, getWidth());
    juce::Rectangle<float> evarea_right_fade(xcor3-10, ycor+16, 20, 10);
    if (evarea_right_fade.contains(x, y))
        return 201;
    return 0;
}

mixer_event_t::~mixer_event_t()
{
	if (m_resampler != nullptr)
		g_resamplerpool.release(m_resampler);
}

EventPropertiesComponent::EventPropertiesComponent()
{
}

void EventPropertiesComponent::setEvents(EventRefVector events)
{
	m_events = events;
}

template<typename F>
inline void transformEventRefVector(EventRefVector& events, F&& f)
{
	for (auto& e : events)
		f(e);
}

void EventPropertiesComponent::sliderValueChanged(Slider * slid)
{
	if (slid == &m_mono_pan_slider)
		transformEventRefVector(m_events, [slid](mixer_event_t* ev) { ev->m_mono_pan = slid->getValue(); });
	if (slid == &m_gain_slider)
		transformEventRefVector(m_events, [slid](mixer_event_t* ev) { ev->m_gain = slid->getValue(); });
}

void EventPropertiesComponent::comboBoxChanged(ComboBox * combo)
{
	if (combo == &m_outchan_offset_combo)
		transformEventRefVector(m_events, [combo](mixer_event_t* ev) { ev->m_outchan_offset = combo->getSelectedId()-1; });
}

void EventPropertiesComponent::updateComponents()
{
	if (m_events.size() == 1)
		m_name_edit.setText(m_events[0]->m_eventname, false);
	else m_name_edit.setText("Multiple selected", false);

}
