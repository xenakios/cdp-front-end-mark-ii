#include "node_chooser_components.h"
#include "node_factory.h"
#include "jcdp_audio_playback.h"
#include "node_properties_panel.h"
#include "preset_component.h"

extern std::unique_ptr<PropertiesFile> g_propsfile;

String g_last_used_program_filter;

CDPProgramsComponent::CDPProgramsComponent(std::shared_ptr<IJCDPreviewPlayback> preview)
    : m_playback(preview)
{
	
    m_treeview = std::make_unique<NodeChooserTreeView>();
	addAndMakeVisible(m_treeview.get());
	m_line_edit = std::make_unique<MyTextEditor>();
	m_line_edit->OnArrowDownPressed = [this](TextEditor*)
	{
		m_treeview->grabKeyboardFocus();
		m_treeview->getRootItem()->getSubItem(0)->setSelected(true, true, dontSendNotification);
	};
	addAndMakeVisible(m_line_edit.get());
	m_line_edit->setBounds(0, 0, 10, 10);
	m_line_edit->addListener(this);

	m_tip_line_edit = std::make_unique<TextEditor>();
	m_tip_line_edit->setMultiLine(true);
	addAndMakeVisible(m_tip_line_edit.get());


	m_unsupported_toggle = std::make_unique<ToggleButton>("Show untested Lua defined programs");
	addAndMakeVisible(m_unsupported_toggle.get());
	bool showunsup = g_propsfile->getBoolValue("general/showuntestedluaprogs", false);
	m_unsupported_toggle->setToggleState(showunsup, dontSendNotification);
	m_unsupported_toggle->addListener(this);

    m_preview_but = std::make_unique<TextButton>();
    m_preview_but->setButtonText("Preview");
    addAndMakeVisible(m_preview_but.get());
    m_preview_but->addListener(this);
    
    m_file_but = std::make_unique<TextButton>();
    m_file_but->setButtonText("File...");
    addAndMakeVisible(m_file_but.get());
    m_file_but->addListener(this);
    m_file_but->setTooltip("Choose file to use for preview input");
    
	setSize(1000, 750);
	m_line_edit->setText(g_last_used_program_filter, false);
    m_node_inputfile = m_preview_graph.createNode("Input Sound File");
    String fn = g_propsfile->getValue("general/nodechooserpreviewfile");
    if (fn.isEmpty()==false)
		m_node_inputfile->set_parameter(0, fn);
    m_node_pvoc_anal = m_preview_graph.createNode("PVOC ANAL - Create PVOC analysis file.");
    m_node_pvoc_resynth = m_preview_graph.createNode("PVOC RESYNTH - Resynthesize .ana file to .wav file");
    connect_nodes(m_node_inputfile, m_node_pvoc_anal);
    updateTree();
}

CDPProgramsComponent::~CDPProgramsComponent()
{
    m_playback->stop();
    m_treeview->deleteRootItem();
}

void CDPProgramsComponent::resized()
{
	int w = 500;
	m_line_edit->setBounds(0, 0, w, 23);
	m_unsupported_toggle->setBounds(0, 24, w/2, 23);
    m_preview_but->setBounds(w/2+2, 24, w/4-10, 23);
    m_file_but->setBounds(m_preview_but->getRight()+2, 24, w/4-10, 23);
    m_treeview->setBounds(0, 48, w, getHeight() - 48 - 200);
	m_tip_line_edit->setBounds(0, getHeight() - 198, w, 198);
	if (m_node_inspector != nullptr)
		m_node_inspector->setBounds(w + 2, 24, w - 4, getHeight() - 2);
}

void CDPProgramsComponent::textEditorTextChanged(TextEditor & ed)
{
	g_last_used_program_filter = ed.getText();
	updateTree();
}

void CDPProgramsComponent::paint(Graphics & g)
{
	g.fillAll(Colours::lightgrey);
}

void CDPProgramsComponent::buttonClicked(Button *b)
{
	if (b == m_unsupported_toggle.get())
	{
		textEditorTextChanged(*m_line_edit);
		g_propsfile->setValue("general/showuntestedluaprogs", b->getToggleState());
	}
    if (b == m_file_but.get())
    {
		importPreviewFile();
	}
	if (b == m_preview_but.get())
	{
		if (m_playback->is_playing() == false)
			start_preview();
		else stop_preview();
	}
}

void CDPProgramsComponent::treeItemClicked(NodeChooserTreeItem * item, bool doubleclick)
{
	if (doubleclick == false)
	{
		for (auto& e : node_factory::instance()->m_entries)
		{
			if (item->getNodeType() == e.m_name)
			{
				m_tip_line_edit->setText(e.m_tiptext);
				if (m_last_selected_node_type != item->getNodeType())
				{
					m_last_selected_node_type = item->getNodeType();
					startTimer(1, 500);
				}
				break;
			}
		}
	}
	else
	{
		OnProgramSelected(item->getNodeType());
	}
}

bool CDPProgramsComponent::keyPressed(const KeyPress & press)
{
	if (press == KeyPress::spaceKey)
	{
		if (m_playback->is_playing() == false)
		{
			start_preview();
		}
		else
		{
			stop_preview();
		}
		return true;
	}
	return false;
}

void CDPProgramsComponent::timerCallback(int id)
{
	if (id == 1)
	{
		stopTimer(1);
		buildPreviewGraph(m_last_selected_node_type);
	}
}

void CDPProgramsComponent::buildPreviewGraph(String nodetype)
{
    if (m_playback->is_playing())
    {
        m_playback->stop();
        m_preview_but->setButtonText("Preview");
    }
    disconnect_node_from_graph(m_node_preview);
    auto node = m_preview_graph.createNode(nodetype);
    if (node!=nullptr)
    {
		m_node_inspector = std::make_unique<node_inspector>(true);
		m_node_inspector->setNodes({ node });
		m_node_inspector->set_show_procname(false);
		//m_node_inspector->addChangeListener(this);
		addAndMakeVisible(m_node_inspector.get());
		resized();
		if (node->get_output_type()=="wav")
        {
            m_node_preview = node;
            connect_nodes(m_node_inputfile, m_node_preview);
        }
        if (node->get_output_type()=="ana")
        {
            disconnect_node_from_graph(m_node_pvoc);
            m_node_pvoc = node;
            connect_nodes(m_node_pvoc_anal, m_node_pvoc);
            connect_nodes(m_node_pvoc,m_node_pvoc_resynth);
            m_node_preview = m_node_pvoc_resynth;
        }
    }
}

void CDPProgramsComponent::importPreviewFile()
{
	m_playback->stop();
	m_preview_but->setButtonText("Preview");
	File file = queryAudioFile();
	if (file.existsAsFile())
	{
		m_node_inputfile->set_parameter(0, file.getFullPathName());
		g_propsfile->setValue("general/nodechooserpreviewfile", file.getFullPathName());
	}
}

void CDPProgramsComponent::start_preview()
{
	if (m_playback->is_playing() == false)
	{
		if (m_node_inputfile->get_parameters()[0]->getValue().toString().length() == 0)
		{
			importPreviewFile();
		}
		// parameter change made by ImportPreviewFile initialises stuff in inputfilenode asynchronously, so
		// can't run the following code immediately...
		MessageManager::callAsync([this]()
		{
			auto selected_item = dynamic_cast<NodeChooserTreeItem*>(m_treeview->getSelectedItem(0));
			if (selected_item != nullptr)
			{
				//buildPreviewGraph(selected_item->getNodeType());
				if (m_node_preview != nullptr)
				{
					visit_sources(m_node_preview.get(), [](process_node* visnode) { visnode->set_idle(); });
					m_preview_graph.renderNode(m_node_preview.get(), true);
					if (m_node_preview->get_processing_state() == process_node::FinishedOK)
					{
						m_playback->set_node(m_node_preview);
						m_playback->start();
						m_preview_but->setButtonText("Stop");
					}
					else
						m_tip_line_edit->setText(m_node_preview->get_error_text());
				}
			}
			else Logger::writeToLog("No selected tree item");
		});
	}
}

void CDPProgramsComponent::stop_preview()
{
	if (m_playback->is_playing() == true)
	{
		m_playback->stop();
		m_preview_but->setButtonText("Preview");
	}
}

void CDPProgramsComponent::updateTree()
{
	bool showunsupportedlua = m_unsupported_toggle->getToggleState();
	String filter = m_line_edit->getText();
	m_treeview->deleteRootItem();
	std::map<String, NodeChooserTreeItem*> groupitems;
	NodeChooserTreeItem* rootitem = new NodeChooserTreeItem(this,"Root",false);
	StringArray filtertokens = StringArray::fromTokens(filter, false);
	for (auto& e : node_factory::instance()->m_entries)
	{
		if (showunsupportedlua == false && e.m_supported == false)
			continue;
		bool doadd = false;
		if (filter.isEmpty() == true)
			doadd = true;
		else
		{
			int matches = 0;
			for (auto& f : filtertokens)
				if (e.m_name.containsIgnoreCase(f) || e.m_exe.containsIgnoreCase(f))
					++matches;
			if (matches == filtertokens.size())
				doadd = true;
		}

		if (doadd == true)
		{
			NodeChooserTreeItem* item = new NodeChooserTreeItem(this,e.m_name,true);
			NodeChooserTreeItem* groupitem = nullptr;
			if (groupitems.count(e.m_exe) == 0)
			{
				groupitem = new NodeChooserTreeItem(this,e.m_exe, false);
				groupitems[e.m_exe] = groupitem;
				if (filter.isEmpty()==false)
                    groupitem->setOpen(true);
				rootitem->addSubItem(groupitem, -1);
			}
			else
                groupitem = groupitems[e.m_exe];
			groupitem->addSubItem(item, -1);
		}
	}
	m_treeview->setRootItem(rootitem);
	m_treeview->setRootItemVisible(false);
	rootitem->setOpen(true);
}

bool NodeChooserTreeItem::mightContainSubItems() 
{ 
	return !m_isleaf; 
}

void NodeChooserTreeItem::paintItem(Graphics & g, int w, int h)
{
	if (isSelected())
		g.fillAll(Colours::lightblue);
	else 
		g.fillAll(Colours::Colours::lightgrey);
	g.setColour(Colours::black);
	g.drawText(m_txt, 0, 0, w, h, Justification::left);
}

void NodeChooserTreeItem::itemClicked(const MouseEvent & e)
{
	if (m_isleaf==true)
		m_chooser->treeItemClicked(this, false);
}

void NodeChooserTreeItem::itemDoubleClicked(const MouseEvent & e)
{
	if (m_isleaf==true)
		m_chooser->treeItemClicked(this, true);
}

void NodeChooserTreeItem::itemSelectionChanged (bool isNowSelected)
{
    if (isNowSelected && m_isleaf==true)
        m_chooser->treeItemClicked(this, false);
}

bool NodeChooserTreeView::keyPressed(const KeyPress & ev)
{
	if (ev == KeyPress::returnKey)
	{
		auto selected = dynamic_cast<NodeChooserTreeItem*>(getSelectedItem(0));
		if (selected != nullptr)
			selected->m_chooser->treeItemClicked(selected, true);
		return true;
	}
	return TreeView::keyPressed(ev);
}

