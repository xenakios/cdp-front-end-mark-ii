#pragma once

#include <vector>
#include <functional>
#include "cdp_graph.h"

class node_factory
{
public:
	node_factory();
    ~node_factory();
	static node_factory* instance(bool destroy=false);
	struct entry_t
	{
		std::function<node_ref(void)> CreateFunc;
		String m_name;
		String m_tiptext;
        String m_exe;
		int m_instance_count = 0;
		bool m_lua_defined = false;
		bool m_supported = true;
		bool m_is_plugin = false;
		int m_plugin_id = -1;
	};
	std::vector<entry_t> m_entries;
	KnownPluginList& getPluginList() { return m_pluginlist; }
	void init_plugins(bool forgetpreviousresults);
	void init_lua_defined_programs();
private:
	friend class process_graph;
	friend class chain_process_node;
	node_ref create_from_name(String name);
	node_ref duplicate_node(node_ref src, bool duplicate_connections);
	node_ref create_lua_processor_from_name(String name);
	int m_instance_counter = 0;
	KnownPluginList m_pluginlist;
};
