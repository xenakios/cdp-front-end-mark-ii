#include "waveform_component.h"
#include "envelope_component.h"
#include "cdp_graph.h"

extern std::unique_ptr<AudioThumbnailCache> g_thumb_cache;
extern std::unique_ptr<AudioFormatManager> g_format_manager;
extern std::unique_ptr<PropertiesFile> g_propsfile;

WaveformComponentBase::WaveformComponentBase(bool withzoomscrollbar)
{
	setOpaque(true);
	if (withzoomscrollbar == true)
	{
		m_zsbar = std::make_unique<zoom_scrollbar>();
		m_zsbar->RangeChanged = [this](double t0, double t1)
		{
			m_view_start = t0;
			m_view_end = t1;
			repaint();
		};
		addAndMakeVisible(m_zsbar.get());
	}
}

void WaveformComponentBase::mouseDown(const MouseEvent & ev)
{
	if (m_thumb == nullptr)
		return;
	double srclen = m_thumb->getTotalLength();
	double tpos = jmap<double>(ev.x, 0, getWidth(), srclen*m_view_start, srclen*m_view_end);
	if (OnClicked)
		OnClicked(tpos);
}

void WaveformComponentBase::setAudioFile(String fn)
{
	//Logger::writeToLog("WaveformComponentBase::setAudioFile " + fn);
	m_is_dirty = true;
	if (fn.isEmpty() == false)
	{
		if (fn == m_fn)
			return;
		m_fn = fn;
		if (m_fn.endsWith(".wav"))
		{
			m_spectral_image = Image();
			File thumbfile(fn);
			m_thumb = std::make_unique<AudioThumbnail>(256, *g_format_manager, *g_thumb_cache);
			m_thumb->setSource(new FileInputSource(thumbfile));
			m_thumb->addChangeListener(this);
		}
		else if (m_fn.endsWith(".ana"))
		{
			WavAudioFormat wavformat;
			m_thumb = nullptr;
			auto reader = std::unique_ptr<MemoryMappedAudioFormatReader>(wavformat.createMemoryMappedReader(File(fn)));
			if (reader != nullptr)
			{
				reader->mapEntireFile();
				int numpvocchans = reader->numChannels;
				int64_t numframes = reader->lengthInSamples;
				const double sr = reader->sampleRate;
				m_spectral_image = Image(Image::RGB, numframes, numpvocchans, true);
				float mmreaderbuf[8200];
				for (int i = 0; i < numframes; ++i)
				{
					reader->getSample(i, mmreaderbuf);
					for (int j = 0; j < numpvocchans / 2; ++j)
					{
						float magn = mmreaderbuf[j * 2];
						float decibels = Decibels::gainToDecibels(magn);
						float scaled_decibels = jmap(decibels, -120.0f, -20.0f, 0.0f, 255.0f);
						scaled_decibels = jlimit(0.0f, 255.0f, scaled_decibels);
						float freq = mmreaderbuf[j * 2 + 1];
						freq = jmap<float>(freq, 43.0, 22050.0f, 0.0, 1.0);
						freq = pow(freq, 1.0 / 3.0);
						float ycor = numpvocchans - numpvocchans*freq;
						uint8 pixmagn = scaled_decibels;
						m_spectral_image.setPixelAt(i, ycor, Colour(pixmagn, pixmagn, pixmagn));
					}
				}

			}
			else Logger::writeToLog("Could not create mmreader for pvoc file "+fn);
		}
		repaint();
	}
}

void WaveformComponentBase::paint(Graphics & g)
{
	//if (m_is_dirty == true)
	{
		m_is_dirty = false;
		//Logger::writeToLog("waveformcomponent paint");
		g.fillAll(Colours::black);
		g.setColour(Colours::grey);
		if (m_thumb != nullptr)
		{
			juce::Rectangle<int> rect(0, 0, getWidth(), getHeight() - 15);
			if (m_zsbar == nullptr)
				rect = juce::Rectangle<int>(0, 0, getWidth(), getHeight());
			if (m_thumb != nullptr)
			{
				double srclen = m_thumb->getTotalLength();
				if (srclen > 0.0)
				{
					double starttimesecs = srclen*m_view_start;
					double endtimesecs = srclen*m_view_end;
					m_thumb->drawChannels(g, rect, starttimesecs, endtimesecs, 1.0f);
					g.setColour(Colours::white);
					if (m_playcurpos >= 0.0)
					{
						double xcor = jmap<double>(m_playcurpos, srclen*m_view_start, srclen*m_view_end, 0, getWidth());
						g.drawLine(xcor, 0.0, xcor, getHeight(), 1.5f);
					}
				}
			}
		}
		if (m_spectral_image.isValid() == true)
		{
			g.setImageResamplingQuality(Graphics::highResamplingQuality);
			int img_w = m_spectral_image.getWidth();
			int img_h = m_spectral_image.getHeight();
			g.drawImage(m_spectral_image, 0, 0, getWidth(), getHeight(), 0, 0, img_w, img_h);
		}
		paintOverlay(g);
	}
}

void WaveformComponentBase::changeListenerCallback(ChangeBroadcaster *cb)
{
	if (cb == m_thumb.get())
		repaint();
}

void WaveformComponentBase::resized()
{
	m_is_dirty = true;
	repaint();
	if (m_zsbar!=nullptr)
		m_zsbar->setBounds(0, getHeight() - 15, getWidth(), 15);
}

void WaveformComponentBase::setPlayCursorPosition(double tpos)
{
	m_playcurpos = tpos;
	repaint();
}

WaveformWithEditor::WaveformWithEditor(process_node* n) : WaveformComponentBase(true)
{
	m_node = n;
	if (m_node != nullptr)
	{
		//m_time_sel_start_val.referTo(m_node->get_parameters()[0]->m_value);
		//m_time_sel_start_val.addListener(this);
		//m_time_sel_end_val.referTo(m_node->get_parameters()[1]->m_value);
		//m_time_sel_end_val.addListener(this);
		m_node->addActionListener(this);
	}
	//startTimer(0, 1000);
}

WaveformWithEditor::~WaveformWithEditor()
{
	m_time_sel_start_val.removeListener(this);
	m_time_sel_end_val.removeListener(this);
	if (m_node != nullptr)
		m_node->removeActionListener(this);
}

void WaveformWithEditor::paintOverlay(Graphics & g)
{
	if (m_thumb == nullptr)
		return;
	g.setColour(Colours::red.withAlpha(0.5f));
	double srclen = m_thumb->getTotalLength();
	double t0 = m_time_sel_start_val.getValue();
	double t1 = m_time_sel_end_val.getValue();
	float xc = (float)jmap(t0, m_view_start*srclen, m_view_end*srclen, 0.0, (double)getWidth());
	double tslen = t1 - t0;
	float w = (float)jmap(tslen, 0.0, (m_view_end-m_view_start)*srclen, 0.0, (double)getWidth());
	g.fillRect(xc, 0.0f, w, (float)getHeight() - 15.0f);
}

void WaveformWithEditor::mouseDown(const MouseEvent & ev)
{
	m_edge_to_drag = findHotArea(ev.x, ev.y);
	if (m_edge_to_drag == -1)
	{
		if (m_thumb != nullptr)
		{
			double srclen = m_thumb->getTotalLength();
			m_new_time_sel_start = jmap((double)ev.x, 0.0, (double)getWidth(), m_view_start*srclen, m_view_end*srclen);
			//Logger::writeToLog("new sel start " + String(m_new_time_sel_start));
			//repaint();
		}
	}
}

void WaveformWithEditor::mouseUp(const MouseEvent & /*ev*/)
{
	m_edge_to_drag = -1;
}

void WaveformWithEditor::sanitizeTimeSelection()
{
	if ((double)m_time_sel_end_val.getValue() < (double)m_time_sel_start_val.getValue())
	{
		double temp = m_time_sel_start_val.getValue();
		m_time_sel_start_val = m_time_sel_end_val.getValue();
		m_time_sel_end_val = temp;
		if (m_edge_to_drag == 0)
			m_edge_to_drag = 1;
		else if (m_edge_to_drag == 1)
			m_edge_to_drag = 0;
	}
}

void WaveformWithEditor::mouseDrag(const MouseEvent & ev)
{
	if (m_thumb == nullptr)
		return;
	double srclen = m_thumb->getTotalLength();
	double pos_new = jmap((double)ev.x, 0.0, (double)getWidth(), m_view_start*srclen, m_view_end*srclen);
	pos_new = jlimit(0.0, srclen, pos_new);
	if (m_edge_to_drag == -1)
	{
		m_time_sel_start_val = m_new_time_sel_start;
		m_time_sel_end_val = pos_new;
		sanitizeTimeSelection();
		//Logger::writeToLog(m_time_sel_start_val.toString() + " " + m_time_sel_end_val.toString());
		//repaint();
		return;
	}
	if (m_edge_to_drag == 0)
		m_time_sel_start_val = pos_new;
	if (m_edge_to_drag == 1)
		m_time_sel_end_val = pos_new;
	sanitizeTimeSelection();
	//repaint();
}

void WaveformWithEditor::mouseMove(const MouseEvent & ev)
{
	int ha = findHotArea(ev.x, ev.y);
	if (ha == -1)
		setMouseCursor(MouseCursor::NormalCursor);
	if (ha == 0 || ha == 1)
		setMouseCursor(MouseCursor::LeftRightResizeCursor);
	m_hot_area = ha;
}

void WaveformWithEditor::timerCallback(int /*id*/)
{
	
}

void WaveformWithEditor::valueChanged(Value & var)
{
	if (m_thumb == nullptr)
		return;
	if (var.refersToSameSourceAs(m_time_sel_start_val) || var.refersToSameSourceAs(m_time_sel_end_val))
		repaint();
}

void WaveformWithEditor::actionListenerCallback(const String & msg)
{
	Logger::writeToLog(msg);
	if (msg == "conns_changed")
	{
		setAudioFile(m_node->getSourceFileName(0, 0));
	}
}

int WaveformWithEditor::findHotArea(int x, int /*y*/)
{
	if (m_thumb == nullptr)
		return -1;
	double srclen = m_thumb->getTotalLength();
	double t0 = m_time_sel_start_val.getValue();
	double t1 = m_time_sel_end_val.getValue();
	float xc = (float)jmap(t0, m_view_start*srclen, m_view_end*srclen, 0.0, (double)getWidth());
	if (x>=xc-5.0 && x<xc+5.0)
		return 0;
	xc = (float)jmap(t1, m_view_start*srclen, m_view_end*srclen, 0.0, (double)getWidth());
	if (x >= xc - 5.0 && x<xc + 5.0)
		return 1;
	return -1;
}
