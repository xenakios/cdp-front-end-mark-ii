/*
This file is part of CDP Front-end.

CDP front-end is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

CDP front-end is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CDP front-end.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jcdp_audio_playback.h"
#include "jcdp_utilities.h"

#include <memory>


#ifdef BUILD_REAPER_PLUGIN
#include "Source/reaper_plugin_functions.h"
#endif

extern std::unique_ptr<PropertiesFile> g_propsfile;
AudioDeviceManager* g_audiodevice_manager = nullptr;


juce_audio_preview::juce_audio_preview()
{
    m_manager=std::make_unique<AudioDeviceManager>();
	g_audiodevice_manager = m_manager.get();
	m_properties.set("audiodevicemanagerptr", (int64)m_manager.get());
	XmlElement* xmlelem = g_propsfile->getXmlValue("audiodevicesettings");
	m_manager->initialise(0,2,xmlelem,true);
	delete xmlelem;
    m_manager->addAudioCallback(this);
    if (m_manager->getCurrentAudioDevice()!=nullptr)
    {
        m_manager->getCurrentAudioDevice()->start(this);
    }
}

juce_audio_preview::~juce_audio_preview()
{
    m_manager->removeAudioCallback(this);
}

void juce_audio_preview::set_node(node_ref node)
{
	ScopedLock locker(m_mutex);
	m_src_node = node;
	if (m_src_node != nullptr)
	{
		m_src_node->seek(0.0);
	}
}

void juce_audio_preview::audioDeviceIOCallback(const float **, int, 
	float **outputChannelData, int numOutputChannels, int numSamples)
{
    ScopedLock locker(m_mutex);
	for (int i = 0; i<numOutputChannels; ++i)
		for (int j = 0; j<numSamples; ++j)
			outputChannelData[i][j] = 0.0f;
	if (m_src_node != nullptr && m_is_playing == true)
	{
		double sr = m_manager->getCurrentAudioDevice()->getCurrentSampleRate();
		m_src_node->processAudio(outputChannelData, numOutputChannels, numSamples, sr);
		for (int i = 0; i<numOutputChannels; ++i)
			for (int j = 0; j<numSamples; ++j)
				outputChannelData[i][j] *= (float)m_gain;
	}
}

void juce_audio_preview::audioDeviceAboutToStart(AudioIODevice *dev)
{
	
}

void juce_audio_preview::seek(double seconds)
{
    ScopedLock locker(m_mutex);
	if (m_src_node != nullptr)
		m_src_node->seek(seconds);
}

double juce_audio_preview::get_position() 
{ 
	if (m_src_node != nullptr)
		return m_src_node->getPlaybackPosition();
	return 0.0;
}

void juce_audio_preview::start()
{
	if (m_manager->getCurrentAudioDevice() == nullptr)
		return;
	ScopedLock locker(m_mutex);
	int numchans = m_manager->getCurrentAudioDevice()->getActiveOutputChannels().countNumberOfSetBits();
	int bufsize = m_manager->getCurrentAudioDevice()->getCurrentBufferSizeSamples();
	double sr = m_manager->getCurrentAudioDevice()->getCurrentSampleRate();
	if (m_src_node != nullptr)
	{
		m_src_node->prepareToPlay(bufsize, numchans, sr);
	}
	m_is_playing = true;
}

void juce_audio_preview::stop()
{
    ScopedLock locker(m_mutex);
    m_is_playing=false;
    if (m_src_node!=nullptr)
        m_src_node->releaseResources();
}

void juce_audio_preview::set_volume(double gain)
{
	m_gain = gain;
}

void juce_audio_preview::set_looped(bool b)
{
	m_mutex.enter();
	m_looped = b;
	m_mutex.exit();
}

#ifdef BUILD_REAPER_PLUGIN
reaper_audio_preview::reaper_audio_preview() : m_mutex(&m_prev_reg)
{
    m_prev_reg.loop=true;
    m_prev_reg.curpos=0.0;
    m_prev_reg.m_out_chan=0;
    m_prev_reg.preview_track=nullptr;
    m_prev_reg.volume=1.0;
    m_prev_reg.src=nullptr;
}

reaper_audio_preview::~reaper_audio_preview()
{
    stop();
    delete m_src;
    remove_file_if_exists(m_filename);
}

bool reaper_audio_preview::is_playing()
{
    return m_is_playing;
}

void reaper_audio_preview::set_audio_file(String fn)
{
    const char* foo=fn.toRawUTF8();
    PCM_source* temp=PCM_Source_CreateFromFile(foo);
    if (temp!=nullptr)
    {
        m_mutex.lock();
        PCM_source* old_src=m_src;
        m_src=temp;
        m_prev_reg.src=temp;
        m_prev_reg.curpos=0.0;
        m_mutex.unlock();
        delete old_src;
        m_filename=fn;
    } else Logger::writeToLog("Could not create PCM_source");
}

void reaper_audio_preview::set_volume(double gain)
{
    m_mutex.lock();
    m_prev_reg.volume=gain;
    m_mutex.unlock();
}

void reaper_audio_preview::start()
{
    if (m_is_playing==true)
        return;
    PlayPreviewEx(&m_prev_reg,1,-1.0);
    m_is_playing=true;
}

void reaper_audio_preview::stop()
{
    if (m_is_playing==false)
        return;
    StopPreview(&m_prev_reg);
    m_is_playing=false;
}

void reaper_audio_preview::seek(double pos)
{
    m_mutex.lock();
    if (m_src!=nullptr)
    {
        m_prev_reg.curpos=pos;
    }
    m_mutex.unlock();
}

double reaper_audio_preview::get_position()
{
    return m_prev_reg.curpos;
}
#endif
