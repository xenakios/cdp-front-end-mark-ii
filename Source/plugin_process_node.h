#pragma once

#include "cdp_graph.h"
#include <memory>
#include "routingmatrix.h"

class plugin_process_node : 
	public process_node, 
	public MultiTimer, 
	public AudioProcessorListener,
	public ValueListener
{
public:
	plugin_process_node(String procname, AudioPluginInstance* pinst);
	~plugin_process_node();
	
	StringArray get_output_filenames() override;

	String get_supported_input_type() override;

	String get_output_type() override;

	void tick() override;
	MD5 extra_state_hash() override;
	
	ValueTree getAdditionalStateValueTree() override;
	void restoreAdditionalStateFromValueTree(ValueTree tree) override;
	void removeLastOutputFile() override;
	void valueChanged(Value& var) override;
	String get_error_text() override;
	int getNumOutChansForNumInChans(int inchans) override;
	process_node::CustomGUIType getCustomEditorType2() override;
	Component* getCustomEditor() override;
	bool usesCommandLine() const override { return false; }
	bool isOfflineOnly() override { return false; }
	void prepareToPlay(int expectedbufsize, int numchans, double sr) override;
	void seek(double s) override;
	void processAudio(float** buf, int numchans, int numframes, double sr) override;
	double getPlaybackPosition() override;
private:
	AudioPluginInstance* m_plugin_instance = nullptr;
	String m_out_fn;
	int64_t m_outcounter = 0;
	int64_t m_infile_pos = 0;
	int64_t m_outlen = 0;
	int m_proc_buf_size = 256;
	std::unique_ptr<MemoryMappedAudioFormatReader> m_audio_in_reader;
	std::unique_ptr<AudioFormatWriter> m_audio_out_writer;
	AudioSampleBuffer m_proc_buffer;
	std::unique_ptr<std::thread> m_thread;
	std::atomic<int> m_thread_state{ 0 };
	
	void timerCallback(int id) override;
    void audioProcessorParameterChanged (AudioProcessor *processor, int parameterIndex, float newValue) override;
    void audioProcessorChanged (AudioProcessor *processor) override;
	void audioProcessorParameterChangeGestureEnd(AudioProcessor *processor, int parameterIndex) override;
	
	int m_last_changed_par = -2;
	bool m_block_undo_adding = false;
	breakpoint_envelope m_fade_env;
};



