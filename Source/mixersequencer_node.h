#pragma once

#include "cdp_graph.h"
#include "sol.hpp"
#include "script_editor.h"
#include "envelope_component.h"
#include <map>
#include "resample.h"
#include "misc_components.h"

class SequencerTimeLineComponent;
class MixerSequencerComponent;

using MemoryMappedReaderCache = std::map<String, std::unique_ptr<MemoryMappedAudioFormatReader>>;

class ResamplerPool
{
public:
	ResamplerPool(int initialresamplers = 16)
	{
		for (int i = 0; i < initialresamplers; ++i)
		{
			pool_entry entry;
			entry.m_resampler = std::make_shared<WDL_Resampler>();
			entry.m_available = true;
			m_entries.push_back(entry);
		}
	}
	std::shared_ptr<WDL_Resampler> obtain()
	{
		for (auto& e : m_entries)
		{
			if (e.m_available == true)
			{
				e.m_available = false;
				return e.m_resampler;
			}
		}
		return nullptr;
	}
	void release(std::shared_ptr<WDL_Resampler> r)
	{
		for (auto& e : m_entries)
		{
			if (e.m_resampler == r)
			{
				//Logger::writeToLog("Returned resampler "+String(r.get()))
				e.m_available = true;
				//r = nullptr;
				return;
			}
		}
	}
private:
	struct pool_entry
	{
		std::shared_ptr<WDL_Resampler> m_resampler;
		bool m_available = false;
	};
	std::vector<pool_entry> m_entries;
};

class mixer_event_t
{
public:
	mixer_event_t() {}
	mixer_event_t(double tpos, String fn, double len, double offset, int outchoffs, double gain,
		double fadeinlen, double fadeoutlen, double monopan, int lane) :
		m_tpos(tpos), m_fn(fn), m_len(len), m_src_offset(offset),
		m_outchan_offset(outchoffs), m_gain(gain), m_fade_in_len(fadeinlen), m_fade_out_len(fadeoutlen),
		m_mono_pan(monopan), m_lane(lane) {}
	~mixer_event_t();
	double m_tpos = 0.0;
	String m_fn;
	String m_real_fn;
	String m_eventname{ "Untitled" };
	double m_len = 0.0;
	double m_max_len = 0.0;
	double m_src_offset = 0.0;
	int m_outchan_offset = 0;
	double m_gain = 1.0;
	double m_fade_in_len = 0.5;
	double m_fade_out_len = 0.5;
	double m_mono_pan = 0.5;
	bool m_selected = false;
	double m_src_samplerate = 44100.0;
	int m_lane = -1;
	juce::int64 m_thumbhash = 0;
	bool m_playing = false;
	int64_t m_playpos = -1;
    int64_t m_outpos = 0;
	std::shared_ptr<WDL_Resampler> m_resampler;
};

class filemixer_node : public process_node
{
public:
	friend class SequencerTimeLineComponent;
	
	filemixer_node(String procname);
	~filemixer_node() {}
	StringArray get_output_filenames() override;
	String get_supported_input_type() override;
	String get_output_type() override;
	void tick() override;
	String get_error_text() override { return m_error_text; }
	void addMixEvent(double tpos, String fn, double len, double srcoffset, int outchoffs, 
		double gain, double fadeinlen, double fadeoutlen, double monopan, int lane=-1);
	void addMixEvent(mixer_event_t ev);
	void addMixEvents(sol::table table);
    void clearMixEvents();
	void setNumOutChans(int chans);
	MD5 extra_state_hash() override;
	ValueTree getAdditionalStateValueTree() override;
	void restoreAdditionalStateFromValueTree(ValueTree tree) override;
	process_node::CustomGUIType getCustomEditorType2() override;
	Component* getCustomEditor() override;
	String getScriptCode() { return m_script_code; }
	void setScriptCode(String text);
	String runScript();
	double maxEventTime();
	bool usesCommandLine() const override { return false; }
    std::vector<MixerSequencerComponent*> m_components;

	bool isOfflineOnly() override { return true; }
	void removeLastOutputFile() {}
	void prepareToPlay(int expectedbufsize, int numchans, double sr) override;
	void seek(double s) override;
	void processAudio(float** buf, int numchans, int numframes, double sr) override;
	double getPlaybackPosition() override;
	
private:
	int64_t m_mix_playpos = 0;
	MemoryMappedReaderCache m_mm_readers;
	String m_output_fn;
	
	std::unique_ptr<std::thread> m_thread;
	std::atomic<int> m_thread_state{ 0 };
	AudioBuffer<float> m_mix_buffer;
	void performMix2(String outfn);
	std::vector<mixer_event_t> m_mix_events;
	std::vector<double> m_resampler_out_buf;
	String m_script_code;
	int m_num_outchans = 2;
	double m_out_sr = 44100.0;
	int m_mix_blocksize = 32768;
    void sortEvents();
	void conformMixEvents();
	ResamplerPool m_resampler_pool;
	std::vector<mixer_event_t*> m_eventstoplay;
};

using EventVector = std::vector<mixer_event_t>;
using EventRefVector = std::vector<mixer_event_t*>;

class EventPropertiesComponent : public Component, public Slider::Listener, public ComboBox::Listener
{
public:
	EventPropertiesComponent();
	void setEvents(EventRefVector events);
	void sliderValueChanged(Slider* slid) override;
	void comboBoxChanged(ComboBox* combo) override;
private:
	EventRefVector m_events;
	TextEditor m_name_edit;
	Slider m_gain_slider;
	Slider m_mono_pan_slider;
	ComboBox m_outchan_offset_combo;
	void updateComponents();
};

class MixerSequencerComponent : public Component, public ActionListener, public TextEditor::Listener
{
public:
	MixerSequencerComponent(filemixer_node* node);
	~MixerSequencerComponent();
	void resized() override;
	void actionListenerCallback(const String& message) override;
	void textEditorTextChanged(TextEditor& ed) override;
    ValueTree saveState();
    void restoreState(ValueTree state);
private:
	filemixer_node* m_node = nullptr;
	ScriptTextEditor m_script_ed;
	zoom_scrollbar m_zs;
	std::unique_ptr<SequencerTimeLineComponent> m_timelinecomp;
	void runScript(String txt);
	SplitterLayout m_layout;
};

class SequencerTimeLineComponent : public Component
{
public:
	SequencerTimeLineComponent(filemixer_node* node, zoom_scrollbar* zs);
	void paint(Graphics& g) override;
	void mouseDown(const MouseEvent& ev) override;
	void mouseDrag(const MouseEvent& ev) override;
	void mouseMove(const MouseEvent& ev) override;
    void mouseUp(const MouseEvent& ev) override;
	bool keyPressed(const KeyPress& ev) override;
	void setMaxEventTime(double t) { m_max_event_time = t; }
    void updateThumbNails();
	void setViewRange(double t0, double t1);
private:
	filemixer_node* m_node = nullptr;
	zoom_scrollbar* m_zs = nullptr;
	double m_max_event_time = 10.0;
	int m_hot_event = -1;
	int m_hot_event_control = 0;
	double m_drag_start_time_pos = 0.0;
	double m_drag_start_len = 0.0;
	double m_drag_start_src_offset = 0.0;
    double m_drag_start_gain = 0.0;
    double m_drag_start_fade_in = 0.0;
	double m_drag_start_fade_out = 0.0;
    int m_num_lanes = 8;
	double m_view_start_time = 0.0;
	double m_view_end_time = 10.0;
	int findEventFromCoordinates(float x, float y);
	
    int getHotEventControl(int hotevent, float x, float y);
    std::map<String,std::shared_ptr<AudioThumbnail>> m_thumb_nails;
    bool m_events_were_changed = false;
    void renderNode();
	double m_debug_cursor_pos = 0.0;
	double m_debug_cursor_len = 1.0;
	bool m_debug_cursor_hot = false;
	bool isDebugCursorHot(float x, float y);
	float m_default_lane_heigth = 50.0f;
	std::vector<float> m_lane_heights;
    std::vector<float> m_lane_ycoords;
    void updateLaneYCoords();
    int laneFromYCoord(float y);
	std::vector<float> yCoordsFromLanes();
	void showEventContextMenu();
};
