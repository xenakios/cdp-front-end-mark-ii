#ifndef logger_component_h
#define logger_component_h

#include "../JuceLibraryCode/JuceHeader.h"
#include <mutex>

class LoggerComponent : public ResizableWindow, public Timer, public Logger
{
public:
    LoggerComponent() : ResizableWindow("Log messages",true)
    {
        
        m_messages.add("Test message");
        m_ed = new TextEditor;
        m_ed->setMultiLine(true);
        m_ed->setReadOnly(true);
        
        startTimer(1000);
        setSize(400,300);
        m_ed->setSize(390,290);
        setContentOwned(m_ed, true);
        setVisible(true);
        setTopLeftPosition(50,50);
        setResizable(true, true);
        setUsingNativeTitleBar(true);
    }
    void timerCallback() override
    {
        std::lock_guard<std::mutex> locker(m_mut);
        for (int i=0;i<m_messages.size();++i)
        {
            m_ed->setText(m_ed->getText()+m_messages[i]);
        }
        m_messages.clear();
    }
    void logMessage (const String &message) override
    {
        std::lock_guard<std::mutex> locker(m_mut);
        m_messages.add(message);
    }
private:
    std::mutex m_mut;
    StringArray m_messages;
    TextEditor* m_ed=nullptr;
};

#endif /* logger_component_h */
